package com.smartinout;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.smartinout.base.App;
import com.smartinout.base.BaseActivity;
import com.smartinout.databinding.ActivityHomeBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.model.timesheet.quickpick.QuickPickPojo;
import com.smartinout.model.timesheet.quickpick.QuickTask;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.ActivityUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;
import com.smartinout.utilities.ImageDisplayUitls;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import retrofit2.Response;

import static com.raizlabs.android.dbflow.config.FlowManager.getContext;

public class HomeActivity extends BaseActivity implements View.OnClickListener {

    ActivityHomeBinding binding;
    public static final int INDEX_OTHER = 0;
    public static final int INDEX_INOUTMY = 1;
    public static final int INDEX_INOUTCOM = 2;
    public static final int INDEX_TIMESHEET_DAILY = 3;
    public static final int INDEX_TIMESHEET_WEEKLY = 4;
    public static final int INDEX_DASHBOARD = 5;

    public int currentIndex = INDEX_OTHER;

    private static final int ZXING_CAMERA_PERMISSION = 1;
    private Class<?> mClss;
    Context ctx;
    private NavController navController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(HomeActivity.this, R.layout.activity_home);
        //getQuickPicks();
        ctx = this;
        initiateUI();
        registerBroadcast();
    }

    @Override
    protected void initiateUI() {
        binding.layoutToolBarInclude.tvTitle.setText(getResources().getString(R.string.strDasboard));
        binding.navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        loadHeaderDashboard();

        binding.layoutToolBarInclude.ivIconLeft.setOnClickListener(this);
        binding.layoutToolBarInclude.ivIconRight.setOnClickListener(this);
        navController= Navigation.findNavController(getActivity(), R.id.nav_host_frag_home);
    }

    public void registerBroadcast (){
        try {
            IntentFilter filter = new IntentFilter(Constants.ACTION_STATUSCHANGED);
            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
            localBroadcastManager.registerReceiver(moveToDashboard, filter);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {



            int currentId = navController.getCurrentDestination().getId();

            switch (item.getItemId()) {
                case R.id.navigation_dashboard:
                    if (currentId != R.id.fragment_dashboard) {
                        hideShowHeader(true);
                        binding.layoutToolBarInclude.tvTitle.setText(getResources().getString(R.string.strDasboard));
                        navController.navigate(R.id.fragment_dashboard, null);
                    }
                    return true;
                case R.id.navigation_vehicles:
                    if (currentId != R.id.fragment_vehicles) {
                        showHeaderSearch(INDEX_OTHER);
                        binding.layoutToolBarInclude.tvTitle.setText(getResources().getString(R.string.strVehicle));
                        navController.navigate(R.id.fragment_vehicles, null);
                    }
                    return true;
                case R.id.navigation_inout:
                    if (currentId != R.id.fragment_inout) {
                        showHeaderSearch(INDEX_INOUTMY);
                        binding.layoutToolBarInclude.tvTitle.setText(getResources().getString(R.string.strInout));
                        navController.navigate(R.id.fragment_inout, null);
                    }
                    return true;
                case R.id.navigation_timesheet:
                    if (currentId != R.id.fragment_timesheet) {
                        showHeaderSearch(INDEX_TIMESHEET_DAILY);
                        binding.layoutToolBarInclude.tvTitle.setText(getResources().getString(R.string.strTimesheet));
                        navController.navigate(R.id.fragment_timesheet, null);
                    }
                    return true;
                case R.id.navigation_settings:
                    if (currentId != R.id.fragment_settings) {
                        hideShowHeader(false);
                        binding.layoutToolBarInclude.tvTitle.setText(getResources().getString(R.string.strSetting));
                        navController.navigate(R.id.fragment_settings, null);
                    }
                    return true;


            }

            return false;
        }
    };

    public void showHeaderSearch(int index) {

        currentIndex = index;
        binding.layoutToolBarInclude.ivIconLeft.setVisibility(View.GONE);

        if (currentIndex == INDEX_INOUTCOM) {
            binding.layoutToolBarInclude.ivIconRight.setVisibility(View.VISIBLE);
            binding.layoutToolBarInclude.ivIconRight.setImageResource(R.drawable.ic_search);

        } else if (currentIndex == INDEX_INOUTMY) {
            binding.layoutToolBarInclude.ivIconRight.setVisibility(View.GONE);

        } else if (currentIndex == INDEX_OTHER) {
            binding.layoutToolBarInclude.ivIconRight.setVisibility(View.VISIBLE);
            binding.layoutToolBarInclude.ivIconRight.setImageResource(R.drawable.ic_qr_code);
        } else if (currentIndex == INDEX_TIMESHEET_DAILY) {
            binding.layoutToolBarInclude.ivIconRight.setVisibility(View.VISIBLE);
            binding.layoutToolBarInclude.ivIconRight.setImageResource(R.drawable.ic_add_white);

        } else if (currentIndex == INDEX_TIMESHEET_WEEKLY) {
            binding.layoutToolBarInclude.ivIconRight.setVisibility(View.GONE);

        } else {
            binding.layoutToolBarInclude.ivIconRight.setVisibility(View.GONE);
            binding.layoutToolBarInclude.ivIconRight.setImageResource(R.drawable.ic_search);
        }
    }

    public void hideShowHeader(boolean isDashboard) {
        if (isDashboard) {
            loadHeaderDashboard();
        } else {
            binding.layoutToolBarInclude.ivIconLeft.setVisibility(View.GONE);
            binding.layoutToolBarInclude.ivIconRight.setVisibility(View.GONE);
        }
    }

    public void loadHeaderDashboard() {
        currentIndex = INDEX_DASHBOARD;
        binding.layoutToolBarInclude.ivIconLeft.setVisibility(View.VISIBLE);
        String firstname = prefs.getString(ApiUtils.FIRSTNAME, "");
        String lastname = prefs.getString(ApiUtils.LASTNAME, "");
        String profileImg = String.valueOf(firstname.charAt(0)) + String.valueOf(lastname.charAt(0));

        if (!prefs.getString(ApiUtils.PROFILE_URL, "").isEmpty())
        {
            ImageDisplayUitls.displayImage(prefs.getString(ApiUtils.PROFILE_URL, ""), HomeActivity.this, binding.layoutToolBarInclude.ivIconLeft);
        }
        else
        {
            ImageDisplayUitls.displayTextDrawable(profileImg, binding.layoutToolBarInclude.ivIconLeft
                    ,getActivity().getResources().getColor(android.R.color.white),
                    getActivity().getResources().getColor(R.color.colorPrimary));
        }



        binding.layoutToolBarInclude.ivIconRight.setVisibility(View.VISIBLE);
        binding.layoutToolBarInclude.ivIconRight.setImageResource(R.drawable.ic_qr_code);
    }

    private static final int TIME_INTERVAL = 2000; // # milliseconds, desired time passed between two back presses.
    private long mBackPressed;


    @Override
    public void onBackPressed() {
        int currentId = navController.getCurrentDestination().getId();
        if(currentId != R.id.fragment_dashboard){
            hideShowHeader(true);
            binding.layoutToolBarInclude.tvTitle.setText(getResources().getString(R.string.strDasboard));
            navController.popBackStack(R.id.fragment_dashboard,false);
            binding.navigation.getMenu().getItem(0).setChecked(true);

        }else {
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                super.onBackPressed();
                return;
            } else {
                AppUtilities.showToast(getBaseContext(), getResources().getString(R.string.exitapp));
            }
            mBackPressed = System.currentTimeMillis();
        }
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivIconLeft:
                ActivityUtils.launchStartActivityForResult(
                        HomeActivity.this, ProfileActivity.class, Constants.RESULT_CODE_UPLOAD_IMG);
                break;
            case R.id.ivIconRight:
                if (currentIndex == INDEX_TIMESHEET_DAILY) {
                    launchActivity( AddDailyTimeSheet.class);

                } else if (currentIndex == INDEX_DASHBOARD) {
                    launchActivity(ScannerActivity.class);

                } else if (currentIndex == INDEX_OTHER) {
                    launchActivity(ScannerActivity.class);

                } else {
                    launchActivity(CompanySearchActivity.class);
                }
                break;
        }
    }

    public void launchActivity(Class<?> clss) {
        if (currentIndex == INDEX_DASHBOARD || currentIndex == INDEX_OTHER) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                mClss = clss;
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA}, ZXING_CAMERA_PERMISSION);
            } else {
                Intent intent = new Intent(this, clss);
                startActivity(intent);
            }
        }else{
            Intent intent = new Intent(this, clss);
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ZXING_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mClss != null) {
                        Intent intent = new Intent(this, mClss);
                        startActivity(intent);
                    }
                } else {
                    Toast.makeText(this, "Please grant camera permission to use the QR Scanner", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constants.RESULT_CODE_UPLOAD_IMG) {
            String profileImg = prefs.getString(ApiUtils.PROFILE_URL, "");
            if (!prefs.getString(ApiUtils.PROFILE_URL, "").isEmpty())
            {
                ImageDisplayUitls.displayImage(profileImg, HomeActivity.this,
                        binding.layoutToolBarInclude.ivIconLeft, R.drawable.ic_user_placeholder);
            }
            else
            {
                ImageDisplayUitls.displayTextDrawable(profileImg, binding.layoutToolBarInclude.ivIconLeft
                        ,getActivity().getResources().getColor(android.R.color.white),
                        getActivity().getResources().getColor(R.color.colorPrimary));
            }

        }
    }

    private void getQuickPicks() {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        callService.getQuickPicks(getQuickPicks);
    }

    public ApiResponse getQuickPicks = new ApiResponse() {
        @Override
        public void onSuccess(Response response) {
            hideProgressDialog();

            QuickPickPojo quickData = (QuickPickPojo) response.body();


            if (quickData == null) {
                AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
                return;
            }

            if (!AppUtilities.getValidateString(quickData.getError()).isEmpty()) {
                AppUtilities.showToast(getContext(), quickData.getError());
            }

            List<QuickTask> quickTaskList = quickData.getQuickPickDatat();
            App.setQuickTaskListIn(quickTaskList);

        }

        @Override
        public void onFailure(String error) {
            hideProgressDialog();
            AppUtilities.showToast(getContext(), error);
        }
    };

    private BroadcastReceiver moveToDashboard = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equalsIgnoreCase(Constants.ACTION_STATUSCHANGED)){
                hideShowHeader(true);

                binding.navigation.getMenu().getItem(0).setChecked(true);
                binding.layoutToolBarInclude.tvTitle.setText(getResources().getString(R.string.strDasboard));
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_frag_home);
                navController.navigate(R.id.fragment_dashboard, null);
            }
        }

    };

}
