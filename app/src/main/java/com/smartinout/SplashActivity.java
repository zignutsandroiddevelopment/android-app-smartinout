package com.smartinout;

import android.os.Bundle;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import com.smartinout.base.BaseActivity;
import com.smartinout.databinding.ActivitySplashBinding;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.ActivityUtils;

import androidx.databinding.DataBindingUtil;

public class SplashActivity extends BaseActivity {

    ActivitySplashBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(SplashActivity.this, R.layout.activity_splash);
        initiateUI();
    }

    @Override
    protected void initiateUI() {
        performIconAnimation();
    }

    public void performIconAnimation() {
        Animation fadeOut = new AlphaAnimation(0, 1);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(1500);
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                String userId = prefs.getString(ApiUtils.ID, "");
                boolean isFirstLaunch = prefs.getBoolean(ApiUtils.ISFIRSTLAUNCH,false);
                if(isFirstLaunch){
                    if (userId == null || userId.isEmpty())
                        ActivityUtils.launchActivity(SplashActivity.this, LoginActivity.class, true, null);
                    else
                        ActivityUtils.launchActivity(SplashActivity.this, HomeActivity.class, true, null);
                }else{
                    if (userId == null || userId.isEmpty())
                        ActivityUtils.launchActivity(SplashActivity.this, IntroductionActivity.class, true, null);
                    else
                        ActivityUtils.launchActivity(SplashActivity.this, HomeActivity.class, true, null);
                }

            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        binding.ivLogo.startAnimation(fadeOut);
    }
}
