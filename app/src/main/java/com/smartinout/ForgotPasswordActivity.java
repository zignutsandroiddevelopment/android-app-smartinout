package com.smartinout;

import android.os.Bundle;
import android.view.View;

import com.smartinout.base.BaseActivity;
import com.smartinout.databinding.ActivityForgotpasswordBinding;

import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener {

    ActivityForgotpasswordBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(ForgotPasswordActivity.this, R.layout.activity_forgotpassword);
        initiateUI();
    }

    @Override
    protected void initiateUI() {
        changeTitle(getResources().getString(R.string.forgotpassword));
        binding.layoutToolBar.ivBack.setVisibility(View.VISIBLE);
        binding.layoutToolBar.ivBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:
                onBackpress();
                break;
        }
    }

    public void changeTitle(String title) {
        binding.layoutToolBar.tvTitle.setText(title);
    }

    @Override
    public void onBackPressed() {
        onBackpress();
    }

    public void onBackpress() {

        final NavController navController
                = Navigation.findNavController(getActivity(), R.id.nav_host_frag_forgotpwd);

        int currentId = navController.getCurrentDestination().getId();

        if (currentId == R.id.fragment_forgotpwd) {
            super.onBackPressed();
        }

        if (currentId == R.id.fragment_pinverify) {
            changeTitle(getResources().getString(R.string.forgotpassword));
            navController.popBackStack();
        }

        if (currentId == R.id.fragment_resetpwd) {
            changeTitle(getResources().getString(R.string.verification));
            navController.popBackStack();
        }
    }
}
