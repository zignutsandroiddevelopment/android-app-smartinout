package com.smartinout;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.smartinout.base.BaseActivity;
import com.smartinout.databinding.ActivityScannerBinding;
import com.smartinout.fragments.scanners.CameraSelectorDialogFragment;
import com.smartinout.fragments.scanners.FormatSelectorDialogFragment;
import com.smartinout.fragments.scanners.MessageDialogFragment;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.interfaces.OnItemClick;
import com.smartinout.model.login.OnlineUserReference;
import com.smartinout.model.qrcode.QrCodeOfficeData;
import com.smartinout.model.qrcode.QrCodeOfficeModel;
import com.smartinout.model.qrcode.QrCodeVehicleModel;
import com.smartinout.model.vehiclebooking.CreateBookingData;
import com.smartinout.model.vehiclebooking.CreateBookingModel;
import com.smartinout.model.vehicleinout.OfficeModel;
import com.smartinout.model.vehicleinout.VehicleInOutModel;
import com.smartinout.model.vehicleinout.VehicleInfoData;
import com.smartinout.model.vehicleinout.VehicleInfoModel;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.ActivityUtils;
import com.smartinout.utilities.AlertDialog;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import com.smartinout.utilities.Prefs;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Response;

import static com.smartinout.utilities.AppUtilities.getDayHour;

public class ScannerActivity extends BaseActivity implements View.OnClickListener, MessageDialogFragment.MessageDialogListener,
        ZXingScannerView.ResultHandler, FormatSelectorDialogFragment.FormatSelectorDialogListener,
        CameraSelectorDialogFragment.CameraSelectorDialogListener, ApiResponse, OnItemClick {

    ActivityScannerBinding binding;

    private static final String FLASH_STATE = "FLASH_STATE";
    private static final String AUTO_FOCUS_STATE = "AUTO_FOCUS_STATE";
    private static final String SELECTED_FORMATS = "SELECTED_FORMATS";
    private static final String CAMERA_ID = "CAMERA_ID";
    private ZXingScannerView mScannerView;
    private boolean mFlash;
    private boolean mAutoFocus;
    private ArrayList<Integer> mSelectedIndices;
    private int mCameraId = -1;
    private boolean isAvailable = false;
    String id = "";
    String from_date, to_date, from_time, to_time;
    private VehicleInOutModel bookedData;
    private OfficeModel officeData;
    int fromYear, fromMinute, fromDay, fromMonth, fromHours, toHours, toMinutes;
    public static int TYPE_USER_IN = 1;
    public static int TYPE_VEHICLE_IN = 3;
    int selectedType = 1;
    private Prefs prefs;
    private String myInoutTime = "",myInoutTimeWithStatus ="", currentDate = "",vehicleId = "";
    private Context context;
    OnItemClick onItemClick;

    @Override
    protected void onCreate(Bundle state) {
        super.onCreate(state);
        if (state != null) {
            mFlash = state.getBoolean(FLASH_STATE, false);
            mAutoFocus = state.getBoolean(AUTO_FOCUS_STATE, true);
            mSelectedIndices = state.getIntegerArrayList(SELECTED_FORMATS);
            mCameraId = state.getInt(CAMERA_ID, -1);
        } else {
            mFlash = false;
            mAutoFocus = true;
            mSelectedIndices = null;
            mCameraId = -1;
        }

        binding = DataBindingUtil.setContentView(ScannerActivity.this, R.layout.activity_scanner);
        context = this;
        onItemClick = this;
        initiateUI();
        if (getIntent().hasExtra("fromGo"))
        {
            setUpRightLeftBtnClicks();

            selectedType = TYPE_VEHICLE_IN;
        }
        else
        {
            setUpLeftRightBtnClicks();
            selectedType = TYPE_USER_IN;
        }

    }

    @Override
    protected void initiateUI() {

        binding.layoutToolBarInclude.tvTitle.setText(getResources().getString(R.string.app_name));
        binding.layoutToolBarInclude.ivBack.setVisibility(View.VISIBLE);
        binding.layoutToolBarInclude.ivBack.setOnClickListener(this);

        binding.includeLeftRightBtn.tvLeftBtn.setText(getResources().getString(R.string.strTimesheet));
        binding.includeLeftRightBtn.tvRightBtn.setText(getResources().getString(R.string.vehicleinout));

        prefs = Prefs.getInstance();

        getLastupdatedStatus();
        setupScanner();
        setupInOutStatus();
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera(mCameraId);
        mScannerView.setFlash(mFlash);
        mScannerView.setAutoFocus(mAutoFocus);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(FLASH_STATE, mFlash);
        outState.putBoolean(AUTO_FOCUS_STATE, mAutoFocus);
        outState.putIntegerArrayList(SELECTED_FORMATS, mSelectedIndices);
        outState.putInt(CAMERA_ID, mCameraId);
    }

    private void setupInOutStatus() {
        myInoutTime = prefs.getString(Prefs.TODAYINTIME, "");
        myInoutTimeWithStatus = prefs.getString(Prefs.TODAYINTIMEWITHSTATUS,"");
        currentDate = AppUtilities.getCurrentDateEEEEddMMMMYYYY();
        binding.tvData.setText(currentDate);

        if (myInoutTimeWithStatus.equalsIgnoreCase(getActivity().getResources().getString(R.string.cloclkedoff))) {
            binding.tvInTime.setTextColor(getActivity().getResources().getColor(R.color.clrred));
            binding.tvInTime.setText(this.getResources().getString(R.string.cloclkedoff));
            binding.tvInStatus.setTextColor(getActivity().getResources().getColor(R.color.clrred));
            binding.tvInStatus.setText(this.getResources().getString(R.string.cloclkedoff));
        } else {
            if(myInoutTimeWithStatus.contains("IN")){
                binding.tvInTime.setTextColor(getActivity().getResources().getColor(R.color.clrgreen));
                binding.tvInStatus.setTextColor(getActivity().getResources().getColor(R.color.clrgreen));
                binding.tvInStatus.setText(this.getResources().getString(R.string.clockedin));
            }else if(myInoutTimeWithStatus.contains("OUT")){
                binding.tvInTime.setTextColor(getActivity().getResources().getColor(R.color.orange));
                binding.tvInStatus.setTextColor(getActivity().getResources().getColor(R.color.orange));
                binding.tvInStatus.setText(this.getResources().getString(R.string.out).toUpperCase());
            }else if(myInoutTimeWithStatus.contains("Break")){
                binding.tvInTime.setTextColor(getActivity().getResources().getColor(R.color.clrYellow));
                binding.tvInStatus.setTextColor(getActivity().getResources().getColor(R.color.clrYellow));
                binding.tvInStatus.setText(this.getResources().getString(R.string.bin));
            }

            binding.tvInTime.setText(myInoutTime);
            /*binding.tvInTime.setTextColor(getActivity().getResources().getColor(R.color.clrgreen));
            binding.tvInTime.setText(myInoutTime);
            binding.tvInStatus.setTextColor(getActivity().getResources().getColor(R.color.clrgreen));
            binding.tvInStatus.setText(this.getResources().getString(R.string.clockedin));*/

        }


    }

    private void setupScanner() {
        mScannerView = new ZXingScannerView(this);
        setupFormats();
        binding.contentFrame.addView(mScannerView);
    }

    public void setUpLeftRightBtnClicks() {
        defaultSelection(binding.includeLeftRightBtn.tvLeftBtn);
        clickLeftBtn(binding.includeLeftRightBtn.tvLeftBtn,
                binding.includeLeftRightBtn.tvRightBtn);
        clickRightBtn(binding.includeLeftRightBtn.tvRightBtn,
                binding.includeLeftRightBtn.tvLeftBtn);
    }

    public void setUpRightLeftBtnClicks() {
        defaultSelection(binding.includeLeftRightBtn.tvRightBtn);
        clickLeftBtn(binding.includeLeftRightBtn.tvLeftBtn,
                binding.includeLeftRightBtn.tvRightBtn);
        clickRightBtn(binding.includeLeftRightBtn.tvRightBtn,
                binding.includeLeftRightBtn.tvLeftBtn);
    }

    @Override
    public void onLeftBtnClick(View view) {
        super.onLeftBtnClick(view);
        selectedType = TYPE_USER_IN;
    }

    @Override
    public void onRightBtnClick(View view) {
        super.onRightBtnClick(view);
        selectedType = TYPE_VEHICLE_IN;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:
                ScannerActivity.this.finish();
                break;
        }
    }

    private void scanUserQrCode(String office_id) {
        if (!AppUtilities.hasInternet(this, true)) {
            return;
        }
        showProgressDialog();
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.OFFICEID, office_id);
        callService.getOfficeQrDetails(params, myInoutRes);
    }

    private void getLastupdatedStatus() {
        if (!AppUtilities.hasInternet(this, true)) {
            return;
        }
        showProgressDialog();
        callService.getOnlineStatus( onlineResponse);
    }

    private void scanQrCode(String id) {
        if (!AppUtilities.hasInternet(this, true)) {
            return;
        }
        showProgressDialog();
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.VEHICALID, id);
        callService.getQrCodeDetails(params, this);
    }

    private void onBookTrip(String id) {
        if (!AppUtilities.hasInternet(this, true)) {
            return;
        }
        showProgressDialog();
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.VEHICALID, id);
        params.put(ApiUtils.FROM_DATE, from_date);
        params.put(ApiUtils.FROM_TIME, from_time);
        params.put(ApiUtils.TO_DATE, to_date);
        params.put(ApiUtils.TO_TIME, to_time);

        callService.bookVehicle(params, onBookingResponse);
    }

    public void setupFormats() {
        List<BarcodeFormat> formats = new ArrayList<BarcodeFormat>();
        if (mSelectedIndices == null || mSelectedIndices.isEmpty()) {
            mSelectedIndices = new ArrayList<Integer>();
            for (int i = 0; i < ZXingScannerView.ALL_FORMATS.size(); i++) {
                mSelectedIndices.add(i);
            }
        }

        for (int index : mSelectedIndices) {
            formats.add(ZXingScannerView.ALL_FORMATS.get(index));
        }
        if (mScannerView != null) {
            mScannerView.setFormats(formats);
        }
    }

    @Override
    public void handleResult(Result rawResult) {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
        }
        id = rawResult.getText();
        if (selectedType == TYPE_USER_IN) {
            scanUserQrCode(id);
        } else {
            scanQrCode(id);
        }
        //showMessageDialog("Contents = " + rawResult.getText() + ", Format = " + rawResult.getBarcodeFormat().toString());
    }

    /*public void showMessageDialog(String message) {
        DialogFragment fragment = MessageDialogFragment.newInstance("Scan Results", message, this);
        fragment.show(getSupportFragmentManager(), "scan_results");
    }

    public void closeMessageDialog() {
        closeDialog("scan_results");
    }

    public void closeFormatsDialog() {
        closeDialog("format_selector");
    }

    public void closeDialog(String dialogName) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogFragment fragment = (DialogFragment) fragmentManager.findFragmentByTag(dialogName);
        if (fragment != null) {
            fragment.dismiss();
        }
    }*/

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
        /*closeMessageDialog();
        closeFormatsDialog();*/
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

    }

    @Override
    public void onCameraSelected(int cameraId) {

    }

    @Override
    public void onFormatsSaved(ArrayList<Integer> selectedIndices) {

    }

    @Override
    public void onSuccess(Response response) {

        hideProgressDialog();
        QrCodeVehicleModel parsRes = (QrCodeVehicleModel) response.body();

        if (parsRes == null) {
            AppUtilities.showToast(this, getResources().getString(R.string.invalidresposne));
            return;
        }

        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(this, parsRes.getError());
        }

        if (parsRes.getData().isIs_available()) {
/*            if (parsRes.getData().getData().getIs_breakdown()) {
                new AlertDialog(ScannerActivity.this, this).showAlertDialog(
                        this.getResources().getString(R.string.sorry),
                        this.getResources().getColor(R.color.clrred),
                        this.getResources().getString(R.string.faultyVehicle));
            } else */if (parsRes.getData().isIs_booked()) {
                isAvailable = false;
                bookedData = parsRes.getData().getData();
                new AlertDialog(ScannerActivity.this, this).showAlertDialog(
                        this.getResources().getString(R.string.success),
                        this.getResources().getColor(R.color.clrgreen),
                        this.getResources().getString(R.string.scansuccessmsg));
            } else {
                isAvailable = true;
                String date = parsRes.getData().getData().getDate();
                String time = parsRes.getData().getData().getTime();
                String message;
                if (date == null && time == null) {
                    setFromToDate(null, null);
                    String toDate;
                    try {
                        toDate = AppUtilities.formatDateFromDateString(Constants.ddMMyyyy, Constants.DATE_FORMAT_5, to_date);
                    }catch (Exception e){

                        toDate = to_date;
                        e.printStackTrace();
                    }
                    message = String.format(getResources().getString(R.string.available_nowmsg),
                            to_time + " " + toDate);
                } else {
                    setFromToDate(date, time);
                    String convertedTime = AppUtilities.convertTimeHHMMSS(time);
                    String toDate;
                    try {
                        toDate = AppUtilities.formatDateFromDateString(Constants.ddMMyyyy, Constants.DATE_FORMAT_5, date);
                    }catch (Exception e){

                        toDate = date;
                        e.printStackTrace();
                    }
                    message = String.format(getResources().getString(R.string.available_nowmsg),
                            convertedTime + " " + toDate);

                }

                new AlertDialog(ScannerActivity.this, this).showBookingAlertDialog(
                        this.getResources().getString(R.string.available_now),
                        this.getResources().getColor(R.color.clrgreen),
                        message, id,"", 0);
            }
        } else {
            /*if (parsRes.getData().getData().getIs_breakdown()) {
                new AlertDialog(ScannerActivity.this, this).showAlertDialog(
                        this.getResources().getString(R.string.sorry),
                        this.getResources().getColor(R.color.clrred),
                        this.getResources().getString(R.string.faultyVehicle));
            } else */if (parsRes.getData().isIs_booked() && !parsRes.getData().isBooked_by_other()) {
                bookedData = parsRes.getData().getData();
                isAvailable = false;
                /*new AlertDialog(ScannerActivity.this, this).showAlertDialog(
                        this.getResources().getString(R.string.success),
                        this.getResources().getColor(R.color.clrgreen),
                        this.getResources().getString(R.string.scansuccessmsg));*/

                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.FROM, Constants.STATUS_START_TRIP);
                bundle.putSerializable(Constants.DATA, bookedData);
                ActivityUtils.launchActivity(ScannerActivity.this, BookVehicleActivity.class, true, bundle);

            } else {
                new AlertDialog(ScannerActivity.this, this).showAlertDialog(
                        this.getResources().getString(R.string.sorry),
                        this.getResources().getColor(R.color.clrred),
                        this.getResources().getString(R.string.scanerror));
            }
        }

    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        new AlertDialog(ScannerActivity.this, this).showAlertDialog(
                this.getResources().getString(R.string.sorry),
                this.getResources().getColor(R.color.clrred),
                error);

    }

    public void setFromToDate(String date, String time) {

        Calendar c = Calendar.getInstance();

        fromYear = c.get(Calendar.YEAR);
        fromMonth = c.get(Calendar.MONTH);
        fromDay = c.get(Calendar.DAY_OF_MONTH);


        fromHours = c.get(Calendar.HOUR);
        fromMinute = c.get(Calendar.MINUTE);


        String amPMFrom = c.get(Calendar.AM_PM) == Calendar.AM ? "AM" : "PM";

        if (date == null && time == null) {

            from_date = fromDay + "/" + (fromMonth + 1) + "/" + fromYear;
            from_time = fromHours + ":" + fromMinute + " " + amPMFrom;

            c.add(Calendar.HOUR, 2);
            toHours = c.get(Calendar.HOUR);
            toMinutes = c.get(Calendar.MINUTE);

            String toamPMFrom = c.get(Calendar.AM_PM) == Calendar.AM ? "AM" : "PM";
            to_date = from_date;
            to_time = String.format("%02d:%02d", toHours, toMinutes)+ " " + toamPMFrom;//toHours + ":" + toMinutes + " " + amPMFrom;


        } else {
            from_date = date;
            from_time = fromHours + ":" + fromMinute + " " + amPMFrom;

            try {
                String[] timeFinal = time.split(":");
                int min = Integer.parseInt(timeFinal[0]);
                int hour = Integer.parseInt(timeFinal[1]);

                c.add(Calendar.HOUR, hour);
                c.add(Calendar.MINUTE, min);
            } catch (Exception e) {

                c.add(Calendar.HOUR, 2);
                e.printStackTrace();

            }

            toHours = c.get(Calendar.HOUR);
            toMinutes = c.get(Calendar.MINUTE);
            String toamPMFrom = c.get(Calendar.AM_PM) == Calendar.AM ? "AM" : "PM";
            to_date = date;
            to_time = String.format("%02d:%02d", toHours, toMinutes)+ " " + toamPMFrom;//toHours + ":" + toMinutes + " " + amPMFrom;
        }
    }


    @Override
    public void onButtonClick(boolean isCancle) {

        if (isCancle) {
            finish();
        } else {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.FROM, Constants.STATUS_START_TRIP);
            bundle.putSerializable(Constants.DATA, bookedData);
            bundle.putSerializable(Constants.OFFICE_DATA, officeData);
            ActivityUtils.launchActivity(ScannerActivity.this, BookVehicleActivity.class, true, bundle);
        }

    }

    @Override
    public void onUpdateClick(int position, String id, boolean isUnbook) {

        onBookTrip(id);
    }

    @Override
    public void onItemClick(int position) {


    }

    @Override
    public void onItemDelete(int position, String id, String number, String from_time, String to_time) {

    }

    public ApiResponse onlineResponse = new ApiResponse() {
        @Override
        public void onSuccess(Response response) {

            hideProgressDialog();


            OnlineUserReference parsRes = (OnlineUserReference) response.body();

            if (parsRes == null) {
                hideProgressDialog();
                AppUtilities.showToast(getActivity(), getResources().getString(R.string.invalidresposne));
                return;
            }

            if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
                hideProgressDialog();
                AppUtilities.showToast(getActivity(), parsRes.getError());
            }

            if (parsRes.getOnlineData() != null) {

                long inoutcreatedtime = parsRes.getOnlineData().getStartTime();
                long lastupdatedtime = parsRes.getOnlineData().getCreatedInoutTime();
                String secondaryStatus = parsRes.getOnlineData().getSecondryStatus();
                String mainstatus = parsRes.getOnlineData().getMainStatus();
                if (AppUtilities.getValidateString(mainstatus).equals(ApiUtils.CIN)) {// Main Status Clocked In
                    prefs.save(Prefs.ISCLOKCEDIN, true);

                    Calendar calendar = Calendar.getInstance();

                    if (AppUtilities.getValidateString(secondaryStatus).equals(ApiUtils.BI)) {// Break In
                        calendar.setTimeInMillis(lastupdatedtime);
                    } else {
                        calendar.setTimeInMillis(inoutcreatedtime);
                    }

                    String hrsMns = getDayHour(getActivity(), calendar.get(Calendar.HOUR_OF_DAY)) + ":" +
                            String.format("%02d", calendar.get(Calendar.MINUTE));

                    String inoutTime = "";
                    if (calendar.get(Calendar.AM_PM) == Calendar.AM) {
                        inoutTime = hrsMns + " AM";
                    } else {
                        inoutTime = hrsMns + " PM";
                    }
                    if (AppUtilities.getValidateString(secondaryStatus).equals(ApiUtils.BI)) {// Break In
                        prefs.save(Prefs.TODAYINTIMEWITHSTATUS, "Break " + inoutTime);
                    }else if (AppUtilities.getValidateString(secondaryStatus).equals(ApiUtils.OUT)) {// Break In
                        prefs.save(Prefs.TODAYINTIMEWITHSTATUS,"OUT " + inoutTime);
                    } else{
                        prefs.save(Prefs.TODAYINTIMEWITHSTATUS, "IN " + inoutTime);
                    }
                    prefs.save(Prefs.TODAYINTIME, inoutTime);
                }else{
                    prefs.save(Prefs.ISCLOKCEDIN, false);
                }
            }else{
                prefs.save(Prefs.ISCLOKCEDIN,false);
            }

        }

        @Override
        public void onFailure(String error) {
            hideProgressDialog();
            AppUtilities.showToast(ScannerActivity.this, error);
        }
    };

    public ApiResponse onBookingResponse = new ApiResponse() {
        @Override
        public void onSuccess(Response response) {

            CreateBookingModel parsRes = (CreateBookingModel) response.body();

            if (parsRes == null) {
                hideProgressDialog();
                AppUtilities.showToast(ScannerActivity.this, getResources().getString(R.string.invalidresposne));
                return;
            }

            if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
                hideProgressDialog();
                AppUtilities.showToast(ScannerActivity.this, parsRes.getError());
            }

            CreateBookingData bookingData = parsRes.getData();

            if (bookingData != null) {

                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.FROM, Constants.STATUS_ONGOING_TRIP);
                bundle.putSerializable(Constants.DATA, bookingData);
                ActivityUtils.launchActivity(ScannerActivity.this, BookVehicleActivity.class, true, bundle);
            }
            /*if (bookingVehicleData != null) {
                getVehicleDetails(id);

            }*/

        }

        @Override
        public void onFailure(String error) {
            hideProgressDialog();
            new AlertDialog(ScannerActivity.this, onItemClick).showAlertDialog(
                    getActivity().getResources().getString(R.string.sorry),
                    getActivity().getResources().getColor(R.color.clrred),
                    error);
        }
    };



    public ApiResponse myInoutRes = new ApiResponse() {
        @Override
        public void onSuccess(Response response) {
            hideProgressDialog();

            QrCodeOfficeModel parsRes = (QrCodeOfficeModel) response.body();

            if (parsRes == null) {
                AppUtilities.showToast(ScannerActivity.this, getResources().getString(R.string.invalidresposne));
                return;
            }

            if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
                AppUtilities.showToast(ScannerActivity.this, parsRes.getError());
                finish();

            }

            QrCodeOfficeData bookedData = parsRes.getData();
            if (bookedData != null) {
                AppUtilities.showToast(ScannerActivity.this, getResources().getString(R.string.officein_success));
                finish();
            }

        }

        @Override
        public void onFailure(String error) {
            hideProgressDialog();
            new AlertDialog(ScannerActivity.this, onItemClick).showAlertDialog(
                    getActivity().getResources().getString(R.string.sorry),
                    getActivity().getResources().getColor(R.color.clrred),
                    error);
            /*AppUtilities.showToast(getActivity(), error);
            finish();*/
        }
    };


}
