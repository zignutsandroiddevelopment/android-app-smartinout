package com.smartinout.utilities;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import com.google.gson.Gson;
import com.smartinout.base.App;
import com.smartinout.model.ForgotPassReqPojo;
import com.smartinout.model.login.LoginPojo;
import com.smartinout.model.vehicleinout.starttrip.StartTripData;
import com.smartinout.retrofit.ApiUtils;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.Set;


/**
 * The type Prefs.
 */
public class Prefs {


    private static final String TAG = "Prefs";
    private static Prefs singleton = null;
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;

    public static final String FORGOTPWDREQDATA = "forgotpwddata";

    public static final String ISCLOKCEDIN = "isClockedIn";
    public static final String TODAYINTIMEWITHSTATUS = "todayintimeStatus";
    public static final String TODAYINTIME = "todayintime";
    public static final String FAULT_REPORTED = "fault_reported";
    public static final String FAULT_TYPE = "fault_type";
    public static final String FUEL_RECIEPT = "fault_receipt";
    public static final String FUEL_RECIEPT_NO = "fault_receipt_no";

    public static final String VEHICLEID = "vehicalid";
    public static final String EMAIL = "userEmail";

    public static final String START_TRIP_DATA = "vehicledata";

    public static final String TIMEZONE = "timezone";
    public static final String COMPANYUSERLIST = "companyUserList";

    /**
     * Instantiates a new Prefs.
     *
     * @param context the context
     */
    private Prefs(Context context) {
        preferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static Prefs getInstance() {
        if (singleton == null) {
            singleton = new Builder(App.getAppContext()).build();
        }
        return singleton;
    }

    /**
     * Save Start trip obj.
     * @param key   the key
     * @param value the value
     *
     */
    public void saveStartTripData(String key, StartTripData value) {
        Gson gson = new Gson();
        if(value != null) {
            String json = gson.toJson(value);
            editor.putString(key, json).commit();
        }else{
            editor.putString(key, null).commit();
        }
    }



    /**
     * Save boolean.
     * @param key   the key
     * @param value the value
     *
     */
    public void save(String key, boolean value) {
        editor.putBoolean(key, value).apply();
    }


    /**
     * Save string.
     * @param key   the key
     * @param value the value
     *
     */
    public void save(String key, String value) {
        editor.putString(key, value).apply();
    }

    /**
     * Save int.
     * @param key   the key
     * @param value the value
     *
     */
    public void save(String key, int value) {
        editor.putInt(key, value).apply();
    }

    /**
     * Save float.
     * @param key   the key
     * @param value the value
     *
     */
    public void save(String key, float value) {
        editor.putFloat(key, value).apply();
    }

    /**
     * Save long.
     * @param key   the key
     * @param value the value
     *
     */
    public void save(String key, long value) {
        editor.putLong(key, value).apply();
    }

    /**
     * save Set<String>
     * @param key   the key
     * @param value the value
     *
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void save(String key, Set<String> value) {
        editor.putStringSet(key, value).apply();
    }


    /**
     * @param key      the key
     * @param defValue the def value
     * @return return boolean
     */
    public boolean getBoolean(String key, boolean defValue) {
        return preferences.getBoolean(key, defValue);
    }


    /**
     * @param key      the key
     * @param defValue the def value
     * @return return string
     */
    public String getString(String key, String defValue) {
        return preferences.getString(key, defValue);
    }

    /**
     * @param key      the key
     * @param defValue the def value
     * @return return int
     */
    public int getInt(String key, int defValue) {
        return preferences.getInt(key, defValue);
    }

    /**
     * @param key      the key
     * @param defValue the def value
     * @return return float
     */
    public float getFloat(String key, float defValue) {
        return preferences.getFloat(key, defValue);
    }

    /**
     * @param key      the key
     * @param defValue the def value
     * @return return long
     */
    public long getLong(String key, long defValue) {
        return preferences.getLong(key, defValue);
    }


    /**
     * @param key      the key
     * @param defValue the def value
     * @return return string set
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public Set<String> getStringSet(String key, Set<String> defValue) {
        return preferences.getStringSet(key, defValue);
    }

    /**
     * @return the all
     */
    public Map<String, ?> getAll() {
        return preferences.getAll();
    }

    /**
     * @param key the key
     * Remove.
     */
    public void remove(String key) {
        editor.remove(key).apply();
    }

    /**
     * Clear all.
     */
    public void clearAll() {
        editor.clear();
        editor.commit();

    }

    public ForgotPassReqPojo getForgotPwdReq() {
        return new Gson().fromJson(preferences.getString(FORGOTPWDREQDATA, null), ForgotPassReqPojo.class);
    }

    public void saveUserData(LoginPojo parsRes) {

        // User personal information
        editor.putString(ApiUtils.ID, parsRes.getUserData().getId()).apply();
        editor.putString(ApiUtils.EMAIL, parsRes.getUserData().getEmail()).apply();
        editor.putString(ApiUtils.FIRSTNAME, StringUtils.capitalize(parsRes.getUserData().getFirstname())).apply();
        editor.putString(ApiUtils.LASTNAME, StringUtils.capitalize(parsRes.getUserData().getLastname())).apply();
        editor.putString(ApiUtils.PHONE, parsRes.getUserData().getPhone()).apply();
        editor.putString(ApiUtils.TOKEN, parsRes.getUserData().getToken()).apply();
        editor.putString(ApiUtils.PROFILE_URL, parsRes.getUserData().getProfile_image()).apply();

        // User from Organization
        editor.putString(ApiUtils.ORGID, parsRes.getUserData().getOrganization()).apply();

        // User from Office
        editor.putString(ApiUtils.OFFICEID, parsRes.getUserData().getOffice()).apply();

        // User from Department
        editor.putString(ApiUtils.DEPARTMENTID, parsRes.getUserData().getDepartment()).apply();
    }

    private static class Builder {

        private final Context context;

        /**
         * Instantiates a new Builder.
         *
         * @param context the context
         */
        Builder(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null.");
            }
            this.context = context.getApplicationContext();
        }

        /**
         * Build prefs.
         *
         * @return the prefs
         */
        Prefs build() {
            return new Prefs(context);
        }
    }
}
