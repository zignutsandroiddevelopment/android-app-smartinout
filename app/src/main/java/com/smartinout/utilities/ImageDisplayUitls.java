package com.smartinout.utilities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.Base64;
import android.widget.ImageView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.LazyHeaderFactory;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.smartinout.R;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * The type Image display uitls.
 */
public class ImageDisplayUitls {

    /**
     * Display image.
     *
     * @param image     the image
     * @param activity  the activity
     * @param imageView the image view
     */
    public static void displayImage(String image, Activity activity
            , ImageView imageView) {

        Glide.with(activity)
                .load(image)
//                .error(R.mipmap.ic_launcher_round)
//                .placeholder(R.mipmap.ic_launcher_round)
                .into(imageView);
    }

    /**
     * Display image.
     *
     * @param image     the image
     * @param activity  the activity
     * @param imageView the image view
     */
    public static void displayImage(String image, Context activity
            , ImageView imageView) {

        Glide.with(activity)
                .load(image)
//                .error(R.mipmap.ic_launcher_round)
//                .placeholder(R.mipmap.ic_launcher_round)
                .into(imageView);
    }

    /**
     * Display image empty place.
     *
     * @param image     the image
     * @param activity  the activity
     * @param imageView the image view
     */
    public static void displayImageEmptyPlace(String image, Activity activity
            , ImageView imageView) {

        Glide.with(activity)
                .load(image)
                .into(imageView);
    }

    /**
     * Display image uri.
     *
     * @param image     the image
     * @param activity  the activity
     * @param imageView the image view
     */
    public static void displayImageUri(Uri image, Activity activity
            , ImageView imageView) {
        Glide.with(activity)
                .load(image)
                .error(R.mipmap.ic_launcher_round)
                .placeholder(R.mipmap.ic_launcher_round)
                .into(imageView);
    }

    /**
     * Display text.
     *
     * @param image      the image
     * @param imageView  the image view
     */
    public static void displayTextDrawable(String image, CircleImageView imageView, int color, int textClr) {

        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .textColor(textClr)
                .bold()
                .width(200)
                .height(200)
                .toUpperCase()
                .endConfig()
                .buildRound(image, color);
        imageView.setImageDrawable(drawable);
    }


    /**
     * Display image.
     *
     * @param image      the image
     * @param activity   the activity
     * @param imageView  the image view
     * @param placehoder the placehoder
     */
    public static void displayImage(String image, Activity activity
            , ImageView imageView, int placehoder) {

        Glide.with(activity)
                .load(image) // GlideUrl is created anyway so there's no extra objects allocated
                .error(placehoder)
                .placeholder(placehoder)
                .dontAnimate()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imageView);
    }

    /**
     * Display image.
     *
     * @param image      the image
     * @param activity   the activity
     * @param imageView  the image view
     * @param placehoder the placehoder
     */
    public static void displayImage(String image, Context activity
            , ImageView imageView, int placehoder) {

        Glide.with(activity)
                .load(image) // GlideUrl is created anyway so there's no extra objects allocated
                .error(placehoder)
                .placeholder(placehoder)
                .dontAnimate()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imageView);
    }

    /**
     * The type Basic authorization.
     */
    public class BasicAuthorization implements LazyHeaderFactory {
        private final String username;
        private final String password;

        /**
         * Instantiates a new Basic authorization.
         *
         * @param username the username
         * @param password the password
         */
        public BasicAuthorization(String username, String password) {
            this.username = username;
            this.password = password;
        }

        @Override
        public String buildHeader() {
            return "Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
        }
    }
}
