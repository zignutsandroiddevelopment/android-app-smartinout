package com.smartinout.utilities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.smartinout.R;
import com.smartinout.adapters.MenuSettingsAdapter;
import com.smartinout.base.App;
import com.smartinout.model.ActivityHours;
import com.smartinout.model.timesheet.quickpick.QuickTask;
import com.smartinout.model.login.Inoutreference;
import com.smartinout.model.settings.MenuSettings;
import com.smartinout.model.timesheet.CalendarModel;
import com.smartinout.retrofit.ApiUtils;

import java.sql.Time;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class AppUtilities {

    /**
     * list of dummy data for pie chart in dashboard
     */
    public static final String[] parties = new String[]{
            "Party A", "Party B", "Party C", "Party D", "Party E"};

    /**
     * @param context activity or fragment reference
     * @return list dummy data for quick task listing dashboard
     */
    public static ArrayList<QuickTask> getQuickTask(Context context) {

        ArrayList<QuickTask> listData = new ArrayList<>();



      /*  QuickTask quickTask = new QuickTask();
        quickTask.setmQuickTaskTitle("Team Meeting");
        listData.add(quickTask);

        quickTask = new QuickTask();
        quickTask.setmQuickTaskTitle("Sales Visit");
        listData.add(quickTask);

        quickTask = new QuickTask();
        quickTask.setmQuickTaskTitle("Quick Break");
        listData.add(quickTask);*/

        return listData;
    }

    /**
     * @param context activity or fragment reference
     * @return list of settings menu data
     */
    public static ArrayList<MenuSettings> getMenuSettings(Context context) {
        ArrayList<MenuSettings> listData = new ArrayList<>();


        /*settings.setmTitle(context.getResources().getString(R.string.notification));
        settings.setSwitch(true);
        settings.setViewType(MenuSettingsAdapter.VIEW_TYPE_LIGHT);
        listData.add(settings);*/

        MenuSettings settings = new MenuSettings();
        settings = new MenuSettings();
        settings.setmTitle(context.getResources().getString(R.string.profile));
        settings.setViewType(MenuSettingsAdapter.VIEW_TYPE_DARK);
        listData.add(settings);

        settings = new MenuSettings();
        settings.setmTitle(context.getResources().getString(R.string.changepassword));
        settings.setViewType(MenuSettingsAdapter.VIEW_TYPE_DARK);
        listData.add(settings);

        settings = new MenuSettings();
        settings.setmTitle(context.getResources().getString(R.string.termconditions));
        settings.setViewType(MenuSettingsAdapter.VIEW_TYPE_LIGHT);
        settings.setContent(Constants.URL);
        listData.add(settings);

        settings = new MenuSettings();
        settings.setmTitle(context.getResources().getString(R.string.privacypolicy));
        settings.setViewType(MenuSettingsAdapter.VIEW_TYPE_DARK);
        settings.setContent(Constants.URL);
        listData.add(settings);

        settings = new MenuSettings();
        settings.setmTitle(context.getResources().getString(R.string.legal));
        settings.setViewType(MenuSettingsAdapter.VIEW_TYPE_LIGHT);
        settings.setContent(Constants.URL);
        listData.add(settings);

        /*settings = new MenuSettings();
        settings.setmTitle(context.getResources().getString(R.string.faqs));
        settings.setViewType(MenuSettingsAdapter.VIEW_TYPE_DARK);
        settings.setContent("https://www.google.com/");
        listData.add(settings);*/

        settings = new MenuSettings();
        settings.setmTitle(context.getResources().getString(R.string.logout));
        settings.setViewType(MenuSettingsAdapter.VIEW_TYPE_LIGHT);
        listData.add(settings);

        return listData;
    }

    /**
     * @param hr  hour
     * @param min minute
     * @return Get time am/pm from hr and minute
     */
    public static String getTime(int hr, int min) {
        Time tme = new Time(hr, min, 0);//seconds by default set to zero
        Format formatter;
        formatter = new SimpleDateFormat("hh:mm a");
        return formatter.format(tme);
    }

    /**
     * Open list of data in popup
     *
     * @param context     activity or fragment reference
     * @param mTitle      dialog title
     * @param btnNagTitle dialog negative button title
     * @param listData    dialog list data
     * @param listener    click event handler for list item
     */
    public static void openDropDownPopup(Context context, String mTitle, String btnNagTitle
            , ArrayList<String> listData, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(mTitle);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, listData);
        builder.setAdapter(adapter, listener);
        builder.setNegativeButton(btnNagTitle, null);
        builder.create().show();
    }

    /**
     * @return Dummy list data for inout status
     */
    public static ArrayList<String> getListInoutStatus() {
        ArrayList<String> listData = new ArrayList<>();
        listData.add("LEAVE OFFICE");
        listData.add("ENTER OFFICE");
        listData.add("VISITING");
        return listData;
    }

    /**
     * Open dialog fragment with title,content,tag
     *
     * @param context  activity or fragment reference
     * @param fragment Dialog fragment to open
     * @param mTitle   dialog fragment title
     * @param mContent dialog fragment content to show
     * @param tag      dialog fragment tag
     */
    public static void openDialogFragment(Fragment context,
                                          DialogFragment fragment, String mTitle, String mContent,
                                          String tag) {
        FragmentTransaction ft = context.getFragmentManager().beginTransaction();
        Fragment prev = context.getFragmentManager().findFragmentByTag(tag);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TITLE, mTitle);
        bundle.putString(Constants.URL, mContent);
        fragment.setArguments(bundle);
        fragment.show(ft, tag);
    }

    /**
     * @param context  activity or fragment reference
     * @param fragment dialog fragment to open
     * @param tag      Open dialog fragment with title,tag
     */
    public static void openDialogFragment(AppCompatActivity context, DialogFragment fragment, String tag) {
        FragmentTransaction ft = context.getSupportFragmentManager().beginTransaction();
        Fragment prev = context.getSupportFragmentManager().findFragmentByTag(tag);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        ft.addToBackStack(null);
        fragment.show(ft, tag);
    }

    /**
     * @param context  activity or fragment reference
     * @param fragment dialog fragment to open
     * @param tag      Open dialog fragment with title,tag
     */
    public static void openDialogFragment(Fragment context, DialogFragment fragment, String tag) {
        FragmentTransaction ft = context.getFragmentManager().beginTransaction();
        Fragment prev = context.getFragmentManager().findFragmentByTag(tag);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        ft.addToBackStack(null);
        fragment.show(ft, tag);
    }

    /**
     * validate string if valid return string otherwise return an empty string
     *
     * @param value
     * @return
     */
    public static String getValidateString(String value) {
        if (value == null || value.equals("null") || value.length() == 0) {
            return "";
        }
        return value;
    }

    /**
     * validate password : check password contains at least one capital latter, one number and one special charcter.
     *
     * @param password
     * @return
     */
    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    /**
     * Has internet boolean.
     *
     * @param context the context
     * @return the boolean
     */
    public static boolean hasInternet(Context context, boolean showtoast) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (!(networkInfo != null && networkInfo.isConnectedOrConnecting()) && showtoast) {
            showToast(context, context.getResources().getString(R.string.no_internet));
        }
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    /**
     * Show toast.
     *
     * @param context the context
     * @param message the message
     */
    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /*public static ArrayList<CalendarModel> getWeeksOfMonth() {

        ArrayList<CalendarModel> listofWeeks = new ArrayList<>();

        Calendar weekstartDate = getWeekStartDate();

        int year1 = weekstartDate.get(Calendar.YEAR);
        int month1 = weekstartDate.get(Calendar.MONTH) + 1;
        int day1 = weekstartDate.get(Calendar.DAY_OF_MONTH);
        String date1 = day1 + "-" + month1 + "-" + year1;

        Calendar weekEndDate = getWeekEndDate();
        int year7 = weekEndDate.get(Calendar.YEAR);
        int month7 = weekEndDate.get(Calendar.MONTH) + 1;
        int day7 = weekEndDate.get(Calendar.DAY_OF_MONTH) +7;

        String date2 = day7 + "-" + month7 + "-" + year7;

        CalendarModel calendarModel = new CalendarModel();
        calendarModel.setWeeklydates(date1 + " TO " + date2);
        calendarModel.setCurrentweek(true);

        listofWeeks.add(calendarModel);

        return listofWeeks;
    }*/

    public static ArrayList<CalendarModel> getWeeksOfMonth() {

        ArrayList<CalendarModel> listofWeeks = new ArrayList<>();

        Calendar weekstartDate = getWeekStartDate();
        try {
            int year1 = weekstartDate.get(Calendar.YEAR);
            int month1 = weekstartDate.get(Calendar.MONTH) + 1;
            int day1 = weekstartDate.get(Calendar.DAY_OF_MONTH);
            String date1 = day1 + "-" + month1 + "-" + year1;

            SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.dd_MM_YYYY);

            Calendar weekEnd = Calendar.getInstance();
            Date myDate = dateFormat.parse(date1);
            weekEnd.setTime(myDate);
            weekEnd.add(Calendar.DAY_OF_YEAR, +6);

            int year7 = weekEnd.get(Calendar.YEAR);
            int month7 = weekEnd.get(Calendar.MONTH) + 1;
            int day7 = weekEnd.get(Calendar.DAY_OF_MONTH);

            String date2 = day7 + "-" + month7 + "-" + year7;

            CalendarModel calendarModel = new CalendarModel();
            calendarModel.setWeeklydates(date1 + " TO " + date2);
            calendarModel.setCurrentweek(true);

            listofWeeks.add(calendarModel);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
        }

        return listofWeeks;
    }

    public static void getWeek(TextView textView, String date, int differenceDate) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.dd_MM_YYYY);
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date);
            c.setTime(myDate);
            c.add(Calendar.DATE, differenceDate);
            c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            String startDate = dateFormat.format(c.getTime());
            int startyear = c.get(Calendar.YEAR);
            for (int i = 0; i < 6; i++) {
                c.add(Calendar.DATE, 1);
            }
            String endDate = dateFormat.format(c.getTime());

            textView.setText(startDate + " TO " + endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    /**
     * get current weeks and dates - from start date of week to end date of week
     *
     * @param dateString1
     * @param dateString2
     * @return
     */
    public static List<Date> getDates(String dateString1, String dateString2) {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat(Constants.dd_MM_YYYY);

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    /**
     * get day in string-  day from timestemp parameter
     *
     * @param timestemp
     * @return
     */
    public static String getDayinString(long timestemp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestemp);
        String[] days = new String[]{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        String day = days[calendar.get(Calendar.DAY_OF_WEEK) - 1];
        return day;
    }

    /**
     * on code scan success, show success popup
     * @param context
     */
    /*public static void showSuccess(Context context) {
        new com.smartinout.utilities.AlertDialog(context).showAlertDialog(
                context.getResources().getString(R.string.success),
                context.getResources().getColor(R.color.clrgreen),
                context.getResources().getString(R.string.scansuccessmsg)
        );
    }

    *//**
     * on code scan error, show success popup
     * @param context
     *//*
    public static void showError(Context context) {
        new com.smartinout.utilities.AlertDialog(context).showAlertDialog(
                context.getResources().getString(R.string.sorry),
                context.getResources().getColor(R.color.clrred),
                context.getResources().getString(R.string.scanerror)
        );
    }*/

    /**
     * get the in/out status and set the title of current status in dashboard
     *
     * @param context
     * @param textView
     * @param mainstatus
     * @param secondaryStatus
     * @param inoutcreatedtime
     * @param lastupdatedtime
     */
    public static void setStatusTitle(Context context, TextView textView, String mainstatus, String secondaryStatus, String quickStatus,
                                      String comment, long inoutcreatedtime, long lastupdatedtime) {

        Prefs prefs = Prefs.getInstance();
        if (AppUtilities.getValidateString(mainstatus).equals(ApiUtils.COUT)) {// Main Status Clocked Out
            prefs.save(Prefs.ISCLOKCEDIN,false);
            prefs.save(Prefs.TODAYINTIMEWITHSTATUS, context.getResources().getString(R.string.cloclkedoff));
            textView.setTextColor(context.getResources().getColor(R.color.clrred));
            textView.setText(context.getResources().getString(R.string.cloclkedoff));
            return;
        }

        if (AppUtilities.getValidateString(mainstatus).equals(ApiUtils.CIN)) {// Main Status Clocked In
            prefs.save(Prefs.ISCLOKCEDIN,true);
            Calendar calendar = Calendar.getInstance();

            if (AppUtilities.getValidateString(secondaryStatus).equals(ApiUtils.BI)) {// Break In
                calendar.setTimeInMillis(lastupdatedtime);
            } else {
                calendar.setTimeInMillis(inoutcreatedtime);
            }

            String hrsMns = getDayHour(context, calendar.get(Calendar.HOUR_OF_DAY)) + ":" +
                    String.format("%02d", calendar.get(Calendar.MINUTE));

            String inoutTime = "";
            if (calendar.get(Calendar.AM_PM) == Calendar.AM) {
                inoutTime = hrsMns + " AM";
            } else {
                inoutTime = hrsMns + " PM";
            }

            if (AppUtilities.getValidateString(secondaryStatus).equals(ApiUtils.BI)) {// Break In

                if (quickStatus != null && quickStatus.length() > 0) {
                    textView.setTextColor(context.getResources().getColor(R.color.clrYellow));
                    textView.setText("Break " +quickStatus + " " + inoutTime);
                }else{
                    textView.setTextColor(context.getResources().getColor(R.color.clrYellow));
                    textView.setText("Break " + inoutTime);
                }

                prefs.save(Prefs.TODAYINTIMEWITHSTATUS, "Break " + inoutTime);
            }else if (AppUtilities.getValidateString(secondaryStatus).equals(ApiUtils.OUT)) {// Break In
                textView.setTextColor(context.getResources().getColor(R.color.orange));
                if (comment != null && comment.length() > 0) {
                    textView.setText("OUT " +comment + " " + inoutTime);
                }else if (quickStatus != null && quickStatus.length() > 0) {
                    textView.setTextColor(context.getResources().getColor(R.color.orange));
                    textView.setText("OUT " +quickStatus + " " + inoutTime);
                }else{
                    textView.setText("OUT " + inoutTime);
                }

                prefs.save(Prefs.TODAYINTIMEWITHSTATUS,"OUT " + inoutTime);
            } else { // Clocked In

                if (comment != null && comment.length() > 0) {
                    textView.setTextColor(context.getResources().getColor(R.color.clrgreen));
                    textView.setText("IN " +comment + " " + inoutTime);
                } else if (quickStatus != null && quickStatus.length() > 0) {
                    textView.setTextColor(context.getResources().getColor(R.color.clrgreen));
                    textView.setText("IN " +quickStatus + " " + inoutTime);
                } else {
                    textView.setTextColor(context.getResources().getColor(R.color.clrgreen));
                    textView.setText("IN " + inoutTime);
                }
                prefs.save(Prefs.TODAYINTIMEWITHSTATUS, "IN " + inoutTime);
                prefs.save(Prefs.TODAYINTIME, inoutTime);
            }

            textView.setSelected(true);
        }
    }

    /**
     * get hour from the hours of day - Hours of day will be 1 to 24 and this will return 1-12 according
     * to Hours of day for example if Hours of day is 16, it will return 4
     *
     * @param context
     * @param hoursofday
     * @return
     */
    public static String getDayHour(Context context, int hoursofday) {
        if (hoursofday > 0)
            return context.getResources().getStringArray(R.array.hoursofday)[hoursofday - 1];
        else
            return "12";// if hours of day is 0 then it will be a 12 am.
    }

    /**
     * return formatted date Example dd/MM/yyyy - (09/09/2019)
     *
     * @return
     */
    public static String getCurrentDateDDMMYYYY() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat(Constants.ddMMyyyy);
        return df.format(c);
    }

    /**
     * return formatted date Example EEE dd MMM YYYY - (Friday 20 Sep 2019)
     *
     * @return
     */
    public static String getCurrentDateEEEEddMMMMYYYY() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat(Constants.EEEEddMMMMYYYY);
        return df.format(c);
    }

    /**
     * return formatted date Example EEE dd MMM  - (Friday 20 Sep)
     *
     * @return
     */
    public static String getCurrentDateEEEEddMMMM() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat(Constants.EEEEddMMM);
        return df.format(c);
    }

    /**
     * return formatted date Example EEE dd MMM  - (Friday 20 Sep, 01:10 Pm)
     *
     * @return
     */
    public static String getCurrentDateEEEEddMMMMHHMM() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat(Constants.YYYY_MM_DD_T_HH_MM_AA);
        return df.format(c);
    }

    /**
     * return formatted date Example EEE dd MMM  - (Friday 20 Sep, 01:10 Pm)
     *
     * @return
     */
    public static String getCurrentDateEEEEddMMMMHHMM(long miliseconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(miliseconds);
        Date c = calendar.getTime();
        SimpleDateFormat df = new SimpleDateFormat(Constants.YYYY_MM_DD_T_HH_MM_AA);
        return df.format(c);
    }


    /**
     * return formatted date Example EEE dd MMM  - (Friday 20 Sep)
     *
     * @return
     */
    public static String getCurrentDateEEEEddMMMMYYYY(long miliseconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(miliseconds);
        Date c = calendar.getTime();
        SimpleDateFormat df = new SimpleDateFormat(Constants.EEEEddMMMMYYYY);
        return df.format(c);
    }

    /**
     * return formatted date Example dd/mm/yyyy  - (20/09/2019)
     *
     * @return
     */
    public static String getCurrentDateddMMyyyy(long miliseconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(miliseconds);
        Date c = calendar.getTime();
        SimpleDateFormat df = new SimpleDateFormat(Constants.ddMMyyyy);
        return df.format(c);
    }

    /**
     * return formatted date Example dd MMM YYYY  - (20 Sep 2019)
     *
     * @return
     */
    public static String getCurrentDateddMMMYYYY() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat(Constants.ddMMMYYYY);
        return df.format(c);
    }

    public static String convertTimeHHMMSS(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.hhMMss);
        SimpleDateFormat sdfs = new SimpleDateFormat(Constants.hhMMA);
        Date dt;
        try {
            dt = sdf.parse(time);
            return sdfs.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
            return time;
        }
    }

    public static long convertDateToLong(String dateString){
        long startDate = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.ddMMyyyyhhMMA);
            Date date = sdf.parse(dateString);

            startDate = date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate;
    }

    public static Date convertStringToDate(String dateString){
        Date startDate = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.ddMMyyyy);
            Date date = sdf.parse(dateString);

            startDate = date;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate;
    }

    public static Date convertStringToDateNew(String dateString){
        Date startDate = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_5);
            Date date = sdf.parse(dateString);

            startDate = date;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate;
    }

    public static String convertRegoDateFormat(String dateString) {
        String result = "";

        SimpleDateFormat formatterOld = new SimpleDateFormat(Constants.YYYY_MM_DD, Locale.getDefault());
        SimpleDateFormat formatterNew = new SimpleDateFormat(Constants.ddMMyyyy, Locale.getDefault());
        Date date = null;
        try {
            date = formatterOld.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            result = formatterNew.format(date);
        }
        return result;
    }


    /**
     * calculate break hours, in hours od current day.
     *
     * @param inoutreferenceList
     * @return
     */
    public static ActivityHours timesheetCalculation(List<Inoutreference> inoutreferenceList) {
        long totlaBreakHours = 0;
        long totlaInHours = 0;
        String lastStatus = "";
        String lastMainStatus = "";
        for (int i = 0; i < inoutreferenceList.size(); i++) {

            Inoutreference inoutreference = inoutreferenceList.get(i);
            String currentStatus = inoutreference.getSecondryStatus();
            lastMainStatus = inoutreference.getMainStatus();
            lastStatus = currentStatus;

            if (i == inoutreferenceList.size() - 1 && inoutreference.getSecondryStatus().equals(ApiUtils.COUT))
                continue;

            long InTime = inoutreference.getCreatedInoutTime();
            long OutTime = Calendar.getInstance().getTimeInMillis();
            if (i < inoutreferenceList.size() - 1) {
                OutTime = inoutreferenceList.get(i + 1).getCreatedInoutTime();
            }

            if (currentStatus.equals(ApiUtils.BI)) {
                totlaBreakHours = totlaBreakHours + (OutTime - InTime);
            } else if (currentStatus.equals(ApiUtils.IN) || currentStatus.equals(ApiUtils.OUT)) {
                totlaInHours = totlaInHours + (OutTime - InTime);
            }
        }

        ActivityHours activityHours = new ActivityHours();
        activityHours.setLastStatus(lastStatus);
        activityHours.setLastmainStatus(lastMainStatus);
        return activityHours;
    }

    /**
     * return format of hours minute to 00h 00m - (02h 10m)
     *
     * @param milliSeconds
     * @return
     */
    public static String convertSecondsToHMmSs(long milliSeconds) {
        long seconds = TimeUnit.MILLISECONDS.toSeconds(milliSeconds);
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60)) % 24;
        return String.format("%dh %02dm", h, m);
    }

    /**
     * return format of hours minute to 00h 00m - (02:10)
     *
     * @param milliSeconds
     * @return
     */
    public static String hmTimeFormatter(long milliSeconds) {
        /*long seconds = (int)((milliSeconds / 1000) % 60);//TimeUnit.MILLISECONDS.toSeconds(milliSeconds);
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60)) % 24;
        return String.format("%02d:%02d", h, m);*/

        long hour = milliSeconds/(3600*1000);
        long min = milliSeconds/(60*1000) % 60;
        long sec =  milliSeconds/1000 % 60;

        String hms = String.format("%02d:%02d", hour, min);

        return hms;
    }

    public static long getHour(long milliSeconds) {
        /*long seconds = (int)((milliSeconds / 1000) % 60);//TimeUnit.MILLISECONDS.toSeconds(milliSeconds);
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60)) % 24;
        return String.format("%02d:%02d", h, m);*/

        long hour = milliSeconds/(3600*1000);
        long min = milliSeconds/(60*1000) % 60;
        long sec =  milliSeconds/1000 % 60;

        String hms = String.format("%02d:%02d", hour, min);

        return hour;
    }

    /**
     * return format of hours minute to 00h 00m - (02:10)
     *
     * @param milliSeconds
     * @return
     */
    public static String msTimeFormatter(long milliSeconds) {
        long seconds = TimeUnit.MILLISECONDS.toSeconds(milliSeconds);
        long m = (seconds / 60) % 60;
        long s = seconds % 60;
        return String.format("%02dm %02ds", m, s);
    }

    public static String getTime(long milliseconds) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.hhMMA, Locale.getDefault());
        return dateFormat.format(new Date(milliseconds));
    }

    /**
     * return format of hours minute to 00h 00m - (02:10)
     *
     * @param minutes
     * @return
     */
    public static String hmTimeFormatterfromMinutes(long minutes) {
        long h = minutes / 60;
        long m = minutes % 60;
        return String.format("%02dh %02dm", h, m);
    }

    /**
     * return format of hours minute to 00h 00m - (02:10)
     *
     * @param minutes
     * @return
     */
    public static String hmTimeFormatterfromMinutesForDaily(long minutes) {
        long h = minutes / 60;
        long m = minutes % 60;
        //return (h + ":" + m);
        return String.format("%02d:%02d", h, m);
    }

    /**
     * return format of hours minute to 00h 00m - (02:10)
     *
     * @param minutes
     * @return
     */
    public static String timeFormatterfromMinutes(long minutes) {
        long h = minutes / 60;
        long m = minutes % 60;
        return String.format("%02d:%02d", h, m);
    }

    /**
     * return format of hours minute to 00h 00m - (02:10)
     *
     * @param minutes
     * @return
     */
    /*public static String timeFormatterfromMinutes(long minutes) {
        long seconds = TimeUnit.MILLISECONDS.toSeconds(minutes * 60000);
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60)) % 24;
        return String.format("%02d:%02d", h, m);
    }*/
    public static Calendar getWeekStartDate() {
        Calendar calendar = Calendar.getInstance();
        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            calendar.add(Calendar.DATE, -1);
        }
        return calendar;
    }

    public static Calendar getWeekEndDate() {
        Calendar calendar = Calendar.getInstance();
        /*while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            System.out.print("WeekEndDate : " + calendar.get(Calendar.DAY_OF_WEEK));
            calendar.add(Calendar.DATE, 1);
        }

        calendar.add(Calendar.DATE, -1);*/
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        for (int i = 0; i <6; i++) {
            calendar.add(Calendar.DATE, 1);
        }
        return calendar;
    }

    public static final String CLR_GREEN = "green";
    public static final String CLR_GREY = "grey";
    public static final String CLR_RED = "red";

    public static int getColorofProgress(Context context, String color) {
        if (CLR_GREEN.equals(color)) {
            return context.getResources().getColor(R.color.clrgreen);
        } else if (CLR_GREY.equals(color)) {
            return context.getResources().getColor(R.color.grayprogress);
        } else if (CLR_RED.equals(color)) {
            return context.getResources().getColor(R.color.clrred);
        }
        return context.getResources().getColor(R.color.colorPrimary);
    }

    public static String getUserFullName(String userFirstname, String userLastname) {
        String name = "";
        if (userFirstname != null && userLastname != null && !userFirstname.isEmpty() && !userLastname.isEmpty()) {
            name = userFirstname + " " + userLastname;
        } else {
            name = userFirstname;
        }
        return name;
    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate) {

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return outputDate;

    }

    public static String formatDateFromDateString(String inputDateFormat, String outputDateFormat, String inputDate) throws ParseException {
        Date mParsedDate = null;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat = new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat = new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        try {
            mParsedDate = mInputDateFormat.parse(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }

    /**
     * Get Timestamp from date and time
     *
     * @param mDateTime   datetime String
     * @param mDateFormat Date Format
     * @return
     * @throws ParseException
     */
    public static String getTimeStampFromDateTime(String mDateFormat, String mOutDateFormat, String mDateTime) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(mDateFormat);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = dateFormat.parse(mDateTime);
        String outputdate = convertHumanredableDate(mOutDateFormat, date.getTime());
        return outputdate;
    }

    public static String convertHumanredableDate(String mOutDateFormat, long timestamp) {
        String mOutputDateString;
        SimpleDateFormat mOutputDateFormat = new SimpleDateFormat(mOutDateFormat);
        mOutputDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date inputDate = new Date(timestamp);
        mOutputDateString = mOutputDateFormat.format(inputDate);
        return mOutputDateString;
    }


    public static String getHrsInOutFromLong(Context context, long timestemp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestemp);
        if (calendar.get(Calendar.AM_PM) == Calendar.AM) {
            return String.format("%s:%02d", getDayHour(context, calendar.get(Calendar.HOUR_OF_DAY)), calendar.get(Calendar.MINUTE)) + " AM ";

        } else {
            return String.format("%s:%02d", getDayHour(context, calendar.get(Calendar.HOUR_OF_DAY)), calendar.get(Calendar.MINUTE)) + " PM ";
        }
    }

    public static long currentDate(String currentTimezone) {
        long convertedmilli = 0;
        try {
            TimeZone etTimeZone = TimeZone.getTimeZone(currentTimezone);
            Date currentDate = Calendar.getInstance().getTime();
            SimpleDateFormat mOutputDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mma z");
            mOutputDateFormat.setTimeZone(etTimeZone);
            String test = mOutputDateFormat.format(currentDate);
            convertedmilli = mOutputDateFormat.parse(test).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedmilli;
    }
}
