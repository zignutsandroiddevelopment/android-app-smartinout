package com.smartinout.utilities;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import com.smartinout.R;
import com.smartinout.databinding.LayoutAlertpopupBinding;
import com.smartinout.databinding.LayoutAlertpopupBookedBinding;
import com.smartinout.interfaces.OnItemClick;

import androidx.databinding.DataBindingUtil;

public class AlertDialog {

    private Context context;
    public Dialog dialog;
    LayoutAlertpopupBinding binding;
    LayoutAlertpopupBookedBinding bookedBinding;
    private OnItemClick onItemClick;
    boolean isUnbook = false;

    public AlertDialog(Context context, OnItemClick onItemClick) {
        this.context = context;
        this.onItemClick = onItemClick;
    }

    public void showAlertDialog(final String title, int titleclr, String message) {

        dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        dialog.setCanceledOnTouchOutside(false);
        binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.layout_alertpopup, null, false);
        dialog.setContentView(binding.getRoot());


        binding.tvTitle.setTextColor(titleclr);
        binding.tvMessage.setText(message);
        binding.tvTitle.setText(title);

        binding.includeWidgetBtn.tvBtnTitle.setText(context.getResources().getString(R.string.ok_button));
        binding.includeWidgetBtn.tvBtnTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (title.equalsIgnoreCase(context.getResources().getString(R.string.success))) {
                    onItemClick.onButtonClick(false);
                } else {
                    onItemClick.onButtonClick(true);
                }
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
    }

    public void showCancelAlertDialog(final String title, int titleclr, String message) {

        dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        dialog.setCanceledOnTouchOutside(false);
        bookedBinding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.layout_alertpopup_booked, null, false);
        dialog.setContentView(bookedBinding.getRoot());

        bookedBinding.tvTitle.setTextColor(titleclr);
        bookedBinding.tvMessage.setText(message);
        bookedBinding.tvTitle.setText(title);


        bookedBinding.includeWidgetBtnDouble.tvBtnSubmit.setText(context.getResources().getString(R.string.ok_button));


        bookedBinding.includeWidgetBtnDouble.tvBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onButtonClick(false);
                dialog.dismiss();
            }
        });

        bookedBinding.includeWidgetBtnDouble.tvBtnCancel.setText(context.getResources().getString(R.string.cancel));
        bookedBinding.includeWidgetBtnDouble.tvBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onButtonClick(true);
                dialog.dismiss();
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.show();
    }

    public void showBookingAlertDialog(final String title, int titleclr, String message
            , final String vehicleId,String vehicleNo, final int position) {

        dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        dialog.setCanceledOnTouchOutside(false);
        bookedBinding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.layout_alertpopup_booked, null, false);
        dialog.setContentView(bookedBinding.getRoot());

        bookedBinding.tvTitle.setTextColor(titleclr);
        bookedBinding.tvMessage.setText(message);
        bookedBinding.tvTitle.setText(title);


        String finalTitle = String.format(context.getString(R.string.vehicleBookingtitle),
                vehicleNo);
        if (title.equalsIgnoreCase(finalTitle)
                || title.equalsIgnoreCase(context.getResources().getString(R.string.available_now))) {
            isUnbook = false;
            bookedBinding.includeWidgetBtnDouble.tvBtnSubmit.setText(context.getResources().getString(R.string.book_button));
        } else {
            isUnbook = true;
            bookedBinding.includeWidgetBtnDouble.tvBtnSubmit.setText(context.getResources().getString(R.string.unbook_button));
        }


        bookedBinding.includeWidgetBtnDouble.tvBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onUpdateClick(position, vehicleId, isUnbook);
                dialog.dismiss();
            }
        });

        bookedBinding.includeWidgetBtnDouble.tvBtnCancel.setText(context.getResources().getString(R.string.cancel));
        bookedBinding.includeWidgetBtnDouble.tvBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onButtonClick(true);
                dialog.dismiss();
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;

        dialog.show();
    }


}
