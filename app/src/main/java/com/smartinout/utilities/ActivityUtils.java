/*
 * Copyright (c) 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential by BNBJobs
 */

package com.smartinout.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.fragment.app.Fragment;


/**
 * The type Activity utils.
 */
public class ActivityUtils {

    /**
     * Launch activity.
     *
     * @param context              the context
     * @param activity             the activity
     * @param closeCurrentActivity the close current activity
     * @param bundle               the bundle
     */
    public static void launchActivity(Activity context, Class<? extends Activity> activity,
                                      boolean closeCurrentActivity, Bundle bundle) {
        Intent intent = new Intent(context, activity);

        if (bundle != null) {
            intent.putExtras(bundle);
        }

        context.startActivity(intent);
        if (closeCurrentActivity) {
            context.finish();
        }
    }

    /**
     * Launch start activity for result.
     *
     * @param fragment    the fragment
     * @param activity    the activity
     * @param requestCode the request code
     * @param bdl         the bdl
     */
    public static void launchStartActivityForResult(Fragment fragment, Class<? extends Activity> activity
            , int requestCode
            , Bundle bdl) {
        Intent intent = new Intent(fragment.getContext(), activity);
        if (bdl != null)
            intent.putExtras(bdl);
        fragment.startActivityForResult(intent, requestCode);
    }

    /**
     * Launch start activity for result.
     *
     * @param context     the context
     * @param activity    the activity
     * @param requestCode the request code
     * @param bdl         the bdl
     */
    public static void launchStartActivityForResult(Activity context
            , Class<? extends Activity> activity
            , int requestCode
            , Bundle bdl) {
        Intent intent = new Intent(context, activity);
        intent.putExtras(bdl);
        context.startActivityForResult(intent, requestCode);
    }

    /**
     * Launch activity with opt.
     *
     * @param context              the context
     * @param activity             the activity
     * @param closeCurrentActivity the close current activity
     * @param bundle               the bundle
     * @param options              the options
     */
    public static void launchActivityWithOpt(Activity context, Class<? extends Activity> activity,
                                             boolean closeCurrentActivity, Bundle bundle, ActivityOptionsCompat options) {
        Intent intent = new Intent(context, activity);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivity(intent, options.toBundle());
        if (closeCurrentActivity) {
            context.finish();
        }
    }


    /**
     * Launch activity.
     *
     * @param context  the context
     * @param activity the activity
     */
    public static void launchActivity(Activity context, Class<? extends Activity> activity) {
        Intent intent = new Intent(context, activity);
        context.startActivity(intent);
        ActivityCompat.finishAffinity(context);
    }

    /**
     * Launch activity.
     *
     * @param context              the context
     * @param activity             the activity
     * @param closeCurrentActivity the close current activity
     */
    public static void launchActivity(Activity context, Class<? extends Activity> activity, boolean closeCurrentActivity) {
        ActivityUtils.launchActivity(context, activity, closeCurrentActivity, null);
    }

    /**
     * Launch start activity for result.
     *
     * @param context              the context
     * @param activity             the activity
     * @param closeCurrentActivity the close current activity
     * @param bundle               the bundle
     * @param requestCode          the request code
     */
    public static void launchStartActivityForResult(Activity context, Class<? extends Activity> activity
            , boolean closeCurrentActivity, int requestCode, Bundle bundle) {
        Intent intent = new Intent(context, activity);

        if (bundle != null) {
            intent.putExtras(bundle);
        }

        context.startActivityForResult(intent, requestCode);
        if (closeCurrentActivity) {
            context.finish();
        }
    }


    /**
     * Launch start activity for result.
     *
     * @param context     the context
     * @param activity    the activity
     * @param bundle      the bundle
     * @param requestCode the request code
     */
    public static void launchStartActivityForResult(Fragment context, Class<? extends Activity> activity
            , Bundle bundle
            , int requestCode) {
        Intent intent = new Intent(context.getContext(), activity);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivityForResult(intent, requestCode);

    }

    /**
     * Launch start activity for result.
     *
     * @param context     the context
     * @param activity    the activity
     * @param bundle      the bundle
     */
    public static void launchStartActivityForFragment(Fragment context, Class<? extends Activity> activity
            , Bundle bundle) {
        Intent intent = new Intent(context.getContext(), activity);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivity(intent);

    }

    /**
     * Launch start activity for result.
     *
     * @param fragment    the fragment
     * @param activity    the activity
     * @param requestCode the request code
     */
    public static void launchStartActivityForResult(Fragment fragment, Class<? extends Activity> activity
            , int requestCode) {
        Intent intent = new Intent(fragment.getContext(), activity);
        fragment.startActivityForResult(intent, requestCode);
    }

    /**
     * Launch start activity for result.
     *
     * @param context     the context
     * @param activity    the activity
     * @param requestCode the request code
     */
    public static void launchStartActivityForResult(Activity context, Class<? extends Activity> activity
            , int requestCode) {
        Intent intent = new Intent(context, activity);
        context.startActivityForResult(intent, requestCode);
    }

    /**
     * Launch activity with clear back stack.
     *
     * @param context  the context
     * @param activity the activity
     */
    public static void launchActivityWithClearBackStack(Context context, Class<? extends Activity> activity) {
        Intent intent = new Intent(context, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    /**
     * Force screen to turn on if the phone is asleep.
     *
     * @param context The current Context or Activity that this method is called from
     */
    public static void turnScreenOn(Activity context) {
        try {
            Window window = context.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
            window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        } catch (Exception ex) {
            Logger.withTag("Caffeine").log("Unable to turn on screen for activity " + context);
        }
    }

    /**
     * Call phone.
     *
     * @param context     the context
     * @param phoneNumber the phone number
     */
    public static void callPhone(Context context, String phoneNumber) {
        try {
            Intent dialIntent = new Intent(Intent.ACTION_DIAL);
            dialIntent.setData(Uri.parse("tel:" + phoneNumber));
            context.startActivity(dialIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Email intent.
     *
     * @param context the context
     * @param emailid the emailid
     */
    public static void EmailIntent(Context context, String emailid) {
        try {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:"));
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailid + ""});
            intent.putExtra(Intent.EXTRA_SUBJECT, "From Xkeeper");
            context.startActivity(Intent.createChooser(intent, "Email via..."));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Hide keyboard.
     *
     * @param context the context
     */
    public static void hideKeyboard(Activity context) {
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}