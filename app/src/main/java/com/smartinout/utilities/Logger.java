package com.smartinout.utilities;

import android.util.Log;

import com.smartinout.BuildConfig;


/**
 * The type Logger.
 */
public class Logger {

    private final String TAG;
    private final int priority;

    /**
     * With tag logger.
     *
     * @param tag the tag
     * @return the logger
     */
    public static Logger withTag(String tag) {
        return new Logger(tag);
    }

    private Logger(String TAG) {
        this.TAG = TAG;
        this.priority = Log.DEBUG;
    }

    /**
     * Log logger.
     *
     * @param message the message
     * @return the logger
     */
    public Logger log(String message) {
        if (BuildConfig.DEBUG) {
            Log.println(priority, TAG, message);
        }
        return this;
    }

    /**
     * With cause.
     *
     * @param cause the cause
     */
    public void withCause(Exception cause) {
        if (BuildConfig.DEBUG) {
            Log.println(priority, TAG, Log.getStackTraceString(cause));
        }
    }
}