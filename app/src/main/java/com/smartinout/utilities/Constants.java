package com.smartinout.utilities;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.smartinout.R;

public class Constants {
    public static final String TITLE = "title";
    public static final String URL = "http://smartinout.zignuts.com/";

    public static String DIALOG_FRAG_TAG_WEBVIEW = "webview";
    public static String DIALOG_FRAG_TAG_FORGOTPASS = "forgotpass";

    public static final int RESULT_CODE_BOOK_VEHICLE = 100;
    public static final int RESULT_CODE_UPLOAD_IMG = 200;
    public static final int RESULT_CODE_EDITTIMESHEET = 300;
    public static final String ACTION_REFRESHTIMESHEET = R.string.app_name + ".refresh";
    public static final String ACTION_STATUSCHANGED = R.string.app_name + ".statuschanged";




    public static final String STATUS_ONGOING_TRIP = "OnGoing";
    public static final String STATUS_START_TRIP = "Start";
    public static final String STATUS_END_TRIP = "End";
    public static final String BOOKID = "bookid";
    public static final String DATA = "data";
    public static final String OFFICE_DATA = "officeData";
    public static final String FROM = "from";

    public static final String INOUTPREF = "inoutref";
    public static final String INOUTPREF_PREVIOUSTIME = "inoutref_previoustime";
    public static final String INOUTHRS = "00:00";
    public static final String INOUTHRSTEMP = "00h 00m";

    public static final String hhMMA = "hh:mm a";
    public static final String hhMMss = "hh:mm:ss";
    public static final String ddMMyyyyhhMMA = "dd/MM/yyyy hh:mm:ss";
    public static final String ddMMyyyy = "dd/MM/yyyy";
    public static final String EEEEddMMM = "EEEE dd MMM";
    public static final String EEEEddMMMMYYYY = "EEEE dd MMMM YYYY";
    public static final String ddMMMYYYY = "dd MMM YYYY";
    public static final String dd_MM_YYYY = "dd-MM-yyyy";
    public static final String DATE_FORMAT_5 = "dd MMM, yyyy";
    public static final String DATE_FORMAT_6 = "dd MMMM yyyy";
    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String dd = "dd";
    public static final String YYYY_MM_DD_T_HH_MM_AA = "EEEE dd MMM , HH:mm a";
    public static final String YYYY_MM_DD_T_HH_MM_SS_Z = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String EEEMMMDDHHMMSSZZZYYYY = "EEE MMM dd HH:mm:ss zzz yyyy";


    //New Key
    public static final String AMAZONE_KEY = "AKIAQN76CZGJTUTNXVXY";
    public static final String AMAZONE_SECRET = "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";

    public static final String S3_PROFILE_BUCKET = "smartinout-dev";
    public static final Region S3_REGION = Region.getRegion(Regions.AP_SOUTH_1);

}
