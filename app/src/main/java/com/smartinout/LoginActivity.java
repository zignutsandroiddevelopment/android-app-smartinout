package com.smartinout;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.smartinout.base.App;
import com.smartinout.base.BaseActivity;
import com.smartinout.databinding.ActivityLoginBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.model.login.LoginPojo;
import com.smartinout.model.timesheet.quickpick.QuickPickPojo;
import com.smartinout.model.timesheet.quickpick.QuickTask;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.ActivityUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.EmailValidator;
import com.smartinout.utilities.Prefs;

import java.util.HashMap;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import retrofit2.Response;

import static com.raizlabs.android.dbflow.config.FlowManager.getContext;

public class LoginActivity extends BaseActivity implements View.OnClickListener,TextWatcher, ApiResponse {

    ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(LoginActivity.this, R.layout.activity_login);
        initiateUI();
    }

    @Override
    protected void initiateUI() {

        binding.includeWidgetBtn.tvBtnTitle.setText(getResources().getString(R.string.login));
        binding.edtEmail.addTextChangedListener(this);
        binding.tvForgetPassword.setOnClickListener(this);
        binding.includeWidgetBtn.tvBtnTitle.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvForgetPassword:
                ActivityUtils.launchActivity(LoginActivity.this, ForgotPasswordActivity.class, false);
                break;
            case R.id.tvBtnTitle:
                submitData();
                /*String timeMH = AppUtilities.hmTimeFormatter(86400000);
                System.out.print("Time : "+timeMH);*/
                break;
        }
    }

    public void submitData() {

        if (!AppUtilities.hasInternet(LoginActivity.this, true)) {
            return;
        }


        String emailId = binding.edtEmail.getText().toString();
        String password = binding.edtPassword.getText().toString();

        if (AppUtilities.getValidateString(emailId).isEmpty()) {
            AppUtilities.showToast(LoginActivity.this, getResources().getString(R.string.enteryouremail));
            return;
        }

        if (AppUtilities.getValidateString(password).isEmpty()) {
            AppUtilities.showToast(LoginActivity.this, getResources().getString(R.string.enterpwd));
            return;
        }

        EmailValidator emailValidator = new EmailValidator();
        if (!emailValidator.validateEmail(emailId)) {
            AppUtilities.showToast(LoginActivity.this, getResources().getString(R.string.enteryouremail));
            return;
        }


        showProgressDialog();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(ApiUtils.EMAIL, emailId);
        hashMap.put(ApiUtils.PASSWORD, password);
        callService.loginApi(hashMap, this);
    }

    @Override
    public void onSuccess(Response response) {
        hideProgressDialog();
        LoginPojo parsRes = (LoginPojo) response.body();
        if (parsRes == null) {
            AppUtilities.showToast(LoginActivity.this, getResources().getString(R.string.invalidresposne));
            return;
        }
        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(LoginActivity.this, parsRes.getError());
        }

        if (!parsRes.getUserData().getRoles().equals("U")){
            AppUtilities.showToast(LoginActivity.this, getResources().getString(R.string.youarenotuserofanyorg));
            return;
        }

        /*if (!parsRes.getUserData().getRoles().equals("HR")){
            AppUtilities.showToast(LoginActivity.this, getResources().getString(R.string.youarenotuserofanyorg));
            return;
        }*/
        inOutDBCtrl.clearAll();
        prefs.saveUserData(parsRes);
        ActivityUtils.launchActivity(LoginActivity.this, HomeActivity.class, true, null);
        //getQuickPicks();

    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        AppUtilities.showToast(LoginActivity.this, error);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        prefs.save(Prefs.EMAIL,s.toString());

    }

    //Himadri
    /*private void getQuickPicks() {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        callService.getQuickPicks(getQuickPicks);
    }

    public ApiResponse getQuickPicks = new ApiResponse() {
        @Override
        public void onSuccess(Response response) {
            hideProgressDialog();

            QuickPickPojo quickData = (QuickPickPojo) response.body();

            ActivityUtils.launchActivity(LoginActivity.this, HomeActivity.class, true, null);
            if (quickData == null) {
                AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
                return;
            }

            if (!AppUtilities.getValidateString(quickData.getError()).isEmpty()) {
                AppUtilities.showToast(getContext(), quickData.getError());
            }

            List<QuickTask> quickTaskList = quickData.getQuickPickDatat();
            App.setQuickTaskListIn(quickTaskList);

        }

        @Override
        public void onFailure(String error) {
            hideProgressDialog();
            AppUtilities.showToast(getContext(), error);
            ActivityUtils.launchActivity(LoginActivity.this, HomeActivity.class, true, null);
        }
    };*/




}
