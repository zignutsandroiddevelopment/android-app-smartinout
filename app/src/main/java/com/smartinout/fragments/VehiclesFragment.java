package com.smartinout.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.smartinout.R;
import com.smartinout.adapters.BookedVehicleInOutAdapter;
import com.smartinout.adapters.VehicleInOutAdapter;
import com.smartinout.base.BaseFragment;
import com.smartinout.databinding.FragmentVehiclesBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.interfaces.OnItemClick;
import com.smartinout.livedatamodel.VehicleViewModel;
import com.smartinout.model.vehiclebooking.AvailableData;
import com.smartinout.model.vehiclebooking.AvailableModel;
import com.smartinout.model.vehiclebooking.BookedData;
import com.smartinout.model.vehiclebooking.BookedModel;
import com.smartinout.model.vehiclebooking.CreateBookingData;
import com.smartinout.model.vehiclebooking.CreateBookingModel;
import com.smartinout.model.vehiclebooking.UnBookData;
import com.smartinout.model.vehiclebooking.UnBookModel;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.AlertDialog;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;
import com.smartinout.utilities.VehicleTimePickerDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import retrofit2.Response;

public class VehiclesFragment extends BaseFragment implements View.OnClickListener, ApiResponse, OnItemClick {

    FragmentVehiclesBinding binding;
    private DatePickerDialog datePickerDialog;
    private VehicleTimePickerDialog timePickerDialog;
    private int fromYear, fromMonth, fromDay, fromHours, fromMinute;
    private int toYear, toMonth, toDay, toHours, toMinute;
    private VehicleInOutAdapter inOutAdapter;
    private BookedVehicleInOutAdapter bookedVehicleInOutAdapter;
    private List<AvailableData> listData = new ArrayList<>();
    private List<BookedData> bookedListData = new ArrayList<>();
    private int viewType = VehicleInOutAdapter.TYPE_AVAILABLE;
    private VehicleViewModel viewModel;
    private Class<?> mClss;
    private static final int ZXING_CAMERA_PERMISSION = 1;
    private boolean isStartDateTime = false;
    private String from_date, from_time, to_date, to_time;
    private boolean isUnBooked = false;
    private boolean isFromRefresh = false;
    final Calendar minToDate = Calendar.getInstance();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.fragment_vehicles, container, false);
        viewModel = ViewModelProviders.of(VehiclesFragment.this).get(VehicleViewModel.class);
        setUpLeftRightBtnClicks();
        initUI();
        //setInitialFromToDate();
        //loadAvailableVehicle();
//        loadBookedOrCompleteList(ApiUtils.BO);
        onCenterBtnClick(binding.includeLeftRightBtn.tvCenterBtn);
        return binding.getRoot();
    }

    private void initUI() {

        Calendar c = Calendar.getInstance();
        fromHours = c.get(Calendar.HOUR_OF_DAY);
        fromMinute = c.get(Calendar.MINUTE);

        binding.tvSelectFromDateTime.setOnClickListener(this);
        binding.tvSelectToDateTime.setOnClickListener(this);
        binding.rvlistData.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvlistData.setVisibility(View.GONE);
        binding.tvError.setVisibility(View.VISIBLE);
//        binding.linearDateTime.setVisibility(View.VISIBLE);
    }

    public void fillAvailableVehicle(List<AvailableData> listData) {

        this.listData.clear();
        this.listData.addAll(listData);


        inOutAdapter = new VehicleInOutAdapter(getContext(), this.listData, VehicleInOutAdapter.TYPE_AVAILABLE, VehiclesFragment.this);
        binding.rvlistData.setAdapter(inOutAdapter);

    }

    public void fillBookedVehicle(List<BookedData> listData) {

        this.bookedListData.clear();
        this.bookedListData.addAll(listData);


        bookedVehicleInOutAdapter = new BookedVehicleInOutAdapter(getContext(), this.bookedListData, this, viewType);
        binding.rvlistData.setAdapter(bookedVehicleInOutAdapter);

    }

    public void fillVehicleRideHisData(List<AvailableData> listData) {


        this.listData.clear();
        this.listData.addAll(listData);

        inOutAdapter = new VehicleInOutAdapter(getContext(), listData, VehicleInOutAdapter.TYPE_RIDEHIS, this);
        binding.rvlistData.setAdapter(inOutAdapter);
    }

    public void setUpLeftRightBtnClicks() {
        defaultSelection(binding.includeLeftRightBtn.tvLeftBtn,
                binding.includeLeftRightBtn.tvCenterBtn
                , binding.includeLeftRightBtn.tvRightBtn);

        clickCenterLeftBtn(binding.includeLeftRightBtn.tvLeftBtn,
                binding.includeLeftRightBtn.tvCenterBtn,
                binding.includeLeftRightBtn.tvRightBtn);
        clickCenterRightBtn(binding.includeLeftRightBtn.tvRightBtn,
                binding.includeLeftRightBtn.tvCenterBtn,
                binding.includeLeftRightBtn.tvLeftBtn);
        clickCenterBtn(binding.includeLeftRightBtn.tvCenterBtn,
                binding.includeLeftRightBtn.tvRightBtn,
                binding.includeLeftRightBtn.tvLeftBtn);
        /*clickLeftBtn(binding.includeLeftRightBtn.tvLeftBtn,
                binding.includeLeftRightBtn.tvRightBtn);
        clickRightBtn(binding.includeLeftRightBtn.tvRightBtn,
                binding.includeLeftRightBtn.tvLeftBtn);*/
    }

    @Override
    public void onLeftBtnClick(View view) {
        super.onLeftBtnClick(view);
        viewType = VehicleInOutAdapter.TYPE_AVAILABLE;
        binding.linearDateTime.setVisibility(View.VISIBLE);
        if (from_date != null && to_date != null) {
            loadAvailableVehicle();
        } else {
            binding.rvlistData.setVisibility(View.GONE);
            binding.tvError.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onRightBtnClick(View view) {
        super.onRightBtnClick(view);
        viewType = VehicleInOutAdapter.TYPE_RIDEHIS;
        binding.linearDateTime.setVisibility(View.GONE);
        loadBookedOrCompleteList(ApiUtils.CO);
    }

    @Override
    public void onCenterBtnClick(View view) {
        super.onCenterBtnClick(view);
        viewType = VehicleInOutAdapter.TYPE_BOOKED;
        binding.linearDateTime.setVisibility(View.GONE);
        loadBookedOrCompleteList(ApiUtils.BO);

    }

    public void initCalendarData() {
        minToDate.set(Calendar.YEAR, fromYear);
        minToDate.set(Calendar.DAY_OF_MONTH, fromDay);
        minToDate.set(Calendar.MONTH, fromMonth);
        minToDate.set(Calendar.HOUR_OF_DAY, fromHours);
        minToDate.set(Calendar.MINUTE, fromMinute);
    }

    @Override
    public void onClick(View v) {
        initCalendarData();
        switch (v.getId()) {
            case R.id.tvSelectFromDateTime:
                isStartDateTime = true;
                binding.tvSelectToDateTime.setText(getActivity().getResources().getString(R.string.selecttodatetime));
                datePickerDialog = new DatePickerDialog(getContext(), R.style.MyCalendarTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        fromYear = year;
                        fromMonth = monthOfYear;
                        fromDay = dayOfMonth;

                        openTimePicker();
                    }
                }, fromYear, fromMonth, fromDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
                break;
            case R.id.tvSelectToDateTime:
                if (isStartDateTime) {
                    toHours = fromHours;
                    toMinute = fromMinute;

                    datePickerDialog = new DatePickerDialog(getContext(), R.style.MyCalendarTheme, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            toYear = year;
                            toMonth = monthOfYear;
                            toDay = dayOfMonth;

                            openTimePicker();
                        }
                    }, toYear, toMonth, toDay);
                    datePickerDialog.getDatePicker().setMinDate(minToDate.getTimeInMillis());
                    datePickerDialog.show();
                } else {
                    toHours = fromHours;
                    toMinute = fromMinute;

                    datePickerDialog = new DatePickerDialog(getContext(), R.style.MyCalendarTheme, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            toYear = year;
                            toMonth = monthOfYear;
                            toDay = dayOfMonth;

                            openTimePicker();
                        }
                    }, toYear, toMonth, toDay);
                    datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    datePickerDialog.show();
                }
                isStartDateTime = false;

                break;
        }
    }

    private int mHour = 0, mMinute = 0;

    public void openTimePicker() {

        if (isStartDateTime) {
            mHour = fromHours;
            mMinute = fromMinute;
        } else {
            mHour = toHours;
            mMinute = toMinute;
        }
        timePickerDialog = new VehicleTimePickerDialog(getContext(), R.style.MyCalendarTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                if (isStartDateTime) {

                    fromHours = hourOfDay;
                    fromMinute = minute;


                    from_date = fromDay + "/" + (fromMonth + 1) + "/" + fromYear;
                    from_time = AppUtilities.getTime(fromHours, fromMinute);

                    binding.tvSelectFromDateTime.setText(from_date + " " + from_time);

                } else {

                    toHours = hourOfDay;
                    toMinute = minute;

                    to_date = toDay + "/" + (toMonth + 1) + "/" + toYear;
                    to_time = AppUtilities.getTime(toHours, toMinute);

                    binding.tvSelectToDateTime.setText(to_date + " " + to_time);

                    loadAvailableVehicle();


                }
            }
        }, mHour, mMinute, false);
        if (isStartDateTime) {
            timePickerDialog.setMin(mHour, mMinute);
        }
        timePickerDialog.show();
    }


    @Override
    public void onButtonClick(boolean isCancle) {

    }

    @Override
    public void onUpdateClick(int position, String id, boolean isUnBooked) {
        isFromRefresh = true;
        if (isUnBooked) {
            bookedVehicleInOutAdapter.removeItem(position);
            bookUnbookVehicle(id, true);
        } else {
            bookUnbookVehicle(id, false);
        }
    }

    @Override
    public void onItemClick(int position) {
        /*VehicleInOut vehicleInOut = viewModel.getItem(position);
        if (viewType == VehicleInOutAdapter.TYPE_AVAILABLE) {
            ActivityUtils.launchStartActivityForResult(VehiclesFragment.this,
                    BookVehicleActivity.class, Constants.RESULT_CODE_BOOK_VEHICLE, null);
        } else {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.commingsoon));
        }
        showAlertDialogBooked(getActivity(), vehicleId);*/
        AvailableData vehicleData = listData.get(position);
        String vehicleId = listData.get(position).getId();
        String vehicleNo = vehicleData.getBrand() + "-" +vehicleData.getName()+"-"+
                vehicleData.getNumber();
        String fromDate = "";
        String toDate = "";
        try {
            fromDate = AppUtilities.formatDateFromDateString(Constants.ddMMyyyy, Constants.DATE_FORMAT_5, from_date);
            toDate = AppUtilities.formatDateFromDateString(Constants.ddMMyyyy, Constants.DATE_FORMAT_5, to_date);
        }catch (Exception e){
            fromDate = from_date;
            toDate = to_date;
            e.printStackTrace();
        }
        String fDate = fromDate + " " +from_time;
        String tDate = toDate + " " +to_time;

        String message = String.format(getResources().getString(R.string.vehicleBookingmessage),
                fDate, tDate);
        String title = String.format(
                vehicleNo);

        new AlertDialog(getActivity(), this).showBookingAlertDialog(
                title,
                this.getResources().getColor(R.color.clrgreen),
                message, vehicleId,vehicleNo, position);

    }

    @Override
    public void onItemDelete(int position, String id,String number, String from_time, String to_time) {

        isFromRefresh = true;

        String message = String.format(getResources().getString(R.string.vehicleUnbookmessage),
                from_time, to_time);
        String title = String.format(number);
        new AlertDialog(getActivity(), this).showBookingAlertDialog(
                title,
                this.getResources().getColor(R.color.clrred),
                message, id,number, position);


    }

    @Override
    public void onSuccess(Response response) {
        hideProgressDialog();
        AvailableModel parsRes = (AvailableModel) response.body();

        if (parsRes == null) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
            return;
        }

        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(getContext(), parsRes.getError());
        }

        List<AvailableData> listData = parsRes.getData();
        if (listData != null && listData.size() > 0) {

            mHour = 0;
            mMinute = 0;

            binding.tvError.setVisibility(View.GONE);
            binding.rvlistData.setVisibility(View.VISIBLE);

            fillAvailableVehicle(listData);

        } else {
            binding.tvError.setVisibility(View.VISIBLE);
            binding.rvlistData.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        AppUtilities.showToast(getContext(), error);
    }

    public void setInitialFromToDate() {

        Calendar c = Calendar.getInstance();

        fromYear = c.get(Calendar.YEAR);
        fromMonth = c.get(Calendar.MONTH);
        fromDay = c.get(Calendar.DAY_OF_MONTH);

        toYear = c.get(Calendar.YEAR);
        toMonth = c.get(Calendar.MONTH);
        toDay = c.get(Calendar.DAY_OF_MONTH);

        fromHours = c.get(Calendar.HOUR);
        fromMinute = c.get(Calendar.MINUTE);

        String amPMFrom = c.get(Calendar.AM_PM) == Calendar.AM ? "AM" : "PM";

        from_date = fromDay + "/" + (fromMonth + 1) + "/" + fromYear;
        from_time = fromHours + ":" + fromMinute + " " + amPMFrom;

        binding.tvSelectFromDateTime.setText(from_date + " " + from_time);

        c.set(Calendar.HOUR, 11);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.AM_PM, Calendar.PM);

        toHours = c.get(Calendar.HOUR);
        toMinute = c.get(Calendar.MINUTE);

        String amPMto = c.get(Calendar.AM_PM) == Calendar.AM ? "AM" : "PM";

        to_date = toDay + "/" + (toMonth + 1) + "/" + toYear;
        to_time = toHours + ":" + toMinute + " " + amPMto;

        binding.tvSelectToDateTime.setText(to_date + " " + to_time);
    }


    private void loadAvailableVehicle() {

        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }

        String finalDate = from_date + " " + from_time;
        String finalToDate = to_date + " " + to_time;

        if (binding.tvSelectFromDateTime.getText().toString().equalsIgnoreCase(getActivity().getResources().getString(R.string.selectfromdatetime))) {
            //binding.tvSelectToDateTime.setText(getActivity().getResources().getString(R.string.selecttodatetime));
            AppUtilities.showToast(getActivity(), getActivity().getResources().getString(R.string.err_fromdate));
            return;
        } else if (binding.tvSelectToDateTime.getText().toString().equalsIgnoreCase(getActivity().getResources().getString(R.string.selecttodatetime))) {
            AppUtilities.showToast(getActivity(), getActivity().getResources().getString(R.string.err_todate));
            return;
        } else if (compareDates(finalDate, finalToDate)) {
            AppUtilities.showToast(getActivity(), getActivity().getResources().getString(R.string.err_datenotvalid));
            return;
        } else {
            if (!isFromRefresh) {
                showProgressDialog();
            } else {
                isFromRefresh = false;
            }
            HashMap<String, String> params = new HashMap<>();
            params.put(ApiUtils.FROM_DATE, from_date);
            params.put(ApiUtils.FROM_TIME, from_time);
            params.put(ApiUtils.TO_DATE, to_date);
            params.put(ApiUtils.TO_TIME, to_time);
            callService.getAvailableVehicle(params, this);
        }
    }

    private void loadBookedOrCompleteList(String is_from) {

        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        if (!isFromRefresh) {
            showProgressDialog();
        } else {
            isFromRefresh = false;
        }
        HashMap<String, String> params = new HashMap<>();
        if (is_from.equalsIgnoreCase(ApiUtils.BO)) {
            params.put(ApiUtils.STATUS, ApiUtils.BO);
        } else {
            params.put(ApiUtils.STATUS, ApiUtils.CO);
        }
        callService.getBookedVehicle(params, bookedOrCompletedData);
    }


    public static boolean compareDates(String date1, String date2) {
        boolean isEqual = false;
        try {
            String pattern = "dd/MM/yyyy HH:mm:ss";
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");
            DateFormat outputformat = new SimpleDateFormat(pattern);
            Date fDate = null;
            Date eDate = null;
            String foutput = null, eoutput = null;

            fDate = df.parse(date1);
            foutput = outputformat.format(fDate);

            eDate = df.parse(date2);
            eoutput = outputformat.format(eDate);

            Date d1 = outputformat.parse(foutput);
            Date d2 = outputformat.parse(eoutput);
            System.out.println(foutput + " " + eoutput);

            if (d1.equals(d2)) {
                isEqual = true;
            } else if (d1.after(d2)) {
                isEqual = true;
            } else {
                isEqual = false;
            }

        } catch (Exception e1) {
            e1.printStackTrace();
            isEqual = false;
        }
        return isEqual;
    }


    private void bookUnbookVehicle(String id, boolean isUnbook) {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        showProgressDialog();
        HashMap<String, String> params = new HashMap<>();
        if (!isUnbook) {
            isUnBooked = false;
            params.put(ApiUtils.FROM_DATE, from_date);
            params.put(ApiUtils.FROM_TIME, from_time);
            params.put(ApiUtils.TO_DATE, to_date);
            params.put(ApiUtils.TO_TIME, to_time);
            params.put(ApiUtils.VEHICALID, id);
            callService.bookVehicle(params, bookingResponse);
        } else {
            isUnBooked = true;
            params.put(ApiUtils.ID, id);
            callService.unbookVehicle(params, unbookingResponse);
        }


    }

    public ApiResponse bookedOrCompletedData = new ApiResponse() {
        @Override
        public void onSuccess(Response response) {
            hideProgressDialog();

            BookedModel parsRes = (BookedModel) response.body();

            if (parsRes == null) {
                AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
                return;
            }

            if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
                AppUtilities.showToast(getContext(), parsRes.getError());
            }

            List<BookedData> bookedVehicleDataList = parsRes.getData();
            if (bookedVehicleDataList.size() > 0) {
                binding.tvError.setVisibility(View.GONE);
                binding.rvlistData.setVisibility(View.VISIBLE);

                fillBookedVehicle(bookedVehicleDataList);
                //enableSwipeToDelete();
            } else {
                binding.tvError.setVisibility(View.VISIBLE);
                binding.rvlistData.setVisibility(View.GONE);
            }

        }

        @Override
        public void onFailure(String error) {
            hideProgressDialog();
            AppUtilities.showToast(getContext(), error);
        }
    };

    public ApiResponse unbookingResponse = new ApiResponse() {
        @Override
        public void onSuccess(Response response) {
            hideProgressDialog();

            UnBookModel parsRes = (UnBookModel) response.body();

            if (parsRes == null) {
                AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
                return;
            }

            if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
                AppUtilities.showToast(getContext(), parsRes.getError());
            }

            UnBookData bookingVehicleData = parsRes.getData();
            if (bookingVehicleData != null) {
                if (!isUnBooked) {
                    AppUtilities.showToast(getContext(), getResources().getString(R.string.onbook));
                    onLeftBtnClick(binding.includeLeftRightBtn.tvLeftBtn);

                } else {
                    AppUtilities.showToast(getContext(), getResources().getString(R.string.onUnbook));
                    onCenterBtnClick(binding.includeLeftRightBtn.tvCenterBtn);
                }
            }


        }

        @Override
        public void onFailure(String error) {
            hideProgressDialog();
            AppUtilities.showToast(getContext(), error);
        }
    };

    public ApiResponse bookingResponse = new ApiResponse() {
        @Override
        public void onSuccess(Response response) {
            hideProgressDialog();

            CreateBookingModel parsRes = (CreateBookingModel) response.body();

            if (parsRes == null) {
                AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
                return;
            }

            if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
                AppUtilities.showToast(getContext(), parsRes.getError());
            }

            CreateBookingData bookingVehicleData = parsRes.getData();
            if (bookingVehicleData != null) {
                if (!isUnBooked) {
                    AppUtilities.showToast(getContext(), getResources().getString(R.string.onbook));
                    onLeftBtnClick(binding.includeLeftRightBtn.tvLeftBtn);

                } else {
                    AppUtilities.showToast(getContext(), getResources().getString(R.string.onUnbook));
                    onCenterBtnClick(binding.includeLeftRightBtn.tvCenterBtn);
                }
            }


        }

        @Override
        public void onFailure(String error) {
            hideProgressDialog();
            AppUtilities.showToast(getContext(), error);
        }
    };


}


