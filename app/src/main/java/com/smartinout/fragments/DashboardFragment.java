package com.smartinout.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.gson.Gson;
import com.smartinout.BookVehicleActivity;
import com.smartinout.R;
import com.smartinout.ScannerActivity;
import com.smartinout.adapters.ActivityLogsAdapter;
import com.smartinout.adapters.QuickTaskAdapter;
import com.smartinout.base.App;
import com.smartinout.base.BaseFragment;
import com.smartinout.databinding.FragmentDashboardBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.model.ActivityHours;
import com.smartinout.model.dashboard.DashbordVehicleData;
import com.smartinout.model.dashboard.DashbordVehicleModel;
import com.smartinout.model.login.Inoutreference;
import com.smartinout.model.timesheet.Activities;
import com.smartinout.model.timesheet.DailyTimeSheetsPojo;
import com.smartinout.model.timesheet.InoutDatum;
import com.smartinout.model.timesheet.ResponseOfProgressBar;
import com.smartinout.model.timesheet.UserDailyProgressData;
import com.smartinout.model.timesheet.quickpick.QuickPickPojo;
import com.smartinout.model.timesheet.quickpick.QuickTask;
import com.smartinout.model.userinout.ClockedInOutPojo;
import com.smartinout.model.vehicleinout.starttrip.StartTripData;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.ActivityUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;
import com.smartinout.utilities.CustomTypefaceSpan;
import com.smartinout.utilities.Prefs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import retrofit2.Response;

public class DashboardFragment extends BaseFragment implements OnChartValueSelectedListener, View.OnClickListener, QuickTaskAdapter.OnItemSelected, ApiResponse {

    FragmentDashboardBinding binding;
    private QuickTaskAdapter quickTaskAdapter;
    private ActivityLogsAdapter logsAdapter;
    private ActivityHours activityHours;
    private String currentDate = "";
    QuickTaskAdapter.OnItemSelected listner;
    String mainStatus, secondryStatus;
    String quickStatus = "", quickStatusName = "";
    private boolean isTokenExpired = false;
    Prefs pref;
    private String hhmmaTime;
    private String clockTime;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.fragment_dashboard, container, false);
        pref = Prefs.getInstance();
        currentDate = AppUtilities.getCurrentDateDDMMYYYY();
        listner = this;
        initiateChart();
        /*loadfromLocal();*/

        initViews();

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        Gson gson = new Gson();
        String json = pref.getString(Prefs.START_TRIP_DATA,"");
        if(json != null && json.length() > 0){
            StartTripData tripData = gson.fromJson(json, StartTripData.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.FROM, Constants.STATUS_END_TRIP);
            bundle.putSerializable(Constants.DATA, tripData);
            ActivityUtils.launchActivity(getActivity(), BookVehicleActivity.class, true, bundle);
        }else{
            loadfromLocal();
        }
    }

    private void getuserLivestatus() {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        showProgressDialog();
        callService.dailyTimesheet(currentDate, liveInOut);
    }

    public void loadfromLocal() {
        List<Activities> liveInOut = inOutDBCtrl.getDailyTimeSheet(currentDate);
        if (liveInOut != null && liveInOut.get(0).getUsersInOut() != null) {
            setInOutData(liveInOut, true);
        } else {
            if (!AppUtilities.hasInternet(getContext(), true)) {
                manageBtns(ApiUtils.COUT, ApiUtils.OUT);
                setData();
                return;
            }
            if (!isTokenExpired) {
                getuserLivestatus();
            }
        }
    }

    public ApiResponse liveInOut = new ApiResponse() {
        @Override
        public void onSuccess(Response response) {
            hideProgressDialog();
            isTokenExpired = false;
            isVehicleBooked();

            DailyTimeSheetsPojo userData = (DailyTimeSheetsPojo) response.body();
            UserDailyProgressData parsRes = userData.getDailyProgressData();

            if (parsRes == null) {
                AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
                return;
            }

            if (!AppUtilities.getValidateString(userData.getError()).isEmpty()) {
                AppUtilities.showToast(getContext(), userData.getError());
            }

            List<Activities> activitiesList = parsRes.getUserTimesheet().getActivities();

            List<InoutDatum> listProgressbar = new ArrayList<>();

            if (activitiesList != null && activitiesList.size() > 0) {

                Activities activities = activitiesList.get(0);

                if(activities.getUsersInOut() == null){
                    pref.save(Prefs.ISCLOKCEDIN,false);
                }

                long totalActiveMinute = 0;

                if (parsRes.getResponseOfProgressBar() != null && parsRes.getResponseOfProgressBar().size() > 0) {
                    ResponseOfProgressBar progressBar = parsRes.getResponseOfProgressBar().get(0);
                    listProgressbar = progressBar.getInoutData();
                    totalActiveMinute = progressBar.getTotalActiveMinutes();
                }

                List<Inoutreference> inoutreferenceList = activitiesList.get(0).getUsersData();

                if ((inoutreferenceList == null || inoutreferenceList.size() == 0) && activitiesList.get(0).getUsersInOut() != null) {
                    inoutreferenceList.add(activitiesList.get(0).getUsersInOut());
                    activities.setUsersData(inoutreferenceList);
                    activitiesList.set(0, activities);
                }

                for (int i=0;i<activitiesList.get(0).getUsersData().size();i++)
                {
                    if (activitiesList.get(0).getUsersData().get(i).isClockedIn())
                    {
                        hhmmaTime = ""+AppUtilities.getTime(activitiesList.get(0).getUsersData().get(i).getStartTime());
//                        binding.timechart.setCenterText(generateCenterSpannableText());


                        break;
                    }
                }

                if (activitiesList.get(0).getUsersInOut() != null) {


                    //Save Daily time sheet to local database
                    inOutDBCtrl
                            .saveDailyTimeSheet(currentDate, activitiesList, totalActiveMinute, listProgressbar);
                    setInOutData(activitiesList, false);

                } else {
                    manageBtns(ApiUtils.COUT, ApiUtils.OUT);
                    setData();
                }

            } else {
                manageBtns(ApiUtils.COUT, ApiUtils.OUT);
                setData();
            }
        }

        @Override
        public void onFailure(String error) {
            hideProgressDialog();
            isVehicleBooked();
            AppUtilities.showToast(getContext(), error);
            isTokenExpired = true;
            loadfromLocal();

        }
    };

    public ApiResponse getQuickPicksRes = new ApiResponse() {
        @Override
        public void onSuccess(Response response) {

            QuickPickPojo quickData = (QuickPickPojo) response.body();


            if (quickData == null) {
                AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
                return;
            }

            if (!AppUtilities.getValidateString(quickData.getError()).isEmpty()) {
                AppUtilities.showToast(getContext(), quickData.getError());
            }

            List<QuickTask> quickTaskList = quickData.getQuickPickDatat();
            App.setQuickTaskListIn(quickTaskList);

            if (App.getQuickTaskListIn().size() > 0) {
                binding.rvQuickTask.
                        setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                //binding.rvQuickTask.setVisibility(View.VISIBLE);
                quickTaskAdapter = new QuickTaskAdapter(getContext(), App.getQuickTaskListIn(), listner);//AppUtilities.getQuickTask(getContext()));
                binding.rvQuickTask.setAdapter(quickTaskAdapter);
            }


        }

        @Override
        public void onFailure(String error) {
            hideProgressDialog();
            loadfromLocal();
            AppUtilities.showToast(getContext(), error);
        }
    };

    public ApiResponse quickInOut = new ApiResponse() {
        @Override
        public void onSuccess(Response response) {
            hideProgressDialog();
            getuserLivestatus();
        }

        @Override
        public void onFailure(String error) {
            hideProgressDialog();
            loadfromLocal();
            AppUtilities.showToast(getContext(), error);
        }
    };


    boolean isAnimated = false;

    public void setInOutData(List<Activities> activitiesList, boolean isFromLoacal) {
        List<Inoutreference> listInOuts = activitiesList.get(0).getUsersData();

        Inoutreference userInOut = activitiesList.get(0).getUsersInOut();
        Inoutreference startInOut = listInOuts.get(0);
        Inoutreference lastInOut = listInOuts.get(listInOuts.size() - 1);

        long todayinTime = lastInOut.getStartTime();

        long todaylastactTime = lastInOut.getCreatedInoutTime();
        String lastMainStatus = lastInOut.getMainStatus();
        String lastSecondStatus = lastInOut.getSecondryStatus();
        String comment = "";
//        String comment = lastInOut.getComment();
        String quickStatusName = userInOut.getQuickpick_name();

        if (lastMainStatus.equalsIgnoreCase(ApiUtils.CIN)) {
            if (!isTokenExpired) {
                getQuickPicks();
            }
        }

        activityHours = AppUtilities
                .timesheetCalculation(activitiesList.get(0).getUsersData());
        AppUtilities.setStatusTitle(getContext(), binding.tvStatus,
                lastMainStatus,
                lastSecondStatus,
                quickStatusName,comment,
                todayinTime,
                todaylastactTime);


        manageBtns(lastMainStatus, lastSecondStatus);
        isAnimated = isFromLoacal;
        setData();
        if (isFromLoacal) {
            if (!AppUtilities.hasInternet(getContext(), true)) {
                return;
            }
            if (!isTokenExpired) {
                getuserLivestatus();
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linearClockedIn:
                String tag = v.getTag().toString();
                if (tag.equals(TAG_CLOCKEDIN)) {//CIN
                    clockedIn();
                } else if (tag.equals(TAG_BREAKEND)) {//Break Out
                    breakEnd();
                }


                break;
            case R.id.linearBreakIn:// Break In
                breakIn();
                break;
            case R.id.linearClockedOff:// Clock Off
                clockedOff();
                break;

            case R.id.tvGO:
                Intent i = new Intent(getContext(), ScannerActivity.class);
                i.putExtra("fromGo","someValue");
                this.startActivity(i);
                break;
        }
    }

    private void clockedIn() {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        showProgressDialog();
        callService.clockedIn(this);
    }

    private void breakIn() {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        showProgressDialog();
        callService.breakIn(this);
    }

    private void breakEnd() {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        showProgressDialog();
        callService.breakEnd(this);
    }

    private void clockedOff() {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        showProgressDialog();
        callService.clockedOut(this);
    }

    private void updateQuickPickIn(String quikId,String quickPickStatus) {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        showProgressDialog();
        callService.updateQuickPickIn(quickInOut, quikId,quickPickStatus);
    }

    private void updateQuickPickOut(String quikId,String quickPickStatus) {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        showProgressDialog();
        callService.updateQuickPickOut(quickInOut, quikId,quickPickStatus);
    }

    //Himadri
    private void getQuickPicks() {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        callService.getQuickPicks(getQuickPicksRes);
    }

    private void initViews() {

        binding.rvActivityLogs.setFocusable(false);
        binding.rvQuickTask.setFocusable(false);

        // Set UserData Quick Task
        binding.rvQuickTask.
                setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        if (App.getQuickTaskListIn() != null) {
            if (App.getQuickTaskListIn().size() > 0) {
                quickTaskAdapter = new QuickTaskAdapter(getContext(), App.getQuickTaskListIn(), listner);//AppUtilities.getQuickTask(getContext()));
                binding.rvQuickTask.setAdapter(quickTaskAdapter);
            }
        } else {
            getQuickPicks();
        }

        binding.layoutTimeBoard.linearClockedIn.setOnClickListener(this);
        binding.layoutTimeBoard.linearBreakIn.setOnClickListener(this);
        binding.layoutTimeBoard.linearClockedOff.setOnClickListener(this);
        binding.layoutTimeBoard.linearClockedOff.setOnClickListener(this);
        binding.includeBookedVehicle.tvGO.setOnClickListener(this);


    }

    public void initiateChart() {

        binding.timechart.setUsePercentValues(true);
        binding.timechart.getDescription().setEnabled(false);
        binding.timechart.setExtraOffsets(15, 5, 5, 5);

        binding.timechart.setDragDecelerationFrictionCoef(0.95f);

        binding.timechart.setCenterText(generateCenterSpannableText(Constants.INOUTHRS,Constants.INOUTHRSTEMP));

        binding.timechart.setDrawHoleEnabled(true);
        binding.timechart.setHoleColor(Color.WHITE);

        binding.timechart.setTransparentCircleColor(Color.WHITE);
        binding.timechart.setTransparentCircleAlpha(110);

        binding.timechart.setHoleRadius(65f);
        binding.timechart.setTransparentCircleRadius(65f);

        binding.timechart.setDrawCenterText(true);

        binding.timechart.setRotationAngle(270);
        // enable rotation of the binding.timechart by touch
        binding.timechart.setRotationEnabled(false);
        binding.timechart.setHighlightPerTapEnabled(false);

        // add a selection listener
        binding.timechart.setOnChartValueSelectedListener(this);

        Legend l = binding.timechart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        binding.timechart.setEntryLabelvisibility(false);
        binding.timechart.setDrawValuevisibility(false);
        binding.timechart.setDrawRoundedSlices(true);
        binding.timechart.setRenderLegend(false);
    }

    public void setActivitiesData(List<InoutDatum> listData) {
        // Set UserData for Activity logs
        binding.rvActivityLogs.setLayoutManager(new LinearLayoutManager(getContext()));
        logsAdapter = new ActivityLogsAdapter(getContext(), listData);
        binding.rvActivityLogs.setAdapter(logsAdapter);
    }

    private void setData() {

        String[] parties = AppUtilities.parties;

        List<InoutDatum> listData = inOutDBCtrl.getDailyTimeSheetProgressData(currentDate);
        long activeMinutes = inOutDBCtrl.getTotalActiveMinute(currentDate);

        ArrayList<PieEntry> entries = new ArrayList<>();
        ArrayList<Integer> colors = new ArrayList<>();

        if (listData != null && listData.size() > 0) {

            binding.linearActivitylog.setVisibility(View.VISIBLE);
            setActivitiesData(listData);

            long totalMinutes = 0;
            long currentInHrs = 0;
            long totalInHrs = 0;
            long totalBreakHrs = 0;
            int totlaHrs = 100;

            for (int i = 0; i < listData.size(); i++) {
                InoutDatum datum = listData.get(i);
                totalMinutes = totalMinutes + datum.getMinutes();
                int color = AppUtilities.getColorofProgress(getContext(), datum.getColor());
                colors.add(color);
                if (datum.getColor().equals(AppUtilities.CLR_GREEN)) {
                    currentInHrs = datum.getMinutes();
                    totalInHrs = totalInHrs + datum.getMinutes();
                } else if (datum.getColor().equals(AppUtilities.CLR_RED)) {
                    totalBreakHrs = totalBreakHrs + datum.getMinutes();
                }
                int percentage = Math.round(datum.getPercentage());
                totlaHrs = totlaHrs - percentage;
                entries.add(new PieEntry(percentage, parties[0 % parties.length]));
            }

            if (totlaHrs > 0) {
                colors.add(getResources().getColor(R.color.grayprogress));
                entries.add(new PieEntry(totlaHrs, parties[0 % parties.length]));
            }

            String inoutHrs = AppUtilities.hmTimeFormatterfromMinutes(activeMinutes);
            activityHours.setTotalHrsmns(inoutHrs);
            activityHours.setCurrentInHours(currentInHrs * 60000);
            activityHours.setTotlaInHours(totalInHrs * 60000);
            activityHours.setTotlaBreakHours(totalBreakHrs * 60000);



            setTimesheetCounter();

        } else {

            binding.linearActivitylog.setVisibility(View.GONE);

            binding.layoutTimeBoard.tvtimesheetCounter.setText(getResources().getString(R.string.hhmm));
            binding.layoutTimeBoard.tvCurrentDate.setText(AppUtilities.getCurrentDateEEEEddMMMM());

            //binding.tvStatus.setText(getResources().getString(R.string.cloclkedoff));
            colors.add(getResources().getColor(R.color.grayprogress));
            entries.add(new PieEntry(100, parties[0 % parties.length]));

        }

        PieDataSet dataSet = new PieDataSet(entries, getResources().getString(R.string.strInout));

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(0f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setColors(colors);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(binding.timechart));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        //data.setValueTypeface(tfLight);
        binding.timechart.setData(data);
        // undo all highlights
        binding.timechart.highlightValues(null);
        binding.timechart.invalidate();
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

    private SpannableString generateCenterSpannableText(String inoutHrs, String newTime) {

        Typeface typefaceBold = ResourcesCompat.getFont(getContext(), R.font.opensans_bold);

        String today = getResources().getString(R.string.today);

        String currentDate = AppUtilities.getCurrentDateddMMMYYYY();


        String complete = inoutHrs + "\n" + today +"\n" + newTime + "\n" + currentDate;

        SpannableString s = new SpannableString(complete);

        s.setSpan(new ForegroundColorSpan(Color.BLACK), 0, inoutHrs.length(), 0);//Common black color for all exclude date
        s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.clrPieChartDate))
                , inoutHrs.length() + today.length() + 2, s.length(), 0);//Date color
        s.setSpan(new CustomTypefaceSpan("", typefaceBold), 0, s.length(), 0);// font type face

        s.setSpan(new RelativeSizeSpan(2f), 0, inoutHrs.length(), 0);// Today font size
        s.setSpan(new RelativeSizeSpan(1f), inoutHrs.length(), today.length() + inoutHrs.length() + 1, 0);// Time font size
        s.setSpan(new RelativeSizeSpan(1f), inoutHrs.length() + today.length() + 1, s.length(), 0);// Date font size

        return s;
    }

    public void clockStart() {
        binding.layoutTimeBoard.linearClockedIn.setVisibility(View.GONE);
        binding.layoutTimeBoard.linearStartBreakClockedOff.setVisibility(View.VISIBLE);

        Animation aniSlide = AnimationUtils.loadAnimation(getContext(), R.anim.zoom_in);

        binding.layoutTimeBoard.imgBreakIn.startAnimation(aniSlide);
        binding.layoutTimeBoard.imgClockedOff.startAnimation(aniSlide);
    }

    public void animateCenterClockedBtn() {

        if (isAnimated) {
            return;
        }

        binding.layoutTimeBoard.linearClockedIn.setVisibility(View.VISIBLE);
        binding.layoutTimeBoard.linearStartBreakClockedOff.setVisibility(View.GONE);

        Animation aniSlide = AnimationUtils.loadAnimation(getContext(), R.anim.zoom_in);

        binding.layoutTimeBoard.imgClockedIn.startAnimation(aniSlide);
    }

    @Override
    public void onSuccess(Response response) {

        ClockedInOutPojo parsRes = (ClockedInOutPojo) response.body();

        if (parsRes == null) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
            return;
        }

        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(getContext(), parsRes.getError());
        }


        getuserLivestatus();
    }

    public void manageBtns(String statusMain, String statusSecondary) {
        mainStatus = statusMain;
        secondryStatus = statusSecondary;
        App.setInOutMainStatus(mainStatus);

        if (statusMain.equals(ApiUtils.CIN)) {// Main Status Clocked In
            if (statusSecondary.equals(ApiUtils.BI)) {// Break In
                binding.rvQuickTask.setVisibility(View.GONE);
                mngStartEndBreakBtn(TAG_BREAKEND);
            } else if (statusSecondary.equals(ApiUtils.OUT)) {// Out
                binding.rvQuickTask.setVisibility(View.GONE);
                clockStart();
            } else if (statusSecondary.equals(ApiUtils.COUT)) {// Clocked Out
                binding.rvQuickTask.setVisibility(View.GONE);
                mngStartEndBreakBtn(TAG_CLOCKEDIN);
            } else { // Clocked In
                binding.rvQuickTask.setVisibility(View.VISIBLE);
                clockStart();
            }
        } else if (statusMain.equals(ApiUtils.COUT)) {// Main Status Clocked Out
            binding.rvQuickTask.setVisibility(View.GONE);
            mngStartEndBreakBtn(TAG_CLOCKEDIN);
        }

    }

    public static final String TAG_CLOCKEDIN = "clockedin";
    public static final String TAG_BREAKEND = "breakend";

    public void mngStartEndBreakBtn(String tag) {

        binding.layoutTimeBoard.linearClockedIn.setVisibility(View.VISIBLE);
        binding.layoutTimeBoard.linearStartBreakClockedOff.setVisibility(View.GONE);

        binding.layoutTimeBoard.linearClockedIn.setTag(tag);

        if (tag.equals(TAG_CLOCKEDIN)) {
            binding.layoutTimeBoard.imgClockedIn.setImageResource(R.drawable.ic_clockedin);
            binding.layoutTimeBoard.tvCenterBtnlbl.setTextSize(18);
            binding.layoutTimeBoard.tvCenterBtnlbl.setText(getResources().getString(R.string.start));
            binding.layoutTimeBoard.tvCenterBtnlbl.setTextColor(getResources().getColor(R.color.colorPrimary));

        }

        if (tag.equals(TAG_BREAKEND)) {
            binding.layoutTimeBoard.imgClockedIn.setImageResource(R.drawable.ic_end_break);
            binding.layoutTimeBoard.tvCenterBtnlbl.setText(getResources().getString(R.string.endbreak));
            binding.layoutTimeBoard.tvCenterBtnlbl.setTextColor(getResources().getColor(R.color.clrBlack));
            binding.layoutTimeBoard.tvCenterBtnlbl.setTextSize(18);
        }

        animateCenterClockedBtn();
    }

    public void setTimesheetCounter() {

        binding.layoutTimeBoard.tvCurrentDate.setText(AppUtilities.getCurrentDateEEEEddMMMM());

        if (activityHours == null) {
            binding.layoutTimeBoard.tvtimesheetCounter.setText(getResources().getString(R.string.hhmm));
            return;
        }

        String inoutHrs = Constants.INOUTHRSTEMP;
        if (activityHours != null && activityHours.getTotalHrsmns() != null) {
            inoutHrs = activityHours.getTotalHrsmns();
        }
//        for (int i=0;i<l)
        refreshChartCenterText(inoutHrs);

        if (activityHours.getLastmainStatus().equals(ApiUtils.COUT)) {
            binding.layoutTimeBoard.tvtimesheetCounter.setText(getResources().getString(R.string.hhmm));
            return;
        }

        if (activityHours.getLastStatus().equals(ApiUtils.BI)) {
            startTimer(activityHours.getTotlaBreakHours());
        } else if (activityHours.getLastStatus().equals(ApiUtils.IN)
                || activityHours.getLastStatus().equals(ApiUtils.OUT)) {
            startTimer(activityHours.getCurrentInHours());
            /*startTimer(activityHours.getTotlaInHours());*/
        } else {
            binding.layoutTimeBoard.tvtimesheetCounter.setText(getResources().getString(R.string.hhmm));
        }
    }

    private void refreshChartCenterText(String text) {
        binding.timechart.setCenterText(generateCenterSpannableText(hhmmaTime,text));
        binding.timechart.invalidate();
    }

    private CountDownTimer countDownTimer;
    private CountDownTimer chartCountDownTimer;

    public void startChartTimer(long totalHrs) {
        if (chartCountDownTimer != null) {
            chartCountDownTimer.cancel();
            chartCountDownTimer = null;
        }
        chartCountDownTimer = new CountDownTimer(Long.MAX_VALUE - totalHrs, 60000) {
            @Override
            public void onTick(long l) {
                // Update Chart Center Text
                String time0H0M = AppUtilities.getTime(Long.MAX_VALUE - l);
                refreshChartCenterText(time0H0M);
            }

            @Override
            public void onFinish() {
            }
        }.start();
    }

    public void startTimer(final long max_value) {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
        countDownTimer = new CountDownTimer(Long.MAX_VALUE - max_value, 60000) {
            @Override
            public void onTick(long l) {
                // Update Time sheet Counter Clock
                String timeMH = AppUtilities.hmTimeFormatter(Long.MAX_VALUE - l);
                binding.layoutTimeBoard.tvtimesheetCounter.setText(timeMH);
            }

            @Override
            public void onFinish() {
            }
        }.start();
    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        AppUtilities.showToast(getContext(), error);
    }

    @Override
    public void getItemSelected(QuickTask data) {

        String quickId = data.getId();
        quickStatus = data.getStatus();
        quickStatusName = data.getName();
        App.setQuickStatuss(quickStatus);

        if (mainStatus.equals(ApiUtils.CIN)) {// Main Status Clocked In
            if (!secondryStatus.equals(ApiUtils.BI)
                    && !secondryStatus.equals(ApiUtils.OUT)
                    && !secondryStatus.equals(ApiUtils.COUT)) {// Clocked Out/Out/Break In
                if (quickStatus.equals(ApiUtils.IN)) {
                    updateQuickPickIn(data.getId(),data.getStatus());
                } else {
                    updateQuickPickOut(data.getId(),data.getStatus());
                }
            }
        }/* else if (mainStatus.equals(ApiUtils.COUT)) {// Main Status Clocked Out
            Toast.makeText(getActivity(), "Clocked Out", Toast.LENGTH_SHORT).show();
        }*/
    }

    private void isVehicleBooked() {

        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }

        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.STATUS, ApiUtils.BO);
        params.put(ApiUtils.DASHBOARD, "true");

        callService.bookedDataDashboard(params, bookedData);
    }
    //5df0a02b32b14307dbe71cf1





    public ApiResponse bookedData = new ApiResponse() {
        @Override
        public void onSuccess(Response response) {
            hideProgressDialog();

            DashbordVehicleModel parsRes = (DashbordVehicleModel) response.body();

            if (parsRes == null) {
                AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
                return;
            }

            if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
                AppUtilities.showToast(getContext(), parsRes.getError());
            }

            DashbordVehicleData bookedVehicleDataList = parsRes.getData();
            if (bookedVehicleDataList != null) {

                binding.includeBookedVehicle.getRoot().setVisibility(View.VISIBLE);

                String date = bookedVehicleDataList.getFromdate();
                String time = bookedVehicleDataList.getFromtime();
                String vNumber = bookedVehicleDataList.getVehicalId().getBrand()+"-"+bookedVehicleDataList.getVehicalId().getName()+"-"+bookedVehicleDataList.getVehicalId().getNumber();

                binding.includeBookedVehicle.date.setText(getActivity().getResources().getString(R.string.today) + " : " + time);
                binding.includeBookedVehicle.vehicleId.setText(getActivity().getResources().getString(R.string.makeId) + " "+ vNumber);


            } else {
                binding.includeBookedVehicle.getRoot().setVisibility(View.GONE);
            }

        }

        @Override
        public void onFailure(String error) {
            hideProgressDialog();
            AppUtilities.showToast(getContext(), error);
        }
    };


}
