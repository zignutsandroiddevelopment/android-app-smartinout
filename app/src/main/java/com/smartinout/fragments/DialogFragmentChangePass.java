package com.smartinout.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.LoginActivity;
import com.smartinout.R;
import com.smartinout.base.BaseDialogFragment;
import com.smartinout.databinding.FragmentChangepassBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.model.BaseResponse;
import com.smartinout.model.timesheet.DailyTimeSheetsPojo;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.ActivityUtils;
import com.smartinout.utilities.AppUtilities;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import retrofit2.Response;

public class DialogFragmentChangePass extends BaseDialogFragment implements View.OnClickListener, ApiResponse {

    FragmentChangepassBinding binding;

    @Override
    public int getTheme() {
        return R.style.AppTheme_NoActionBar_FullScreenDialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.fragment_changepass, container, false);
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.layoutToolBar.tvTitle.setText(
                getResources().getString(R.string.changepassword));
        binding.layoutToolBar.ivBack.setVisibility(View.VISIBLE);
        binding.includeWidgetBtn.tvBtnTitle.setText(getResources().getString(R.string.submit));
        binding.layoutToolBar.ivBack.setOnClickListener(this);
        binding.includeWidgetBtn.tvBtnTitle.setOnClickListener(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:
                dismiss();
                break;
            case R.id.tvBtnTitle:
                submitData();
                break;
        }
    }

    public void submitData() {

        String oldPwd = binding.edtOldPass.getText().toString();
        String newPwd = binding.edtNewPass.getText().toString();
        String conNewPwd = binding.edtConNewPassword.getText().toString();

        if (AppUtilities.getValidateString(oldPwd).isEmpty()) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.enteroldpass));
            return;
        }


        /*if (oldPwd.length() < 6) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.pwdshouldgt6));
            return;
        }*/

        if (AppUtilities.getValidateString(newPwd).isEmpty()) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.enternewpass));
            return;
        }

        /*if (newPwd.length() < 6) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.pwdshouldgt6));
            return;
        }*/

        if (!AppUtilities.isValidPassword(newPwd)) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.pwdshouldcontainlater));
            return;
        }


        if (AppUtilities.getValidateString(conNewPwd).isEmpty()) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.enterconfnewpass));
            return;
        }

        if (!newPwd.equals(conNewPwd)) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.confnewpwdnotmatch));
            return;
        }

        showProgressDialog();
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.OLDPWD, oldPwd);
        params.put(ApiUtils.NEWPWD, newPwd);
        callService.changePwd(params, this);
    }



    @Override
    public void onSuccess(Response response) {
        hideProgressDialog();

        BaseResponse parsRes = (BaseResponse) response.body();

        if (parsRes == null) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
            return;
        }

        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(getContext(), parsRes.getError());
        }

        //dismiss();
        pref.clearAll();
        inOutDBCtrl.clearAll();
        ActivityUtils.launchActivityWithClearBackStack(getContext(), LoginActivity.class);
    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        AppUtilities.showToast(getContext(), error);
    }
}
