package com.smartinout.fragments.bookvehicle;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.HomeActivity;
import com.smartinout.ProfileActivity;
import com.smartinout.R;
import com.smartinout.base.BaseFragment;
import com.smartinout.databinding.FragmentReportfaultBinding;
import com.smartinout.imagepicker.FilePickUtils;
import com.smartinout.imageuploadtos3.ImageUploadServer;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.model.faultreport.FaultReportModel;
import com.smartinout.model.fuel.FuelRecieptModel;
import com.smartinout.model.vehicleinout.starttrip.StartTripData;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.ActivityUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;
import com.smartinout.utilities.ImageDisplayUitls;
import com.smartinout.utilities.Prefs;

import java.io.File;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import retrofit2.Response;

public class FragmentReportFault extends BaseFragment implements View.OnClickListener, ApiResponse, FilePickUtils.OnFileChoose, ImageUploadServer.SetFileUpload {

    private FragmentReportfaultBinding binding;
    private NavController navController;
    private Bundle bundle;
    private Prefs pref;
    private String vehicleId, bookingId;
    private String image = "";
    private ImageUploadServer imageUploadServer;
    private File imgFile;
    private StartTripData tripData;
    private String isFrom;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_reportfault, container, false);
        navController = Navigation.findNavController(getActivity(), R.id.navhost_frag_bookvehicle);
        pref = Prefs.getInstance();
        pref.save(Prefs.FAULT_TYPE,"");
        bundle = getArguments();
        if (bundle != null) {
            isFrom = bundle.getString(Constants.FROM);
            if (isFrom.equalsIgnoreCase(Constants.STATUS_START_TRIP)) {
                vehicleId = bundle.getString(Constants.DATA);
                bookingId = bundle.getString(Constants.BOOKID);
            }else{
                tripData = (StartTripData) bundle.getSerializable(Constants.DATA);
                vehicleId = tripData.getVehicalData().getId();
                bookingId = tripData.getVehicalBookingId();
            }

        }


        imageUploadServer = new ImageUploadServer(getActivity()
                , Constants.AMAZONE_SECRET
                , Constants.AMAZONE_KEY
                , this);
        imageUploadServer.initUploadFileSrvice();

        initUI();
        return binding.getRoot();
    }

    private void initUI() {
        binding.includeWidgetBtn.tvBtnCancel.setText(getResources().getString(R.string.cancel));
        binding.includeWidgetBtn.tvBtnSubmit.setText(getResources().getString(R.string.submit));
        binding.includeWidgetBtn.tvBtnCancel.setOnClickListener(this);
        binding.includeWidgetBtn.tvBtnSubmit.setOnClickListener(this);
        binding.imgFault.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        final NavController navController
                = Navigation.findNavController(getActivity(), R.id.navhost_frag_bookvehicle);

        switch (v.getId()) {
            case R.id.imgFault:
                showImagePickerDialog(this);
                break;
            case R.id.tvBtnCancel:
                pref.save(Prefs.FAULT_REPORTED,"");
                navController.navigateUp();
                break;
            case R.id.tvBtnSubmit:

                String faultType = binding.edtFaultType.getText().toString();
                String comment = binding.edtFaultComm.getText().toString();

                uploadFaultReport(vehicleId, bookingId, faultType, comment, image);
                break;
        }
    }

    @Override
    public void showImagePickerDialog(FilePickUtils.OnFileChoose onFileChoose) {
        super.showImagePickerDialog(onFileChoose);
    }

    private void uploadFaultReport(String id, String bookingid, String faultType, String comment, String image) {

        if (faultType.length() == 0) {
            AppUtilities.showToast(getActivity(), getActivity().getString(R.string.err_faulttype));
            return;
        }
        if (comment.length() == 0) {
            AppUtilities.showToast(getActivity(), getActivity().getString(R.string.err_comment));
            return;
        }


        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }

        showProgressDialog();
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.VEHICALID, id);
        params.put(ApiUtils.BOOKINGID, bookingid);
        params.put(ApiUtils.FAULT_TYPE, faultType);
        params.put(ApiUtils.COMMENT, comment);
        params.put(ApiUtils.IMAGE, image);
        params.put(ApiUtils.BOOKINGID, bookingId);

        callService.faultReport(params, this);
    }

    @Override
    public void onSuccess(Response response) {
        hideProgressDialog();
        FaultReportModel parsRes = (FaultReportModel) response.body();

        if (parsRes == null) {
            AppUtilities.showToast(getActivity(), getResources().getString(R.string.invalidresposne));
            return;
        }

        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(getActivity(), parsRes.getError());
        }




        AppUtilities.showToast(getActivity(), getActivity().getResources().getString(R.string.fault_confirmation));

        if(isFrom.equalsIgnoreCase(Constants.STATUS_START_TRIP)) {
            ActivityUtils.launchActivity(getActivity(), HomeActivity.class);
        }else {
            pref.save(Prefs.FAULT_REPORTED,parsRes.getData().getId());
            pref.save(Prefs.FAULT_TYPE,binding.edtFaultType.getText().toString());
            navController.navigate(R.id.fragment_stoptrip, bundle);
        }
    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        pref.save(Prefs.FAULT_REPORTED,"");
        AppUtilities.showToast(getActivity(), error);
    }

    @Override
    public void onFileChoose(String fileUri, int requestCode) {
        imgFile = new File(fileUri + "");
        if (imgFile.exists()) {
            ImageDisplayUitls.displayImage(fileUri, getActivity(), binding.imgFault, R.drawable.ic_placeholder);
            imageUploadServer.uploadFileOnS3(imgFile.getPath(), Constants.S3_PROFILE_BUCKET,
                    false);
        }



    }

    @Override
    public void onFileUploadSuccess(String fileName) {
        image = fileName;
    }

    @Override
    public void onFileUploadFail(String error) {

    }

    @Override
    public void onFileUploadProgress(float progress) {

    }


}
