package com.smartinout.fragments.bookvehicle;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.R;
import com.smartinout.base.BaseFragment;
import com.smartinout.databinding.FragmentFuelreceiptBinding;
import com.smartinout.imagepicker.FilePickUtils;
import com.smartinout.imageuploadtos3.ImageUploadServer;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.model.fuel.FuelRecieptModel;
import com.smartinout.model.vehicleinout.starttrip.StartTripData;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;
import com.smartinout.utilities.ImageDisplayUitls;
import com.smartinout.utilities.Prefs;

import java.io.File;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import retrofit2.Response;

public class FragmentFuelReceipt extends BaseFragment implements View.OnClickListener,ApiResponse,FilePickUtils.OnFileChoose,ImageUploadServer.SetFileUpload {

    FragmentFuelreceiptBinding binding;
    NavController navController;
    Prefs pref;
    String image = "";
    Bundle bundle;
    String vehicleId, bookingId;
    ImageUploadServer imageUploadServer;
    File imgFile;
    StartTripData tripData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_fuelreceipt, container, false);
        navController = Navigation.findNavController(getActivity(), R.id.navhost_frag_bookvehicle);
        pref = Prefs.getInstance();
        pref.save(Prefs.FUEL_RECIEPT_NO,"");
        initUI();

        imageUploadServer = new ImageUploadServer(getActivity()
                , Constants.AMAZONE_SECRET
                , Constants.AMAZONE_KEY
                , this);
        imageUploadServer.initUploadFileSrvice();

        bundle = getArguments();
        if (bundle != null) {
            tripData = (StartTripData) bundle.getSerializable(Constants.DATA);
        }
        return binding.getRoot();
    }

    private void initUI() {
        binding.includeWidgetBtn.tvBtnCancel.setText(getResources().getString(R.string.cancel));
        binding.includeWidgetBtn.tvBtnSubmit.setText(getResources().getString(R.string.submit));
        binding.includeWidgetBtn.tvBtnCancel.setOnClickListener(this);
        binding.includeWidgetBtn.tvBtnSubmit.setOnClickListener(this);
        binding.imgReciept.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgReciept:
                showImagePickerDialog(this);
                break;
            case R.id.tvBtnCancel:
                pref.save(Prefs.FUEL_RECIEPT,"");
                navController.navigateUp();
                break;
            case R.id.tvBtnSubmit:
                String id = tripData.getVehicalData().getId();
                String amount = binding.edtAmount.getText().toString();
                String receiptNo = binding.edtReceiptNo.getText().toString();
                String quantity = binding.edtQuantity.getText().toString();

                uploadFuelReciept(id,amount,receiptNo,quantity,image);
                break;
        }
    }

    private void uploadFuelReciept(String id,String amount,String receiptNo,String quantity,String image) {

        if (receiptNo.length() == 0) {
            AppUtilities.showToast(getActivity(),getActivity().getString(R.string.err_recieptno));
            return;
        }
        if (amount.length() == 0) {
            AppUtilities.showToast(getActivity(),getActivity().getString(R.string.err_amount));
            return;
        }
        if (quantity.length() == 0) {
            AppUtilities.showToast(getActivity(),getActivity().getString(R.string.err_quntity));
            return;
        }

        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }

        showProgressDialog();
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.VEHICALID, id);
        params.put(ApiUtils.AMOUNT, amount);
        params.put(ApiUtils.RECEIPT_NO, receiptNo);
        params.put(ApiUtils.QUANTITY, quantity);
        params.put(ApiUtils.RECEIPTIMAGE, image);

        callService.fuelReciept(params, this);
    }

    @Override
    public void onSuccess(Response response) {
        hideProgressDialog();
        FuelRecieptModel parsRes = (FuelRecieptModel) response.body();

        if (parsRes == null) {
            AppUtilities.showToast(getActivity(), getResources().getString(R.string.invalidresposne));
            return;
        }

        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(getActivity(), parsRes.getError());
        }
        AppUtilities.showToast(getActivity(),getActivity().getResources().getString(R.string.recipt_confirmation));


        pref.save(Prefs.FUEL_RECIEPT,parsRes.getData().getId());
        pref.save(Prefs.FUEL_RECIEPT_NO,binding.edtReceiptNo.getText().toString());
        navController.navigate(R.id.fragment_stoptrip, bundle);
    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        pref.save(Prefs.FUEL_RECIEPT,"");
        AppUtilities.showToast(getActivity(), error);
    }

    @Override
    public void showImagePickerDialog(FilePickUtils.OnFileChoose onFileChoose) {
        super.showImagePickerDialog(onFileChoose);
    }

    @Override
    public void onFileChoose(String fileUri, int requestCode) {
        imgFile = new File(fileUri + "");
        if (imgFile.exists()) {
            ImageDisplayUitls.displayImage(fileUri, getActivity(), binding.imgReciept, R.drawable.ic_placeholder);
            imageUploadServer.uploadFileOnS3(imgFile.getPath(), Constants.S3_PROFILE_BUCKET,
                    false);
        }

    }

    @Override
    public void onFileUploadSuccess(String fileName) {
        image = fileName;
    }

    @Override
    public void onFileUploadFail(String error) {

    }

    @Override
    public void onFileUploadProgress(float progress) {

    }
}
