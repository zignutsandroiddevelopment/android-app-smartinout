package com.smartinout.fragments.bookvehicle;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.HomeActivity;
import com.smartinout.ProfileActivity;
import com.smartinout.R;
import com.smartinout.base.BaseFragment;
import com.smartinout.databinding.FragmentEndtripBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.model.vehicleinout.starttrip.StartTripData;
import com.smartinout.model.vehicleinout.starttrip.StartTripModel;
import com.smartinout.model.vehicleinout.stoptrip.StopTripData;
import com.smartinout.model.vehicleinout.stoptrip.StopTripModel;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.ActivityUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;
import com.smartinout.utilities.Prefs;

import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import retrofit2.Response;

public class FragmentEndTrip extends BaseFragment implements View.OnClickListener,ApiResponse {
    FragmentEndtripBinding binding;
    private Bundle bundle;
    String bookingId,vehicleId;
    Prefs pref;
    StartTripData bookedData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_endtrip, container, false);
        pref = Prefs.getInstance();
        initUI();
        bundle = getArguments();
        if (bundle != null) {
            String isFrom = bundle.getString(Constants.FROM);
            if(isFrom != null){
                if(isFrom.equalsIgnoreCase(Constants.STATUS_END_TRIP)){
                    bookedData = (StartTripData) bundle.get(Constants.DATA);
                    if (bookedData != null) {
                        vehicleId = bookedData.getId();

                        String startingKm = String.valueOf(bookedData.getStartKm());
                        binding.tvStartKm.setText(startingKm);// + " " + getResources().getString(R.string.kmlable));
                    }
                }
            }else {
                bookedData = (StartTripData) bundle.get(Constants.DATA);
                if (bookedData != null) {
                    vehicleId = bookingId = bookedData.getId();
                    String startingKm = String.valueOf(bookedData.getStartKm());

                    binding.tvStartKm.setText(startingKm);// + " " + getResources().getString(R.string.kmlable));
                }
            }
        }
        return binding.getRoot();
    }

    private void initUI() {
        binding.includeWidgetBtn.tvBtnTitle.setText(getResources().getString(R.string.stoptrip));
        binding.linearWidgetButton.setOnClickListener(this);
        String fuelRecipt = pref.getString(Prefs.FUEL_RECIEPT,"");
        String reportFault = pref.getString(Prefs.FAULT_REPORTED,"");
        String reciptNo = pref.getString(Prefs.FUEL_RECIEPT_NO,"");
        String faultType = pref.getString(Prefs.FAULT_TYPE,"");



        if(reportFault != null && reportFault.length() > 0){
            binding.llReportMain.setVisibility(View.GONE);
            binding.llReportSecondry.setVisibility(View.VISIBLE);
            binding.ivSelectFault.setImageResource(R.drawable.ic_selected_blue_check_new);
            binding.tvFaultType.setVisibility(View.VISIBLE);
            binding.tvFaultType.setText(getActivity().getResources().getString(R.string.lblfaulttype) +": "+ faultType);
        }else{
            binding.llReportMain.setVisibility(View.VISIBLE);
            binding.llReportSecondry.setVisibility(View.GONE);
            binding.rlReportFault.setOnClickListener(this);
            binding.ivSelectFault.setImageResource(R.drawable.ic_blue_border_rounded);
            binding.tvFaultType.setVisibility(View.INVISIBLE);
        }

        if(fuelRecipt != null && fuelRecipt.length() > 0){
            binding.llFuelMain.setVisibility(View.GONE);
            binding.llFuelSecondry.setVisibility(View.VISIBLE);
            binding.ivSelectFuel.setImageResource(R.drawable.ic_selected_blue_check);
            binding.tvFuelRecNo.setVisibility(View.VISIBLE);
            binding.tvFuelRecNo.setText(getActivity().getResources().getString(R.string.receiptno) + reciptNo);
        }else{
            binding.llFuelMain.setVisibility(View.GONE);
            binding.llFuelSecondry.setVisibility(View.VISIBLE);
            binding.rlFuelReceipt.setOnClickListener(this);
            binding.tvFuelRecNo.setVisibility(View.INVISIBLE);
            binding.ivSelectFuel.setImageResource(R.drawable.ic_blue_border_rounded);
        }
    }

    @Override
    public void onClick(View v) {

        final NavController navController
                = Navigation.findNavController(getActivity(), R.id.navhost_frag_bookvehicle);

        switch (v.getId()) {
            case R.id.linearWidgetButton:
                String endKm = binding.tvEndKm.getText().toString();
                String startKm = binding.tvStartKm.getText().toString();
                if(endKm.length() == 0){
                    AppUtilities.showToast(getActivity(),getActivity().getResources().getString(R.string.err_endkm));
                    return;
                }
                if(startKm.length() == 0){
                    AppUtilities.showToast(getActivity(),getActivity().getResources().getString(R.string.err_startkm));
                    return;
                }
                endTrip(vehicleId,startKm,endKm);
                break;
            case R.id.rlReportFault:
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.FROM,Constants.STATUS_END_TRIP);
                bundle.putSerializable(Constants.DATA,bookedData);
                navController.navigate(R.id.fragment_reportfault, bundle);
                break;
            case R.id.rlFuelReceipt:
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable(Constants.DATA,bookedData);
                navController.navigate(R.id.fragment_fuelreceipt, bundle1);
                break;
        }
    }

    private void endTrip(String id,String startKm,String endKm) {
        if (!AppUtilities.hasInternet(getActivity(), true)) {
            return;
        }
        showProgressDialog();
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.ID, id);
        params.put(ApiUtils.STARTKM, startKm);
        params.put(ApiUtils.ENDKM, endKm);

        callService.onEndTrip(params, this);
    }

    @Override
    public void onSuccess(Response response) {
        hideProgressDialog();
        StopTripModel parsRes = (StopTripModel) response.body();

        if (parsRes == null) {
            AppUtilities.showToast(getActivity(), getResources().getString(R.string.invalidresposne));
            return;
        }

        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(getActivity(), parsRes.getError());
        }
        pref.saveStartTripData(Prefs.START_TRIP_DATA,null);
        pref.save(Prefs.FUEL_RECIEPT,"");
        pref.save(Prefs.FAULT_REPORTED,"");
        AppUtilities.showToast(getActivity(),getActivity().getResources().getString(R.string.tripendsuccess));
        ActivityUtils.launchActivity(getActivity(), HomeActivity.class);

    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        AppUtilities.showToast(getActivity(), error);
        getActivity().finish();
    }
}
