package com.smartinout.fragments.bookvehicle;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.R;
import com.smartinout.base.BaseFragment;
import com.smartinout.databinding.FragmentStarttripBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.model.vehiclebooking.CreateBookingData;
import com.smartinout.model.vehicleinout.VehicleInfoData;
import com.smartinout.model.vehicleinout.VehicleInfoModel;
import com.smartinout.model.vehicleinout.starttrip.StartTripData;
import com.smartinout.model.vehicleinout.starttrip.StartTripModel;
import com.smartinout.model.vehicleinout.VehicleInOutModel;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;
import com.smartinout.utilities.Prefs;

import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import retrofit2.Response;

public class FragmentStartTrip extends BaseFragment implements View.OnClickListener,ApiResponse {

    FragmentStarttripBinding binding;
    private Bundle bundle;
    private String from,vehicalId,bookingId;
    private NavController navController;
    private Prefs pref;
    StartTripData data;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_starttrip, container, false);
        navController = Navigation.findNavController(getActivity(), R.id.navhost_frag_bookvehicle);
        pref = Prefs.getInstance();
        initUI();

        bundle = getArguments();
        if (bundle != null) {
            from = bundle.getString(Constants.FROM);
            if (from.equalsIgnoreCase(Constants.STATUS_START_TRIP)) {

                VehicleInOutModel bookedData = (VehicleInOutModel) bundle.get(Constants.DATA);

                if (bookedData != null) {
                    bookingId = bookedData.getId();
                    vehicalId = bookedData.getVehicalData().getId();

                    String intime = bookedData.getFrom_time();
                    String outtime = bookedData.getTo_time();
                    String startingKm = String.valueOf(bookedData.getVehicalData().getStartingKm());

                    binding.tvInOutTime.setText(getActivity().getResources().getString(R.string.in) + " " + intime
                            + " | " + getActivity().getResources().getString(R.string.out) + " " + outtime);
                    binding.tvStartKm.setText(startingKm);// + " "+ getResources().getString(R.string.kmlable));
                }


            }else if(from.equalsIgnoreCase(Constants.STATUS_ONGOING_TRIP)){
                CreateBookingData bookedData = (CreateBookingData) bundle.get(Constants.DATA);

                if (bookedData != null) {
                    /*vehicalId = bookedData.getId();
                    bookingId = bookedData.getVehicalData().getId();*/
                    bookingId = bookedData.getId();
                    vehicalId = bookedData.getVehicalData().getId();

                    String intime = bookedData.getFrom_time();
                    String outtime = bookedData.getTo_time();
                    String startingKm = String.valueOf(bookedData.getVehicalData().getStartingKm());

                    binding.tvInOutTime.setText(getActivity().getResources().getString(R.string.in) + " " + intime
                            + " | " + getActivity().getResources().getString(R.string.out) + " " + outtime);
                    binding.tvStartKm.setText(startingKm);// + " "+ getResources().getString(R.string.kmlable));
                }
            }


        }
        return binding.getRoot();
    }


    private void initUI() {
        binding.includeWidgetBtn.tvBtnTitle.setText(getResources().getString(R.string.starttrip));
        binding.linearWidgetButton.setOnClickListener(this);

        String reportFault = pref.getString(Prefs.FAULT_REPORTED,"");
        String faultType = pref.getString(Prefs.FAULT_TYPE,"");

        if(reportFault != null && reportFault.length() > 0){
            binding.llReportMain.setVisibility(View.GONE);
            binding.llReportSecondry.setVisibility(View.VISIBLE);
            binding.ivSelectFault.setImageResource(R.drawable.ic_selected_blue_check);
            binding.tvFaultType.setVisibility(View.VISIBLE);
            binding.tvFaultType.setText(getActivity().getResources().getString(R.string.lblfaulttype) +": "+ faultType);
        }else{
            binding.llReportMain.setVisibility(View.VISIBLE);
            binding.llReportSecondry.setVisibility(View.GONE);
            binding.rlReportFault.setOnClickListener(this);
            binding.ivSelectFault.setImageResource(R.drawable.ic_blue_border_rounded);
            binding.tvFaultType.setVisibility(View.INVISIBLE);

        }


    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.linearWidgetButton:
                String startKm = binding.tvStartKm.getText().toString();
                if(startKm.length() == 0){
                    AppUtilities.showToast(getActivity(),getActivity().getResources().getString(R.string.err_startkm));
                    return;
                }
                startTrip(bookingId,startKm);
                break;

            case R.id.rlReportFault:
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.FROM,Constants.STATUS_START_TRIP);
                bundle.putString(Constants.DATA,vehicalId);
                bundle.putString(Constants.BOOKID,bookingId);
                navController.navigate(R.id.fragment_reportfault, bundle);
                break;

//            case R.id.tvBtnTitle:
//                startTrip(bookingId);
//                break;
        }
    }

    private void startTrip(String id,String startKm) {
        if (!AppUtilities.hasInternet(getActivity(), true)) {
            return;
        }


        showProgressDialog();
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.BOOKINGID, id);
        params.put(ApiUtils.STARTKM, startKm);

        callService.onStartTrip(params, this);
    }

    @Override
    public void onSuccess(Response response) {

        hideProgressDialog();
        StartTripModel parsRes = (StartTripModel) response.body();

        if (parsRes == null) {
            AppUtilities.showToast(getActivity(), getResources().getString(R.string.invalidresposne));
            return;
        }

        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(getActivity(), parsRes.getError());
        }

        StartTripData tripData = parsRes.getData();
        if(tripData != null){
            pref.saveStartTripData(Prefs.START_TRIP_DATA,tripData);
            bundle.putString(Constants.FROM, Constants.STATUS_END_TRIP);
            bundle.putSerializable(Constants.DATA, tripData);
            NavController navController = Navigation.findNavController(getActivity(), R.id.navhost_frag_bookvehicle);;
            navController.navigate(R.id.fragment_stoptrip, bundle);

            //AppUtilities.showToast(getActivity(),getActivity().getResources().getString(R.string.start_trip_confirmation));
            //getActivity().finish();

        }

    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        AppUtilities.showToast(getActivity(), error);
    }


}
