package com.smartinout.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.LoginActivity;
import com.smartinout.ProfileActivity;
import com.smartinout.R;
import com.smartinout.adapters.MenuSettingsAdapter;
import com.smartinout.base.BaseFragment;
import com.smartinout.databinding.FragmentSettingsBinding;
import com.smartinout.model.settings.MenuSettings;
import com.smartinout.utilities.ActivityUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;
import com.smartinout.utilities.Prefs;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

public class SettingsFragment extends BaseFragment implements MenuSettingsAdapter.OnItemClick {

    FragmentSettingsBinding binding;
    private MenuSettingsAdapter settingsAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.fragment_settings, container, false);
        initUI();
        return binding.getRoot();
    }

    private void initUI() {

        // Menu settings data
        binding.rvSettings.setLayoutManager(new LinearLayoutManager(getContext()));

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider));

        binding.rvSettings.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        settingsAdapter = new MenuSettingsAdapter(getContext(), AppUtilities.getMenuSettings(getContext()), this);
        binding.rvSettings.setAdapter(settingsAdapter);

    }

    @Override
    public void getItemClick(MenuSettings menuSettings) {
        if (menuSettings.getmTitle().equals(getResources().getString(R.string.termconditions))
                || menuSettings.getmTitle().equals(getResources().getString(R.string.privacypolicy))
                || menuSettings.getmTitle().equals(getResources().getString(R.string.legal))
                || menuSettings.getmTitle().equals(getResources().getString(R.string.faqs))) {

            AppUtilities.openDialogFragment(
                    SettingsFragment.this, new DialogFragmentWebView(), menuSettings.getmTitle(),
                    menuSettings.getContent(), Constants.DIALOG_FRAG_TAG_WEBVIEW
            );

        } else if (menuSettings.getmTitle().equals(getResources().getString(R.string.logout))) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(getResources().getString(R.string.logout));
            builder.setMessage(getResources().getString(R.string.doyouwantologout));
            builder.setPositiveButton(getResources().getString(R.string.btnLogout), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (AppUtilities.hasInternet(getContext(), true)) {
                        pref.clearAll();
                        inOutDBCtrl.clearAll();
                        ActivityUtils.launchActivityWithClearBackStack(getContext(), LoginActivity.class);
                    }
                }
            });
            builder.setNegativeButton(getResources().getString(R.string.btnCancel), null);
            builder.show();
        } else if (menuSettings.getmTitle().equals(getResources().getString(R.string.profile))) {
            ActivityUtils.launchStartActivityForFragment(SettingsFragment.this, ProfileActivity.class, null);

        } else if (menuSettings.getmTitle().equals(getResources().getString(R.string.changepassword))) {
            AppUtilities.openDialogFragment(SettingsFragment.this, new DialogFragmentChangePass()
                    , Constants.DIALOG_FRAG_TAG_FORGOTPASS);
        }
    }
}
