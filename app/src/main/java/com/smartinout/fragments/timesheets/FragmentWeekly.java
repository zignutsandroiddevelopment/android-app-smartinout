package com.smartinout.fragments.timesheets;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.R;
import com.smartinout.adapters.CalendarViewAdapter;
import com.smartinout.base.BaseFragment;
import com.smartinout.databinding.FragmentWeeklyBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.interfaces.OnItemClick;
import com.smartinout.interfaces.OnWeeklyItemClick;
import com.smartinout.model.timesheet.CalendarModel;
import com.smartinout.model.timesheet.weekly.DateWiseDatum;
import com.smartinout.model.timesheet.weekly.WeeklyData;
import com.smartinout.model.timesheet.weekly.WeeklyTimeSheet;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import retrofit2.Response;

public class FragmentWeekly extends BaseFragment implements View.OnClickListener, OnItemClick, ApiResponse {

    FragmentWeeklyBinding binding;
    private ArrayList<CalendarModel> listData = new ArrayList<>();
    private CalendarViewAdapter adapter;
    private String currentDate = "", startDate = "", endDate = "";
    private ArrayList<CalendarModel> listweekly = new ArrayList<>();
    private OnWeeklyItemClick onDailyView;

    public FragmentWeekly getInitiate(OnWeeklyItemClick onDailyView) {
        FragmentWeekly fragmentDaily = new FragmentWeekly();
        fragmentDaily.onDailyView = onDailyView;
        return fragmentDaily;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.fragment_weekly, container, false);
        currentDate = AppUtilities.getCurrentDateDDMMYYYY();
        initUI();
        return binding.getRoot();

    }

    private void initUI() {

        binding.rlNext.setOnClickListener(this);
        binding.rlPrev.setOnClickListener(this);

        listData = AppUtilities.getWeeksOfMonth();
        binding.tvCurrentWeek.setText(listData.get(0).getWeeklydates());
        getStartEndDate();

        binding.listData.setLayoutManager(new LinearLayoutManager(getContext()));

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider));

        binding.listData.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        getWeeklyData();
    }

    private void getWeeklyData() {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        //currentDate = "26/9/2019";
        showProgressDialog();
        callService.weeklyTimesheet(currentDate, this);
    }

    public void getStartEndDate() {
        startDate = binding.tvCurrentWeek.getText().toString().split("TO")[0].trim();
        endDate = binding.tvCurrentWeek.getText().toString().split("TO")[1].trim();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlNext:
                AppUtilities.getWeek(binding.tvCurrentWeek, endDate, 1);
                getStartEndDate();
                currentDate = startDate.replace("-", "/");
                getWeeklyData();
                break;
            case R.id.rlPrev:
                AppUtilities.getWeek(binding.tvCurrentWeek, startDate, -6);
                getStartEndDate();
                currentDate = startDate.replace("-", "/");
                getWeeklyData();
                break;
        }
    }


    @Override
    public void onButtonClick(boolean isCancle) {

    }

    @Override
    public void onUpdateClick(int position,String id, boolean isUnbook) {

    }

    @Override
    public void onItemClick(int position) {
        Date selectedDate = AppUtilities.convertStringToDateNew(listweekly.get(position).getDate());
        Date currentDate = AppUtilities.convertStringToDate(AppUtilities.getCurrentDateDDMMYYYY());

        if(selectedDate.before(currentDate) || selectedDate.equals(currentDate)) {
            CalendarModel calendarModel = listweekly.get(position);
            calendarModel.setWeeklydates(binding.tvCurrentWeek.getText().toString());
            onDailyView.onClickDailyView(calendarModel);
        }
    }

    @Override
    public void onItemDelete(int position,String id, String number,String from_time,String  to_time) {

    }

    @Override
    public void onSuccess(Response response) {
        hideProgressDialog();

        WeeklyTimeSheet parsRes = (WeeklyTimeSheet) response.body();

        if (parsRes == null) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
            return;
        }

        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(getContext(), parsRes.getError());
        }

        List<WeeklyData> listData = parsRes.getData();
        if (listData != null && listData.size() > 0) {
            List<DateWiseDatum> datumList = listData.get(0).getDateWiseData();

            binding.layoutEmptyView.tvEmptyText.setVisibility(View.GONE);

            listweekly.clear();

            for (DateWiseDatum wiseDatum: datumList) {
                try {

                    CalendarModel calendarModel = new CalendarModel();

                    //Date
                    /*String date = AppUtilities.formatDateFromDateString(Constants.YYYY_MM_DD_T_HH_MM_SS_Z, Constants.ddMMMMYYYY, "2019-12-30T20:17:46.384Z");//wiseDatum.getDate());*/
                    String date = AppUtilities.getTimeStampFromDateTime(Constants.YYYY_MM_DD_T_HH_MM_SS_Z, Constants.DATE_FORMAT_5,  wiseDatum.getDate());//wiseDatum.getDate());
                    calendarModel.setDate(date);

                    // Date
                    /*String currentDate = AppUtilities.formatDateFromDateString(Constants.YYYY_MM_DD_T_HH_MM_SS_Z, Constants.dd, wiseDatum.getDate());*/
                    String currentDate = AppUtilities.getTimeStampFromDateTime(Constants.YYYY_MM_DD_T_HH_MM_SS_Z, Constants.dd, wiseDatum.getDate());
                    calendarModel.setCurrentDate(Integer.parseInt(currentDate));

                    //Day
                    /*String day = AppUtilities.formatDateFromDateString(Constants.YYYY_MM_DD_T_HH_MM_SS_Z, Constants.EEEEddMMM, wiseDatum.getDate()).split(" ")[0];*/
                    String day = AppUtilities.getTimeStampFromDateTime(Constants.YYYY_MM_DD_T_HH_MM_SS_Z, Constants.EEEEddMMM, wiseDatum.getDate()).split(" ")[0];
                    calendarModel.setDay(day);

                    // Totla Hours Spent
                    String totalhrsspent = AppUtilities.timeFormatterfromMinutes(wiseDatum.getActiveMinutes());// + wiseDatum.getBreakMinutes());
                    calendarModel.setHours(totalhrsspent);

                    listweekly.add(calendarModel);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            adapter = new CalendarViewAdapter(getContext(), listweekly, CalendarViewAdapter.TYPE_WEEKLY, this);
            binding.listData.setAdapter(adapter);

        } else {
            binding.layoutEmptyView.tvEmptyText.setVisibility(View.VISIBLE);
            if (adapter != null) {
                listweekly.clear();
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        AppUtilities.showToast(getContext(), error);
    }
}
