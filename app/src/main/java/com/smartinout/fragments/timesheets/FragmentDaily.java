package com.smartinout.fragments.timesheets;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.EditDailyTimeSheet;
import com.smartinout.R;
import com.smartinout.adapters.CalendarViewAdapter;
import com.smartinout.adapters.DailyInOutAdapter;
import com.smartinout.adapters.DailyInOutAdapterOld;
import com.smartinout.base.BaseFragment;
import com.smartinout.databinding.FragmentDailyBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.interfaces.OnItemClick;
import com.smartinout.model.login.Inoutreference;
import com.smartinout.model.timesheet.CalendarModel;
import com.smartinout.model.timesheet.DailyTimeSheetsPojo;
import com.smartinout.model.timesheet.UserDailyProgressData;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.ActivityUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;
import com.smartinout.utilities.Prefs;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import retrofit2.Response;

public class FragmentDaily extends BaseFragment implements View.OnClickListener, OnItemClick, ApiResponse {

    FragmentDailyBinding binding;
    private CalendarViewAdapter adapter;
    private ArrayList<CalendarModel> listData = new ArrayList<>();
    private List<Inoutreference> listinout = new ArrayList<>();
    private DailyInOutAdapter dailyInOutAdapter;
    private String currentDateStr = "";
    private int selectedItemPos = -1;
    private Prefs prefs;
    private Calendar calendar;
    private Context mContext;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.fragment_daily, container, false);
        currentDateStr = AppUtilities.getCurrentDateDDMMYYYY();
        prefs = Prefs.getInstance();
        mContext = getActivity();
        initUI();
        return binding.getRoot();
    }


    private void initUI() {

        binding.rvCalView.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false));

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider));

        binding.rvDailyInout.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        binding.rvDailyInout.setLayoutManager(new LinearLayoutManager(getContext()));

        String strDate = (getArguments() != null && getArguments().containsKey(ApiUtils.DATE))
                ? getArguments().getString(ApiUtils.DATE)
                : null;

        if (!AppUtilities.getValidateString(strDate).isEmpty()) {
            String dateFirst = strDate.split("TO")[0].trim();
            String dateSecond = strDate.split("TO")[1].trim();

            int currentDate = getArguments().getInt(ApiUtils.CURRENTDATE);
            getWeekandWeekDays(dateFirst, dateSecond, currentDate);

        } else {
            getStartEndDateOfWeek();
        }

        getDailyInOut();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View v) {

    }

    private void getDailyInOut() {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        showProgressDialog();
        callService.dailyTimesheet(currentDateStr, this);
    }

    public void getStartEndDateOfWeek() {
        try {

            Calendar weekEnd = AppUtilities.getWeekEndDate();
            int year1 = weekEnd.get(Calendar.YEAR);
            int month1 = weekEnd.get(Calendar.MONTH) + 1;
            int day1 = weekEnd.get(Calendar.DAY_OF_MONTH);

            String currentWeekEndDate = day1 + "-" + month1 + "-" + year1;

            weekEnd.add(Calendar.DATE, -13);

            int year7 = weekEnd.get(Calendar.YEAR);
            int month7 = weekEnd.get(Calendar.MONTH) + 1;
            int day7 = weekEnd.get(Calendar.DAY_OF_MONTH);

            String dateSecond = day7 + "-" + month7 + "-" + year7;

            int intCurrentDate = Calendar.getInstance().get(Calendar.DATE);

            getWeekandWeekDays(dateSecond, currentWeekEndDate, intCurrentDate);
        }catch (Exception e){
            e.printStackTrace();
        }
    }




    /*public void getStartEndDateOfWeek() {





        Calendar weekStart = AppUtilities.getWeekStartDate();
        int year1 = weekStart.get(Calendar.YEAR);
        int month1 = weekStart.get(Calendar.MONTH) + 1;
        int day1 = weekStart.get(Calendar.DAY_OF_MONTH);


        String dateFirst = day1 + "-" + month1 + "-" + year1;

        SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.dd_MM_YYYY);
        try {
            Date myDate = dateFormat.parse(dateFirst);
            Date newDate = new Date(myDate.getTime() - 604800000L);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            Log.e("TAG", "Error in Parsing Date : " + e.getMessage());
        }


        Calendar weekEnd = AppUtilities.getWeekEndDate();
        int year7 = weekEnd.get(Calendar.YEAR);
        int month7 = weekEnd.get(Calendar.MONTH) + 1;
        int day7 = weekEnd.get(Calendar.DAY_OF_MONTH) +7;


        String dateSecond = day7 + "-" + month7 + "-" + year7;

        int intCurrentDate = Calendar.getInstance().get(Calendar.DATE);

        getWeekandWeekDays(dateFirst, dateSecond, intCurrentDate);
    }*/


    public void getWeekandWeekDays(String dateFirst, String dateSecond, int intCurrentDate) {
        List<Date> listWeekDates = AppUtilities.getDates(dateFirst, dateSecond);
        if (listWeekDates != null && listWeekDates.size() > 0) {
            for (int i = 0; i < listWeekDates.size(); i++) {
                Date date = listWeekDates.get(i);
                CalendarModel model = new CalendarModel();

                String formatedate = AppUtilities.formateDateFromstring(
                        Constants.EEEMMMDDHHMMSSZZZYYYY, Constants.ddMMyyyy, date.toString()
                );
                model.setFormatedDate(formatedate);

                model.setDate(String.valueOf(date.getDate()));
                model.setDay(AppUtilities.getDayinString(date.getTime()));
                if (date.getDate() == intCurrentDate) {
                    model.setSelected(true);
                    currentDateStr = AppUtilities.getCurrentDateddMMyyyy(date.getTime());
                }
                listData.add(model);
            }
        }

        adapter = new CalendarViewAdapter(getContext(), listData, CalendarViewAdapter.TYPE_DAILY, this);
        binding.rvCalView.setAdapter(adapter);
        binding.rvCalView.getLayoutManager().scrollToPosition(7);
    }


    @Override
    public void onButtonClick(boolean isCancle) {

    }

    @Override
    public void onUpdateClick(int position, String id, boolean isUnbook) {

    }

    @Override
    public void onItemClick(int position) {
        String currentDateStr1 = AppUtilities.getCurrentDateDDMMYYYY();

        Date selectedDate = AppUtilities.convertStringToDate(listData.get(position).getFormatedDate());
        Date currentDate = AppUtilities.convertStringToDate(currentDateStr1);

        if (selectedDate.before(currentDate) || selectedDate.equals(currentDate)) {
            CalendarModel calendarModel = listData.get(position);
            calendarModel.setSelected(true);
            listData.set(position, calendarModel);
            for (int i = 0; i < listData.size(); i++) {
                if (i != position) {
                    CalendarModel temp = listData.get(i);
                    temp.setSelected(false);
                    listData.set(i, temp);
                }
            }
            adapter.notifyDataSetChanged();
            currentDateStr = calendarModel.getFormatedDate();
            getDailyInOut();
        }
    }

    @Override
    public void onItemDelete(int position, String id, String number, String from_time, String to_time) {

    }

    @Override
    public void onSuccess(Response response) {
        hideProgressDialog();

        DailyTimeSheetsPojo userData = (DailyTimeSheetsPojo) response.body();
        UserDailyProgressData parsRes = userData.getDailyProgressData();

        if (parsRes == null) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
            return;
        }

        if (!AppUtilities.getValidateString(userData.getError()).isEmpty()) {
            AppUtilities.showToast(getContext(), userData.getError());
        }

        if (parsRes.getUserTimesheet() != null && parsRes.getUserTimesheet().getActivities() != null
                && parsRes.getUserTimesheet().getActivities().size() > 0
                && parsRes.getUserTimesheet().getActivities().get(0).getUsersData() != null
                && parsRes.getUserTimesheet().getActivities().get(0).getUsersData().size() > 0) {

            binding.layoutEmptyView.tvEmptyText.setVisibility(View.GONE);
            binding.linearTotalHours.setVisibility(View.VISIBLE);

            prefs.save(Prefs.TIMEZONE, parsRes.getUserTimesheet().getTimeZone());


            List<Inoutreference> inoutreferences = parsRes.getUserTimesheet().getActivities().get(0).getUsersData();

            String totalInHrs = AppUtilities.hmTimeFormatterfromMinutesForDaily(parsRes.getResponseOfProgressBar().get(0).getTotalActiveMinutes());
            binding.tvTotalhrs.setText(totalInHrs + " " + mContext.getResources().getString(R.string.hours));

            setDailyListData(inoutreferences, true);

        } else {
            binding.linearTotalHours.setVisibility(View.GONE);
            binding.layoutEmptyView.tvEmptyText.setVisibility(View.VISIBLE);

            if (dailyInOutAdapter != null) {
                listinout.clear();
                dailyInOutAdapter.notifyDataSetChanged();
            }
        }
    }

    public void setDailyListData(List<Inoutreference> inoutreferences, boolean isClearData) {

        if (isClearData)
            listinout.clear();

        long totalMillis = 0;

        for (int i = 0; i < inoutreferences.size(); i++) {


            Inoutreference inoutreference = inoutreferences.get(i);

            String startTime = AppUtilities.getHrsInOutFromLong(mContext, inoutreference.getStartTime());
            inoutreference.setStartTimelbl(startTime);

            String endTime = AppUtilities.getHrsInOutFromLong(mContext, inoutreference.getEndTime());
            inoutreference.setEndTimelbl(endTime);

            if (inoutreference.getSecondryStatus().equals(ApiUtils.OUT)) {
                if (inoutreference.isClockedOut()) {

                    if (inoutreference.getEndTime() != 0 && inoutreference.getEndTime() != 0) {

                        String startDateStr = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getStartTime());//+ " "+ inoutreference.getStartTimelbl();
                        String endDateStr = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getEndTime());

                        Date startDate = AppUtilities.convertStringToDate(startDateStr);
                        Date currentDate = AppUtilities.convertStringToDate(currentDateStr);
                        Date endDate = AppUtilities.convertStringToDate(endDateStr);

                        /*Log.d("DailyTimesheet ","Time :"+ startDateStr +" "+ inoutreference.getStartTimelbl() + "::"+ endDateStr+ " "+ inoutreference.getEndTimelbl());
                        Log.d("DailyTimesheet ","Start :"+ startDate);
                        Log.d("DailyTimesheet ","current :"+ currentDate);
                        Log.d("DailyTimesheet ","end :"+ endDate);*/
                        if(startDate.equals(currentDate)) {

                            if (startDate.equals(endDate)) {
                                totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                                String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                                inoutreference.setTotalHrs(totalHrs);

                                if (isClearData)
                                    listinout.add(inoutreference);
                                else
                                    listinout.set(i, inoutreference);
                            } else if (currentDate.equals(startDate)) {
                                if (startDate.before(currentDate)) {
                                    String temp1 = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getEndTime()) + " 00:00:00";
                                    long val1 = inoutreference.getStartTime();
                                    long val2 = AppUtilities.convertDateToLong(temp1);

                                    totalMillis = val2 - val1;
                                    String totalHrs = AppUtilities.hmTimeFormatter(totalMillis);
                                    inoutreference.setTotalHrs(totalHrs);

                                } else if (startDate.equals(currentDate)) {


                                    String temp1 = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getStartTime()) + " 24:00:00";
                                    long val1 = inoutreference.getStartTime();
                                    long val2 = AppUtilities.convertDateToLong(temp1);

                                    totalMillis = val2 - val1;
                                    String totalHrs = AppUtilities.hmTimeFormatter(totalMillis);
                                    inoutreference.setTotalHrs(totalHrs);

                                    if (isClearData)
                                        listinout.add(inoutreference);
                                    else
                                        listinout.set(i, inoutreference);

                                }

                            } else if (currentDate.equals(endDate)) {
                                if (startDate.before(currentDate)) {
                                    String temp1 = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getEndTime()) + " 00:00:00";
                                    long val1 = AppUtilities.convertDateToLong(temp1);
                                    long val2 = inoutreference.getEndTime();

                                    totalMillis = val2 - val1;
                                    String totalHrs = AppUtilities.hmTimeFormatter(totalMillis);
                                    inoutreference.setTotalHrs(totalHrs);

                                    if (isClearData)
                                        listinout.add(inoutreference);
                                    else
                                        listinout.set(i, inoutreference);

                                } else if (startDate.equals(currentDate)) {
                                    totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                                    String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                                    inoutreference.setTotalHrs(totalHrs);

                                    if (isClearData)
                                        listinout.add(inoutreference);
                                    else
                                        listinout.set(i, inoutreference);

                                } else {
                                    totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                                    String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                                    inoutreference.setTotalHrs(totalHrs);

                                    if (isClearData)
                                        listinout.add(inoutreference);
                                    else
                                        listinout.set(i, inoutreference);
                                }

                            } else {
                                if (endDate.equals(currentDate)) {
                                    totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                                    String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                                    inoutreference.setTotalHrs(totalHrs);

                                    if (isClearData)
                                        listinout.add(inoutreference);
                                    else
                                        listinout.set(i, inoutreference);

                                } else if (endDate.after(currentDate)) {

                                    long diffHour = AppUtilities.getHour(inoutreference.getDiffrenceInMiliseconds());

                                    if (diffHour > 24) {
                                        String temp1 = AppUtilities.getCurrentDateddMMyyyy(currentDate.getTime()) + " 00:00:00";
                                        String temp2 = AppUtilities.getCurrentDateddMMyyyy(currentDate.getTime()) + " 24:00:00";
                                        long val1 = AppUtilities.convertDateToLong(temp1);
                                        long val2 = AppUtilities.convertDateToLong(temp2);

                                        totalMillis = val2 - val1;
                                        String totalHrs = AppUtilities.hmTimeFormatter(totalMillis);
                                        inoutreference.setTotalHrs(totalHrs);
                                    } else {
                                        String temp1 = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getStartTime()) + " 24:00:00";
                                        long val1 = inoutreference.getStartTime();
                                        long val2 = AppUtilities.convertDateToLong(temp1);

                                        totalMillis = val2 - val1;
                                        String totalHrs = AppUtilities.hmTimeFormatter(totalMillis);
                                        inoutreference.setTotalHrs(totalHrs);
                                    }

                                    if (isClearData)
                                        listinout.add(inoutreference);
                                    else
                                        listinout.set(i, inoutreference);


                                } else {
                                    totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                                    String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                                    inoutreference.setTotalHrs(totalHrs);
                                }

                            }
                        }
                    }else{

                        inoutreference.setTotalHrs(getResources().getString(R.string.dash));

                        if (isClearData)
                            listinout.add(inoutreference);
                        else
                            listinout.set(i, inoutreference);
                    }
                }else{
                    String startDateStr = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getStartTime());//+ " "+ inoutreference.getStartTimelbl();
                    String endDateStr = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getEndTime());

                    Date startDate = AppUtilities.convertStringToDate(startDateStr);
                    Date currentDate = AppUtilities.convertStringToDate(currentDateStr);
                    Date endDate = AppUtilities.convertStringToDate(endDateStr);

                        /*Log.d("DailyTimesheet ","Time :"+ startDateStr +" "+ inoutreference.getStartTimelbl() + "::"+ endDateStr+ " "+ inoutreference.getEndTimelbl());
                        Log.d("DailyTimesheet ","Start :"+ startDate);
                        Log.d("DailyTimesheet ","current :"+ currentDate);
                        Log.d("DailyTimesheet ","end :"+ endDate);*/
                    if(startDate.equals(currentDate)) {
                        totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                        String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                        inoutreference.setTotalHrs("");
                        if (isClearData)
                            listinout.add(inoutreference);
                        else
                            listinout.set(i, inoutreference);
                    }
                }

            } else {

                if (inoutreference.getEndTime() != 0 && inoutreference.getEndTime() != 0) {

                    String startDateStr = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getStartTime());//+ " "+ inoutreference.getStartTimelbl();
                    String endDateStr = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getEndTime());

                    Date startDate = AppUtilities.convertStringToDate(startDateStr);
                    Date currentDate = AppUtilities.convertStringToDate(currentDateStr);
                    Date endDate = AppUtilities.convertStringToDate(endDateStr);

                    /*Log.d("DailyTimesheet else ","Time :"+ startDateStr +" "+ inoutreference.getStartTimelbl() + "::"+ endDateStr+ " "+ inoutreference.getEndTimelbl());
                    Log.d("DailyTimesheet else ","Start :"+ startDate);
                    Log.d("DailyTimesheet else ","current :"+ currentDate);
                    Log.d("DailyTimesheet else ","end :"+ endDate);*/


                    if (startDate.equals(endDate)) {
                        totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                        String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                        inoutreference.setTotalHrs(totalHrs);

                        if (isClearData)
                            listinout.add(inoutreference);
                        else
                            listinout.set(i, inoutreference);
                    } else if (currentDate.equals(startDate)) {
                        if (startDate.before(currentDate)) {
                            String temp1 = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getEndTime()) + " 00:00:00";
                            long val1 = inoutreference.getStartTime();
                            long val2 = AppUtilities.convertDateToLong(temp1);

                            totalMillis = val2 - val1;
                            String totalHrs = AppUtilities.hmTimeFormatter(totalMillis);
                            inoutreference.setTotalHrs(totalHrs);

                        } else if (startDate.equals(currentDate)) {


                            String temp1 = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getStartTime()) + " 24:00:00";
                            long val1 = inoutreference.getStartTime();
                            long val2 = AppUtilities.convertDateToLong(temp1);

                            totalMillis = val2 - val1;
                            String totalHrs = AppUtilities.hmTimeFormatter(totalMillis);
                            inoutreference.setTotalHrs(totalHrs);

                            if (isClearData)
                                listinout.add(inoutreference);
                            else
                                listinout.set(i, inoutreference);

                        }

                    } else if (currentDate.equals(endDate)) {
                        if (startDate.before(currentDate)) {
                            String temp1 = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getEndTime()) + " 00:00:00";
                            long val1 = AppUtilities.convertDateToLong(temp1);
                            long val2 = inoutreference.getEndTime();

                            totalMillis = val2 - val1;
                            String totalHrs = AppUtilities.hmTimeFormatter(totalMillis);
                            inoutreference.setTotalHrs(totalHrs);

                            if (isClearData)
                                listinout.add(inoutreference);
                            else
                                listinout.set(i, inoutreference);

                        } else if (startDate.equals(currentDate)) {
                            totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                            String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                            inoutreference.setTotalHrs(totalHrs);

                            if (isClearData)
                                listinout.add(inoutreference);
                            else
                                listinout.set(i, inoutreference);

                        } else {
                            totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                            String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                            inoutreference.setTotalHrs(totalHrs);

                            if (isClearData)
                                listinout.add(inoutreference);
                            else
                                listinout.set(i, inoutreference);
                        }

                    } else {
                        if (endDate.equals(currentDate)) {
                            totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                            String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                            inoutreference.setTotalHrs(totalHrs);

                            if (isClearData)
                                listinout.add(inoutreference);
                            else
                                listinout.set(i, inoutreference);

                        } else if (endDate.after(currentDate)) {

                            long diffHour = AppUtilities.getHour(inoutreference.getDiffrenceInMiliseconds());

                            if (diffHour > 24) {
                                String temp1 = AppUtilities.getCurrentDateddMMyyyy(currentDate.getTime()) + " 00:00:00";
                                String temp2 = AppUtilities.getCurrentDateddMMyyyy(currentDate.getTime()) + " 24:00:00";
                                long val1 = AppUtilities.convertDateToLong(temp1);
                                long val2 = AppUtilities.convertDateToLong(temp2);

                                totalMillis = val2 - val1;
                                String totalHrs = AppUtilities.hmTimeFormatter(totalMillis);
                                inoutreference.setTotalHrs(totalHrs);
                            } else {
                                String temp1 = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getStartTime()) + " 24:00:00";
                                long val1 = inoutreference.getStartTime();
                                long val2 = AppUtilities.convertDateToLong(temp1);

                                totalMillis = val2 - val1;
                                String totalHrs = AppUtilities.hmTimeFormatter(totalMillis);
                                inoutreference.setTotalHrs(totalHrs);
                            }

                            if (isClearData)
                                listinout.add(inoutreference);
                            else
                                listinout.set(i, inoutreference);


                        } else {
                            totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                            String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                            inoutreference.setTotalHrs(totalHrs);
                        }

                    }

                } else {
                    inoutreference.setTotalHrs(mContext.getResources().getString(R.string.dash));
                    if (isClearData)
                        listinout.add(inoutreference);
                    else
                        listinout.set(i, inoutreference);
                }
            }
        }


        //}

        if (dailyInOutAdapter == null) {
            selectedItemPos = -1;
            dailyInOutAdapter = new DailyInOutAdapter(mContext, currentDateStr, listinout, new OnItemClick() {


                @Override
                public void onItemClick(int position) {
                    selectedItemPos = position;
                    Inoutreference inoutreference = listinout.get(position);
                    String previousIn = "";
                    try {

                        if (position > 0) {
                            previousIn = listinout.get(position - 1).getStartTimelbl();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.INOUTPREF, inoutreference);
                    bundle.putSerializable(Constants.INOUTPREF_PREVIOUSTIME, previousIn);
                    ActivityUtils.launchStartActivityForResult(FragmentDaily.this, EditDailyTimeSheet.class,
                            bundle, Constants.RESULT_CODE_EDITTIMESHEET);
                }

                @Override
                public void onItemDelete(int position, String id, String number, String from_time, String to_time) {

                }


                @Override
                public void onButtonClick(boolean isCancle) {

                }

                @Override
                public void onUpdateClick(int position, String id, boolean isUnbook) {

                }


            });
            binding.rvDailyInout.setAdapter(dailyInOutAdapter);

        } else {
            dailyInOutAdapter.setCurrentDate(currentDateStr);
            dailyInOutAdapter.notifyDataSetChanged();
        }
        //long activeMinutes = inOutDBCtrl.getTotalActiveMinute(currentDate);
        //binding.tvTotalhrs.setText(AppUtilities.hmTimeFormatter(totalMillis) + " " + getResources().getString(R.string.hours));

        /*String totalInHrs = AppUtilities.hmTimeFormatterfromMinutesForDaily(activeMinutes);
        binding.tvTotalhrs.setText(totalInHrs + " " + getResources().getString(R.string.hours));*/
    }

    public void setDailyListDataOld(List<Inoutreference> inoutreferences, boolean isClearData) {

        if (isClearData)
            listinout.clear();

        long totalMillis = 0;

        for (int i = 0; i < inoutreferences.size(); i++) {


            Inoutreference inoutreference = inoutreferences.get(i);

            String startTime = AppUtilities.getHrsInOutFromLong(mContext, inoutreference.getStartTime());
            inoutreference.setStartTimelbl(startTime);

            String endTime = AppUtilities.getHrsInOutFromLong(mContext, inoutreference.getEndTime());
            inoutreference.setEndTimelbl(endTime);

            if (inoutreference.getEndTime() != 0 && inoutreference.getEndTime() != 0) {
                /*totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getEndTime() - inoutreference.getStartTime());
                inoutreference.setTotalHrs(totalHrs);*/

                String startTime1 = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getStartTime());//+ " "+ inoutreference.getStartTimelbl();
                String endTime1 = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getEndTime());


                Date startDate = AppUtilities.convertStringToDate(startTime1);
                Date currentDate1 = AppUtilities.convertStringToDate(currentDateStr);
                Date endDate = AppUtilities.convertStringToDate(endTime1);
                Log.d("DailyInOut ", startTime1 + " " + inoutreference.getStartTimelbl() + " End : " + endTime1 + " " + inoutreference.getEndTimelbl());
                Log.d("DailyInOut ", "===============");
                Log.d("DailyInOut ", "Current " + currentDate1);
                Log.d("DailyInOut ", "Start " + startDate);
                Log.d("DailyInOut ", "End  " + endDate);

                if (currentDate1.equals(startDate)) {

                    if (startDate.before(currentDate1)) {
                        Log.d("DailyInOut ", "current date before start date ");
                        String temp1 = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getEndTime()) + " 00:00:00";
                        long val1 = inoutreference.getStartTime();
                        long val2 = AppUtilities.convertDateToLong(temp1);

                        totalMillis = val2 - val1;
                        String totalHrs = AppUtilities.hmTimeFormatter(totalMillis);
                        inoutreference.setTotalHrs(totalHrs);

                    } else if (startDate.equals(currentDate1)) {
                        Log.d("DailyInOut ", "current date = start date ");
                        totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                        String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                        inoutreference.setTotalHrs(totalHrs);

                        if (isClearData)
                            listinout.add(inoutreference);
                        else
                            listinout.set(i, inoutreference);

                    } else {
                        Log.d("DailyInOut ", "current date after start date ");

                        totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                        String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                        inoutreference.setTotalHrs(totalHrs);

                        if (isClearData)
                            listinout.add(inoutreference);
                        else
                            listinout.set(i, inoutreference);
                    }

                } else if (currentDate1.equals(endDate)) {

                    if (startDate.before(currentDate1)) {
                        Log.d("DailyInOut ", "current date before start date :: ");
                        String temp1 = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getEndTime()) + " 00:00:00";
                        long val1 = inoutreference.getStartTime();
                        long val2 = AppUtilities.convertDateToLong(temp1);

                        totalMillis = val2 - val1;
                        String totalHrs = AppUtilities.hmTimeFormatter(totalMillis);
                        inoutreference.setTotalHrs(totalHrs);

                    } else if (startDate.equals(currentDate1)) {
                        Log.d("DailyInOut ", "current date = start date:: ");
                        totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                        String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                        inoutreference.setTotalHrs(totalHrs);

                        if (isClearData)
                            listinout.add(inoutreference);
                        else
                            listinout.set(i, inoutreference);

                    } else {
                        Log.d("DailyInOut ", "current date != start date ::");

                        totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                        String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                        inoutreference.setTotalHrs(totalHrs);

                        if (isClearData)
                            listinout.add(inoutreference);
                        else
                            listinout.set(i, inoutreference);
                    }

                }/*else {

                    if (endDate.equals(currentDate1)) {
                        Log.d("DailyInOut ", "current date = End date ");
                        totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                        String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                        inoutreference.setTotalHrs(totalHrs);

                        if (isClearData)
                            listinout.add(inoutreference);
                        else
                            listinout.set(i, inoutreference);

                    } else if (endDate.after(currentDate1)) {

                        long diffHour = AppUtilities.getHour(inoutreference.getDiffrenceInMiliseconds());
                        Log.d("DailyInOut ", "current date after End date " + diffHour);

                        if (diffHour > 24) {
                            String temp1 = AppUtilities.getCurrentDateddMMyyyy(currentDate1.getTime()) + " 00:00:00";
                            String temp2 = AppUtilities.getCurrentDateddMMyyyy(currentDate1.getTime()) + " 24:00:00";
                            long val1 = AppUtilities.convertDateToLong(temp1);
                            long val2 = AppUtilities.convertDateToLong(temp2);

                            totalMillis = val2 - val1;
                            String totalHrs = AppUtilities.hmTimeFormatter(totalMillis);
                            inoutreference.setTotalHrs(totalHrs);
                        } else {
                            String temp1 = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getStartTime()) + " 24:00:00";
                            long val1 = inoutreference.getStartTime();
                            long val2 = AppUtilities.convertDateToLong(temp1);

                            totalMillis = val2 - val1;
                            String totalHrs = AppUtilities.hmTimeFormatter(totalMillis);
                            inoutreference.setTotalHrs(totalHrs);
                        }

                        if (isClearData)
                            listinout.add(inoutreference);
                        else
                            listinout.set(i, inoutreference);


                    } else {
                        Log.d("DailyInOut ", "current date before End date ");
                        totalMillis = totalMillis + (inoutreference.getEndTime() - inoutreference.getStartTime());
                        String totalHrs = AppUtilities.hmTimeFormatter(inoutreference.getDiffrenceInMiliseconds());
                        inoutreference.setTotalHrs(totalHrs);
                    }

                }*/


            } else {
                inoutreference.setTotalHrs(getResources().getString(R.string.dash));
                if (isClearData)
                    listinout.add(inoutreference);
                else
                    listinout.set(i, inoutreference);
            }


        }

        if (dailyInOutAdapter == null) {
            selectedItemPos = -1;
            dailyInOutAdapter = new DailyInOutAdapter(mContext, currentDateStr, listinout, new OnItemClick() {


                @Override
                public void onItemClick(int position) {
                    selectedItemPos = position;
                    Inoutreference inoutreference = listinout.get(position);
                    String previousIn = "";
                    try {

                        if (position > 0) {
                            previousIn = listinout.get(position - 1).getStartTimelbl();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.INOUTPREF, inoutreference);
                    bundle.putSerializable(Constants.INOUTPREF_PREVIOUSTIME, previousIn);
                    ActivityUtils.launchStartActivityForResult(FragmentDaily.this, EditDailyTimeSheet.class,
                            bundle, Constants.RESULT_CODE_EDITTIMESHEET);
                }

                @Override
                public void onItemDelete(int position, String id, String number, String from_time, String to_time) {

                }


                @Override
                public void onButtonClick(boolean isCancle) {

                }

                @Override
                public void onUpdateClick(int position, String id, boolean isUnbook) {

                }


            });
            binding.rvDailyInout.setAdapter(dailyInOutAdapter);

        } else {
            dailyInOutAdapter.setCurrentDate(currentDateStr);
            dailyInOutAdapter.notifyDataSetChanged();
        }
        //long activeMinutes = inOutDBCtrl.getTotalActiveMinute(currentDate);
        //binding.tvTotalhrs.setText(AppUtilities.hmTimeFormatter(totalMillis) + " " + getResources().getString(R.string.hours));

        /*String totalInHrs = AppUtilities.hmTimeFormatterfromMinutesForDaily(activeMinutes);
        binding.tvTotalhrs.setText(totalInHrs + " " + getResources().getString(R.string.hours));*/
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.RESULT_CODE_EDITTIMESHEET) {
            if (data != null && data.getExtras().containsKey(ApiUtils.DATE)) {
                long startDate = data.getExtras().getLong(ApiUtils.DATE, 0);
                if (startDate != 0 && selectedItemPos != -1) {
                    Inoutreference inoutreference = listinout.get(selectedItemPos);
                    inoutreference.setStartTime(startDate);
                    listinout.set(selectedItemPos, inoutreference);

                    //Get previous item if exist, update that item end time as the new updated time
                    int prevousItemPos = selectedItemPos - 1;
                    if (prevousItemPos >= 0 && prevousItemPos < listinout.size() - 1) {
                        Inoutreference previousItem = listinout.get(prevousItemPos);
                        previousItem.setEndTime(startDate);
                        listinout.set(prevousItemPos, previousItem);
                    }
                    setDailyListData(listinout, false);
                }
            }
        }
    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        AppUtilities.showToast(mContext, error);
    }


}
