package com.smartinout.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.http.SslError;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.smartinout.R;
import com.smartinout.databinding.FragmentWebviewBinding;
import com.smartinout.utilities.Constants;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

public class DialogFragmentWebView extends DialogFragment implements View.OnClickListener {

    FragmentWebviewBinding binding;

    @Override
    public int getTheme() {
        return R.style.AppTheme_NoActionBar_FullScreenDialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.fragment_webview, container, false);
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null && !TextUtils.isEmpty(getArguments().getString(Constants.TITLE)))
            binding.layoutToolBar.tvTitle.setText(getArguments().getString(Constants.TITLE));

        binding.layoutToolBar.ivBack.setVisibility(View.VISIBLE);
        binding.layoutToolBar.ivBack.setOnClickListener(this);

        loadWebViewData();
    }

    private void loadWebViewData() {

        binding.progrssbarTandc.setVisibility(View.VISIBLE);

        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.getSettings().setDomStorageEnabled(true);
        binding.webView.clearView();
        binding.webView.measure(100, 100);
        binding.webView.getSettings().setUseWideViewPort(true);
        binding.webView.getSettings().setLoadWithOverviewMode(true);

        binding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                binding.progrssbarTandc.setVisibility(View.GONE);
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                binding.progrssbarTandc.setVisibility(View.GONE);
            }

            @SuppressLint("NewApi")
            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                String message = getResources().getString(R.string.sslcertificaterror);
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = getResources().getString(R.string.certificateautorityisnottrusted);
                        break;
                    case SslError.SSL_EXPIRED:
                        message = getResources().getString(R.string.thecertificateexpired);
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = getResources().getString(R.string.certificatehostnamemismatch);
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = getResources().getString(R.string.thecertificatenotyetvalid);
                        break;
                }
                message += " " + getResources().getString(R.string.doyoucontinueanyway);

                builder.setTitle(getResources().getString(R.string.sslcertificateerror));
                builder.setMessage(message);
                builder.setPositiveButton(getResources().getString(R.string.lbl_continue), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.btnCancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        try {
            binding.webView.loadUrl(getArguments().getString(Constants.URL));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}
