package com.smartinout.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.HomeActivity;
import com.smartinout.R;
import com.smartinout.base.BaseFragment;
import com.smartinout.databinding.FragmentInoutBinding;
import com.smartinout.fragments.inout.FragmentCompany;
import com.smartinout.fragments.inout.FragmentMyInOut;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class InOutFragment extends BaseFragment {

    FragmentInoutBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.fragment_inout, container, false);
        setUpLeftRightBtnClicks();
        initUI();
        return binding.getRoot();
    }

    private void initUI() {
        binding.includeLeftRightBtn.tvLeftBtn.setText(getResources().getString(R.string.myinout));
        binding.includeLeftRightBtn.tvRightBtn.setText(getResources().getString(R.string.company));
        changeFragment(new FragmentMyInOut());
    }

    public void setUpLeftRightBtnClicks() {
        defaultSelection(binding.includeLeftRightBtn.tvLeftBtn);
        clickLeftBtn(binding.includeLeftRightBtn.tvLeftBtn,
                binding.includeLeftRightBtn.tvRightBtn);
        clickRightBtn(binding.includeLeftRightBtn.tvRightBtn,
                binding.includeLeftRightBtn.tvLeftBtn);
    }

    @Override
    public void onLeftBtnClick(View view) {
        super.onLeftBtnClick(view);
        ((HomeActivity) getActivity()).showHeaderSearch(HomeActivity.INDEX_INOUTMY);
        changeFragment(new FragmentMyInOut());
    }

    @Override
    public void onRightBtnClick(View view) {
        super.onRightBtnClick(view);
        ((HomeActivity) getActivity()).showHeaderSearch(HomeActivity.INDEX_INOUTCOM);
        changeFragment(new FragmentCompany());

    }

    public void changeFragment(Fragment fragment) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();
    }
}
