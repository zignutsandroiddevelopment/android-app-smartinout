package com.smartinout.fragments.forgotpwd;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.R;
import com.smartinout.base.BaseFragment;
import com.smartinout.databinding.FragmentResetpwdBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.model.BaseResponse;
import com.smartinout.model.ForgotPassReqPojo;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.AppUtilities;

import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import retrofit2.Response;

public class FragmentResetPwd extends BaseFragment implements View.OnClickListener, ApiResponse {

    FragmentResetpwdBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.fragment_resetpwd, container, false);
        return binding.getRoot();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.includeWidgetBtn.tvBtnTitle.setText(getResources().getString(R.string.submit));
        binding.includeWidgetBtn.tvBtnTitle.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvBtnTitle:
                submitData();
                break;
        }
    }

    private void submitData() {

        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }

        String newPwd = binding.edtNewPass.getText().toString();
        String conNewPwd = binding.edtConNewPassword.getText().toString();

        if (AppUtilities.getValidateString(newPwd).isEmpty()) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.enternewpass));
            return;
        }

        /*if (newPwd.length() < 6) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.pwdshouldgt6));
            return;
        }*/

        if (!AppUtilities.isValidPassword(newPwd)) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.pwdshouldcontainlater));
            return;
        }

        if (AppUtilities.getValidateString(conNewPwd).isEmpty()) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.enterconfnewpass));
            return;
        }

        if (!newPwd.equals(conNewPwd)) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.confnewpwdnotmatch));
            return;
        }

        ForgotPassReqPojo passReqPojo = pref.getForgotPwdReq();
        if (passReqPojo == null) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.noreqforresetpwd));
            return;
        }

        showProgressDialog();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(ApiUtils.EMAIL, passReqPojo.getEmail());
        hashMap.put(ApiUtils.OTP, String.valueOf(passReqPojo.getData().getOtp()));
        hashMap.put(ApiUtils.NEWPWD, newPwd);
        callService.setPwdUser(hashMap, this);
    }

    @Override
    public void onSuccess(Response response) {
        hideProgressDialog();
        BaseResponse parsRes = (BaseResponse) response.body();
        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(getContext(), parsRes.getError());
        }
        getActivity().finish();
    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        AppUtilities.showToast(getContext(), error);
    }
}
