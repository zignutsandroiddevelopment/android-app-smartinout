package com.smartinout.fragments.forgotpwd;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goodiebag.pinview.Pinview;
import com.google.gson.Gson;
import com.smartinout.ForgotPasswordActivity;
import com.smartinout.R;
import com.smartinout.base.BaseFragment;
import com.smartinout.databinding.FragmentPinverifyBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.model.ForgotPassReqPojo;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Prefs;

import java.util.Calendar;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import retrofit2.Response;

public class FragmentEnterPin extends BaseFragment implements View.OnClickListener, ApiResponse {

    FragmentPinverifyBinding binding;
    private String enterCode = "";
    private CountDownTimer countDownTimer;
    private View.OnClickListener clickListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.fragment_pinverify, container, false);
        clickListener = this;
        return binding.getRoot();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.includeWidgetBtn.tvBtnTitle.setText(getResources().getString(R.string.submit));
        binding.includeWidgetBtn.tvBtnTitle.setOnClickListener(this);
        binding.pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                enterCode = pinview.getValue();
            }

        });

        calledTimer();
    }

    public void startTimer(final long max_value) {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
        countDownTimer = new CountDownTimer(max_value, 1000) {
            @Override
            public void onTick(long l) {
                if (getActivity() != null) {
                    try {
                        String minSec = AppUtilities.msTimeFormatter(l);
                        binding.tvResentCodeTimer.setText(getResources().getString(R.string.resentOtptimer) +" "+ minSec );
                        binding.tvResentCode.setTextColor(getResources().getColor(R.color.clrlightGray));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFinish() {
                binding.tvResentCode.setOnClickListener(clickListener);
                binding.tvResentCodeTimer.setText("");
                binding.tvResentCode.setTextColor(getResources().getColor(R.color.colorPrimary));
            }
        }.start();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvBtnTitle:
                verifyOtp();
                break;
            case R.id.tvResentCode:
                resendCode();
                break;
        }
    }

    String email = "";

    private void resendCode() {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        ForgotPassReqPojo reqPojo = pref.getForgotPwdReq();
        if (reqPojo == null) {
            AppUtilities.showToast(getContext(), getString(R.string.noreqforresetpwd));
            return;
        }
        showProgressDialog();
        HashMap<String, String> hashMap = new HashMap<>();
        email = reqPojo.getEmail();
        hashMap.put(ApiUtils.EMAIL, reqPojo.getEmail());
        callService.generatePwd(hashMap, this);
    }

    public void verifyOtp() {

        if (enterCode == null || enterCode.isEmpty()) {
            AppUtilities.showToast(getContext(), getString(R.string.entercode));
            return;
        }

        ForgotPassReqPojo reqPojo = pref.getForgotPwdReq();
        if (reqPojo == null) {
            AppUtilities.showToast(getContext(), getString(R.string.noreqforresetpwd));
            return;
        }

        int otp = reqPojo.getData().getOtp();

        if (enterCode.equals(String.valueOf(otp))) {
            ((ForgotPasswordActivity) getActivity()).changeTitle(getResources().getString(R.string.titleresetpwd));
            final NavController navController
                    = Navigation.findNavController(getActivity(), R.id.nav_host_frag_forgotpwd);
            navController.navigate(R.id.fragment_resetpwd, null);
        } else {
            binding.pinview.clearValue();
            AppUtilities.showToast(getContext(), getString(R.string.enteredinvalidotp));
        }
    }

    @Override
    public void onSuccess(Response response) {
        hideProgressDialog();
        ForgotPassReqPojo parsRes = (ForgotPassReqPojo) response.body();
        if (parsRes == null) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
            return;
        }
        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(getContext(), parsRes.getError());
        }

        long currenttime = Calendar.getInstance().getTimeInMillis();
        parsRes.setTimestemp(currenttime);
        parsRes.setEmail(email);
        parsRes.setTimestemp(Calendar.getInstance().getTimeInMillis());
        pref.save(Prefs.FORGOTPWDREQDATA, new Gson().toJson(parsRes));

        calledTimer();
    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        AppUtilities.showToast(getContext(), error);
    }

    public boolean calledTimer() {
        ForgotPassReqPojo reqPojo = pref.getForgotPwdReq();
        if (reqPojo == null || reqPojo.getData() == null || reqPojo.getData().getOtp() == 0)
            return false;

        long currenttimestemp = Calendar.getInstance().getTimeInMillis();

        long timeDiff = currenttimestemp - reqPojo.getTimestemp();
        int days = (int) (timeDiff / (1000 * 60 * 60 * 24));
        int hours = (int) ((timeDiff - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
        int min = (int) (timeDiff - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);

        if (min <= 1) {
            /*startTimer(900000 - timeDiff);*/
            startTimer(60000 - timeDiff);
            return true;
        }
        return false;
    }
}
