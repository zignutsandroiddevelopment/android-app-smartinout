package com.smartinout.fragments.forgotpwd;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.smartinout.ForgotPasswordActivity;
import com.smartinout.R;
import com.smartinout.base.BaseFragment;
import com.smartinout.databinding.FragmentForgotpasswordBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.model.ForgotPassReqPojo;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.EmailValidator;
import com.smartinout.utilities.Prefs;

import java.util.Calendar;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import retrofit2.Response;

public class FragmentForgotPwd extends BaseFragment implements View.OnClickListener, ApiResponse {

    FragmentForgotpasswordBinding binding;
    Prefs pref;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.fragment_forgotpassword, container, false);
        pref = Prefs.getInstance();
        return binding.getRoot();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.edtEmail.setText(pref.getString(Prefs.EMAIL,""));
        binding.includeWidgetBtn.tvBtnTitle.setText(getResources().getString(R.string.submit));
        binding.includeWidgetBtn.tvBtnTitle.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvBtnTitle:
                submitData();
                break;
        }
    }

    public void submitData() {

        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }

        String emailId = binding.edtEmail.getText().toString();

        if (AppUtilities.getValidateString(emailId).isEmpty()) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.enteryouremail));
            return;
        }

        EmailValidator emailValidator = new EmailValidator();
        if (!emailValidator.validateEmail(emailId)) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.enteryouremail));
            return;
        }

        /*if (checkifReGenReq(emailId)) {
            return;
        }*/

        showProgressDialog();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(ApiUtils.EMAIL, emailId);
        callService.generatePwd(hashMap, this);
    }

    @Override
    public void onSuccess(Response response) {
        hideProgressDialog();
        ForgotPassReqPojo parsRes = (ForgotPassReqPojo) response.body();
        if (parsRes == null) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
            return;
        }
        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(getContext(), parsRes.getError());
        }

        parsRes.setEmail(binding.edtEmail.getText().toString());
        long currenttime = Calendar.getInstance().getTimeInMillis();
        parsRes.setTimestemp(currenttime);
        pref.save(Prefs.FORGOTPWDREQDATA, new Gson().toJson(parsRes));

        redirectTopinentry();
    }

    public void redirectTopinentry() {
        ((ForgotPasswordActivity) getActivity()).changeTitle(getResources().getString(R.string.verification));
        final NavController navController
                = Navigation.findNavController(getActivity(), R.id.nav_host_frag_forgotpwd);
        Bundle bundle = new Bundle();
        navController.navigate(R.id.fragment_pinverify, bundle);
    }

    public boolean checkifReGenReq(String email) {
        ForgotPassReqPojo reqPojo = pref.getForgotPwdReq();
        if (reqPojo == null || reqPojo.getData() == null || reqPojo.getData().getOtp() == 0)
            return false;

        if (!reqPojo.getEmail().equals(email)) {
            return false;
        }

        long currenttimestemp = Calendar.getInstance().getTimeInMillis();

        Log.e("TimeStemp", "" + currenttimestemp + " " + reqPojo.getTimestemp());

        long timeDiff = currenttimestemp - reqPojo.getTimestemp();
        int days = (int) (timeDiff / (1000 * 60 * 60 * 24));
        int hours = (int) ((timeDiff - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
        int min = (int) (timeDiff - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);

        if (min < 1) {
            redirectTopinentry();
            return true;
        }

        return false;
    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        AppUtilities.showToast(getContext(), error);
    }
}
