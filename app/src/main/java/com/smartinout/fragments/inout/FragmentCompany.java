package com.smartinout.fragments.inout;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.R;
import com.smartinout.adapters.CompanyInOutAdapter;
import com.smartinout.base.BaseFragment;
import com.smartinout.databinding.FragmentCompanyBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.loadmore.OnLoadMoreListener;
import com.smartinout.loadmore.RecyclerViewLoadMoreScroll;
import com.smartinout.model.organize.OrganizeData;
import com.smartinout.model.organize.OrganizeModel;
import com.smartinout.model.userinout.ClockedInOutPojo;
import com.smartinout.model.userinout.CompanyInOut;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Prefs;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Response;

public class FragmentCompany extends BaseFragment implements ApiResponse {

    FragmentCompanyBinding binding;
    private CompanyInOutAdapter companyInOutAdapter;
    private List<OrganizeData> listData = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    private RecyclerViewLoadMoreScroll scrollListener;
    private Handler handler = null;
    private int page = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_company, container, false);
        initUI();
        return binding.getRoot();
    }

    private void initUI() {

        handler = new Handler();
        linearLayoutManager = new LinearLayoutManager(getContext());

        binding.rvlistData.setLayoutManager(linearLayoutManager);
//        DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
//        itemDecorator.setDrawable(ContextCompat.getDrawable(CoursesListActivity.this, R.drawable.divider));
//        binding.rvMyLikes.addItemDecoration(itemDecorator);
        androidx.recyclerview.widget.DividerItemDecoration itemDecorator = new androidx.recyclerview.widget.DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL);
        //itemDecorator.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider));
        binding.rvlistData.addItemDecoration(itemDecorator);
        scrollListener = new RecyclerViewLoadMoreScroll(linearLayoutManager);
        scrollListener.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                companyInOutAdapter.addLoadingView();
                page = page + 1;
                getCompanyUser(page, false);
            }
        });
        binding.rvlistData.addOnScrollListener(scrollListener);
        getCompanyUser(page,true);
    }



    private void getCompanyUser(int page,boolean isLoading) {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        showProgressDialog(isLoading);

        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.MOBILE, "true");
        params.put(ApiUtils.PAGENO, String.valueOf(page));
        params.put(ApiUtils.PAGELIMIT, "20");
        callService.getOrganizationData(params,this);
    }

    @Override
    public void onSuccess(Response response) {
        removeLoadmoreview();
        hideProgressDialog();
        OrganizeModel parsRes = (OrganizeModel) response.body();

        if (parsRes == null) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
            return;
        }

        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(getContext(), parsRes.getError());
        }

       /* List<OrganizeData> userData = parsRes.getData();
        if(userData != null){
            if(userData.size() != 0){
                page = page - 1;
            }
        }*/


        if (page == 0) {
            listData.clear();
        }
        String loginUsername = pref.getString(ApiUtils.FIRSTNAME,"") + " " +pref.getString(ApiUtils.LASTNAME,"");

        listData.addAll(parsRes.getData());
        setLoadMoreForScroll();
        setUserData();

    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        removeLoadmoreview();
        setLoadMoreForScroll();
        if (page > 0) {
            page = page - 1;
            return;
        }
        if (page == 0) {
            AppUtilities.showToast(getActivity(),error);
        }


    }

    private void setUserData() {
        if(page >0){
            companyInOutAdapter.notifyDataSetChanged();
        }else {
            companyInOutAdapter = new CompanyInOutAdapter(getContext(), listData);
            binding.rvlistData.setAdapter(companyInOutAdapter);
        }
    }


    /**
     * This method is set loading variable of scroll listner false for load
     * more data.
     */
    private void setLoadMoreForScroll() {
        if (scrollListener != null) {
            scrollListener.setLoaded();
        }
    }
    /**
     * If page counter is >1 is remove loading indicator.
     * If page counter is 1 then remove the list data
     * and fill it again
     */
    private void removeLoadmoreview() {
        if (page > 0 && companyInOutAdapter != null) {
            companyInOutAdapter.removeLoadingView();
        } else {
            listData.clear();
        }
    }
}
