package com.smartinout.fragments.inout;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.smartinout.AddDailyTimeSheet;
import com.smartinout.HomeActivity;
import com.smartinout.R;
import com.smartinout.ScannerActivity;
import com.smartinout.base.BaseFragment;
import com.smartinout.databinding.FragmentMyinoutBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.interfaces.OnItemClick;
import com.smartinout.model.login.OnlineUserReference;
import com.smartinout.model.timesheet.Activities;
import com.smartinout.model.userinout.ClockedInOutPojo;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.AlertDialog;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;
import com.smartinout.utilities.ImageDisplayUitls;
import com.smartinout.utilities.Prefs;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import retrofit2.Response;

import static com.smartinout.utilities.AppUtilities.getDayHour;

public class FragmentMyInOut extends BaseFragment implements View.OnClickListener
        , RadioGroup.OnCheckedChangeListener, ApiResponse, OnItemClick {

    FragmentMyinoutBinding binding;
    private String inOutStatus = ApiUtils.IN;
    private String currentDate, myInoutTime,myInoutTimeWithStatus, username;
    private Prefs prefs;
    private boolean isClockedIn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_myinout, container, false);
        prefs = Prefs.getInstance();
        initUI();
        setData();
        getLastupdatedStatus();
        return binding.getRoot();
    }

    private void initUI() {
        binding.includeWidgetBtn.tvBtnTitle.setText(getResources().getString(R.string.update));
        binding.radioGroupMyInOut.check(R.id.rdIn);
        binding.radioGroupMyInOut.setOnCheckedChangeListener(this);
        /*binding.linearWidgetButton.setOnClickListener(this);*/

    }

    private void setData() {

        isClockedIn = prefs.getBoolean(Prefs.ISCLOKCEDIN, false);
        myInoutTime = prefs.getString(Prefs.TODAYINTIME, "");
        myInoutTimeWithStatus = prefs.getString(Prefs.TODAYINTIMEWITHSTATUS, "");
        currentDate = AppUtilities.getCurrentDateEEEEddMMMM();
        String firstname = prefs.getString(ApiUtils.FIRSTNAME, "");
        String lastname = prefs.getString(ApiUtils.LASTNAME, "");
        String profileImg = String.valueOf(firstname.charAt(0)) + String.valueOf(lastname.charAt(0));
        if (!prefs.getString(ApiUtils.PROFILE_URL, "").isEmpty())
        {
            ImageDisplayUitls.displayImage(prefs.getString(ApiUtils.PROFILE_URL, ""), getContext(), binding.ivUserIcon);
        }
        else
        {
            ImageDisplayUitls.displayTextDrawable(profileImg, binding.ivUserIcon
                    ,getActivity().getResources().getColor(android.R.color.white),
                    getActivity().getResources().getColor(R.color.colorPrimary));
        }
   

        if (isClockedIn) {

            binding.tvSuggestion.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
            binding.tvSuggestion.setText(getActivity().getResources().getString(R.string.clockin_suggestion));

            if (myInoutTimeWithStatus.equalsIgnoreCase(getActivity().getResources().getString(R.string.cloclkedoff))) {
                binding.includeWidgetBtn.tvBtnTitle.setBackgroundColor(getActivity().getResources().getColor(R.color.clrVehicleInout));
                binding.tvUserInTime.setTextColor(getActivity().getResources().getColor(R.color.clrred));
            } else {
                binding.linearWidgetButton.setOnClickListener(this);
                binding.tvUserInTime.setTextColor(getActivity().getResources().getColor(R.color.clrgreen));
            }

            if(myInoutTimeWithStatus.contains("IN")){
                binding.tvUserInTime.setTextColor(getActivity().getResources().getColor(R.color.clrgreen));
            }else if(myInoutTimeWithStatus.contains("OUT")){
                binding.tvUserInTime.setTextColor(getActivity().getResources().getColor(R.color.orange));
            }else if(myInoutTimeWithStatus.contains("Break")){
                binding.tvUserInTime.setTextColor(getActivity().getResources().getColor(R.color.clrYellow));
            }

            binding.tvUserInTime.setText(myInoutTimeWithStatus);
            binding.tvUserInTimeDate.setText(currentDate + ", " + myInoutTime);
        } else {
            binding.includeWidgetBtn.tvBtnTitle.setBackgroundColor(getActivity().getResources().getColor(R.color.clrVehicleInout));
            binding.tvSuggestion.setTextColor(getActivity().getResources().getColor(R.color.clrred));
            binding.tvSuggestion.setText(getActivity().getResources().getString(R.string.needs_toclockin));

            if (myInoutTimeWithStatus.equalsIgnoreCase(getActivity().getResources().getString(R.string.cloclkedoff))) {
                binding.tvUserInTime.setTextColor(getActivity().getResources().getColor(R.color.clrred));
            } else {
                binding.tvUserInTime.setTextColor(getActivity().getResources().getColor(R.color.clrgreen));
            }
            binding.tvUserInTime.setText(myInoutTimeWithStatus);
        }




    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.rdIn) {
            inOutStatus = ApiUtils.IN;
        } else if (checkedId == R.id.rdOut) {
            inOutStatus = ApiUtils.OUT;
        } else {
            AppUtilities.showToast(getActivity(), getResources().getString(R.string.err_status));
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linearWidgetButton:


                if (binding.edtComment.getText().toString().length() == 0) {
                    AppUtilities.showToast(getActivity(), getActivity().getResources().getString(R.string.err_comment));
                    return;
                } else {
                    if (inOutStatus == ApiUtils.IN) {
                        HashMap<String, String> params = new HashMap<>();
                        params.put(ApiUtils.STATUS, ApiUtils.IN);
                        params.put(ApiUtils.COMMENT, binding.edtComment.getText().toString());
                        clockedIn(params);
                    } else {
                        HashMap<String, String> params = new HashMap<>();
                        params.put(ApiUtils.STATUS, ApiUtils.OUT);
                        params.put(ApiUtils.COMMENT, binding.edtComment.getText().toString());
                        clockedOff(params);
                    }


                    /*new AlertDialog(getActivity(), this).showCancelAlertDialog(
                            this.getResources().getString(R.string.inouttitle),
                            this.getResources().getColor(R.color.clrgreen),
                            this.getResources().getString(R.string.inoutmessage));*/

                }

                break;
        }
    }

    private void clockedIn(HashMap<String, String> params) {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        showProgressDialog();
        callService.updateStatus(params, this);
    }

    private void clockedOff(HashMap<String, String> params) {
        if (!AppUtilities.hasInternet(getContext(), true)) {
            return;
        }
        showProgressDialog();
        callService.updateStatus(params, this);
    }

    @Override
    public void onSuccess(Response response) {
        hideProgressDialog();
        binding.edtComment.setText("");
        ClockedInOutPojo parsRes = (ClockedInOutPojo) response.body();

        if (parsRes == null) {
            AppUtilities.showToast(getContext(), getResources().getString(R.string.invalidresposne));
            return;
        }

        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(getContext(), parsRes.getError());
        }
        AppUtilities.showToast(getContext(), getResources().getString(R.string.statusupdatesuccess));

        Intent localIntent = new Intent(Constants.ACTION_STATUSCHANGED);
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getActivity());
        localBroadcastManager.sendBroadcast(localIntent);

    }

    @Override
    public void onFailure(String error) {
        binding.edtComment.setText("");
        hideProgressDialog();
        AppUtilities.showToast(getContext(), error);
    }

    @Override
    public void onButtonClick(boolean isCancle) {
        if (!isCancle) {
            if (inOutStatus == ApiUtils.IN) {
                HashMap<String, String> params = new HashMap<>();
                params.put(ApiUtils.STATUS, ApiUtils.CIN);
                params.put(ApiUtils.COMMENT, binding.edtComment.getText().toString());
                clockedIn(params);
            } else {
                HashMap<String, String> params = new HashMap<>();
                params.put(ApiUtils.STATUS, ApiUtils.COUT);
                params.put(ApiUtils.COMMENT, binding.edtComment.getText().toString());
                clockedOff(params);
            }
        }

    }

    private void getLastupdatedStatus() {
        if (!AppUtilities.hasInternet(getActivity(), true)) {
            return;
        }
        //showProgressDialog();
        callService.getOnlineStatus(onlineResponse);
    }


    public ApiResponse onlineResponse = new ApiResponse() {
        @Override
        public void onSuccess(Response response) {

            //hideProgressDialog();

            OnlineUserReference parsRes = (OnlineUserReference) response.body();

            if (parsRes == null) {
                hideProgressDialog();
                AppUtilities.showToast(getActivity(), getResources().getString(R.string.invalidresposne));
                return;
            }

            if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
                hideProgressDialog();
                AppUtilities.showToast(getActivity(), parsRes.getError());
            }

            try {
                if (parsRes.getOnlineData() != null) {

                    long inoutcreatedtime = parsRes.getOnlineData().getStartTime();
                    long lastupdatedtime = parsRes.getOnlineData().getCreatedInoutTime();
                    String secondaryStatus = parsRes.getOnlineData().getSecondryStatus();
                    String mainstatus = parsRes.getOnlineData().getMainStatus();
                    if (AppUtilities.getValidateString(mainstatus).equals(ApiUtils.CIN)) {// Main Status Clocked In
                        prefs.save(Prefs.ISCLOKCEDIN, true);

                        Calendar calendar = Calendar.getInstance();

                        if (AppUtilities.getValidateString(secondaryStatus).equals(ApiUtils.BI)) {// Break In
                            calendar.setTimeInMillis(lastupdatedtime);
                        } else {
                            calendar.setTimeInMillis(inoutcreatedtime);
                        }

                        String hrsMns = getDayHour(getActivity(), calendar.get(Calendar.HOUR_OF_DAY)) + ":" +
                                String.format("%02d", calendar.get(Calendar.MINUTE));

                        String inoutTime = "";
                        if (calendar.get(Calendar.AM_PM) == Calendar.AM) {
                            inoutTime = hrsMns + " AM";
                        } else {
                            inoutTime = hrsMns + " PM";
                        }
                        if (AppUtilities.getValidateString(secondaryStatus).equals(ApiUtils.BI)) {// Break In
                            prefs.save(Prefs.TODAYINTIMEWITHSTATUS, "Break " + inoutTime);
                        } else if (AppUtilities.getValidateString(secondaryStatus).equals(ApiUtils.OUT)) {// Break In
                            prefs.save(Prefs.TODAYINTIMEWITHSTATUS, "OUT " + inoutTime);
                        } else {
                            prefs.save(Prefs.TODAYINTIMEWITHSTATUS, "IN " + inoutTime);
                        }
                        prefs.save(Prefs.TODAYINTIME, inoutTime);

                    } else {
                        prefs.save(Prefs.ISCLOKCEDIN, false);
                    }
                } else {
                    prefs.save(Prefs.ISCLOKCEDIN, false);
                }
                setData();
            }catch (Exception e){
                e.printStackTrace();
            }



        }

        @Override
        public void onFailure(String error) {
            hideProgressDialog();
            AppUtilities.showToast(getActivity(), error);
        }
    };

    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void onItemDelete(int position, String id, String number, String from_time, String to_time) {

    }

    @Override
    public void onUpdateClick(int position, String id, boolean isUnbook) {

    }
}
