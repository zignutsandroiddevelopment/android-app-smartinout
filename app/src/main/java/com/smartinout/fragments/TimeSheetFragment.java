package com.smartinout.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.HomeActivity;
import com.smartinout.R;
import com.smartinout.base.BaseFragment;
import com.smartinout.databinding.FragmentTimesheetBinding;
import com.smartinout.fragments.timesheets.FragmentDaily;
import com.smartinout.fragments.timesheets.FragmentWeekly;
import com.smartinout.interfaces.OnWeeklyItemClick;
import com.smartinout.model.timesheet.CalendarModel;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class TimeSheetFragment extends BaseFragment implements OnWeeklyItemClick {

    FragmentTimesheetBinding binding;

    @Override
    public void onClickDailyView(CalendarModel model) {
        ((HomeActivity) getActivity()).showHeaderSearch(HomeActivity.INDEX_TIMESHEET_DAILY);
        defaultSelection(binding.includeLeftRightBtn.tvLeftBtn, binding.includeLeftRightBtn.tvRightBtn);

        FragmentDaily fragmentDaily = new FragmentDaily();
        Bundle bundle = new Bundle();
        bundle.putString(ApiUtils.DATE, model.getWeeklydates());
        bundle.putInt(ApiUtils.CURRENTDATE, model.getCurrentDate());
        fragmentDaily.setArguments(bundle);
        changeFragment(fragmentDaily);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.fragment_timesheet, container, false);
        setUpLeftRightBtnClicks();
        initUI();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
                new IntentFilter(Constants.ACTION_REFRESHTIMESHEET));
        return binding.getRoot();
    }

    private void initUI() {
        binding.includeLeftRightBtn.tvLeftBtn.setText(getResources().getString(R.string.daily));
        binding.includeLeftRightBtn.tvRightBtn.setText(getResources().getString(R.string.weekly));
        changeFragment(new FragmentDaily());
    }

    public void setUpLeftRightBtnClicks() {
        defaultSelection(binding.includeLeftRightBtn.tvLeftBtn);
        clickLeftBtn(binding.includeLeftRightBtn.tvLeftBtn,
                binding.includeLeftRightBtn.tvRightBtn);
        clickRightBtn(binding.includeLeftRightBtn.tvRightBtn,
                binding.includeLeftRightBtn.tvLeftBtn);
    }

    @Override
    public void onLeftBtnClick(View view) {
        super.onLeftBtnClick(view);
        ((HomeActivity) getActivity()).showHeaderSearch(HomeActivity.INDEX_TIMESHEET_DAILY);
        changeFragment(new FragmentDaily());
    }

    @Override
    public void onRightBtnClick(View view) {
        super.onRightBtnClick(view);
        ((HomeActivity) getActivity()).showHeaderSearch(HomeActivity.INDEX_TIMESHEET_WEEKLY);
        changeFragment(new FragmentWeekly().getInitiate(this));

    }

    public void changeFragment(Fragment fragment) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.timesheetcontainer, fragment);
        //transaction.commit();
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            if(intent.getAction().equalsIgnoreCase(Constants.ACTION_REFRESHTIMESHEET)){
                changeFragment(new FragmentDaily());
            }
        }
    };
}
