package com.smartinout.livedatamodel;

import android.app.Application;


import com.smartinout.model.vehiclebooking.AvailableData;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

public class VehicleViewModel extends AndroidViewModel {

    private MutableLiveData<ArrayList<AvailableData>> listData;

    public VehicleViewModel(@NonNull Application application) {
        super(application);
    }

    public MutableLiveData<ArrayList<AvailableData>> getData() {
        if (listData == null) {
            listData = new MutableLiveData<>();
            //loadData();
        }
        return listData;
    }

    public AvailableData getItem(int position) {
        return listData.getValue().get(position);
    }

    /*private void loadData() {

        ArrayList<AvailableData> listData = new ArrayList<>();

        VehicleInOut vehicleIn = new VehicleInOut();
        vehicleIn.setmTitle("Vehicle No. 15489");
        listData.add(vehicleIn);

        vehicleIn = new VehicleInOut();
        vehicleIn.setmTitle("Vehicle No. 15489");
        listData.add(vehicleIn);

        this.listData.setValue(listData);
    }
*/
    public void addAll() {
        this.listData.setValue(listData.getValue());
    }
}
