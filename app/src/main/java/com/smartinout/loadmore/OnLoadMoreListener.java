package com.smartinout.loadmore;

public interface OnLoadMoreListener {
    void onLoadMore();
}