package com.smartinout;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import com.smartinout.base.BaseActivity;
import com.smartinout.databinding.ActivityAdddailytimesheetBinding;
import com.smartinout.databinding.ActivityEditdailytimesheetBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.interfaces.OnItemClick;
import com.smartinout.model.login.Inoutreference;
import com.smartinout.model.timesheet.edittimesheet.EditTimeSheetModel;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;
import com.smartinout.utilities.VehicleTimePickerDialog;

import java.util.Calendar;
import java.util.HashMap;

import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import retrofit2.Response;

public class AddDailyTimeSheet extends BaseActivity implements View.OnClickListener,RadioGroup.OnCheckedChangeListener, ApiResponse {

    ActivityAdddailytimesheetBinding binding;
    private VehicleTimePickerDialog timePickerDialog;
    private DatePickerDialog datePickerDialog;
    private int fromYear, fromMonth, fromDay, fromHours, fromMinute;
    private String from_date, from_time;
    private String inOutStatus = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(AddDailyTimeSheet.this, R.layout.activity_adddailytimesheet);
        initiateUI();
    }

    @Override
    protected void initiateUI() {


        binding.layoutToolBarInclude.tvTitle.setText(getResources().getString(R.string.addtimesheet));
        binding.layoutToolBarInclude.ivBack.setVisibility(View.VISIBLE);
        binding.layoutToolBarInclude.ivBack.setOnClickListener(this);

        binding.includeWidgetBtn.tvBtnCancel.setText(getResources().getString(R.string.cancel));
        binding.includeWidgetBtn.tvBtnSubmit.setText(getResources().getString(R.string.add));

        setInitialFromToDate();
        binding.includeWidgetBtn.tvBtnCancel.setOnClickListener(this);
        binding.includeWidgetBtn.tvBtnSubmit.setOnClickListener(this);


        binding.radioGroupAddTimeSheet.check(R.id.rdAIn);
        binding.tvADate.setOnClickListener(this);
        binding.tvAinTime.setOnClickListener(this);
        binding.tvASenttoHr.setOnClickListener(this);
        //binding.tvADate.setOnClickListener(this);
        binding.tvAinTime.setOnClickListener(this);
        binding.radioGroupAddTimeSheet.setOnCheckedChangeListener(this);

    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:
                AddDailyTimeSheet.this.finish();
                break;

            case R.id.tvBtnCancel:
                AddDailyTimeSheet.this.finish();
                break;

            case R.id.tvBtnSubmit:
                addTimeSheet();
                break;
            case R.id.tvAinTime:
                timePickerDialog = new VehicleTimePickerDialog(AddDailyTimeSheet.this, R.style.MyCalendarTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        fromHours = hourOfDay;
                        fromMinute = minute;
                        binding.tvAinTime.setText(AppUtilities.getTime(hourOfDay, minute));
                    }
                }, fromHours, fromMinute, false);
                timePickerDialog.show();
                break;
            case R.id.tvADate:
                datePickerDialog = new DatePickerDialog(AddDailyTimeSheet.this, R.style.MyCalendarTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        fromYear = year;
                        fromMonth = monthOfYear;
                        fromDay = dayOfMonth;
                        from_date = fromDay + "/" + (fromMonth + 1) + "/" + fromYear;
                        binding.tvADate.setText(from_date);
                    }
                }, fromYear, fromMonth, fromDay);
                //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
                break;

        }
    }

    public void setInitialFromToDate() {

        Calendar c = Calendar.getInstance();

        fromYear = c.get(Calendar.YEAR);
        fromMonth = c.get(Calendar.MONTH);
        fromDay = c.get(Calendar.DAY_OF_MONTH);

        from_date = fromDay + "/" + (fromMonth + 1) + "/" + fromYear;

        fromHours = c.get(Calendar.HOUR_OF_DAY);
        fromMinute = c.get(Calendar.MINUTE);

        String amPMFrom = c.get(Calendar.AM_PM) == Calendar.AM ? "AM" : "PM";

        from_date = fromDay + "/" + (fromMonth + 1) + "/" + fromYear;
        from_time = String.format("%02d:%02d", fromHours, fromMinute)+ " " + amPMFrom;//fromHours + ":" + fromMinute + " " + amPMFrom;

        binding.tvADate.setText(from_date);//AppUtilities.getCurrentDateEEEEddMMMMYYYY(c.getTimeInMillis()));// + " " + from_time);
        binding.tvAinTime.setText(from_time);
        inOutStatus = ApiUtils.IN;

    }

    private void addTimeSheet() {
        try {
            if (!AppUtilities.hasInternet(AddDailyTimeSheet.this, true)) {
                return;
            }

            if (fromHours == 0 && fromMinute == 0) {
                AppUtilities.showToast(AddDailyTimeSheet.this, getResources().getString(R.string.nochangeindate));
                return;
            }

            String comment = binding.edtComment.getText().toString();
            if (comment.length() == 0) {
                AppUtilities.showToast(AddDailyTimeSheet.this, getResources().getString(R.string.err_comment));
                return;
            }

            String currentDate = from_date;//AppUtilities.getCurrentDateDDMMYYYY();
            showProgressDialog();
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(ApiUtils.FROM_TIME, String.valueOf(fromHours + ":" + fromMinute));
            hashMap.put(ApiUtils.DATE, currentDate);//AppUtilities.getCurrentDateddMMyyyy(inoutreference.getStartTime()));
            hashMap.put(ApiUtils.STATUS, inOutStatus);
            hashMap.put(ApiUtils.COMMENT, comment);

            callService.addTimeSheet(hashMap, this);


        }catch (Exception e){
            e.printStackTrace();
        }


    }


    @Override
    public void onSuccess(Response response) {
        hideProgressDialog();


        EditTimeSheetModel parsRes = (EditTimeSheetModel) response.body();

        if (parsRes == null) {
            AppUtilities.showToast(AddDailyTimeSheet.this, getResources().getString(R.string.invalidresposne));
            return;
        }

        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(AddDailyTimeSheet.this, parsRes.getError());
        }else {
            AppUtilities.showToast(AddDailyTimeSheet.this,getResources().getString(R.string.timesheetadd));
            sendMessage();
            finish();


        }
    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        AppUtilities.showToast(AddDailyTimeSheet.this, error);
    }

    private void sendMessage() {
        Intent intent = new Intent(Constants.ACTION_REFRESHTIMESHEET);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if(checkedId == R.id.rdAIn) {
            inOutStatus = ApiUtils.IN;
        } else if(checkedId == R.id.rdAOut) {
            inOutStatus = ApiUtils.OUT;
        } else if(checkedId == R.id.rdACIn) {
            inOutStatus = ApiUtils.CIN;
        } else if(checkedId == R.id.rdACOut) {
            inOutStatus = ApiUtils.COUT;
        } else if(checkedId == R.id.rdABIn) {
            inOutStatus = ApiUtils.BI;
        } else if(checkedId == R.id.rdABOut) {
            inOutStatus = ApiUtils.BS;
        } else {
            AppUtilities.showToast(AddDailyTimeSheet.this, getResources().getString(R.string.err_status));
        }

    }
}
