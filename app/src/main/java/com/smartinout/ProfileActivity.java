package com.smartinout;

import android.os.Bundle;
import android.view.View;

import com.smartinout.base.BaseActivity;
import com.smartinout.databinding.ActivityProfileBinding;
import com.smartinout.imagepicker.FilePickUtils;
import com.smartinout.imageuploadtos3.ImageUploadServer;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.model.login.LoginPojo;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;
import com.smartinout.utilities.ImageDisplayUitls;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.HashMap;

import androidx.databinding.DataBindingUtil;
import retrofit2.Response;

public class ProfileActivity extends BaseActivity implements View.OnClickListener, ApiResponse, FilePickUtils.OnFileChoose, ImageUploadServer.SetFileUpload {

    ActivityProfileBinding binding;
    private File imgFile;
    //Image Upload
    private ImageUploadServer imageUploadServer;
    private boolean isImgUpload = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(ProfileActivity.this, R.layout.activity_profile);
        imageUploadServer = new ImageUploadServer(ProfileActivity.this
                , Constants.AMAZONE_SECRET
                , Constants.AMAZONE_KEY
                , ProfileActivity.this);
        imageUploadServer.initUploadFileSrvice();
        initiateUI();
    }

    @Override
    protected void initiateUI() {

        isEditProfile(true);

        binding.layoutToolBarInclude.ivBack.setVisibility(View.VISIBLE);
        binding.layoutToolBarInclude.ivBack.setOnClickListener(this);

        binding.layoutToolBarInclude.ivIconRight.setVisibility(View.GONE);
        binding.layoutToolBarInclude.ivIconRight.setImageResource(R.drawable.ic_edit_white);
        binding.layoutToolBarInclude.ivIconRight.setOnClickListener(this);

        binding.layoutEditProfile.includeWidgetBtn.tvBtnSubmit.setText(getResources().getString(R.string.save));
        binding.layoutEditProfile.includeWidgetBtn.tvBtnSubmit.setOnClickListener(this);

        binding.layoutEditProfile.includeWidgetBtn.tvBtnCancel.setText(getResources().getString(R.string.cancel));
        binding.layoutEditProfile.includeWidgetBtn.tvBtnCancel.setOnClickListener(this);

        binding.ivUserIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:
                if (isImgUpload) {
                    setResult(RESULT_OK);
                }
                ProfileActivity.this.finish();
                break;
            case R.id.ivIconRight:
                isEditProfile(true);
                break;
            case R.id.tvBtnCancel:
                //isEditProfile(false);
                ProfileActivity.this.finish();
                break;
            case R.id.tvBtnSubmit:
                submitData();
                break;
            case R.id.ivUserIcon:
                if (binding.rlEditProfile.getVisibility() == View.VISIBLE)
                    showImagePickerDialog(ProfileActivity.this);
                break;
        }
    }

    public void submitData() {

        String fname = binding.layoutEditProfile.edtFname.getText().toString();
        String lname = binding.layoutEditProfile.edtLname.getText().toString();
        String mobile = binding.layoutEditProfile.edtMoblie.getText().toString();

        if (AppUtilities.getValidateString(fname).isEmpty()) {
            AppUtilities.showToast(ProfileActivity.this, getResources().getString(R.string.enterfname));
            return;
        }

        if (AppUtilities.getValidateString(lname).isEmpty()) {
            AppUtilities.showToast(ProfileActivity.this, getResources().getString(R.string.enterlname));
            return;
        }

        if (AppUtilities.getValidateString(mobile).isEmpty()) {
            AppUtilities.showToast(ProfileActivity.this, getResources().getString(R.string.entermobile));
            return;
        }

        if (mobile.length() != 10) {
            AppUtilities.showToast(ProfileActivity.this, getResources().getString(R.string.mobilenumbershould10digit));
            return;
        }

        if (imgFile != null && imgFile.exists()) {
            showProgressDialog();
            imageUploadServer.uploadFileOnS3(imgFile.getPath(), Constants.S3_PROFILE_BUCKET, false);

        } else {
            showProgressDialog();
            submitDatatoServer("");
        }
    }

    public void submitDatatoServer(String imgUrl) {

        String fname = binding.layoutEditProfile.edtFname.getText().toString();
        String lname = binding.layoutEditProfile.edtLname.getText().toString();
        String mobile = binding.layoutEditProfile.edtMoblie.getText().toString();

        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.FIRSTNAME, fname);
        params.put(ApiUtils.LASTNAME, lname);
        params.put(ApiUtils.PHONE, mobile);
        if (!imgUrl.isEmpty()) {
            params.put(ApiUtils.PROFILE_URL, imgUrl);
        }
        params.put(ApiUtils.ID, prefs.getString(ApiUtils.ID, ""));
        callService.editProfile(params, this);
    }

    public void isEditProfile(boolean isEditProfile) {
        if (!isEditProfile) {
            binding.layoutToolBarInclude.tvTitle.setText(getResources().getString(R.string.profile));
            binding.layoutToolBarInclude.ivIconRight.setVisibility(View.VISIBLE);
            binding.rlEditProfile.setVisibility(View.GONE);
            displayData();
        } else {
            binding.layoutToolBarInclude.tvTitle.setText(getResources().getString(R.string.editprofile));
            binding.layoutToolBarInclude.ivIconRight.setVisibility(View.GONE);
            binding.rlEditProfile.setVisibility(View.VISIBLE);
            fillData();
        }
    }

    @Override
    public void onSuccess(Response response) {
        hideProgressDialog();

        LoginPojo parsRes = (LoginPojo) response.body();

        if (parsRes == null) {
            AppUtilities.showToast(ProfileActivity.this, getResources().getString(R.string.invalidresposne));
            return;
        }

        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(ProfileActivity.this, parsRes.getError());
        }

        prefs.saveUserData(parsRes);

        isEditProfile(false);
    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        AppUtilities.showToast(ProfileActivity.this, error);
    }

    public void displayData() {
        String firstname = prefs.getString(ApiUtils.FIRSTNAME, "");
        String lastname = prefs.getString(ApiUtils.LASTNAME, "");
        binding.tvuserFullname.setText(AppUtilities.getUserFullName(firstname, lastname));

        String email = prefs.getString(ApiUtils.EMAIL, "");
        binding.tvuserEmail.setText(AppUtilities.getValidateString(email));

        String phone = prefs.getString(ApiUtils.PHONE, "");
        binding.tvPhoneNumber.setText(AppUtilities.getValidateString(phone));

        /*String profileImg = prefs.getString(ApiUtils.PROFILE_URL, "");*/

        if (!prefs.getString(ApiUtils.PROFILE_URL, "").isEmpty())
        {
            ImageDisplayUitls.displayImage(prefs.getString(ApiUtils.PROFILE_URL, ""), ProfileActivity.this, binding.ivUserIcon);
        }
        else
        {
            String profileImg = String.valueOf(firstname.charAt(0)) + String.valueOf(lastname.charAt(0));
            ImageDisplayUitls.displayTextDrawable(profileImg, binding.ivUserIcon,
                    getActivity().getResources().getColor(R.color.colorPrimary),
                    getActivity().getResources().getColor(android.R.color.white));
        }
    }

    public void fillData() {
        String firstname = prefs.getString(ApiUtils.FIRSTNAME, "");
        binding.layoutEditProfile.edtFname.setText(StringUtils.capitalize(AppUtilities.getValidateString(firstname)));

        String lastname = prefs.getString(ApiUtils.LASTNAME, "");
        binding.layoutEditProfile.edtLname.setText(StringUtils.capitalize(AppUtilities.getValidateString(lastname)));

        String email = prefs.getString(ApiUtils.EMAIL, "");
        binding.layoutEditProfile.edtEmail.setText(email);
        binding.layoutEditProfile.edtEmail.setEnabled(false);

        String phone = prefs.getString(ApiUtils.PHONE, "");
        binding.layoutEditProfile.edtMoblie.setText(AppUtilities.getValidateString(phone));

        if (!prefs.getString(ApiUtils.PROFILE_URL, "").isEmpty())
        {
            ImageDisplayUitls.displayImage(prefs.getString(ApiUtils.PROFILE_URL, ""), ProfileActivity.this, binding.ivUserIcon);
        }
        else
        {
            String profileImg = String.valueOf(firstname.charAt(0)) + String.valueOf(lastname.charAt(0));
            ImageDisplayUitls.displayTextDrawable(profileImg, binding.ivUserIcon,
                    getActivity().getResources().getColor(R.color.colorPrimary),
                    getActivity().getResources().getColor(android.R.color.white));
        }

    }

    @Override
    public void onFileChoose(String fileUri, int requestCode) {
        dismissImagePickerDialog();
        imgFile = new File(fileUri + "");
        if (imgFile.exists()) {
            ImageDisplayUitls.displayImage(fileUri, ProfileActivity.this, binding.ivUserIcon, R.drawable.ic_user_placeholder);
        }
    }

    @Override
    public void onFileUploadSuccess(String fileName) {
        isImgUpload = true;
        submitDatatoServer(fileName);
    }

    @Override
    public void onFileUploadFail(String error) {
        hideProgressDialog();
        AppUtilities.showToast(ProfileActivity.this, error);
    }

    @Override
    public void onFileUploadProgress(float progress) {

    }

    @Override
    public void onBackPressed() {
        if (isImgUpload) {
            setResult(RESULT_OK);
        }
        super.onBackPressed();
    }
}
