package com.smartinout.model;

public class MessageEvent {

    public int mMessage;

    public MessageEvent(int message) {
        mMessage = message;
    }

    public int getMessage() {
        return mMessage;
    }

    public void setmMessage(int mMessage) {
        this.mMessage = mMessage;
    }
}