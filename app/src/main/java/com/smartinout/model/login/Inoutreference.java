
package com.smartinout.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import java.io.Serializable;

public class Inoutreference implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("status")
    @Expose
    private String secondryStatus;
    @SerializedName("main_status")
    @Expose
    private String mainStatus;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("due_back")
    @Expose
    private String dueBack;
    @SerializedName("createdInoutTime")
    @Expose
    private long createdInoutTime;
    @SerializedName("updatedInoutTime")
    @Expose
    private long updatedInoutTime;
    @SerializedName("diffrence_in_miliseconds")
    @Expose
    private long diffrenceInMiliseconds;

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("updatedBy")
    @Expose
    private Object updatedBy;

    @SerializedName("start_time")
    @Expose
    private long startTime;
    @SerializedName("end_time")
    @Expose
    private long endTime;
    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("quickpick_id")
    @Expose
    private String quickpick_id;
    @SerializedName("quickpic_name")
    @Expose
    private String quickpick_name;
    @SerializedName("vehical_inout")
    @Expose
    private String vehical_inout;
    @SerializedName("is_clockedOut")
    @Expose
    private boolean isClockedOut;

    public boolean isClockedIn() {
        return isClockedIn;
    }

    public void setClockedIn(boolean clockedIn) {
        isClockedIn = clockedIn;
    }

    @SerializedName("is_clockedIn")
    @Expose
    private boolean isClockedIn;


    private String startTimelbl;
    private String endTimelbl;
    private String totalHrs;

    private final static long serialVersionUID = -7511365563605571668L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSecondryStatus() {
        return secondryStatus;
    }

    public void setSecondryStatus(String status) {
        this.secondryStatus = secondryStatus;
    }

    public String getMainStatus() {
        return mainStatus;
    }

    public void setMainStatus(String mainStatus) {
        this.mainStatus = mainStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDueBack() {
        return dueBack;
    }

    public void setDueBack(String dueBack) {
        this.dueBack = dueBack;
    }

    public long getCreatedInoutTime() {
        return createdInoutTime;
    }

    public void setCreatedInoutTime(Integer createdInoutTime) {
        this.createdInoutTime = createdInoutTime;
    }

    public long getUpdatedInoutTime() {
        return updatedInoutTime;
    }

    public void setUpdatedInoutTime(Integer updatedInoutTime) {
        this.updatedInoutTime = updatedInoutTime;
    }
    public long getDiffrenceInMiliseconds() {
        return diffrenceInMiliseconds;
    }

    public void setDiffrenceInMiliseconds(long diffrenceInMiliseconds) {
        this.diffrenceInMiliseconds = diffrenceInMiliseconds;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Object getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Object updatedBy) {
        this.updatedBy = updatedBy;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getStartTimelbl() {
        return startTimelbl;
    }

    public void setStartTimelbl(String startTimelbl) {
        this.startTimelbl = startTimelbl;
    }

    public String getEndTimelbl() {
        return endTimelbl;
    }

    public void setEndTimelbl(String endTimelbl) {
        this.endTimelbl = endTimelbl;
    }

    public String getTotalHrs() {
        return totalHrs;
    }

    public void setTotalHrs(String totalHrs) {
        this.totalHrs = totalHrs;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getQuickpick_id() {
        return quickpick_id;
    }

    public void setQuickpick_id(String quickpick_id) {
        this.quickpick_id = quickpick_id;
    }

    public String getQuickpick_name() {
        return quickpick_name;
    }

    public void setQuickpick_name(String quickpick_name) {
        this.quickpick_name = quickpick_name;
    }

    public String getVehical_inout() {
        return vehical_inout;
    }

    public void setVehical_inout(String vehical_inout) {
        this.vehical_inout = vehical_inout;
    }

    public boolean isClockedOut() {
        return isClockedOut;
    }

    public void setClockedOut(boolean clockedOut) {
        isClockedOut = clockedOut;
    }

}
