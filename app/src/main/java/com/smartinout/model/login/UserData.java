
package com.smartinout.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserData implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("roles")
    @Expose
    private String roles;
    @SerializedName("is_active")
    @Expose
    private Boolean isActive;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("resetPasswordToken")
    @Expose
    private String resetPasswordToken;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("is_deleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("qr_code")
    @Expose
    private String qrCode;
    @SerializedName("is_owner")
    @Expose
    private boolean is_owner;
    @SerializedName("notification")
    @Expose
    private boolean notification;
    @SerializedName("profile_image")
    @Expose
    private String profile_image;
    @SerializedName("organization")
    @Expose
    private String organization;
    @SerializedName("office")
    @Expose
    private String office;
    @SerializedName("department")
    @Expose
    private String department;
    @SerializedName("inoutreference")
    @Expose
    private Object inoutreference;
    private final static long serialVersionUID = 2625624601896540671L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getResetPasswordToken() {
        return resetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public boolean isIs_owner() {
        return is_owner;
    }

    public void setIs_owner(boolean is_owner) {
        this.is_owner = is_owner;
    }

    public boolean isNotification() {
        return notification;
    }

    public void setNotification(boolean notification) {
        this.notification = notification;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Object getInoutreference() {
        return inoutreference;
    }

    public void setInoutreference(Object inoutreference) {
        this.inoutreference = inoutreference;
    }

}
