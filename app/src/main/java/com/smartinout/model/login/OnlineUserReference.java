
package com.smartinout.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import java.io.Serializable;

public class OnlineUserReference extends BaseResponse implements Serializable {


    @SerializedName("data")
    @Expose
    private Inoutreference onlineData;
    private final static long serialVersionUID = -445641989598227873L;


    public Inoutreference getOnlineData() {
        return onlineData;
    }

    public void setOnlineData(Inoutreference onlineData) {
        this.onlineData = onlineData;
    }

}
