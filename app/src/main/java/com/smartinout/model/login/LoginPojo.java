
package com.smartinout.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import java.io.Serializable;

public class LoginPojo extends BaseResponse{

    @SerializedName("data")
    @Expose
    private UserData userData;

    private final static long serialVersionUID = -445641989598227873L;

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

}
