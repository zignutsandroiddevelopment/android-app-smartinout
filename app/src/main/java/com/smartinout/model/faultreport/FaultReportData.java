
package com.smartinout.model.faultreport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FaultReportData implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("receipt_no")
    @Expose
    private Float receipt_no;

    @SerializedName("amount")
    @Expose
    private Float amount;

    @SerializedName("quantity")
    @Expose
    private Float quantity;

    @SerializedName("receipt_image")
    @Expose
    private String receipt_image;

    @SerializedName("vehical_id")
    @Expose
    private String vehicalId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Float getReceipt_no() {
        return receipt_no;
    }

    public void setReceipt_no(Float receipt_no) {
        this.receipt_no = receipt_no;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public String getReceipt_image() {
        return receipt_image;
    }

    public void setReceipt_image(String receipt_image) {
        this.receipt_image = receipt_image;
    }

    public String getVehicalId() {
        return vehicalId;
    }

    public void setVehicalId(String vehicalId) {
        this.vehicalId = vehicalId;
    }

}
