
package com.smartinout.model.faultreport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import java.io.Serializable;

public class FaultReportModel extends BaseResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private FaultReportData data;

    public FaultReportData getData() {
        return data;
    }

    public void setData(FaultReportData data) {
        this.data = data;
    }

    private final static long serialVersionUID = -1622815584432633015L;



}
