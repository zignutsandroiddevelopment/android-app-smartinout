package com.smartinout.model.dashboard;

public class ActivityLogs {

    private String activityLogTitle;
    private String activityLogHours;
    private int activityLogProgress;
    private int activityLogType;

    public String getActivityLogTitle() {
        return activityLogTitle;
    }

    public void setActivityLogTitle(String activityLogTitle) {
        this.activityLogTitle = activityLogTitle;
    }

    public String getActivityLogHours() {
        return activityLogHours;
    }

    public void setActivityLogHours(String activityLogHours) {
        this.activityLogHours = activityLogHours;
    }

    public int getActivityLogProgress() {
        return activityLogProgress;
    }

    public void setActivityLogProgress(int activityLogProgress) {
        this.activityLogProgress = activityLogProgress;
    }

    public int getActivityLogType() {
        return activityLogType;
    }

    public void setActivityLogType(int activityLogType) {
        this.activityLogType = activityLogType;
    }
}
