
package com.smartinout.model.dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import java.io.Serializable;

import androidx.annotation.Nullable;

public class DashbordVehicleModel extends BaseResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private DashbordVehicleData data;

    @Nullable
    public DashbordVehicleData getData() {
        return data;
    }

    @Nullable
    public void setData(DashbordVehicleData data) {
        this.data = data;
    }

    private final static long serialVersionUID = -1622815584432633015L;



}
