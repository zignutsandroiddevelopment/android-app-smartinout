
package com.smartinout.model.dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;
import com.smartinout.model.vehicleinout.VehicleInfoData;

import java.io.Serializable;

public class DashbordVehicleData implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;



    @SerializedName("from_date")
    @Expose
    private String fromdate;

    @SerializedName("from_time")
    @Expose
    private String fromtime;

    @SerializedName("vehical_id")
    @Expose
    private VehicleInfoData vehicalId;

    public String getFromdate() {
        return fromdate;
    }

    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    public String getFromtime() {
        return fromtime;
    }

    public void setFromtime(String fromtime) {
        this.fromtime = fromtime;
    }

    public VehicleInfoData getVehicalId() {
        return vehicalId;
    }

    public void setVehicalId(VehicleInfoData vehicalId) {
        this.vehicalId = vehicalId;
    }

}
