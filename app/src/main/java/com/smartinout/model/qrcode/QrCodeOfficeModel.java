package com.smartinout.model.qrcode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import androidx.annotation.Nullable;

public class QrCodeOfficeModel extends BaseResponse {


    @SerializedName("data")
    @Expose
    private QrCodeOfficeData data = null;

    private final static long serialVersionUID = -6432766701130381683L;

    @Nullable
    public QrCodeOfficeData getData() {
        return data;
    }

    @Nullable
    public void setData(QrCodeOfficeData data) {
        this.data = data;
    }

}
