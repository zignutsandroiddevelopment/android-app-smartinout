
package com.smartinout.model.qrcode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;
import com.smartinout.model.vehicleinout.VehicleInOutModel;

import java.io.Serializable;

import androidx.annotation.Nullable;

public class QrCodeOfficeData extends BaseResponse implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("firstname")
    @Expose
    private String firstname;

    @SerializedName("lastname")
    @Expose
    private String lastname;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("phone")
    @Expose
    private String phone;

    @Nullable
    public String getId() {
        return id;
    }

    @Nullable
    public void setId(String id) {
        this.id = id;
    }

    @Nullable
    public String getFirstname() {
        return firstname;
    }

    @Nullable
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Nullable
    public String getLastname() {
        return lastname;
    }

    @Nullable
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    @Nullable
    public void setEmail(String email) {
        this.email = email;
    }

    @Nullable
    public String getPhone() {
        return phone;
    }

    @Nullable
    public void setPhone(String phone) {
        this.phone = phone;
    }


}
