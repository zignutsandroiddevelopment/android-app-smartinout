
package com.smartinout.model.qrcode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;
import com.smartinout.model.vehicleinout.OfficeModel;
import com.smartinout.model.vehicleinout.VehicleInOutModel;

import java.io.Serializable;

public class QrCodeVehicleData extends BaseResponse implements Serializable {

    @SerializedName("is_booked")
    @Expose
    private boolean is_booked;

    @SerializedName("booked_by_other")
    @Expose
    private boolean booked_by_other;

    @SerializedName("is_available")
    @Expose
    private boolean is_available;

    @SerializedName("data")
    @Expose
    private VehicleInOutModel data = null;


    private final static long serialVersionUID = -6432766701130381683L;

    public VehicleInOutModel getData() {
        return data;
    }

    public void setData(VehicleInOutModel data) {
        this.data = data;
    }

    public boolean isIs_booked() {
        return is_booked;
    }

    public void setIs_booked(boolean is_booked) {
        this.is_booked = is_booked;
    }

    public boolean isBooked_by_other() {
        return booked_by_other;
    }

    public void setBooked_by_other(boolean booked_by_other) {
        this.booked_by_other = booked_by_other;
    }

    public boolean isIs_available() {
        return is_available;
    }

    public void setIs_available(boolean is_available) {
        this.is_available = is_available;
    }
}
