
package com.smartinout.model.qrcode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import java.io.Serializable;

public class QrCodeVehicleModel extends BaseResponse implements Serializable {



    @SerializedName("data")
    @Expose
    private QrCodeVehicleData data = null;

    private final static long serialVersionUID = -6432766701130381683L;

    public QrCodeVehicleData getData() {
        return data;
    }

    public void setData(QrCodeVehicleData data) {
        this.data = data;
    }
}
