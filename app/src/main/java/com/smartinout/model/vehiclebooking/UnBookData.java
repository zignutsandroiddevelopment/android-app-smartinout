
package com.smartinout.model.vehiclebooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.vehicleinout.starttrip.StartTripVehicalData;

import java.io.Serializable;

public class UnBookData implements Serializable {


    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("from_time")
    @Expose
    private String from_time;

    @SerializedName("from_date")
    @Expose
    private String from_date;

    @SerializedName("to_date")
    @Expose
    private String to_date;

    @SerializedName("to_time")
    @Expose
    private String to_time;

    @SerializedName("vehical_id")
    @Expose
    private String vehicalData;


    private final static long serialVersionUID = -6432766701130381683L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom_time() {
        return from_time;
    }

    public void setFrom_time(String from_time) {
        this.from_time = from_time;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getTo_time() {
        return to_time;
    }

    public void setTo_time(String to_time) {
        this.to_time = to_time;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVehicalData() {
        return vehicalData;
    }

    public void setVehicalData(String vehicalData) {
        this.vehicalData = vehicalData;
    }


}
