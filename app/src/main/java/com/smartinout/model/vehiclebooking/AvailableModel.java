
package com.smartinout.model.vehiclebooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import java.io.Serializable;
import java.util.List;

public class AvailableModel extends BaseResponse implements Serializable {
    @SerializedName("data")
    @Expose
    private List<AvailableData> data = null;

    private final static long serialVersionUID = -6432766701130381683L;

    public List<AvailableData> getData() {
        return data;
    }

    public void setData(List<AvailableData> data) {
        this.data = data;
    }
}
