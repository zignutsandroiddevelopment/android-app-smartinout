package com.smartinout.model.vehiclebooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import java.io.Serializable;
import java.util.List;

public class BookedModel extends BaseResponse implements Serializable {
    @SerializedName("data")
    @Expose
    private List<BookedData> data = null;

    private final static long serialVersionUID = -6432766701130381683L;

    public List<BookedData> getData() {
        return data;
    }

    public void setData(List<BookedData> data) {
        this.data = data;
    }
}
