
package com.smartinout.model.vehiclebooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import androidx.annotation.Nullable;

public class AvailableData implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("brand")
    @Expose
    private String brand;
    @Nullable
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("starting_km")
    @Expose
    private Integer startingKm;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("qr_code")
    @Expose
    private String qrCode;
    @SerializedName("is_active")
    @Expose
    private Boolean isActive;
    @SerializedName("is_deleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("office_id")
    @Expose
    private String officeId;
    @SerializedName("organization_id")
    @Expose
    private String organizationId;

    /*@SerializedName("rego_next_date")
    @Expose
    private String rego_next_date;

    @SerializedName("service_due_date")
    @Expose
    private String service_due_date;*/


    private final static long serialVersionUID = -1622815584432633015L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getStartingKm() {
        return startingKm;
    }

    public void setStartingKm(Integer startingKm) {
        this.startingKm = startingKm;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    /*public String getRego_next_date() {
        return rego_next_date;
    }

    public void setRego_next_date(String rego_next_date) {
        this.rego_next_date = rego_next_date;
    }

    public String getService_due_date() {
        return service_due_date;
    }

    public void setService_due_date(String service_due_date) {
        this.service_due_date = service_due_date;
    }*/

}
