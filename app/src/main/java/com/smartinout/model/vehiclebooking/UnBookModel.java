
package com.smartinout.model.vehiclebooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import java.io.Serializable;

import androidx.annotation.Nullable;

public class UnBookModel extends BaseResponse implements Serializable {
    @SerializedName("data")
    @Expose
    @Nullable
    private UnBookData data = null;

    private final static long serialVersionUID = -6432766701130381683L;

    @Nullable
    public UnBookData getData() {
        return data;
    }

    @Nullable
    public void setData(UnBookData data) {
        this.data = data;
    }
}
