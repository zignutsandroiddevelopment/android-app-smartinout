
package com.smartinout.model.vehiclebooking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BookedData implements Serializable {



    @SerializedName("id")
    @Expose
    private String id;


    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("from_time")
    @Expose
    private String from_time;

    @SerializedName("from_date")
    @Expose
    private String from_date;

    @SerializedName("to_date")
    @Expose
    private String to_date;

    @SerializedName("to_time")
    @Expose
    private String to_time;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("time")
    @Expose
    private String time;

    @SerializedName("is_breakdown")
    @Expose
    private boolean breakDown;

    @SerializedName("is_breakdown_during_trip")
    @Expose
    private boolean breakdownDuringTrip;

    @SerializedName("vehical_id")
    @Expose
    private AvailableData vehicleData = null;

    private final static long serialVersionUID = -6432766701130381683L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AvailableData getVehicleData() {
        return vehicleData;
    }

    public void setVehicleData(AvailableData data) {
        this.vehicleData = data;
    }

    public String getFrom_time() {
        return from_time;
    }

    public void setFrom_time(String from_time) {
        this.from_time = from_time;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getTo_time() {
        return to_time;
    }

    public void setTo_time(String to_time) {
        this.to_time = to_time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isBreakDown() {
        return breakDown;
    }

    public void setBreakDown(boolean breakDown) {
        this.breakDown = breakDown;
    }

    public boolean isBreakdownDuringTrip() {
        return breakdownDuringTrip;
    }

    public void setBreakdownDuringTrip(boolean breakdownDuringTrip) {
        this.breakdownDuringTrip = breakdownDuringTrip;
    }


}
