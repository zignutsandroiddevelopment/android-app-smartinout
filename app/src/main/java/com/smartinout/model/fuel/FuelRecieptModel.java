
package com.smartinout.model.fuel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import java.io.Serializable;

public class FuelRecieptModel extends BaseResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private FuelRecieptData data;

    public FuelRecieptData getData() {
        return data;
    }

    public void setData(FuelRecieptData data) {
        this.data = data;
    }

    private final static long serialVersionUID = -1622815584432633015L;



}
