package com.smartinout.model;

public class ActivityHours {
    private long totlaBreakHours = 0;
    private long totlaInHours = 0;
    private long currentInHours = 0;
    private String lastStatus;
    private String lastmainStatus;
    private String totalHrsmns;

    public long getCurrentInHours() {
        return currentInHours;
    }

    public void setCurrentInHours(long currentInHours) {
        this.currentInHours = currentInHours;
    }

    public long getTotlaBreakHours() {
        return totlaBreakHours;
    }

    public void setTotlaBreakHours(long totlaBreakHours) {
        this.totlaBreakHours = totlaBreakHours;
    }

    public long getTotlaInHours() {
        return totlaInHours;
    }

    public void setTotlaInHours(long totlaInHours) {
        this.totlaInHours = totlaInHours;
    }

    public String getLastStatus() {
        return lastStatus;
    }

    public void setLastStatus(String lastStatus) {
        this.lastStatus = lastStatus;
    }

    public String getTotalHrsmns() {
        return totalHrsmns;
    }

    public void setTotalHrsmns(String totalHrsmns) {
        this.totalHrsmns = totalHrsmns;
    }

    public String getLastmainStatus() {
        return lastmainStatus;
    }

    public void setLastmainStatus(String lastmainStatus) {
        this.lastmainStatus = lastmainStatus;
    }
}