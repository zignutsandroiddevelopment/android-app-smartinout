
package com.smartinout.model.organize;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;
import com.smartinout.model.vehicleinout.VehicleInOutData;

import java.io.Serializable;
import java.util.List;

public class OrganizeModel extends BaseResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private List<OrganizeData> data;


    public List<OrganizeData> getData() {
        return data;
    }

    public void setDate(List<OrganizeData> data) {
        this.data = data;
    }


    private final static long serialVersionUID = -1622815584432633015L;

}
