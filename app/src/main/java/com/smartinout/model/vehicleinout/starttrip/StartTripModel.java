
package com.smartinout.model.vehicleinout.starttrip;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import java.io.Serializable;

public class StartTripModel extends BaseResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private StartTripData data;

    public StartTripData getData() {
        return data;
    }

    public void setData(StartTripData data) {
        this.data = data;
    }

    private final static long serialVersionUID = -1622815584432633015L;



}
