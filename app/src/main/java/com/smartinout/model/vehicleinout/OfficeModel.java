
package com.smartinout.model.vehicleinout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OfficeModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("office_name")
    @Expose
    private String officeName;

    private final static long serialVersionUID = -1622815584432633015L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }




}
