
package com.smartinout.model.vehicleinout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VehicleInfoData implements Serializable {


    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("number")
    @Expose
    private String number;

    @SerializedName("brand")
    @Expose
    private String brand;

    @SerializedName("starting_km")
    @Expose
    private String startingKm;

    @SerializedName("rego_next_date")
    @Expose
    private String regoNextDate;

    /*@SerializedName("service_due_date")
    @Expose
    private String serviceDueDate;*/

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("qr_code")
    @Expose
    private String qr_code;

    @SerializedName("is_active")
    @Expose
    private String is_active;

    @SerializedName("is_deleted")
    @Expose
    private String is_deleted;

    private final static long serialVersionUID = -1622815584432633015L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getStartingKm() {
        return startingKm;
    }

    public void setStartingKm(String startingKm) {
        this.startingKm = startingKm;
    }

    public String getRegoNextDate() {
        return regoNextDate;
    }

    public void setRegoNextDate(String regoNextDate) {
        this.regoNextDate = regoNextDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQr_code() {
        return qr_code;
    }

    public void setQr_code(String qr_code) {
        this.qr_code = qr_code;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }




}
