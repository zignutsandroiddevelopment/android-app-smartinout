
package com.smartinout.model.vehicleinout.stoptrip;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import java.io.Serializable;

public class StopTripModel extends BaseResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private StopTripData data;

    public StopTripData getData() {
        return data;
    }

    public void setData(StopTripData data) {
        this.data = data;
    }

    private final static long serialVersionUID = -1622815584432633015L;



}
