
package com.smartinout.model.vehicleinout.starttrip;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StartTripData implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("comment")
    @Expose
    private String comment;

    @SerializedName("start_km")
    @Expose
    private Integer startKm;

    @SerializedName("end_km")
    @Expose
    private Integer endKm;

    @SerializedName("due_back")
    @Expose
    private String dueBack;

    @SerializedName("start_time")
    @Expose
    private String startTime;

    @SerializedName("end_time")
    @Expose
    private String endTime;

    @SerializedName("diffrence_in_miliseconds")
    @Expose
    private Integer diffrenceInMiliseconds;

    @SerializedName("is_deleted")
    @Expose
    private boolean isDeleted;

    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("vehical_id")
    @Expose
    private StartTripVehicalData vehicalData;

    @SerializedName("vehical_booking_id")
    @Expose
    private String vehicalBookingId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getStartKm() {
        return startKm;
    }

    public void setStartKm(Integer startKm) {
        this.startKm = startKm;
    }

    public Integer getEndKm() {
        return endKm;
    }

    public void setEndKm(Integer endKm) {
        this.endKm = endKm;
    }

    public String getDueBack() {
        return dueBack;
    }

    public void setDueBack(String dueBack) {
        this.dueBack = dueBack;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getDiffrenceInMiliseconds() {
        return diffrenceInMiliseconds;
    }

    public void setDiffrenceInMiliseconds(Integer diffrenceInMiliseconds) {
        this.diffrenceInMiliseconds = diffrenceInMiliseconds;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public StartTripVehicalData getVehicalData() {
        return vehicalData;
    }

    public void setVehicalData(StartTripVehicalData vehicalData) {
        this.vehicalData = vehicalData;
    }


    public String getVehicalBookingId() {
        return vehicalBookingId;
    }

    public void setVehicalBookingId(String vehicalBookingId) {
        this.vehicalBookingId = vehicalBookingId;
    }



}
