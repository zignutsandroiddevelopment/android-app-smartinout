
package com.smartinout.model.vehicleinout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import java.io.Serializable;

import androidx.annotation.Nullable;

public class VehicleInfoModel extends BaseResponse implements Serializable {


    @SerializedName("data")
    @Expose
    private VehicleInfoData data;

    private final static long serialVersionUID = -1622815584432633015L;

    @Nullable
    public VehicleInfoData getData() {
        return data;
    }

    @Nullable
    public void setData(VehicleInfoData data) {
        this.data = data;
    }


}
