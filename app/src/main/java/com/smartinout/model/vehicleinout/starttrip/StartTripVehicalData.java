
package com.smartinout.model.vehicleinout.starttrip;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.vehicleinout.OfficeModel;

import java.io.Serializable;

public class StartTripVehicalData implements Serializable {

    @SerializedName("createdAt")
    @Expose
    private String createdAt;

    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("brand")
    @Expose
    private String brand;

    @SerializedName("number")
    @Expose
    private String number;

    @SerializedName("starting_km")
    @Expose
    private Integer startingKm;

    @SerializedName("rego_next_date")
    @Expose
    private String regoNextDate;

    @SerializedName("service_due_date")
    @Expose
    private StartTripVServiceData serviceDueData;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("is_active")
    @Expose
    private boolean isActive;

    @SerializedName("is_deleted")
    @Expose
    private boolean isDeleted;

    @SerializedName("office_id")
    @Expose
    private OfficeModel officeData;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getStartingKm() {
        return startingKm;
    }

    public void setStartingKm(Integer startingKm) {
        this.startingKm = startingKm;
    }

    public String getRegoNextDate() {
        return regoNextDate;
    }

    public void setRegoNextDate(String regoNextDate) {
        this.regoNextDate = regoNextDate;
    }

    public StartTripVServiceData getServiceDueData() {
        return serviceDueData;
    }

    public void setServiceDueData(StartTripVServiceData serviceDueData) {
        this.serviceDueData = serviceDueData;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public OfficeModel getOfficeData() {
        return officeData;
    }

    public void setOfficeData(OfficeModel officeData) {
        this.officeData = officeData;
    }


}
