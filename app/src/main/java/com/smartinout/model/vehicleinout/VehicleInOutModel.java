
package com.smartinout.model.vehicleinout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VehicleInOutModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("is_breakdown")
    @Expose
    private Boolean is_breakdown;

    @SerializedName("from_date")
    @Expose
    private String from_date;

    @SerializedName("from_time")
    @Expose
    private String from_time;

    @SerializedName("to_date")
    @Expose
    private String to_date;

    @SerializedName("to_time")
    @Expose
    private String to_time;

    @SerializedName("on_go_booking")
    @Expose
    private Boolean on_go_booking;

    @SerializedName("vehical_id")
    @Expose
    private VehicleInOutData vehicalData;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("time")
    @Expose
    private String time;



    private final static long serialVersionUID = -1622815584432633015L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Boolean getIs_breakdown() {
        return is_breakdown;
    }

    public void setIs_breakdown(Boolean is_breakdown) {
        this.is_breakdown = is_breakdown;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getFrom_time() {
        return from_time;
    }

    public void setFrom_time(String from_time) {
        this.from_time = from_time;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getTo_time() {
        return to_time;
    }

    public void setTo_time(String to_time) {
        this.to_time = to_time;
    }

    public Boolean getOn_go_booking() {
        return on_go_booking;
    }

    public void setOn_go_booking(Boolean on_go_booking) {
        this.on_go_booking = on_go_booking;
    }

    public VehicleInOutData getVehicalData() {
        return vehicalData;
    }

    public void setVehicalData(VehicleInOutData vehicalData) {
        this.vehicalData = vehicalData;
    }


}
