
package com.smartinout.model.vehicleinout.starttrip;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StartTripVServiceData implements Serializable {

    @SerializedName("kmmiles")
    @Expose
    private String kmMiles;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("month")
    @Expose
    private String month;

    public String getKmMiles() {
        return kmMiles;
    }

    public void setKmMiles(String kmMiles) {
        this.kmMiles = kmMiles;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

}
