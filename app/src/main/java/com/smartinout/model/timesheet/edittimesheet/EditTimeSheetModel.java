
package com.smartinout.model.timesheet.edittimesheet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import java.io.Serializable;

public class EditTimeSheetModel extends BaseResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private TimeSheetData data;

    private final static long serialVersionUID = 8492310286315493700L;

    public TimeSheetData getData() {
        return data;
    }

    public void setData(TimeSheetData data) {
        this.data = data;
    }

}
