
package com.smartinout.model.timesheet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import java.io.Serializable;

public class DailyTimeSheetsPojo extends BaseResponse implements Serializable {
    @SerializedName("data")
    @Expose
    private UserDailyProgressData dailyProgressData;
    private final static long serialVersionUID = 8953650846469622902L;

    public UserDailyProgressData getDailyProgressData() {
        return dailyProgressData;
    }

    public void setDailyProgressData(UserDailyProgressData dailyProgressData) {
        this.dailyProgressData = dailyProgressData;
    }
}
