
package com.smartinout.model.timesheet.edittimesheet;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EditedTimeSheetData implements Serializable
{

    @SerializedName("createdAt")
    @Expose
    private long createdAt;
    @SerializedName("updatedAt")
    @Expose
    private long updatedAt;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("main_status")
    @Expose
    private String mainStatus;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("due_back")
    @Expose
    private String dueBack;
    @SerializedName("start_time")
    @Expose
    private long startTime;
    @SerializedName("end_time")
    @Expose
    private long endTime;
    @SerializedName("diffrence_in_miliseconds")
    @Expose
    private long diffrenceInMiliseconds;
    @SerializedName("createdInoutTime")
    @Expose
    private long createdInoutTime;
    @SerializedName("updatedInoutTime")
    @Expose
    private long updatedInoutTime;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("updatedBy")
    @Expose
    private Object updatedBy;
    private final static long serialVersionUID = 546548990157447729L;

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Integer createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Integer updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMainStatus() {
        return mainStatus;
    }

    public void setMainStatus(String mainStatus) {
        this.mainStatus = mainStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDueBack() {
        return dueBack;
    }

    public void setDueBack(String dueBack) {
        this.dueBack = dueBack;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public Object getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getDiffrenceInMiliseconds() {
        return diffrenceInMiliseconds;
    }

    public void setDiffrenceInMiliseconds(long diffrenceInMiliseconds) {
        this.diffrenceInMiliseconds = diffrenceInMiliseconds;
    }

    public long getCreatedInoutTime() {
        return createdInoutTime;
    }

    public void setCreatedInoutTime(long createdInoutTime) {
        this.createdInoutTime = createdInoutTime;
    }

    public long getUpdatedInoutTime() {
        return updatedInoutTime;
    }

    public void setUpdatedInoutTime(Integer updatedInoutTime) {
        this.updatedInoutTime = updatedInoutTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Object getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Object updatedBy) {
        this.updatedBy = updatedBy;
    }

}
