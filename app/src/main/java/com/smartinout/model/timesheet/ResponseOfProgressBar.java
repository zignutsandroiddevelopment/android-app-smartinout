
package com.smartinout.model.timesheet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseOfProgressBar {

    @SerializedName("totalActiveMinutes")
    @Expose
    private long totalActiveMinutes;
    @SerializedName("inoutData")
    @Expose
    private List<InoutDatum> inoutData = null;

    public long getTotalActiveMinutes() {
        return totalActiveMinutes;
    }

    public void setTotalActiveMinutes(long totalActiveMinutes) {
        this.totalActiveMinutes = totalActiveMinutes;
    }

    public List<InoutDatum> getInoutData() {
        return inoutData;
    }

    public void setInoutData(List<InoutDatum> inoutData) {
        this.inoutData = inoutData;
    }

}
