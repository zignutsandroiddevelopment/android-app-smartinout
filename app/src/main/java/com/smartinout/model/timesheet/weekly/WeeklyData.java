
package com.smartinout.model.timesheet.weekly;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.login.Inoutreference;

import java.io.Serializable;
import java.util.List;

public class WeeklyData implements Serializable {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("totalActiveMinutes")
    @Expose
    private Integer totalActiveMinutes;
    @SerializedName("dateWiseData")
    @Expose
    private List<DateWiseDatum> dateWiseData = null;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("users_in_out")
    @Expose
    private Inoutreference usersInOut;
    private final static long serialVersionUID = 6681934153317793797L;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getTotalActiveMinutes() {
        return totalActiveMinutes;
    }

    public void setTotalActiveMinutes(Integer totalActiveMinutes) {
        this.totalActiveMinutes = totalActiveMinutes;
    }

    public List<DateWiseDatum> getDateWiseData() {
        return dateWiseData;
    }

    public void setDateWiseData(List<DateWiseDatum> dateWiseData) {
        this.dateWiseData = dateWiseData;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Inoutreference getUsersInOut() {
        return usersInOut;
    }

    public void setUsersInOut(Inoutreference usersInOut) {
        this.usersInOut = usersInOut;
    }

}
