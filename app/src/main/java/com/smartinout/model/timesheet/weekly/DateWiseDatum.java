
package com.smartinout.model.timesheet.weekly;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DateWiseDatum implements Serializable
{

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("activeMinutes")
    @Expose
    private Integer activeMinutes;
    @SerializedName("breakMinutes")
    @Expose
    private Integer breakMinutes;
    private final static long serialVersionUID = 4835557470900896517L;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getActiveMinutes() {
        return activeMinutes;
    }

    public void setActiveMinutes(Integer activeMinutes) {
        this.activeMinutes = activeMinutes;
    }

    public Integer getBreakMinutes() {
        return breakMinutes;
    }

    public void setBreakMinutes(Integer breakMinutes) {
        this.breakMinutes = breakMinutes;
    }

}
