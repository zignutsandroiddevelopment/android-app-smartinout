
package com.smartinout.model.timesheet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class UserTimesheet implements Serializable {

    @SerializedName("timeZone")
    @Expose
    private String timeZone;
    @SerializedName("data")
    @Expose
    private List<Activities> activities = null;
    private final static long serialVersionUID = -2285215159845169624L;

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public List<Activities> getActivities() {
        return activities;
    }

    public void setActivities(List<Activities> activities) {
        this.activities = activities;
    }

}
