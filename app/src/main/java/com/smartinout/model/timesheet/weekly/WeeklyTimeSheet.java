
package com.smartinout.model.timesheet.weekly;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;

import java.io.Serializable;
import java.util.List;

public class WeeklyTimeSheet extends BaseResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private List<WeeklyData> data = null;

    private final static long serialVersionUID = -6593965380687024785L;

    public List<WeeklyData> getData() {
        return data;
    }

    public void setData(List<WeeklyData> data) {
        this.data = data;
    }
}
