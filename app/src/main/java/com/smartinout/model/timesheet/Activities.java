
package com.smartinout.model.timesheet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.login.Inoutreference;

import java.io.Serializable;
import java.util.List;

public class Activities implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("users_data")
    @Expose
    private List<Inoutreference> usersData = null;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("users_in_out")
    @Expose
    private Inoutreference usersInOut;
    private final static long serialVersionUID = -3992561208003466848L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<Inoutreference> getUsersData() {
        return usersData;
    }

    public void setUsersData(List<Inoutreference> usersData) {
        this.usersData = usersData;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Inoutreference getUsersInOut() {
        return usersInOut;
    }

    public void setUsersInOut(Inoutreference usersInOut) {
        this.usersInOut = usersInOut;
    }

}
