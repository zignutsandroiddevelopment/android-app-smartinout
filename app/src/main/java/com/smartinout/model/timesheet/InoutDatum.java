
package com.smartinout.model.timesheet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InoutDatum {

    @SerializedName("minutes")
    @Expose
    private Integer minutes;
    @SerializedName("percentage")
    @Expose
    private float percentage;
    @SerializedName("color")
    @Expose
    private String color;

    @SerializedName("quickpic_name")
    @Expose
    private String quickpic_name;

    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    public float getPercentage() {
        return percentage;
    }

    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getQuickpic_name() {
        return quickpic_name;
    }

    public void setQuickpic_name(String quickpic_name) {
        this.quickpic_name = quickpic_name;
    }

}
