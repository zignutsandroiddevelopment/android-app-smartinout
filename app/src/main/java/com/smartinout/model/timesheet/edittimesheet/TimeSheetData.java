
package com.smartinout.model.timesheet.edittimesheet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TimeSheetData implements Serializable {

    @SerializedName("timeZone")
    @Expose
    private String timeZone;
    @SerializedName("data")
    @Expose
    private EditedTimeSheetData data;
    private final static long serialVersionUID = -2890230924777472539L;

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public EditedTimeSheetData getData() {
        return data;
    }

    public void setData(EditedTimeSheetData data) {
        this.data = data;
    }

}
