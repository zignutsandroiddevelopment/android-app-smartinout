
package com.smartinout.model.timesheet.quickpick;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;
import com.smartinout.model.timesheet.ResponseOfProgressBar;
import com.smartinout.model.timesheet.UserDailyProgressData;

import java.io.Serializable;
import java.util.List;

public class QuickPickPojo extends BaseResponse{

    @SerializedName("data")
    @Expose
    private List<QuickTask> quickPickDatat;
    //private QuickTask quickPickDatat;

    private final static long serialVersionUID = -445641989598227873L;

    public List<QuickTask> getQuickPickDatat() {
        return quickPickDatat;
    }

    public void setQuickPickDatat(List<QuickTask> quickPickDatat) {
        this.quickPickDatat = quickPickDatat;
    }


}
