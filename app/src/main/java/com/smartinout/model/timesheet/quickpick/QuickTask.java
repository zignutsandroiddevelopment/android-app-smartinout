package com.smartinout.model.timesheet.quickpick;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class QuickTask implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("comment")
    @Expose
    private String comment;

    @SerializedName("due_back")
    @Expose
    private String due_back;

    @SerializedName("is_active")
    @Expose
    private String is_active;

    @SerializedName("is_deleted")
    @Expose
    private String is_deleted;

    @SerializedName("organization_id")
    @Expose
    private String organization_id;

    private final static long serialVersionUID = -445641989598227873L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDue_back() {
        return due_back;
    }

    public void setDue_back(String due_back) {
        this.due_back = due_back;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(String organization_id) {
        this.organization_id = organization_id;
    }



}
