package com.smartinout.model.timesheet;

public class CalendarModel {

    private String day;
    private String date;
    private String hours;
    private String formatedDate;
    private int currentDate;

    private String weeklydates;
    private boolean isCurrentweek;
    private boolean isSelected = false;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getFormatedDate() {
        return formatedDate;
    }

    public int getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(int currentDate) {
        this.currentDate = currentDate;
    }

    public void setFormatedDate(String formatedDate) {
        this.formatedDate = formatedDate;
    }

    public String getWeeklydates() {
        return weeklydates;
    }

    public void setWeeklydates(String weeklydates) {
        this.weeklydates = weeklydates;
    }

    public boolean isCurrentweek() {
        return isCurrentweek;
    }

    public void setCurrentweek(boolean currentweek) {
        isCurrentweek = currentweek;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
