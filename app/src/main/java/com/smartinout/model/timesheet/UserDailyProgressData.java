package com.smartinout.model.timesheet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class UserDailyProgressData implements Serializable {

    @SerializedName("data")
    @Expose
    private UserTimesheet userTimesheet;

    @SerializedName("responseOfProgressBar")
    @Expose
    private List<ResponseOfProgressBar> responseOfProgressBar;

    private final static long serialVersionUID = 8953650846469622902L;

    public UserTimesheet getUserTimesheet() {
        return userTimesheet;
    }

    public void setUserTimesheet(UserTimesheet userTimesheet) {
        this.userTimesheet = userTimesheet;
    }

    public List<ResponseOfProgressBar> getResponseOfProgressBar() {
        return responseOfProgressBar;
    }

    public void setResponseOfProgressBar(List<ResponseOfProgressBar> responseOfProgressBar) {
        this.responseOfProgressBar = responseOfProgressBar;
    }
}
