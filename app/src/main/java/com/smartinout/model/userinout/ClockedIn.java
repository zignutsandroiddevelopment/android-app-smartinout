
package com.smartinout.model.userinout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ClockedIn implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("main_status")
    @Expose
    private String mainStatus;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("due_back")
    @Expose
    private String dueBack;
    @SerializedName("createdInoutTime")
    @Expose
    private long createdInoutTime;
    @SerializedName("updatedInoutTime")
    @Expose
    private long updatedInoutTime;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("updatedBy")
    @Expose
    private Object updatedBy;
    private final static long serialVersionUID = 5744389363705548557L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMainStatus() {
        return mainStatus;
    }

    public void setMainStatus(String mainStatus) {
        this.mainStatus = mainStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDueBack() {
        return dueBack;
    }

    public void setDueBack(String dueBack) {
        this.dueBack = dueBack;
    }

    public long getCreatedInoutTime() {
        return createdInoutTime;
    }

    public void setCreatedInoutTime(Integer createdInoutTime) {
        this.createdInoutTime = createdInoutTime;
    }

    public long getUpdatedInoutTime() {
        return updatedInoutTime;
    }

    public void setUpdatedInoutTime(Integer updatedInoutTime) {
        this.updatedInoutTime = updatedInoutTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Object getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Object updatedBy) {
        this.updatedBy = updatedBy;
    }

}
