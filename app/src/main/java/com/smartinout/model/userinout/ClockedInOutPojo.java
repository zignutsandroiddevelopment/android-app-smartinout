
package com.smartinout.model.userinout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartinout.model.BaseResponse;
import com.smartinout.model.login.Inoutreference;

import java.io.Serializable;

public class ClockedInOutPojo extends BaseResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private Inoutreference data;
    private final static long serialVersionUID = 1609308046380614094L;

    public Inoutreference getData() {
        return data;
    }

    public void setData(Inoutreference data) {
        this.data = data;
    }

}
