package com.smartinout.model.userinout;

public class CompanyInOut {

    private String userName;
    private boolean isInOut;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isInOut() {
        return isInOut;
    }

    public void setInOut(boolean inOut) {
        isInOut = inOut;
    }
}
