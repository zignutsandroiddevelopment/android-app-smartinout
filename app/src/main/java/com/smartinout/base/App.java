package com.smartinout.base;

import android.app.Application;

import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.smartinout.model.timesheet.quickpick.QuickTask;

import java.util.List;

/**
 * Created at 23-07-2018
 */
public class App extends Application {

    public static final String TAG = App.class.getSimpleName();
    private static App app = null;

    private static String inOutMainStatus = "";
    private static String quickStatus = "";
    private static List<QuickTask> mQuickTaskListIn;


    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        // This instantiates DBFlow
        FlowManager.init(new FlowConfig.Builder(this).build());
    }

    public static App getAppContext() {
        return app;
    }

    public static List<QuickTask> getQuickTaskListIn() {
        return mQuickTaskListIn;
    }

    public static void setQuickTaskListIn(List<QuickTask> mQuickTaskList) {
        mQuickTaskListIn = mQuickTaskList;
    }

    public static String getInOutMainStatus() {
        return inOutMainStatus;
    }

    public static void setInOutMainStatus(String inOutMainStatus) {
        App.inOutMainStatus = inOutMainStatus;
    }


    public static String getQuickStatus() {
        return quickStatus;
    }

    public static void setQuickStatuss(String quickStatus) {
        App.quickStatus = quickStatus;
    }



}







