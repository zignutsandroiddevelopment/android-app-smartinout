package com.smartinout.base;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smartinout.R;
import com.smartinout.dbflowdatabase.SmartInOutDBCtrl;
import com.smartinout.imagepicker.BottomDialog;
import com.smartinout.imagepicker.FilePickUtils;
import com.smartinout.imagepicker.LifeCycleCallBackManager;
import com.smartinout.interfaces.LeftRightClick;
import com.smartinout.retrofit.CallService;
import com.smartinout.utilities.Prefs;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


/**
 * Created at 23-07-2018
 */
public class BaseFragment extends Fragment implements LeftRightClick {

    private Dialog progressDialog;
    public Prefs pref;
    public CallService callService;
    public SmartInOutDBCtrl inOutDBCtrl;
    private FilePickUtils filePickUtils;
    private LifeCycleCallBackManager lifeCycleCallBackManager;
    private BottomDialog bottomSheetDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = Prefs.getInstance();
        callService = CallService.getInstance(getContext());
        inOutDBCtrl = SmartInOutDBCtrl.getInstance(getContext());
        disableAutofill();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        return view;
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutofill() {
        getActivity().getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }

    public void updateResources(Context context, String language) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).updateResources(context, language);
        }
    }

    public void showProgressDialog(boolean show) {
        if (show) {
            showProgressDialog();
        } else {
            hideProgressDialog();
        }
    }

    public void showProgressDialog() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showProgressDialog(true);
        }
    }

    public void hideProgressDialog() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).hideProgressDialog();
        }
    }


    public void clickLeftBtn(final TextView textViewLeft, final TextView textViewRight) {
        textViewLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Selected View Left
                textViewLeft.setTextColor(Color.WHITE);
                textViewLeft.setBackgroundResource(R.drawable.ic_rounded_button_blue);

                // Unselect View Right
                textViewRight.setTextColor(getResources().getColor(R.color.clrIntroRegular));
                textViewRight.setBackgroundResource(R.drawable.ic_rounded_btn_trans);

                onLeftBtnClick(v);
            }
        });
    }

    public void clickRightBtn(final TextView textViewRight, final TextView textViewLeft) {
        textViewRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Selected View Left
                textViewRight.setTextColor(Color.WHITE);
                textViewRight.setBackgroundResource(R.drawable.ic_rounded_button_blue);

                // Unselect View Right
                textViewLeft.setTextColor(getResources().getColor(R.color.clrIntroRegular));
                textViewLeft.setBackgroundResource(R.drawable.ic_rounded_btn_trans);

                onRightBtnClick(v);
            }
        });
    }

    public void clickCenterLeftBtn(final TextView textViewLeft, final TextView textViewCenter,final TextView textViewRight) {
        textViewLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Selected View Left
                textViewLeft.setTextColor(Color.WHITE);
                textViewLeft.setBackgroundResource(R.drawable.ic_rounded_button_blue);

                // Unselect View Center
                textViewCenter.setTextColor(getResources().getColor(R.color.clrIntroRegular));
                textViewCenter.setBackgroundResource(R.drawable.ic_rounded_btn_trans);//ic_rounded_btn_trans);


                // Unselect View Right
                textViewRight.setTextColor(getResources().getColor(R.color.clrIntroRegular));
                textViewRight.setBackgroundResource(R.drawable.ic_rounded_btn_trans);//ic_rounded_grey_border);

                onLeftBtnClick(v);
            }
        });
    }

    public void clickCenterBtn(final TextView textViewCenter, final TextView textViewRight,final TextView textViewLeft) {
        textViewCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Selected View Left
                textViewCenter.setTextColor(Color.WHITE);
                textViewCenter.setBackgroundResource(R.drawable.ic_rounded_button_blue);

                // Unselect View Center
                textViewRight.setTextColor(getResources().getColor(R.color.clrIntroRegular));
                textViewRight.setBackgroundResource(R.drawable.ic_rounded_btn_trans);//ic_rounded_grey_border);

                // Unselect View Right
                textViewLeft.setTextColor(getResources().getColor(R.color.clrIntroRegular));
                textViewLeft.setBackgroundResource(R.drawable.ic_rounded_btn_trans);//ic_rounded_grey_border);

                onCenterBtnClick(v);
            }
        });
    }

    public void clickCenterRightBtn(final TextView textViewRight, final TextView textViewCenter,final TextView textViewLeft) {
        textViewRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Selected View Left
                textViewRight.setTextColor(Color.WHITE);
                textViewRight.setBackgroundResource(R.drawable.ic_rounded_button_blue);

                // Unselect View Center
                textViewCenter.setTextColor(getResources().getColor(R.color.clrIntroRegular));
                textViewCenter.setBackgroundResource(R.drawable.ic_rounded_btn_trans);//ic_rounded_grey_border);

                // Unselect View Right
                textViewLeft.setTextColor(getResources().getColor(R.color.clrIntroRegular));
                textViewLeft.setBackgroundResource(R.drawable.ic_rounded_btn_trans);//ic_rounded_grey_border);

                onRightBtnClick(v);
            }
        });
    }

    public void defaultSelection(TextView textView) {
        textView.setTextColor(Color.WHITE);
        textView.setBackgroundResource(R.drawable.ic_rounded_button_blue);
    }

    public void defaultSelection(TextView textViewLeft, TextView textViewRight) {

        textViewLeft.setTextColor(Color.WHITE);
        textViewLeft.setBackgroundResource(R.drawable.ic_rounded_button_blue);

        textViewRight.setTextColor(getResources().getColor(R.color.clrIntroRegular));
        textViewRight.setBackgroundResource(R.drawable.ic_rounded_btn_trans);
    }

    public void defaultSelection(TextView textViewLeft,TextView textViewCenter, TextView textViewRight) {

        textViewCenter.setTextColor(Color.WHITE);
        textViewCenter.setBackgroundResource(R.drawable.ic_rounded_button_blue);

        textViewLeft.setTextColor(getResources().getColor(R.color.clrIntroRegular));
        textViewLeft.setBackgroundResource(R.drawable.ic_rounded_btn_trans);//ic_rounded_grey_border);

        textViewRight.setTextColor(getResources().getColor(R.color.clrIntroRegular));
        textViewRight.setBackgroundResource(R.drawable.ic_rounded_btn_trans);//ic_rounded_grey_border);
    }

    @Override
    public void onLeftBtnClick(View view) {

    }

    @Override
    public void onRightBtnClick(View view) {

    }

    @Override
    public void onCenterBtnClick(View view) {

    }

    // Select Image Picker Show
    public void showImagePickerDialog(FilePickUtils.OnFileChoose onFileChoose) {
        filePickUtils = new FilePickUtils(this, onFileChoose);
        lifeCycleCallBackManager = filePickUtils.getCallBackManager();
        View bottomSheetView = getLayoutInflater().inflate(R.layout.dialog_photo_selector, null);
        bottomSheetDialog = new BottomDialog(getActivity());
        bottomSheetDialog.setContentView(bottomSheetView);
        final TextView tvCamera = bottomSheetView.findViewById(R.id.tvCamera);
        final TextView tvGallery = bottomSheetView.findViewById(R.id.tvGallery);
        tvCamera.setOnClickListener(onCameraListener);
        tvGallery.setOnClickListener(onGalleryListener);
        bottomSheetDialog.show();
    }

    public void dismissImagePickerDialog() {
        bottomSheetDialog.dismiss();
    }


    private View.OnClickListener onCameraListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            dismissImagePickerDialog();
            filePickUtils.requestImageCamera(FilePickUtils.STORAGE_PERMISSION_CAMERA
                    , true, true);
        }
    };

    private View.OnClickListener onGalleryListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            dismissImagePickerDialog();
            filePickUtils.requestImageGallery(FilePickUtils.STORAGE_PERMISSION_IMAGE
                    , true, true);
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (lifeCycleCallBackManager != null) {
            lifeCycleCallBackManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (lifeCycleCallBackManager != null) {
            lifeCycleCallBackManager.onActivityResult(requestCode, resultCode, data);
        }
    }
}
