package com.smartinout.base;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartinout.LoginActivity;
import com.smartinout.R;
import com.smartinout.dbflowdatabase.SmartInOutDBCtrl;
import com.smartinout.imagepicker.BottomDialog;
import com.smartinout.imagepicker.FilePickUtils;
import com.smartinout.imagepicker.LifeCycleCallBackManager;
import com.smartinout.interfaces.LeftRightClick;
import com.smartinout.retrofit.CallService;
import com.smartinout.utilities.Prefs;

import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;


/**
 * Created at 23-07-2018
 */
public abstract class BaseActivity extends AppCompatActivity implements LeftRightClick {

    public Dialog progressDialog;

    private FilePickUtils filePickUtils;

    private BottomDialog bottomSheetDialog;
    private LifeCycleCallBackManager lifeCycleCallBackManager;

    public Prefs prefs;
    public CallService callService;

    public SmartInOutDBCtrl inOutDBCtrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = Prefs.getInstance();
        callService = CallService.getInstance(BaseActivity.this);
        inOutDBCtrl = SmartInOutDBCtrl.getInstance(BaseActivity.this);
        disableAutofill();
    }

    protected abstract void initiateUI();

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutofill() {
        try {
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void showProgressDialog(boolean show) {
        if (show) {
            showProgressDialog();
        } else {
            hideProgressDialog();
        }
    }

    public void showProgressDialog1(boolean show) {
        if (show) {
            showProgressDialog();
        } else {
            hideProgressDialog();
        }
    }


    public void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new Dialog(this);
        } else {
            return;
        }

        View view = LayoutInflater.from(this).inflate(R.layout.app_loading_dialog,
                null, false);

        ImageView imageView1 = view.findViewById(R.id.imageView2);
        Animation a1 = AnimationUtils.loadAnimation(this, R.anim.progress_anim);
        a1.setDuration(1500);
        imageView1.startAnimation(a1);

        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(view);
        Window window = progressDialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(ContextCompat.getDrawable(this, android.R.color.transparent));
        }
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public final void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    protected void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    public Activity getActivity() {
        return this;
    }



    // Select Image Picker Show
    public void showImagePickerDialog(FilePickUtils.OnFileChoose onFileChoose) {
        filePickUtils = new FilePickUtils(this, onFileChoose);
        lifeCycleCallBackManager = filePickUtils.getCallBackManager();
        View bottomSheetView = getLayoutInflater().inflate(R.layout.dialog_photo_selector, null);
        bottomSheetDialog = new BottomDialog(BaseActivity.this);
        bottomSheetDialog.setContentView(bottomSheetView);
        final TextView tvCamera = bottomSheetView.findViewById(R.id.tvCamera);
        final TextView tvGallery = bottomSheetView.findViewById(R.id.tvGallery);
        tvCamera.setOnClickListener(onCameraListener);
        tvGallery.setOnClickListener(onGalleryListener);
        bottomSheetDialog.show();
    }

    public void dismissImagePickerDialog() {
        bottomSheetDialog.dismiss();
    }


    private View.OnClickListener onCameraListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            dismissImagePickerDialog();
            filePickUtils.requestImageCamera(FilePickUtils.STORAGE_PERMISSION_CAMERA
                    , true, true);
        }
    };

    private View.OnClickListener onGalleryListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            dismissImagePickerDialog();
            filePickUtils.requestImageGallery(FilePickUtils.STORAGE_PERMISSION_IMAGE
                    , true, true);
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (lifeCycleCallBackManager != null) {
            lifeCycleCallBackManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (lifeCycleCallBackManager != null) {
            lifeCycleCallBackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void clickLeftBtn(final TextView textViewLeft, final TextView textViewRight) {
        textViewLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Selected View Left
                textViewLeft.setTextColor(Color.WHITE);
                textViewLeft.setBackgroundResource(R.drawable.ic_rounded_button_blue);

                // Unselect View Right
                textViewRight.setTextColor(getResources().getColor(R.color.clrIntroRegular));
                textViewRight.setBackgroundResource(R.drawable.ic_rounded_btn_trans);

                onLeftBtnClick(v);
            }
        });
    }

    public void clickRightBtn(final TextView textViewRight, final TextView textViewLeft) {
        textViewRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Selected View Left
                textViewRight.setTextColor(Color.WHITE);
                textViewRight.setBackgroundResource(R.drawable.ic_rounded_button_blue);

                // Unselect View Right
                textViewLeft.setTextColor(getResources().getColor(R.color.clrIntroRegular));
                textViewLeft.setBackgroundResource(R.drawable.ic_rounded_btn_trans);

                onRightBtnClick(v);
            }
        });
    }

    public void defaultSelection(TextView textView) {
        textView.setTextColor(Color.WHITE);
        textView.setBackgroundResource(R.drawable.ic_rounded_button_blue);
    }

    @Override
    public void onLeftBtnClick(View view) {

    }

    @Override
    public void onRightBtnClick(View view) {

    }

    @Override
    public void onCenterBtnClick(View view) {

    }
}
