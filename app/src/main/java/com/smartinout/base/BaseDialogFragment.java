package com.smartinout.base;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smartinout.R;
import com.smartinout.dbflowdatabase.SmartInOutDBCtrl;
import com.smartinout.interfaces.LeftRightClick;
import com.smartinout.retrofit.CallService;
import com.smartinout.utilities.Prefs;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class BaseDialogFragment extends DialogFragment implements LeftRightClick {

    private Dialog progressDialog;
    public Prefs pref;
    public CallService callService;
    public SmartInOutDBCtrl inOutDBCtrl;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = Prefs.getInstance();
        callService = CallService.getInstance(getContext());
        inOutDBCtrl = SmartInOutDBCtrl.getInstance(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        return view;
    }

    public void updateResources(Context context, String language) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).updateResources(context, language);
        }
    }

    public void showProgressDialog(boolean show) {
        if (show) {
            showProgressDialog();
        } else {
            hideProgressDialog();
        }
    }

    public void showProgressDialog() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showProgressDialog(true);
        }
    }

    public void hideProgressDialog() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).hideProgressDialog();
        }
    }

    public void clickLeftBtn(final TextView textViewLeft, final TextView textViewRight) {
        textViewLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Selected View Left
                textViewLeft.setTextColor(Color.WHITE);
                textViewLeft.setBackgroundResource(R.drawable.ic_rounded_button_blue);

                // Unselect View Right
                textViewRight.setTextColor(getResources().getColor(R.color.clrIntroRegular));
                textViewRight.setBackgroundResource(R.drawable.ic_rounded_btn_trans);

                onLeftBtnClick(v);
            }
        });
    }

    public void clickRightBtn(final TextView textViewRight, final TextView textViewLeft) {
        textViewRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Selected View Left
                textViewRight.setTextColor(Color.WHITE);
                textViewRight.setBackgroundResource(R.drawable.ic_rounded_button_blue);

                // Unselect View Right
                textViewLeft.setTextColor(getResources().getColor(R.color.clrIntroRegular));
                textViewLeft.setBackgroundResource(R.drawable.ic_rounded_btn_trans);

                onRightBtnClick(v);
            }
        });
    }

    public void defaultSelection(TextView textView) {
        textView.setTextColor(Color.WHITE);
        textView.setBackgroundResource(R.drawable.ic_rounded_button_blue);
    }

    @Override
    public void onLeftBtnClick(View view) {

    }

    @Override
    public void onRightBtnClick(View view) {

    }

    @Override
    public void onCenterBtnClick(View view) {

    }
}
