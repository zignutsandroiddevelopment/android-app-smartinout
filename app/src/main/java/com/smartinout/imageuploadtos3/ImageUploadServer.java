package com.smartinout.imageuploadtos3;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobile.auth.core.IdentityHandler;
import com.amazonaws.mobile.auth.core.IdentityManager;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.AWSStartupHandler;
import com.amazonaws.mobile.client.AWSStartupResult;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.smartinout.R;
import com.smartinout.utilities.Constants;

import java.io.File;
import java.util.UUID;

/**
 * The type Image upload server.
 */
public class
ImageUploadServer {

    /**
     * The interface Set file upload.
     */
    public interface SetFileUpload {
        /**
         * On file upload success.
         *
         * @param fileName the file name
         */
        void onFileUploadSuccess(String fileName);

        /**
         * On file uploaf fail.
         *
         * @param error the error
         */
        void onFileUploadFail(String error);

        /**
         * On file upload progress.
         *
         * @param progress the progress
         */
        void onFileUploadProgress(float progress);
    }

    private String TAG = ImageUploadServer.class.getName();

    /**
     * The Activity.
     */
    Activity activity;
    /**
     * The File name.
     */
    String fileName = "";

    /**
     * The Secrate key.
     */
    String secrateKey = "";
    /**
     * The Key.
     */
    String key = "";

    private AmazonS3Client amazonS3Client;
    private BasicAWSCredentials awsCredentials;
    private SetFileUpload setFileUpload;

    //    https://s3.ap-south-1.amazonaws.com/smartinout-dev/profile/test.png
    private static final String s3BasicUrl = "https://s3.ap-south-1.amazonaws.com/";

    /**
     * The Name img.
     */
    String nameImg = "";

    private ProgressDialog mDialog;

    /**
     * This constructor for Initilize the basic variable
     * Which require fir the upload file on s3 server.
     *
     * @param activity      the activity
     * @param secrateKey    the secrate key
     * @param key           the key
     * @param setFileUpload the set file upload
     */
    public ImageUploadServer(Activity activity
            , String secrateKey
            , String key
            , SetFileUpload setFileUpload) {
        this.activity = activity;
        this.secrateKey = secrateKey;
        this.key = key;
        this.setFileUpload = setFileUpload;
        // initlizeAws(activity);
    }

    /**
     * Show progress.
     */
    public void showProgress() {
        mDialog = new ProgressDialog(activity, R.style.NewDialog);
        // pDialog.setMessage("Please wait...");
        mDialog.setCancelable(false);
        //pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("0f7e66")));
        mDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        mDialog.show();
    }

    /**
     * Initlize aws.
     *
     * @param activity the activity
     */
    public void initlizeAws(Activity activity) {
        AWSMobileClient.getInstance().initialize(activity, new AWSStartupHandler() {
            @Override
            public void onComplete(AWSStartupResult awsStartupResult) {

                //Make a network call to retrieve the identity ID
                // using IdentityManager. onIdentityId happens UPon success.
                IdentityManager.getDefaultIdentityManager().getUserID(new IdentityHandler() {

                    @Override
                    public void onIdentityId(String s) {

                        //The network call to fetch AWS credentials succeeded, the cached
                        // user ID is available from IdentityManager throughout your app
                        Log.e("MainActivity", "Identity ID is: " + s);
                        Log.e("MainActivity", "Cached Identity ID: " + IdentityManager.getDefaultIdentityManager().getCachedUserID());
                    }

                    @Override
                    public void handleError(Exception e) {
                        Log.e("MainActivity", "Error in retrieving Identity ID: " + e.getMessage());
                    }
                });
            }
        }).execute();
    }


    /**
     * This Fuction is initilization of the
     * Aws Client credential and s3 Client.
     */
    public void initUploadFileSrvice() {
//        AWSMobileClient.getInstance().initialize(activity).execute();

        awsCredentials = new BasicAWSCredentials(key
                , secrateKey);
        amazonS3Client = new AmazonS3Client(awsCredentials);
        //29-11-2018 Change
//      amazonS3Client.setRegion(Region.getRegion(Regions.US_EAST_2));
        amazonS3Client.setRegion(Constants.S3_REGION);
    }


    /**
     * Upload file on s 3.
     *
     * @param fileName       the file name
     * @param pathOFBuket    the path of buket
     * @param isShowProgress the is show progress
     */
//This Function is for Upload file on
    //s3 server.
    public void uploadFileOnS3(String fileName, final String pathOFBuket, boolean isShowProgress) {

        if (isShowProgress) {
            showProgress();
        }

        this.fileName = fileName;
        if (fileName.isEmpty()) {
            return;
        }

        final File file = new File(fileName);
        if (file.exists()) {
            nameImg = UUID.randomUUID().toString() + fileName.substring(fileName.lastIndexOf("."));
            try {
//                TransferUtility transferUtility =
//                        TransferUtility.builder()
//                                .context(App.getAppContext())
//                                .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
//                                .s3Client(new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider()))
//                                .build();

                TransferUtility transferUtility =
                        new TransferUtility(amazonS3Client, activity);

//                TransferUtility transferUtility = TransferUtility.builder()
//                        .context(activity.getApplicationContext())
//                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
//                        .s3Client(new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider()))
//                        .build();


                final TransferObserver transferObserver =
                        transferUtility.upload(pathOFBuket, nameImg, file
                                , CannedAccessControlList.PublicRead);

//                final TransferObserver transferObserver =
//                        transferUtility.upload(pathOFBuket,key,file
//                                ,CannedAccessControlList.PublicRead);

                String buket = transferObserver.getBucket();
                String key = transferObserver.getKey();

                //Logger.withTag("Key " + key + " buket " + buket);

                transferObserver.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if (state.COMPLETED.equals(transferObserver.getState())) {
                            if (mDialog != null && mDialog.isShowing()) {
                                mDialog.dismiss();
                            }
                            StringBuilder uploadUrl = new StringBuilder(s3BasicUrl);
                            uploadUrl
                                    .append(pathOFBuket)
                                    .append("/")
                                    .append(nameImg);
                            setFileUpload.onFileUploadSuccess(uploadUrl.toString());
                        }

                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        long _bytesCurrent = bytesCurrent;
                        long _bytesTotal = bytesTotal;

                        float percentage = ((float) _bytesCurrent / (float) _bytesTotal * 100);
                        Log.d("percentage", "" + percentage);

                        setFileUpload.onFileUploadProgress(percentage);
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        if (mDialog != null && mDialog.isShowing()) {
                            mDialog.dismiss();
                        }
                        setFileUpload.onFileUploadFail(ex.getMessage());
                    }
                });

            } catch (Exception e) {
                setFileUpload.onFileUploadFail(e.getMessage());
            }
        } else {
            setFileUpload.onFileUploadFail(activity.getResources()
                    .getString(R.string.fileNotExtist));
        }
    }

    /**
     * Delete File or Object From Amazone s3 server
     *
     * @param folderPath the folder path
     * @param buketPath  the buket path
     */
    public void deleteObjectsInFolder(final String folderPath, final String buketPath) {

        final String filename = folderPath.substring(folderPath.lastIndexOf("/") + 1);
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String bucketName = buketPath;
                    DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest(bucketName, filename);
                    amazonS3Client.deleteObject(deleteObjectRequest);
                }
            }).start();
        } catch (Exception e) {
            Log.w(TAG, "Delete Exption ", e);
            setFileUpload.onFileUploadFail(activity.getResources().getString(R.string.fileNotExtist));
        }

    }

    /**
     * The Is exist.
     */
    boolean isExist;

    /**
     * Checks 3 image exist or no boolean.
     *
     * @param buketPath  the buket path
     * @param objectName the object name
     * @return the boolean
     */
    public boolean checks3ImageExistOrNo(final String buketPath, final String objectName) {
        isExist = false;
        new Thread(new Runnable() {
            @Override
            public void run() {
                isExist = amazonS3Client.doesObjectExist(buketPath, objectName);
            }
        }).start();


        return isExist;
    }


}
