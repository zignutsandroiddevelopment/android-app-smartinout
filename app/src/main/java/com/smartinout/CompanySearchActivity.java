package com.smartinout;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.smartinout.adapters.CompanyInOutAdapter;
import com.smartinout.adapters.CompanySearchInOutAdapter;
import com.smartinout.base.BaseActivity;
import com.smartinout.databinding.ActivityCompanySearchBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.model.organize.OrganizeData;
import com.smartinout.model.organize.OrganizeModel;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.AppUtilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import retrofit2.Response;

public class CompanySearchActivity extends BaseActivity implements View.OnClickListener, ApiResponse {

    ActivityCompanySearchBinding binding;
    private CompanySearchInOutAdapter companyInOutAdapter;
    private List<OrganizeData> listData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(CompanySearchActivity.this, R.layout.activity_company_search);

        initiateUI();
    }

    @Override
    protected void initiateUI() {

        binding.tvNoData.setVisibility(View.VISIBLE);
        binding.ivBack.setOnClickListener(this);
        binding.ivClearText.setOnClickListener(this);
        binding.edtSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    submitData(binding.edtSearchText.getText().toString());
                    return true;
                }
                return false;
            }
        });

        binding.edtSearchText.addTextChangedListener(searchViewTextWatcher);

    }

    TextWatcher searchViewTextWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if(s.toString().trim().length()==0){
                binding.ivClearText.setVisibility(View.GONE);
                binding.ivSearchText.setVisibility(View.VISIBLE);
            } else {
                binding.ivClearText.setVisibility(View.VISIBLE);
                binding.ivSearchText.setVisibility(View.GONE);
            }


        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // TODO Auto-generated method stub

        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub

        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:
                CompanySearchActivity.this.finish();
                break;
            case R.id.iv_clear_text:
                binding.edtSearchText.setText("");
                binding.ivClearText.setVisibility(View.GONE);
                binding.ivSearchText.setVisibility(View.VISIBLE);
                listData.clear();
                setData(listData);
                break;

        }
    }



    private void submitData(String searchData) {
        if (!AppUtilities.hasInternet(CompanySearchActivity.this, true)) {
            return;
        }
        showProgressDialog();
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.MOBILE, "true");
        params.put(ApiUtils.SEARCH, searchData);
        //params.put(ApiUtils.PAGELIMIT, "50");
        callService.getOrganizationData(params,this);
    }

    @Override
    public void onSuccess(Response response) {

        hideProgressDialog();
        OrganizeModel parsRes = (OrganizeModel) response.body();

        if (parsRes == null) {
            AppUtilities.showToast(CompanySearchActivity.this, getResources().getString(R.string.invalidresposne));
            return;
        }

        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(CompanySearchActivity.this, parsRes.getError());
        }

        List<OrganizeData> organizeData = parsRes.getData();
        if(organizeData.size() > 0) {
            binding.rvSearchlistData.setVisibility(View.VISIBLE);
            binding.tvNoData.setVisibility(View.GONE);
            setData(organizeData);
        }else{
            binding.rvSearchlistData.setVisibility(View.GONE);
            binding.tvNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        AppUtilities.showToast(CompanySearchActivity.this, error);
    }

    private void setData(List<OrganizeData> organizeData) {
        listData = organizeData;
        companyInOutAdapter = new CompanySearchInOutAdapter(this, organizeData);
        binding.rvSearchlistData.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvSearchlistData.setAdapter(companyInOutAdapter);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
