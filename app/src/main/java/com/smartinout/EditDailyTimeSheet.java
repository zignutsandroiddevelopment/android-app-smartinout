package com.smartinout;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.smartinout.base.BaseActivity;
import com.smartinout.databinding.ActivityEditdailytimesheetBinding;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.interfaces.OnItemClick;
import com.smartinout.model.login.Inoutreference;
import com.smartinout.model.timesheet.edittimesheet.EditTimeSheetModel;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;
import com.smartinout.utilities.VehicleTimePickerDialog;

import java.util.Calendar;
import java.util.HashMap;

import androidx.databinding.DataBindingUtil;
import retrofit2.Response;

public class EditDailyTimeSheet extends BaseActivity implements View.OnClickListener, OnItemClick, ApiResponse {

    ActivityEditdailytimesheetBinding binding;
    private DatePickerDialog datePickerDialog;
    private VehicleTimePickerDialog timePickerDialog;
    private int fromYear, fromMonth, fromDay;
    private String from_date;
    private int mHour, mMinute;
    Inoutreference inoutreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(EditDailyTimeSheet.this, R.layout.activity_editdailytimesheet);
        initiateUI();
    }

    @Override
    protected void initiateUI() {

        binding.layoutToolBarInclude.tvTitle.setText(getResources().getString(R.string.edittimesheet));
        binding.layoutToolBarInclude.ivBack.setVisibility(View.VISIBLE);
        binding.layoutToolBarInclude.ivBack.setOnClickListener(this);

        binding.includeWidgetBtn.tvBtnCancel.setText(getResources().getString(R.string.cancel));
        binding.includeWidgetBtn.tvBtnSubmit.setText(getResources().getString(R.string.update));

        binding.includeWidgetBtn.tvBtnCancel.setOnClickListener(this);
        binding.includeWidgetBtn.tvBtnSubmit.setOnClickListener(this);

        binding.tvDate.setOnClickListener(this);
        binding.tvinTime.setOnClickListener(this);
        binding.tvSenttoHr.setOnClickListener(this);

        setInitialFromToDate();
        getSerializeData();

    }

    private void getSerializeData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle.containsKey(Constants.INOUTPREF)) {
            inoutreference = (Inoutreference) bundle.getSerializable(Constants.INOUTPREF);
            String statusMain = inoutreference.getMainStatus();
            String statusSecondary = inoutreference.getSecondryStatus();

            if (statusMain.equals(ApiUtils.CIN)) {// Main Status Clocked In
                if (statusSecondary.equals(ApiUtils.BI)
                        || (statusSecondary.equals(ApiUtils.OUT))
                        || (statusSecondary.equals(ApiUtils.COUT))){
                        //|| (statusSecondary.equals(ApiUtils.IN))) {// Clocked Out
                    binding.tvDate.setEnabled(false);
                } else { // Clocked In
                    binding.tvDate.setEnabled(true);
                }
            } else if (statusMain.equals(ApiUtils.COUT)) {// Main Status Clocked Out
                binding.tvDate.setEnabled(true);
            }

//            if (statusMain.equals(ApiUtils.CIN) || (statusMain.equals(ApiUtils.COUT))) {// Main Status Clocked Out
//                binding.tvDate.setEnabled(true);
//            }

            String previouin = bundle.getString(Constants.INOUTPREF_PREVIOUSTIME).replace(getResources().getString(R.string.in), "").trim();
            if(previouin.length() == 0){
                binding.tvPreviousIn.setVisibility(View.GONE);
            }else{
                binding.tvPreviousIn.setText(getResources().getString(R.string.previousin) + " "+previouin);
                binding.tvPreviousIn.setVisibility(View.VISIBLE);
            }

            if (inoutreference != null) {
                String inTime = inoutreference.getStartTimelbl().replace(getResources().getString(R.string.in), "").trim();
                try {
                    if (inTime.contains(":")) {
                        String[] newTimeArr = inTime.split(" ");
                        String hourMin = newTimeArr[0];
                        String[] hourMinArr = hourMin.split(":");

                        String hour = hourMinArr[0];
                        String min = hourMinArr[1];

                        mHour = Integer.parseInt(hour);
                        mMinute = Integer.parseInt(min);

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                binding.tvinTime.setText(inoutreference.getStartTimelbl().replace(
                        getResources().getString(R.string.in), ""
                ).trim());
                binding.tvOuTime.setText(inoutreference.getEndTimelbl().replace(
                        getResources().getString(R.string.strOut), ""
                ).trim());
                binding.tvDateold.setText(AppUtilities.getCurrentDateEEEEddMMMMYYYY(inoutreference.getStartTime()));
                binding.tvDate.setText(AppUtilities.getCurrentDateddMMyyyy(inoutreference.getStartTime()));
            } else {
                redirectToBackstack();
            }

        } else {
            redirectToBackstack();
        }
    }

    public void redirectToBackstack() {
        AppUtilities.showToast(EditDailyTimeSheet.this, getResources().getString(R.string.invalidresposne));
        EditDailyTimeSheet.this.finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:
                EditDailyTimeSheet.this.finish();
                break;

            case R.id.tvBtnCancel:
                EditDailyTimeSheet.this.finish();
                break;

            case R.id.tvBtnSubmit:
                editTimeSheet();
                break;

            case R.id.tvDate:
                datePickerDialog = new DatePickerDialog(EditDailyTimeSheet.this, R.style.MyCalendarTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        fromYear = year;
                        fromMonth = monthOfYear;
                        fromDay = dayOfMonth;
                        from_date = fromDay + "/" + (fromMonth + 1) + "/" + fromYear;
                        binding.tvDate.setText(from_date);
                    }
                }, fromYear, fromMonth, fromDay);
                //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
                break;

            case R.id.tvinTime:
                timePickerDialog = new VehicleTimePickerDialog(EditDailyTimeSheet.this, R.style.MyCalendarTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        mHour = hourOfDay;
                        mMinute = minute;
                        binding.tvinTime.setText(AppUtilities.getTime(hourOfDay, minute));
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
                break;
            case R.id.tvSenttoHr:
                break;
        }
    }

    public void setInitialFromToDate() {

        Calendar c = Calendar.getInstance();

        fromYear = c.get(Calendar.YEAR);
        fromMonth = c.get(Calendar.MONTH);
        fromDay = c.get(Calendar.DAY_OF_MONTH);

        from_date = fromDay + "/" + (fromMonth + 1) + "/" + fromYear;

    }

    private void editTimeSheet() {
        if (!AppUtilities.hasInternet(EditDailyTimeSheet.this, true)) {
            return;
        }

        if(binding.edtComment.getText().toString().length() == 0){
            AppUtilities.showToast(getActivity(),getActivity().getResources().getString(R.string.err_comment));
            return;
        }

        if (mHour == 0) {
            AppUtilities.showToast(EditDailyTimeSheet.this, getResources().getString(R.string.nochangeindate));
            return;
        }

        showProgressDialog();
        from_date = binding.tvDate.getText().toString();

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(ApiUtils.ID, inoutreference.get_id());
        hashMap.put(ApiUtils.DATE, from_date);//AppUtilities.getCurrentDateddMMyyyy(inoutreference.getStartTime()));
        hashMap.put(ApiUtils.TIME, String.valueOf(mHour + ":" + mMinute));
        hashMap.put(ApiUtils.COMMENT, binding.edtComment.getText().toString());
        callService.editTimeSheet(hashMap, this);
    }



    @Override
    public void onButtonClick(boolean isCancle) {

    }

    @Override
    public void onUpdateClick(int position,String id, boolean isUnbook) {

    }

    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void onItemDelete(int position,String id, String number,String from_time,String  to_time) {

    }

    @Override
    public void onSuccess(Response response) {
        hideProgressDialog();
        EditTimeSheetModel parsRes = (EditTimeSheetModel) response.body();

        if (parsRes == null) {
            AppUtilities.showToast(EditDailyTimeSheet.this, getResources().getString(R.string.invalidresposne));
            return;
        }

        if (!AppUtilities.getValidateString(parsRes.getError()).isEmpty()) {
            AppUtilities.showToast(EditDailyTimeSheet.this, parsRes.getError());
        }

        AppUtilities.showToast(EditDailyTimeSheet.this,getResources().getString(R.string.timesheetupdate));

        Intent intent = new Intent();
        intent.putExtra(ApiUtils.DATE, parsRes.getData().getData().getStartTime());
        setResult(RESULT_OK, intent);
        EditDailyTimeSheet.this.finish();
    }

    @Override
    public void onFailure(String error) {
        hideProgressDialog();
        AppUtilities.showToast(EditDailyTimeSheet.this, error);
    }
}
