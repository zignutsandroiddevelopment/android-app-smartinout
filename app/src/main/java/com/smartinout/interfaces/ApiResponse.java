package com.smartinout.interfaces;

public interface ApiResponse {
    void onSuccess(retrofit2.Response response);

    void onFailure(String error);
}