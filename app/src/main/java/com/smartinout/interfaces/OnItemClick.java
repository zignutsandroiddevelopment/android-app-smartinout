package com.smartinout.interfaces;

public interface OnItemClick {

    void onButtonClick(boolean isCancle);
    void onItemClick(int position);
    void onItemDelete(int position,String id,String number,String from_time,String to_time);
    void onUpdateClick(int position,String id,boolean isUnbook);
}
