package com.smartinout.interfaces;

import android.view.View;

public interface LeftRightClick {

    void onLeftBtnClick(View view);

    void onRightBtnClick(View view);

    void onCenterBtnClick(View view);
}
