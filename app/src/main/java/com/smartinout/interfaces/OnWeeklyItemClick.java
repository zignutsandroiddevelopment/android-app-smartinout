package com.smartinout.interfaces;

import com.smartinout.model.timesheet.CalendarModel;

public interface OnWeeklyItemClick {
    void onClickDailyView(CalendarModel model);
}