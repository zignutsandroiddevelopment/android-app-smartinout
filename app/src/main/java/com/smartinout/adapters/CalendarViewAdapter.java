package com.smartinout.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.R;
import com.smartinout.databinding.RowItemDailycalviewBinding;
import com.smartinout.databinding.RowItemWeeklyinoutBinding;
import com.smartinout.interfaces.OnItemClick;
import com.smartinout.model.timesheet.CalendarModel;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.AppUtilities;

import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class CalendarViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<CalendarModel> listData;
    public static final int TYPE_DAILY = 1;
    public static final int TYPE_WEEKLY = 2;
    private int viewType;
    private OnItemClick onItemClick;

    public CalendarViewAdapter(Context context, ArrayList<CalendarModel> listData, int viewType, OnItemClick onItemClick) {
        this.context = context;
        this.listData = listData;
        this.viewType = viewType;
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_DAILY) {
            RowItemDailycalviewBinding binding = DataBindingUtil.inflate(
                    LayoutInflater.from(context), R.layout.row_item_dailycalview, parent, false
            );
            return new DailyViewHolder(binding);

        } else if (viewType == TYPE_WEEKLY) {
            RowItemWeeklyinoutBinding binding = DataBindingUtil.inflate(
                    LayoutInflater.from(context), R.layout.row_item_weeklyinout, parent, false
            );
            return new WeeklyViewHolder(binding);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof DailyViewHolder) {
            DailyViewHolder dailyViewHolder = (DailyViewHolder) holder;
            dailyViewHolder.binding.tvDate.setText(listData.get(position).getDate());
            dailyViewHolder.binding.tvDay.setText(listData.get(position).getDay());
            if (listData.get(position).isSelected()) {
                dailyViewHolder.binding.tvDay.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                dailyViewHolder.binding.tvDate.setTextColor(Color.WHITE);
                dailyViewHolder.binding.tvDate.setBackgroundResource(R.drawable.circle_blue);
            } else {
                dailyViewHolder.binding.tvDay.setTextColor(context.getResources().getColor(R.color.clrIntroRegular));
                dailyViewHolder.binding.tvDate.setTextColor(context.getResources().getColor(R.color.clrIntroRegular));
                dailyViewHolder.binding.tvDate.setBackgroundResource(R.drawable.ic_transparent);
            }



            //if (selectedDate.before(currentDate)) {
                dailyViewHolder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClick.onItemClick(position);
                    }
                });
            //}



        } else if (holder instanceof WeeklyViewHolder) {
            WeeklyViewHolder weeklyViewHolder = (WeeklyViewHolder) holder;
            weeklyViewHolder.binding.tvDate.setText(listData.get(position).getDate());
            weeklyViewHolder.binding.tvDay.setText(listData.get(position).getDay());
            weeklyViewHolder.binding.tvTime.setText(listData.get(position).getHours());
            weeklyViewHolder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.onItemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class DailyViewHolder extends RecyclerView.ViewHolder {
        RowItemDailycalviewBinding binding;

        public DailyViewHolder(@NonNull RowItemDailycalviewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public class WeeklyViewHolder extends RecyclerView.ViewHolder {
        RowItemWeeklyinoutBinding binding;

        public WeeklyViewHolder(@NonNull RowItemWeeklyinoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return viewType;
    }
}
