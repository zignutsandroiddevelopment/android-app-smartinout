package com.smartinout.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.BookVehicleActivity;
import com.smartinout.R;
import com.smartinout.ScannerActivity;
import com.smartinout.databinding.RowItemVehiclebookedrideBinding;
import com.smartinout.databinding.RowItemVehiclerideBinding;
import com.smartinout.interfaces.OnItemClick;
import com.smartinout.model.vehiclebooking.BookedData;
import com.smartinout.model.vehiclebooking.AvailableData;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;

import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import static androidx.core.util.Preconditions.checkArgument;

public class BookedVehicleInOutAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<BookedData> listData;
    private OnItemClick onItemClick;
    private int vehicleType;
    String fromDate = "";
    String toDate = "";

    public static int TYPE_AVAILABLE = 1;
    public static int TYPE_BOOKED = 2;
    public static int TYPE_RIDEHIS = 3;

    public BookedVehicleInOutAdapter(Context context, List<BookedData> listData,OnItemClick onItemClick, int vehicleType) {
        this.context = context;
        this.listData = listData;
        this.onItemClick = onItemClick;
        this.vehicleType = vehicleType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_BOOKED) {
            RowItemVehiclebookedrideBinding
                    binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_item_vehiclebookedride, parent, false);
            return new VehicleInHolder(binding);
        }else if(viewType == TYPE_RIDEHIS) {
            RowItemVehiclerideBinding
                    binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_item_vehicleride, parent, false);
            return new VehicleRideHisHolder(binding);
        }
        return null;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof VehicleInHolder) {
            final VehicleInHolder vehicleInHolder = (VehicleInHolder) holder;
            boolean isBreakDown = listData.get(position).isBreakDown();
            boolean breakDownDuringTrip = listData.get(position).isBreakdownDuringTrip();
            String status = listData.get(position).getStatus();

            if(isBreakDown && breakDownDuringTrip){
                vehicleInHolder.binding.tvStatus.setText(context.getString(R.string.faultyLable));
            }else{
                vehicleInHolder.binding.tvStatus.setText("");
            }

            vehicleInHolder.binding.ivIcon.setImageResource(R.drawable.ic_car_blue);
            /*vehicleInHolder.binding.tvInOutTime.setText(
                    context.getResources().getString(R.string.from) + " " +
                            listData.get(position).getFrom_time() + " | " +
                            context.getResources().getString(R.string.to) + " " +
                            listData.get(position).getTo_time());*/


            try {
                String toDateTemp = AppUtilities.formatDateFromDateString(Constants.ddMMyyyy, Constants.dd, listData.get(position).getTo_date());
                String[] separatedSecond = AppUtilities.formatDateFromDateString(Constants.ddMMyyyy, Constants.DATE_FORMAT_6, listData.get(position).getTo_date()).split(" ");

                String fromDateTemp = AppUtilities.formatDateFromDateString(Constants.ddMMyyyy, Constants.dd, listData.get(position).getFrom_date());
                String[] separated = AppUtilities.formatDateFromDateString(Constants.ddMMyyyy, Constants.DATE_FORMAT_6, listData.get(position).getFrom_date()).split(" ");

                fromDate = separated[0]+getDayOfMonthSuffix(Integer.parseInt(fromDateTemp))+" "+separated[1]+" "+separated[2];
                toDate = separatedSecond[0]+getDayOfMonthSuffix(Integer.parseInt(toDateTemp))+" "+separatedSecond[1]+" "+separatedSecond[2];
//                Log.e("main",""+getDayOfMonthSuffix(Integer.parseInt(fromDateTemp)));
//                fromDate = AppUtilities.formatDateFromDateString(Constants.ddMMyyyy, Constants.DATE_FORMAT_6, listData.get(position).getFrom_date());
//                toDate = AppUtilities.formatDateFromDateString(Constants.ddMMyyyy, Constants.DATE_FORMAT_6, listData.get(position).getTo_date());


            }catch (Exception e){
                fromDate = listData.get(position).getFrom_date();
                toDate = listData.get(position).getTo_date();
                e.printStackTrace();
            }
            if (fromDate.matches(toDate))
            {
                vehicleInHolder.binding.tvDesc.setVisibility(View.GONE);
                fromDate = context.getResources().getString(R.string.date) + " : "
                        +fromDate+" "+listData.get(position).getFrom_time()+
                        "-"+ listData.get(position).getTo_time();
                vehicleInHolder.binding.tvInOutTime.setSelected(true);
            }
            else {
                vehicleInHolder.binding.tvDesc.setVisibility(View.VISIBLE);
                fromDate= context.getResources().getString(R.string.from) + " : "+fromDate + " " +
                        listData.get(position).getFrom_time();
            }
            vehicleInHolder.binding.tvInOutTime.setText(fromDate );

            vehicleInHolder.binding.tvDesc.setText(
                    context.getResources().getString(R.string.to) + " : " +
                            toDate + " " +
                            listData.get(position).getTo_time());

            //vehicleInHolder.binding.tvDate.setText(listData.get(position).getFrom_date());

            if (listData != null && listData.size() > 0) {
                    AvailableData vehicleData = listData.get(position).getVehicleData();
                    //vehicleDataList.add(vehicleData);
                if(vehicleData != null) {
                    vehicleInHolder.binding.tvTitle.setText(
                            vehicleData.getBrand() + "-" +vehicleData.getName()+"-"+
                                    vehicleData.getNumber());
                }
            }

            vehicleInHolder.binding.frmSwipe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AvailableData vehicleData = listData.get(position).getVehicleData();
                    vehicleInHolder.binding.swipeLayout.close(true);
                    String id = listData.get(position).getId();
                    String number =vehicleData.getBrand() + "-" +vehicleData.getName()+"-"+
                            vehicleData.getNumber();

                    String from_time = fromDate + " " +listData.get(position).getFrom_time();
                    String to_time = toDate + " " +listData.get(position).getTo_time();
                    onItemClick.onItemDelete(position,id,number,from_time,to_time);
                }
            });

            vehicleInHolder.binding.tvGO.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, ScannerActivity.class);
                    i.putExtra("fromGo","someValue");
                    context.startActivity(i);
                }
            });

        }else if (holder instanceof VehicleRideHisHolder) {
            final VehicleRideHisHolder vehicleRideHisHolder = (VehicleRideHisHolder) holder;
            /*vehicleRideHisHolder.binding.tvInOutTime.setText(
                    context.getResources().getString(R.string.in) + " " +
                            listData.get(position).getFrom_time() + " | " +
                            context.getResources().getString(R.string.out) + " " +
                            listData.get(position).getTo_time());*/
            String fromDate = "";
            String toDate = "";
            try {
//                Log.e("fromDate",""+listData.get(position).getFrom_date());
                fromDate = AppUtilities.formatDateFromDateString(Constants.ddMMyyyy, Constants.DATE_FORMAT_5, listData.get(position).getFrom_date());
                toDate = AppUtilities.formatDateFromDateString(Constants.ddMMyyyy, Constants.DATE_FORMAT_5, listData.get(position).getTo_date());
            }catch (Exception e){
                fromDate = listData.get(position).getFrom_date();
                toDate = listData.get(position).getTo_date();

                e.printStackTrace();
            }
            vehicleRideHisHolder.binding.tvInOutTime.setText(
                    context.getResources().getString(R.string.from) + " : " +
                            fromDate + " " +
                            listData.get(position).getFrom_time());
            Log.e("fromDate",""+listData.get(position).getFrom_date());
            vehicleRideHisHolder.binding.tvDesc.setText(
                    context.getResources().getString(R.string.to) + " : " +
                            toDate + " " +
                            listData.get(position).getTo_time());



            if (listData != null && listData.size() > 0) {
                AvailableData vehicleData = listData.get(position).getVehicleData();
                if(listData.get(position).getStatus().equals("EX")){
                    vehicleRideHisHolder.binding.ivIcon.setImageResource(R.drawable.ic_car_red);
                    if(vehicleData != null) {
                        vehicleRideHisHolder.binding.tvTitle.setText(
                                vehicleData.getBrand() + "-" +vehicleData.getName()+"-"+
                                        vehicleData.getNumber());
                    }
                    vehicleRideHisHolder.binding.tvStatus.setText(context.getResources().getString(R.string.expired));
                }else{
                    vehicleRideHisHolder.binding.ivIcon.setImageResource(R.drawable.ic_car_gray);
                    if(vehicleData != null) {
                        vehicleRideHisHolder.binding.tvTitle.setText(
                                context.getResources().getString(R.string.hash) + " " +
                                        vehicleData.getNumber());
                    }
                    vehicleRideHisHolder.binding.tvStatus.setText("");
                }

            }


        }


    }

    @Override
    public int getItemViewType(int position) {
        return vehicleType;
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public List<BookedData> getData() {
        return listData;
    }

    public void removeItem(int position) {
        listData.remove(position);
        notifyItemRemoved(position);
    }

    public class VehicleInHolder extends RecyclerView.ViewHolder {
        RowItemVehiclebookedrideBinding binding;

        public VehicleInHolder(@NonNull RowItemVehiclebookedrideBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public class VehicleRideHisHolder extends RecyclerView.ViewHolder {
        RowItemVehiclerideBinding binding;

        public VehicleRideHisHolder(@NonNull RowItemVehiclerideBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    @SuppressLint("RestrictedApi")
    private String getDayOfMonthSuffix(final int n) {
        checkArgument(n >= 1 && n <= 31, "illegal day of month: " + n);
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }
}
