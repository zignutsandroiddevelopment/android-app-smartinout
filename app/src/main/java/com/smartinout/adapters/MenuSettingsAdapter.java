package com.smartinout.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.R;
import com.smartinout.databinding.RowItemMenusettingDarkBinding;
import com.smartinout.databinding.RowItemMenusettingLightBinding;
import com.smartinout.model.settings.MenuSettings;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class MenuSettingsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<MenuSettings> listData;
    public static final int VIEW_TYPE_LIGHT = 1;
    public static final int VIEW_TYPE_DARK = 2;
    private OnItemClick onItemClick;

    public interface OnItemClick {
        public void getItemClick(MenuSettings menuSettings);
    }

    public MenuSettingsAdapter(Context context, ArrayList<MenuSettings> listData, OnItemClick onItemClick) {
        this.context = context;
        this.listData = listData;
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_LIGHT) {
            RowItemMenusettingLightBinding
                    binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_item_menusetting_light, parent, false);
            return new LightDividerHolder(binding);
        } else if (viewType == VIEW_TYPE_DARK) {
            RowItemMenusettingDarkBinding
                    binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_item_menusetting_dark, parent, false);
            return new DarkDividerHolder(binding);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof LightDividerHolder) {
            LightDividerHolder lightDividerHolder
                    = (LightDividerHolder) holder;
            lightDividerHolder.binding.tvTitle.setText(listData.get(position).getmTitle());
            if (listData.get(position).isSwitch())
                lightDividerHolder.binding.swtch.setVisibility(View.VISIBLE);
            else
                lightDividerHolder.binding.swtch.setVisibility(View.GONE);
            lightDividerHolder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.getItemClick(listData.get(position));
                }
            });

        } else if (holder instanceof DarkDividerHolder) {
            DarkDividerHolder darkDividerHolder
                    = (DarkDividerHolder) holder;
            darkDividerHolder.binding.tvTitle.setText(listData.get(position).getmTitle());
            if (listData.get(position).isSwitch())
                darkDividerHolder.binding.swtch.setVisibility(View.VISIBLE);
            else
                darkDividerHolder.binding.swtch.setVisibility(View.GONE);
            darkDividerHolder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.getItemClick(listData.get(position));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return listData.get(position).getViewType();
    }

    public class LightDividerHolder extends RecyclerView.ViewHolder {
        RowItemMenusettingLightBinding binding;

        public LightDividerHolder(@NonNull RowItemMenusettingLightBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public class DarkDividerHolder extends RecyclerView.ViewHolder {
        RowItemMenusettingDarkBinding binding;

        public DarkDividerHolder(@NonNull RowItemMenusettingDarkBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }
    }
}
