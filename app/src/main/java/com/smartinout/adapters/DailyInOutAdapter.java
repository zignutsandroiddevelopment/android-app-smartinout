package com.smartinout.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.R;
import com.smartinout.databinding.RowItemDailyinoutBinding;
import com.smartinout.databinding.RowItemDailyinoutBindingImpl;
import com.smartinout.interfaces.OnItemClick;
import com.smartinout.model.login.Inoutreference;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Prefs;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class DailyInOutAdapter extends RecyclerView.Adapter<DailyInOutAdapter.DailyInOutHolder> {

    private Context context;
    private List<Inoutreference> listData;
    private OnItemClick onItemClick;
    private String currentDate;
    private Prefs pref;

    public DailyInOutAdapter(Context context,String currentDate, List<Inoutreference> listData, OnItemClick onItemClick) {
        this.context = context;
        this.listData = listData;
        this.onItemClick = onItemClick;
        this.currentDate = currentDate;
        pref = Prefs.getInstance();
    }

    @NonNull
    @Override
    public DailyInOutHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowItemDailyinoutBindingImpl binding
                = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_item_dailyinout, parent, false);
        return new DailyInOutHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull DailyInOutHolder holder, final int position) {
        Inoutreference inoutreference = listData.get(position);
        String statusSecondary = inoutreference.getSecondryStatus();
        String statusMain = inoutreference.getMainStatus();
        String status = "";

        if (statusMain.equals(ApiUtils.CIN)) {// Main Status Clocked In
            if (statusSecondary.equals(ApiUtils.BI)) {// Break In
                status = ApiUtils.BR;
                holder.binding.tvTotalhrs.setVisibility(View.VISIBLE);
            } else if (statusSecondary.equals(ApiUtils.OUT)) {// Out
                status = ApiUtils.OUT;
                holder.binding.tvTotalhrs.setVisibility(View.GONE);
            } else if (statusSecondary.equals(ApiUtils.COUT)) {// Clocked Out
                status = ApiUtils.COUT;
                holder.binding.tvTotalhrs.setVisibility(View.GONE);
            } else { // Clocked In
                if(inoutreference.isClockedIn()) {
                    status = ApiUtils.CIN;
                    holder.binding.tvTotalhrs.setVisibility(View.VISIBLE);
                }else{
                    status = ApiUtils.IN;
                    holder.binding.tvTotalhrs.setVisibility(View.VISIBLE);
                }

            }
        } else if (statusMain.equals(ApiUtils.COUT)) {// Main Status Clocked Out
            if(inoutreference.isClockedOut()) {
                status = ApiUtils.COUT;
                holder.binding.tvTotalhrs.setVisibility(View.GONE);
            }else{
                status = ApiUtils.OUT;
                holder.binding.tvTotalhrs.setVisibility(View.GONE);
            }
        }

        String startTime = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getStartTime());
        /*String endTime = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getEndTime());





        String timezone = pref.getString(Prefs.TIMEZONE, "");
        long convertedDate = AppUtilities.currentDate(timezone);*/


        if(!currentDate.equals(startTime)){
            holder.binding.tvInTime.setText(context.getResources().getString(R.string.previousin) + " " +startTime + " "+ inoutreference.getStartTimelbl());
            holder.binding.tvTotalhrs.setText(inoutreference.getTotalHrs());
            holder.binding.tvStatus.setText(status);

            holder.binding.tvInTime.setTextColor(context.getResources().getColor(R.color.clrlightGray));
            holder.binding.tvTotalhrs.setTextColor(context.getResources().getColor(R.color.clrlightGray));
            holder.binding.tvStatus.setTextColor(context.getResources().getColor(R.color.clrlightGray));
            holder.binding.ivArrow.setVisibility(View.INVISIBLE);

        }else{

            holder.binding.tvInTime.setTextColor(context.getResources().getColor(android.R.color.black));
            holder.binding.tvTotalhrs.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.binding.tvStatus.setTextColor(context.getResources().getColor(android.R.color.black));
            holder.binding.ivArrow.setVisibility(View.VISIBLE);

            holder.binding.tvInTime.setText(inoutreference.getStartTimelbl());
            holder.binding.tvTotalhrs.setText(inoutreference.getTotalHrs());
            holder.binding.tvStatus.setText(status);

            holder.binding.rlDailyView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.onItemClick(position);
                }
            });
        }


        /*if(startTime.equals(endTime)){

        }else{

            //holder.binding.tvTotalhrs.setText(inoutreference.getTotalHrs());
        }*/



    }

    public void setCurrentDate(String currentDate){
        this.currentDate = currentDate;
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class DailyInOutHolder extends RecyclerView.ViewHolder {
        RowItemDailyinoutBindingImpl binding;

        public DailyInOutHolder(@NonNull RowItemDailyinoutBindingImpl binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
