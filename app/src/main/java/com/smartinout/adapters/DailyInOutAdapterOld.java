package com.smartinout.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.R;
import com.smartinout.databinding.RowItemDailyinoutoldBindingImpl;
import com.smartinout.interfaces.OnItemClick;
import com.smartinout.model.login.Inoutreference;
import com.smartinout.utilities.AppUtilities;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class DailyInOutAdapterOld extends RecyclerView.Adapter<DailyInOutAdapterOld.DailyInOutHolder> {

    private Context context;
    private List<Inoutreference> listData;
    private OnItemClick onItemClick;

    public DailyInOutAdapterOld(Context context, List<Inoutreference> listData, OnItemClick onItemClick) {
        this.context = context;
        this.listData = listData;
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public DailyInOutHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowItemDailyinoutoldBindingImpl binding
                = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_item_dailyinoutold, parent, false);
        return new DailyInOutHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull DailyInOutHolder holder, final int position) {
        Inoutreference inoutreference = listData.get(position);
        String startTime = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getStartTime());
        String endTime = AppUtilities.getCurrentDateddMMyyyy(inoutreference.getEndTime());
        holder.binding.tvIn.setText(startTime + " " +inoutreference.getStartTimelbl());
        holder.binding.tvOut.setText(endTime + " " +inoutreference.getEndTimelbl());
        holder.binding.tvHrs.setText(inoutreference.getTotalHrs());
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class DailyInOutHolder extends RecyclerView.ViewHolder {
        RowItemDailyinoutoldBindingImpl binding;

        public DailyInOutHolder(@NonNull RowItemDailyinoutoldBindingImpl binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
