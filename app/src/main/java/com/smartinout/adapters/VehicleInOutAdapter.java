package com.smartinout.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smartinout.R;
import com.smartinout.databinding.RowItemVehicleinBinding;
import com.smartinout.databinding.RowItemVehiclerideBinding;
import com.smartinout.interfaces.OnItemClick;
import com.smartinout.model.vehiclebooking.AvailableData;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class VehicleInOutAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<AvailableData> listData;
    private int vehicleType;
    public static int TYPE_AVAILABLE = 1;
    public static int TYPE_BOOKED = 2;
    public static int TYPE_RIDEHIS = 3;
    private OnItemClick onItemClick;

    public VehicleInOutAdapter(Context context, List<AvailableData> listData, int vehicleType, OnItemClick onItemClick) {
        this.context = context;
        this.listData = listData;
        this.vehicleType = vehicleType;
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_AVAILABLE) {
            RowItemVehicleinBinding
                    binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_item_vehiclein, parent, false);
            return new VehicleInHolder(binding);
        } else if (viewType == TYPE_RIDEHIS) {
            RowItemVehiclerideBinding
                    binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_item_vehicleride, parent, false);
            return new VehicleRideHisHolder(binding);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof VehicleInHolder) {
            VehicleInHolder vehicleInHolder = (VehicleInHolder) holder;
            vehicleInHolder.binding.tvTitle.setText(
                    context.getResources().getString(R.string.vehiclenumber) + " " +
                            listData.get(position).getNumber());
            vehicleInHolder.binding.tvDesc.setText(
                    context.getResources().getString(R.string.brand) + " - " +
                            listData.get(position).getBrand());

        } else if (holder instanceof VehicleRideHisHolder) {

        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return vehicleType;
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public List<AvailableData> getData() {
        return listData;
    }

    public void removeItem(int position) {
        listData.remove(position);
        notifyItemRemoved(position);
    }

    public class VehicleInHolder extends RecyclerView.ViewHolder {
        RowItemVehicleinBinding binding;

        public VehicleInHolder(@NonNull RowItemVehicleinBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public class VehicleRideHisHolder extends RecyclerView.ViewHolder {
        RowItemVehiclerideBinding binding;

        public VehicleRideHisHolder(@NonNull RowItemVehiclerideBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
