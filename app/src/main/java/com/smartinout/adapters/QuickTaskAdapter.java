package com.smartinout.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.smartinout.R;
import com.smartinout.base.App;
import com.smartinout.databinding.RowItemQuicktaskBinding;
import com.smartinout.model.timesheet.quickpick.QuickTask;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.Logger;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.adapters.AdapterViewBindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

public class QuickTaskAdapter extends RecyclerView.Adapter<QuickTaskAdapter.QuickTaskHolder> {

    private Context context;
    private List<QuickTask> listData;
    private OnItemSelected onItemSelected;

    public QuickTaskAdapter(Context context, List<QuickTask> listData) {
        this.context = context;
        this.listData = listData;
    }
    public QuickTaskAdapter(Context context, List<QuickTask> listData, OnItemSelected onItemSelected) {
        this.context = context;
        this.listData = listData;
        this.onItemSelected = onItemSelected;
    }

    @NonNull
    @Override
    public QuickTaskHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowItemQuicktaskBinding binding
                = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_item_quicktask, parent, false);
        return new QuickTaskHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull QuickTaskHolder holder, final int position) {
        holder.binding.tvQuickTaskTitle.setText(listData.get(position).getName());// + " - "+ listData.get(position).getStatus());

        /*String inoutMainStatus = App.getInOutMainStatus();
        String quickStatus = App.getQuickStatus();

        String status = listData.get(position).getStatus();
        if (inoutMainStatus.equals(ApiUtils.CIN)) {// Main Status Clocked In
            if (quickStatus.equals(ApiUtils.IN)) {
                if (status.equals(ApiUtils.OUT)) {
                    holder.binding.tvQuickTaskTitle.setVisibility(View.VISIBLE);
                } else {
                    holder.binding.tvQuickTaskTitle.setVisibility(View.GONE);
                }
            } else {
                if (status.equals(ApiUtils.IN)) {
                    holder.binding.tvQuickTaskTitle.setVisibility(View.VISIBLE);
                } else {
                    holder.binding.tvQuickTaskTitle.setVisibility(View.GONE);
                }
            }
        }else{
            if (status.equals(ApiUtils.IN)) {
                holder.binding.tvQuickTaskTitle.setVisibility(View.VISIBLE);
            } else {
                holder.binding.tvQuickTaskTitle.setVisibility(View.GONE);
            }
        }*/
        holder.binding.tvQuickTaskTitle.setVisibility(View.VISIBLE);
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemSelected.getItemSelected(listData.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class QuickTaskHolder extends RecyclerView.ViewHolder {

        RowItemQuicktaskBinding binding;

        public QuickTaskHolder(@NonNull RowItemQuicktaskBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface OnItemSelected{
        public void getItemSelected(QuickTask quickTaskData);
    }
}
