package com.smartinout.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.smartinout.R;
import com.smartinout.databinding.RowItemClockedinBinding;
import com.smartinout.model.timesheet.InoutDatum;
import com.smartinout.utilities.AppUtilities;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class ActivityLogsAdapter extends RecyclerView.Adapter<ActivityLogsAdapter.ActivityLogsInOutHolder> {

    private Context context;
    private List<InoutDatum> lisData;

    public ActivityLogsAdapter(Context context, List<InoutDatum> lisData) {
        this.context = context;
        this.lisData = lisData;
    }

    @NonNull
    @Override
    public ActivityLogsAdapter.ActivityLogsInOutHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowItemClockedinBinding binding =
                    DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_item_clockedin, parent, false);
        return new ActivityLogsInOutHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ActivityLogsAdapter.ActivityLogsInOutHolder holder, int position) {
        //lisData.get(position).
        String quickpickId = lisData.get(position).getQuickpic_name();
        if(quickpickId != null){
            holder.binding.tvActivityLogTitle.setText(quickpickId);
        }else{
            holder.binding.tvActivityLogTitle.setText(getTitle(lisData.get(position).getColor()));
        }

        holder.binding.tvActivityLogHours.setText(AppUtilities.hmTimeFormatterfromMinutes(lisData.get(position).getMinutes()));
        holder.binding.progressClockedIn.setProgressDrawable(getProgressDrawable(lisData.get(position).getColor()));
        holder.binding.progressClockedIn.setProgress((int) lisData.get(position).getPercentage());
    }

    @Override
    public int getItemCount() {
        return lisData.size();
    }

    public class ActivityLogsInOutHolder extends RecyclerView.ViewHolder {
        RowItemClockedinBinding binding;

        public ActivityLogsInOutHolder(@NonNull RowItemClockedinBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public Drawable getProgressDrawable(String color) {
        if (AppUtilities.CLR_GREEN.equals(color))
            return context.getResources().getDrawable(R.drawable.custom_progressbar_clockedin);//Green-In
        else if (AppUtilities.CLR_GREY.equals(color))
            return context.getResources().getDrawable(R.drawable.custom_progressbar_out);//Grey-Out
        else if (AppUtilities.CLR_RED.equals(color))
            return context.getResources().getDrawable(R.drawable.custom_progressbar_break);//Red-Break

        return context.getResources().getDrawable(R.drawable.custom_progressbar_clockedin);// Default Blue - primary
    }

    public String getTitle(String color) {
        if (AppUtilities.CLR_GREEN.equals(color)) {
            return context.getResources().getString(R.string.clockedinsmall);

        } else if (AppUtilities.CLR_RED.equals(color)) {
            return context.getResources().getString(R.string.strbreaksmall);

        } else if (AppUtilities.CLR_GREY.equals(color)) {
            return context.getResources().getString(R.string.strOut);
        }

        return context.getResources().getString(R.string.unknow);// Default Texts
    }
}
