package com.smartinout.adapters;

import android.content.Context;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smartinout.ProfileActivity;
import com.smartinout.R;
import com.smartinout.databinding.RowItemCompanyinoutBinding;
import com.smartinout.databinding.RowItemProgressBinding;
import com.smartinout.imagepicker.ImagePickerUtils;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.model.login.Inoutreference;
import com.smartinout.model.organize.OrganizeData;
import com.smartinout.model.userinout.CompanyInOut;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.ImageDisplayUitls;
import com.smartinout.utilities.Prefs;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class CompanyInOutAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<OrganizeData> listData;
    Prefs pref;
    public static int VIEWTYPE_USER = 1;
    public static int VIEWTYPE_LOADING = 3;

    public CompanyInOutAdapter(Context context, List<OrganizeData> listData) {
        this.context = context;
        this.listData = listData;
        pref = Prefs.getInstance();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEWTYPE_USER) {
            RowItemCompanyinoutBinding binding
                    = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_item_companyinout, parent, false);
            return new CompanyInOutHolder(binding);
        } else {
            RowItemProgressBinding progressBinding =
                    DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_item_progress,
                            parent, false);
            return new ProgressBarHolder(progressBinding);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CompanyInOutHolder) {
            final CompanyInOutHolder companyInOutHolder = (CompanyInOutHolder) holder;

            String uName = StringUtils.capitalize(listData.get(position).getFirstname())
                    + " " + StringUtils.capitalize(listData.get(position).getLastname());

            String profileImg = String.valueOf(listData.get(position).getFirstname().charAt(0))
                    + String.valueOf(listData.get(position).getLastname().charAt(0));
            ImageDisplayUitls.displayTextDrawable(profileImg, companyInOutHolder.binding.ivIcon,
                    context.getResources().getColor(R.color.colorPrimary),
                    context.getResources().getColor(android.R.color.white));


            Inoutreference inoutreference = listData.get(position).getInoutreference();
            if (inoutreference != null) {
                String description = listData.get(position).getInoutreference().getComment();

                if(description.length() > 0){
                    companyInOutHolder.binding.llD.setVisibility(View.VISIBLE);
                    companyInOutHolder.binding.llDCentered.setVisibility(View.GONE);
                    companyInOutHolder.binding.tvDesc.setText(description);
                    companyInOutHolder.binding.tvTitle.setText(uName);
                }else{
                    companyInOutHolder.binding.llD.setVisibility(View.GONE);
                    companyInOutHolder.binding.llDCentered.setVisibility(View.VISIBLE);
                    companyInOutHolder.binding.tvTitleD.setText(uName);
                }

                String statusMain = listData.get(position).getInoutreference().getSecondryStatus();
                String statusSecondary = listData.get(position).getInoutreference().getMainStatus();

                companyInOutHolder.binding.tvGreenDot.setVisibility(View.VISIBLE);
                companyInOutHolder.binding.tvStatus.setVisibility(View.VISIBLE);

                if (statusMain.equals(ApiUtils.CIN) || statusMain.equals(ApiUtils.IN)) {
                    if (statusSecondary.equals(ApiUtils.BI)) {
                        companyInOutHolder.binding.tvGreenDot.setBackground(context.getResources().getDrawable(R.drawable.circle_red));
                        companyInOutHolder.binding.tvStatus.setText(ApiUtils.OUT);
                        companyInOutHolder.binding.tvStatus.setTextColor(context.getResources().getColor(R.color.clrred));
                    } else if (statusSecondary.equals(ApiUtils.OUT)) {
                        companyInOutHolder.binding.tvGreenDot.setBackground(context.getResources().getDrawable(R.drawable.circle_red));
                        companyInOutHolder.binding.tvStatus.setText(ApiUtils.OUT);
                        companyInOutHolder.binding.tvStatus.setTextColor(context.getResources().getColor(R.color.clrred));
                    } else if (statusSecondary.equals(ApiUtils.COUT)) {
                        companyInOutHolder.binding.tvGreenDot.setBackground(context.getResources().getDrawable(R.drawable.circle_red));
                        companyInOutHolder.binding.tvStatus.setText(ApiUtils.OUT);
                        companyInOutHolder.binding.tvStatus.setTextColor(context.getResources().getColor(R.color.clrred));
                    } else {
                        companyInOutHolder.binding.tvGreenDot.setBackground(context.getResources().getDrawable(R.drawable.circle_green));
                        companyInOutHolder.binding.tvStatus.setText(ApiUtils.IN);
                        companyInOutHolder.binding.tvStatus.setTextColor(context.getResources().getColor(R.color.clrgreen));
                    }
                } else {
                    companyInOutHolder.binding.tvGreenDot.setBackground(context.getResources().getDrawable(R.drawable.circle_red));
                    companyInOutHolder.binding.tvStatus.setText(ApiUtils.OUT);
                    companyInOutHolder.binding.tvStatus.setTextColor(context.getResources().getColor(R.color.clrred));
                }
            } else {
                companyInOutHolder.binding.tvDesc.setVisibility(View.GONE);
                companyInOutHolder.binding.tvGreenDot.setVisibility(View.GONE);
                companyInOutHolder.binding.tvStatus.setVisibility(View.GONE);
            }


        } else {
            final ProgressBarHolder progressBarHolder = (ProgressBarHolder) holder;
            progressBarHolder.progressBinding.progressLoadmore.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class CompanyInOutHolder extends RecyclerView.ViewHolder {
        RowItemCompanyinoutBinding binding;

        public CompanyInOutHolder(@NonNull RowItemCompanyinoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public class ProgressBarHolder extends RecyclerView.ViewHolder {
        RowItemProgressBinding progressBinding;

        public ProgressBarHolder(@NonNull RowItemProgressBinding progressBinding) {
            super(progressBinding.getRoot());
            this.progressBinding = progressBinding;
        }
    }

    /**
     * Add loading view.
     */
    public void addLoadingView() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                listData.add(null);
                notifyItemInserted(listData.size() - 1);
            }
        });
    }

    /**
     * Remove loading view.
     */
    public void removeLoadingView() {
        listData.remove(listData.size() - 1);
        notifyItemRemoved(listData.size());
    }

    @Override
    public int getItemViewType(int position) {
        if (listData.get(position) == null)
            return VIEWTYPE_LOADING;
        return VIEWTYPE_USER;
    }
}
