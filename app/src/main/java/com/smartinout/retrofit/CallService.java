package com.smartinout.retrofit;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.smartinout.LoginActivity;
import com.smartinout.R;
import com.smartinout.interfaces.ApiResponse;
import com.smartinout.model.BaseResponse;
import com.smartinout.model.ErrorPojo;
import com.smartinout.model.ForgotPassReqPojo;
import com.smartinout.model.dashboard.DashbordVehicleModel;
import com.smartinout.model.faultreport.FaultReportModel;
import com.smartinout.model.fuel.FuelRecieptModel;
import com.smartinout.model.login.LoginPojo;
import com.smartinout.model.login.OnlineUserReference;
import com.smartinout.model.organize.OrganizeModel;
import com.smartinout.model.qrcode.QrCodeOfficeModel;
import com.smartinout.model.qrcode.QrCodeVehicleModel;
import com.smartinout.model.timesheet.DailyTimeSheetsPojo;
import com.smartinout.model.timesheet.edittimesheet.EditTimeSheetModel;
import com.smartinout.model.timesheet.quickpick.QuickPickPojo;
import com.smartinout.model.timesheet.weekly.WeeklyTimeSheet;
import com.smartinout.model.userinout.ClockedInOutPojo;
import com.smartinout.model.vehiclebooking.BookedModel;
import com.smartinout.model.vehiclebooking.AvailableModel;
import com.smartinout.model.vehiclebooking.CreateBookingModel;
import com.smartinout.model.vehiclebooking.UnBookModel;
import com.smartinout.model.vehicleinout.starttrip.StartTripModel;
import com.smartinout.model.vehicleinout.VehicleInfoModel;
import com.smartinout.model.vehicleinout.stoptrip.StopTripModel;
import com.smartinout.utilities.ActivityUtils;
import com.smartinout.utilities.Prefs;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallService {

    private Context context;
    private SOService soService;
    public static CallService callServerApi;

    public CallService(Context context) {
        this.context = context;
        this.soService = ApiUtils.getSOService();
    }

    public HashMap<String, String> getHeaderParameter() {
        HashMap<String, String> params = new HashMap<>();
        Log.e(ApiUtils.TOKEN, Prefs.getInstance().getString(ApiUtils.TOKEN, ""));
        params.put(ApiUtils.X_AUTH, Prefs.getInstance().getString(ApiUtils.TOKEN, ""));
        params.put("Accept", "application/json");
        return params;
    }


    public static CallService getInstance(Context context) {
        if (callServerApi == null) {
            callServerApi = new CallService(context);
        }
        return callServerApi;
    }

    public void loginApi(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.loginUser(params).enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void generatePwd(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.generateUserPwd(params).enqueue(new Callback<ForgotPassReqPojo>() {
            @Override
            public void onResponse(Call<ForgotPassReqPojo> call, Response<ForgotPassReqPojo> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<ForgotPassReqPojo> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void setPwdUser(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.setPswUser(params).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void userlivestatus(final ApiResponse apiResponse) {
        soService.userlivestatus(getHeaderParameter()).enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void dailyTimesheet(String currentDate, final ApiResponse apiResponse) {
        soService.dailyTimesheet(getHeaderParameter(), currentDate, "true")
                .enqueue(new Callback<DailyTimeSheetsPojo>() {
                    @Override
                    public void onResponse(Call<DailyTimeSheetsPojo> call, Response<DailyTimeSheetsPojo> response) {
                        if (response.body() != null) {
                            if (response.body().getStatus() == ApiUtils.STATUS_200)
                                apiResponse.onSuccess(response);
                            else
                                apiResponse.onFailure(response.body().getError());
                        } else {
                            apiResponse.onFailure(getErrorBody(response));
                        }
                    }

                    @Override
                    public void onFailure(Call<DailyTimeSheetsPojo> call, Throwable t) {
                        apiResponse.onFailure(t.getMessage());
                    }
                });
    }

    public void weeklyTimesheet(String currentDate, final ApiResponse apiResponse) {
        soService.weeklyTimesheet(getHeaderParameter(), currentDate)
                .enqueue(new Callback<WeeklyTimeSheet>() {
                    @Override
                    public void onResponse(Call<WeeklyTimeSheet> call, Response<WeeklyTimeSheet> response) {
                        if (response.body() != null) {
                            if (response.body().getStatus() == ApiUtils.STATUS_200)
                                apiResponse.onSuccess(response);
                            else
                                apiResponse.onFailure(response.body().getError());
                        } else {
                            apiResponse.onFailure(getErrorBody(response));
                        }
                    }

                    @Override
                    public void onFailure(Call<WeeklyTimeSheet> call, Throwable t) {
                        apiResponse.onFailure(t.getMessage());
                    }
                });
    }

    public void clockedIn(final ApiResponse apiResponse) {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.STATUS, ApiUtils.CIN);

        soService.updateInOutStatus(getHeaderParameter(), params).enqueue(new Callback<ClockedInOutPojo>() {
            @Override
            public void onResponse(Call<ClockedInOutPojo> call, Response<ClockedInOutPojo> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<ClockedInOutPojo> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void in(final ApiResponse apiResponse,String quickPickId) {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.STATUS, ApiUtils.IN);
        if(quickPickId != null){
            params.put(ApiUtils.QUICKPIC_ID, ApiUtils.CIN);
        }
        soService.updateInOutStatus(getHeaderParameter(), params).enqueue(new Callback<ClockedInOutPojo>() {
            @Override
            public void onResponse(Call<ClockedInOutPojo> call, Response<ClockedInOutPojo> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<ClockedInOutPojo> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void breakIn(final ApiResponse apiResponse) {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.STATUS, ApiUtils.BI);

        soService.updateInOutStatus(getHeaderParameter(), params).enqueue(new Callback<ClockedInOutPojo>() {
            @Override
            public void onResponse(Call<ClockedInOutPojo> call, Response<ClockedInOutPojo> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<ClockedInOutPojo> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void breakEnd(final ApiResponse apiResponse) {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.STATUS, ApiUtils.BS);

        soService.updateInOutStatus(getHeaderParameter(), params).enqueue(new Callback<ClockedInOutPojo>() {
            @Override
            public void onResponse(Call<ClockedInOutPojo> call, Response<ClockedInOutPojo> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<ClockedInOutPojo> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void clockedOut(final ApiResponse apiResponse) {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.STATUS, ApiUtils.COUT);

        soService.updateInOutStatus(getHeaderParameter(), params).enqueue(new Callback<ClockedInOutPojo>() {
            @Override
            public void onResponse(Call<ClockedInOutPojo> call, Response<ClockedInOutPojo> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<ClockedInOutPojo> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void editProfile(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.editProfile(getHeaderParameter(), params).enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }


    public void changePwd(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.changePwdUser(getHeaderParameter(), params).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void editTimeSheet(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.editTimeSheet(getHeaderParameter(), params).enqueue(new Callback<EditTimeSheetModel>() {
            @Override
            public void onResponse(Call<EditTimeSheetModel> call, Response<EditTimeSheetModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<EditTimeSheetModel> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }
    public void addTimeSheet(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.addTimeSheet(getHeaderParameter(), params).enqueue(new Callback<EditTimeSheetModel>() {
            @Override
            public void onResponse(Call<EditTimeSheetModel> call, Response<EditTimeSheetModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<EditTimeSheetModel> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }


    public void getAvailableVehicle(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.getAvailableVehicle(getHeaderParameter(), params).enqueue(new Callback<AvailableModel>() {
            @Override
            public void onResponse(Call<AvailableModel> call, Response<AvailableModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<AvailableModel> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void getBookedVehicle(HashMap<String, String> params,final ApiResponse apiResponse) {
        soService.getBookedVehicle(getHeaderParameter(),params).enqueue(new Callback<BookedModel>() {
            @Override
            public void onResponse(Call<BookedModel> call, Response<BookedModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<BookedModel> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }
    public void bookedDataDashboard(HashMap<String, String> params,final ApiResponse apiResponse) {
        soService.bookedDataDashboard(getHeaderParameter(),params).enqueue(new Callback<DashbordVehicleModel>() {
            @Override
            public void onResponse(Call<DashbordVehicleModel> call, Response<DashbordVehicleModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<DashbordVehicleModel> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void bookVehicle(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.bookVehicle(getHeaderParameter(), params).enqueue(new Callback<CreateBookingModel>() {
            @Override
            public void onResponse(Call<CreateBookingModel> call, Response<CreateBookingModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<CreateBookingModel> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void unbookVehicle(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.unbookVehicle(getHeaderParameter(), params).enqueue(new Callback<UnBookModel>() {
            @Override
            public void onResponse(Call<UnBookModel> call, Response<UnBookModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<UnBookModel> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void getQrCodeDetails(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.getQrCodeDetails(getHeaderParameter(), params).enqueue(new Callback<QrCodeVehicleModel>() {
            @Override
            public void onResponse(Call<QrCodeVehicleModel> call, Response<QrCodeVehicleModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<QrCodeVehicleModel> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void getOfficeQrDetails(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.getOfficeQrDetails(getHeaderParameter(), params).enqueue(new Callback<QrCodeOfficeModel>() {
            @Override
            public void onResponse(Call<QrCodeOfficeModel> call, Response<QrCodeOfficeModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<QrCodeOfficeModel> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void onGoingTrip(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.onGoingTrip(getHeaderParameter(), params).enqueue(new Callback<StartTripModel>() {
            @Override
            public void onResponse(Call<StartTripModel> call, Response<StartTripModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<StartTripModel> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void onStartTrip(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.onStartTrip(getHeaderParameter(), params).enqueue(new Callback<StartTripModel>() {
            @Override
            public void onResponse(Call<StartTripModel> call, Response<StartTripModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<StartTripModel> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void onEndTrip(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.onEndTrip(getHeaderParameter(), params).enqueue(new Callback<StopTripModel>() {
            @Override
            public void onResponse(Call<StopTripModel> call, Response<StopTripModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<StopTripModel> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void fuelReciept(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.fuelReciept(getHeaderParameter(), params).enqueue(new Callback<FuelRecieptModel>() {
            @Override
            public void onResponse(Call<FuelRecieptModel> call, Response<FuelRecieptModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<FuelRecieptModel> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void faultReport(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.faultReport(getHeaderParameter(), params).enqueue(new Callback<FaultReportModel>() {
            @Override
            public void onResponse(Call<FaultReportModel> call, Response<FaultReportModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<FaultReportModel> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void getVehicleInfo(HashMap<String, String> params, final ApiResponse apiResponse) {
        soService.getVehicleInfo(getHeaderParameter(), params).enqueue(new Callback<VehicleInfoModel>() {
            @Override
            public void onResponse(Call<VehicleInfoModel> call, Response<VehicleInfoModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<VehicleInfoModel> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }



    private String getErrorBody(Response response) {
        Gson gson = new GsonBuilder().create();
        ErrorPojo mError = new ErrorPojo();
        String error = "";
        try {
            if (response.code() == ApiUtils.STATUS_401) {
                //mError.setError(response.message());
                Prefs.getInstance().clearAll();
                ActivityUtils.launchActivityWithClearBackStack(context, LoginActivity.class);
            } else if (response.code() == ApiUtils.STATUS_500) {
                mError.setError(context.getResources().getString(R.string.internalservererror));
            } else {
                mError.setError(context.getResources().getString(R.string.internalservererror));
                /*try {
                    mError = gson.fromJson(response.errorBody().string(), ErrorPojo.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
            }
            error = mError.getError();

        } catch (Exception e) {
            // handle failure to read error
            error = context.getResources().getString(R.string.internalservererror);//"";
        }
        return error;
    }

    public void getQuickPicks(final ApiResponse apiResponse) {
        /*soService.getQuickPicks(getHeaderParameter(),"Admin").enqueue(new Callback<QuickPickPojo>() {*/
        soService.getQuickPicks(getHeaderParameter()).enqueue(new Callback<QuickPickPojo>() {
            @Override
            public void onResponse(Call<QuickPickPojo> call, Response<QuickPickPojo> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<QuickPickPojo> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });

    }

    public void updateQuickPickIn(final ApiResponse apiResponse,String quickPickId,String quickPickStatus) {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.STATUS, quickPickStatus);
        params.put(ApiUtils.QUICKPIC_ID, quickPickId);
        soService.updateInOutStatus(getHeaderParameter(), params).enqueue(new Callback<ClockedInOutPojo>() {
            @Override
            public void onResponse(Call<ClockedInOutPojo> call, Response<ClockedInOutPojo> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<ClockedInOutPojo> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void updateQuickPickOut(final ApiResponse apiResponse,String quickPickId,String quickPickStatus) {
        HashMap<String, String> params = new HashMap<>();
        params.put(ApiUtils.STATUS, quickPickStatus);
        params.put(ApiUtils.QUICKPIC_ID, quickPickId);
        soService.updateInOutStatus(getHeaderParameter(), params).enqueue(new Callback<ClockedInOutPojo>() {
            @Override
            public void onResponse(Call<ClockedInOutPojo> call, Response<ClockedInOutPojo> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<ClockedInOutPojo> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void updateStatus(HashMap<String, String> params,final ApiResponse apiResponse) {
        soService.updateInOutStatus(getHeaderParameter(), params).enqueue(new Callback<ClockedInOutPojo>() {
            @Override
            public void onResponse(Call<ClockedInOutPojo> call, Response<ClockedInOutPojo> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<ClockedInOutPojo> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void getOrganizationData(HashMap<String, String> params,final ApiResponse apiResponse) {
        soService.getOrganizationData(getHeaderParameter(), params).enqueue(new Callback<OrganizeModel>() {
            @Override
            public void onResponse(Call<OrganizeModel> call, Response<OrganizeModel> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<OrganizeModel> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }

    public void getOnlineStatus(final ApiResponse apiResponse) {
        soService.getOnlineStatus(getHeaderParameter()).enqueue(new Callback<OnlineUserReference>() {
            @Override
            public void onResponse(Call<OnlineUserReference> call, Response<OnlineUserReference> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == ApiUtils.STATUS_200)
                        apiResponse.onSuccess(response);
                    else
                        apiResponse.onFailure(response.body().getError());
                } else {
                    apiResponse.onFailure(getErrorBody(response));
                }
            }

            @Override
            public void onFailure(Call<OnlineUserReference> call, Throwable t) {
                apiResponse.onFailure(t.getMessage());
            }
        });
    }




}
