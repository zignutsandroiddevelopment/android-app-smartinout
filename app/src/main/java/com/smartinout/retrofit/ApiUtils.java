package com.smartinout.retrofit;

public class ApiUtils {

//    public static final String BASE_URL_TEST = "http://35.165.10.205/smartinout-api/api/";
    public static final String BASE_URL_TEST = "http://smartinout.zignuts.com/api/";
    public static final int STATUS_401 = 401;
    public static final int STATUS_200 = 200;
    public static final int STATUS_500 = 500;

    private static final String AUTH = "auth/";

    public static final String LOGIN_USER = AUTH + "login";
    public static final String GENERATE_USER_PWD = AUTH + "generatePasswordUser";
    public static final String SET_USER_PWD = AUTH + "setPassword";
    public static final String CHANGE_PWD = AUTH + "resetPassword";

    private static final String USER = "user/";

    public static final String EDIT_PROFILE = USER + "editProfile";

    private static final String INOUT = "inout/";

    public static final String UPDATE_INOUT_STATUS = INOUT + "updateinoutStatus";
    public static final String LIVE_INOUT_STATUS = INOUT + "getLiveStatus";
    public static final String DAILY_TIMESHEET = INOUT + "getDailyTimeSheet";
    public static final String WEEKLY_TIMESHEET = INOUT + "getWeeklyTimeSheet";
    public static final String EDIT_TIMESHEET = INOUT + "editTimeSheet";
    public static final String ADD_TIMESHEET = INOUT + "addTimeSheet";


    public static final String GET_QUICKPICKS = "quickPicks";

    public static final String VEHICLE_BOOKING = "vehicalBooking";
    public static final String START_TRIP = "startTrip";
    public static final String END_TRIP = "endTrip";
    public static final String ON_GO_START_TRIP = "ongostartTrip";
    public static final String VEHICLE_BOOKED = "vehicalBooked";
    public static final String QR_CODE_DETAILS = "qrcodeDetails";
    public static final String OFFICE_QR_CODE = "inout/scanOfficeCode";
    public static final String VEHICLE_DETAILS = "vehical";
    public static final String FUEL_RECIEPT = "fuel";
    public static final String FAULT_REPORT = "vehicalfault";

    public static final String COMPANY_DATA = USER + "getOrganizationalUser";
    public static final String USER_STATUS = USER + "online";



    //Keys
    public static final String ISFIRSTLAUNCH = "inFirstLaunch";

    public static final String ID = "id";

    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";

    public static final String OLDPWD = "oldPassword";
    public static final String NEWPWD = "newPassword";

    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String PROFILE_URL = "profile_image";

    public static final String TIME = "time";

    public static final String OTP = "otp";

    public static final String PHONE = "phone";

    public static final String TOKEN = "token";
    public static final String X_AUTH = "x-auth";

    public static final String DATE = "date";
    public static final String CURRENTDATE = "currentdate";
    public static final String FROMDATE = "fromDate";

    public static final String FROM_DATE = "from_date";
    public static final String FROM_TIME = "from_time";
    public static final String TO_DATE = "to_date";
    public static final String TO_TIME = "to_time";
    public static final String COMMENT = "comment";
    public static final String STARTKM = "start_km";
    public static final String ENDKM = "end_km";
    public static final String BOOKINGID = "booking_id";
    public static final String VEHICALID = "vehical_id";
    public static final String OFFICEID = "office_id";
    public static final String DEPARTMENTID = "department_id";
    public static final String ORGID = "org_id";

    public static final String AMOUNT = "amount";
    public static final String RECEIPT_NO = "receipt_no";
    public static final String QUANTITY = "quantity";
    public static final String RECEIPTIMAGE = "receipt_image";

    public static final String FAULT_TYPE = "fault_type";
    public static final String IMAGE = "image";

    public static final String STATUS = "status";
    public static final String DASHBOARD = "dashboard";
    public static final String SEARCH = "search";
    public static final String PAGENO = "pageNo";
    public static final String PAGELIMIT = "pageLimit";

    public static final String QUICKPIC_ID = "quickpic_id";

    public static final String CIN = "CIN";//Clocked In
    public static final String IN = "IN";// Regular In
    public static final String OUT = "OUT";// Regular Out
    public static final String COUT = "COUT";// Clocked Out
    public static final String BI = "BI";// Break IN
    public static final String BR = "BR";
    public static final String BS = "BS";// Break IN
    public static final String BD = "BD";// Break DOWN : Faulted vehicle

    //Status for vehicle list
    public static final String BO = "BO";//Booked
    public static final String IU = "IU";// In Use
    public static final String CO = "CO";// Completed
    public static final String UB = "UB";// Unbooked

    public static final String MOBILE = "mobile";
    public static final String FROM = "from_admin";

    public static SOService getSOService() {
        return RetrofitClient.getClient(BASE_URL_TEST).create(SOService.class);
    }
}