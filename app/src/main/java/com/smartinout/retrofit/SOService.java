package com.smartinout.retrofit;

import com.smartinout.model.BaseResponse;
import com.smartinout.model.ForgotPassReqPojo;
import com.smartinout.model.dashboard.DashbordVehicleModel;
import com.smartinout.model.faultreport.FaultReportModel;
import com.smartinout.model.fuel.FuelRecieptModel;
import com.smartinout.model.login.OnlineUserReference;
import com.smartinout.model.organize.OrganizeModel;
import com.smartinout.model.qrcode.QrCodeOfficeModel;
import com.smartinout.model.qrcode.QrCodeVehicleModel;
import com.smartinout.model.timesheet.quickpick.QuickPickPojo;
import com.smartinout.model.login.LoginPojo;
import com.smartinout.model.timesheet.DailyTimeSheetsPojo;
import com.smartinout.model.timesheet.edittimesheet.EditTimeSheetModel;
import com.smartinout.model.timesheet.weekly.WeeklyTimeSheet;
import com.smartinout.model.userinout.ClockedInOutPojo;
import com.smartinout.model.vehiclebooking.BookedModel;
import com.smartinout.model.vehiclebooking.AvailableModel;
import com.smartinout.model.vehiclebooking.CreateBookingModel;
import com.smartinout.model.vehiclebooking.UnBookModel;
import com.smartinout.model.vehicleinout.starttrip.StartTripModel;
import com.smartinout.model.vehicleinout.VehicleInfoModel;
import com.smartinout.model.vehicleinout.stoptrip.StopTripModel;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface SOService {

    @FormUrlEncoded
    @POST(ApiUtils.LOGIN_USER)
    Call<LoginPojo> loginUser(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST(ApiUtils.GENERATE_USER_PWD)
    Call<ForgotPassReqPojo> generateUserPwd(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST(ApiUtils.SET_USER_PWD)
    Call<BaseResponse> setPswUser(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST(ApiUtils.CHANGE_PWD)
    Call<BaseResponse> changePwdUser(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST(ApiUtils.UPDATE_INOUT_STATUS)
    Call<ClockedInOutPojo> updateInOutStatus(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> params);

    @GET(ApiUtils.LIVE_INOUT_STATUS)
    Call<BaseResponse> liveStatus(@HeaderMap HashMap<String, String> header);

    @GET(ApiUtils.DAILY_TIMESHEET)
    Call<DailyTimeSheetsPojo> dailyTimesheet(@HeaderMap HashMap<String, String> header,
                                             @Query(ApiUtils.DATE) String date,
                                             @Query(ApiUtils.MOBILE) String mobile);

    @GET(ApiUtils.WEEKLY_TIMESHEET)
    Call<WeeklyTimeSheet> weeklyTimesheet(@HeaderMap HashMap<String, String> header,
                                          @Query(ApiUtils.FROMDATE) String fromdate);

    @GET(ApiUtils.LIVE_INOUT_STATUS)
    Call<LoginPojo> userlivestatus(@HeaderMap HashMap<String, String> header);

    @FormUrlEncoded
    @PUT(ApiUtils.EDIT_PROFILE)
    Call<LoginPojo> editProfile(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @PUT(ApiUtils.EDIT_TIMESHEET)
    Call<EditTimeSheetModel> editTimeSheet(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @PUT(ApiUtils.ADD_TIMESHEET)
    Call<EditTimeSheetModel> addTimeSheet(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> params);

    @GET(ApiUtils.VEHICLE_BOOKING)
    Call<AvailableModel> getAvailableVehicle(@HeaderMap HashMap<String, String> header,
                                             @QueryMap HashMap<String, String> params);

    @GET(ApiUtils.VEHICLE_BOOKED)
    Call<BookedModel> getBookedVehicle(@HeaderMap HashMap<String, String> header,
                                       @QueryMap HashMap<String, String> params);

    @GET(ApiUtils.VEHICLE_BOOKED)
    Call<DashbordVehicleModel> bookedDataDashboard(@HeaderMap HashMap<String, String> header,
                                                   @QueryMap HashMap<String, String> params);

    @POST(ApiUtils.VEHICLE_BOOKING)
    Call<CreateBookingModel> bookVehicle(@HeaderMap HashMap<String, String> header,
                                                 @QueryMap HashMap<String, String> params);

    @DELETE(ApiUtils.VEHICLE_BOOKING)
    Call<UnBookModel> unbookVehicle(@HeaderMap HashMap<String, String> header,
                                    @QueryMap HashMap<String, String> params);

    @POST(ApiUtils.ON_GO_START_TRIP)
    Call<StartTripModel> onGoingTrip(@HeaderMap HashMap<String, String> header,
                                         @QueryMap HashMap<String, String> params);
    @FormUrlEncoded
    @POST(ApiUtils.START_TRIP)
    Call<StartTripModel> onStartTrip(@HeaderMap HashMap<String, String> header,
                                     @FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST(ApiUtils.END_TRIP)
    Call<StopTripModel> onEndTrip(@HeaderMap HashMap<String, String> header,
                                  @FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST(ApiUtils.FUEL_RECIEPT)
    Call<FuelRecieptModel> fuelReciept(@HeaderMap HashMap<String, String> header,
                                       @FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST(ApiUtils.FAULT_REPORT)
    Call<FaultReportModel> faultReport(@HeaderMap HashMap<String, String> header,
                                       @FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST(ApiUtils.QR_CODE_DETAILS)
    Call<QrCodeVehicleModel> getQrCodeDetails(@HeaderMap HashMap<String, String> header,
                                              @FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST(ApiUtils.OFFICE_QR_CODE)
    Call<QrCodeOfficeModel> getOfficeQrDetails(@HeaderMap HashMap<String, String> header,
                                               @FieldMap HashMap<String, String> params);

    @GET(ApiUtils.VEHICLE_DETAILS)
    Call<VehicleInfoModel> getVehicleInfo(@HeaderMap HashMap<String, String> header,
                                          @QueryMap HashMap<String, String> params);

    @GET(ApiUtils.GET_QUICKPICKS)
    Call<QuickPickPojo> getQuickPicks(@HeaderMap HashMap<String, String> header);//,@Query(ApiUtils.FROM) String date);

    @GET(ApiUtils.COMPANY_DATA)
    Call<OrganizeModel> getOrganizationData(@HeaderMap HashMap<String, String> header,
                                            @QueryMap HashMap<String, String> params);

    @GET(ApiUtils.USER_STATUS)
    Call<OnlineUserReference> getOnlineStatus(@HeaderMap HashMap<String, String> header);
}