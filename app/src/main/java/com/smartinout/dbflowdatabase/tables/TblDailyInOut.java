package com.smartinout.dbflowdatabase.tables;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.smartinout.dbflowdatabase.SmartInOutDb;

@Table(database = SmartInOutDb.class, name = "dailyinout")
public class TblDailyInOut extends BaseModel {

    @PrimaryKey
    @Column
    String date;

    @Column
    String time_zone;

    @Column
    String data;

    @Column
    String progressData;

    @Column
    long totalactiveminutes;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime_zone() {
        return time_zone;
    }

    public void setTime_zone(String time_zone) {
        this.time_zone = time_zone;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getProgressData() {
        return progressData;
    }

    public void setProgressData(String progressData) {
        this.progressData = progressData;
    }

    public long getTotalactiveminutes() {
        return totalactiveminutes;
    }

    public void setTotalactiveminutes(long totalactiveminutes) {
        this.totalactiveminutes = totalactiveminutes;
    }
}
