package com.smartinout.dbflowdatabase;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.smartinout.dbflowdatabase.tables.TblDailyInOut;
import com.smartinout.dbflowdatabase.tables.TblDailyInOut_Table;
import com.smartinout.model.timesheet.Activities;
import com.smartinout.model.timesheet.InoutDatum;

import java.lang.reflect.Type;
import java.util.List;

public class SmartInOutDBCtrl {

    private Context context;
    private static SmartInOutDBCtrl dbFlowController;

    public SmartInOutDBCtrl(Context context) {
        this.context = context;
    }

    public static SmartInOutDBCtrl getInstance(Context context) {
        if (dbFlowController == null) {
            dbFlowController = new SmartInOutDBCtrl(context);
        }
        return dbFlowController;
    }

    public void saveDailyTimeSheet(String date, List<Activities> activitiesList, long totalMinutes, List<InoutDatum> listProgressData) {
        TblDailyInOut dailyInOut = new TblDailyInOut();
        dailyInOut.setDate(date);
        dailyInOut.setData(new Gson().toJson(activitiesList));
        dailyInOut.setTotalactiveminutes(totalMinutes);
        dailyInOut.setProgressData(new Gson().toJson(listProgressData));
        dailyInOut.save();
    }

    public List<Activities> getDailyTimeSheet(String date) {
        TblDailyInOut inOut = SQLite.select().from(TblDailyInOut.class)
                .where(TblDailyInOut_Table.date.eq(date))
                .querySingle();

        if (inOut == null)
            return null;

        String lisData = inOut.getData();
        Type type = new TypeToken<List<Activities>>() {
        }.getType();

        return new Gson().fromJson(lisData, type);
    }

    public long getTotalActiveMinute(String date) {
        TblDailyInOut inOut = SQLite.select().from(TblDailyInOut.class)
                .where(TblDailyInOut_Table.date.eq(date))
                .querySingle();

        if (inOut == null)
            return 0;

        long lisData = inOut.getTotalactiveminutes();
        return lisData;
    }

    public List<InoutDatum> getDailyTimeSheetProgressData(String date) {
        TblDailyInOut inOut = SQLite.select().from(TblDailyInOut.class)
                .where(TblDailyInOut_Table.date.eq(date))
                .querySingle();

        if (inOut == null)
            return null;

        String lisData = inOut.getProgressData();
        Type type = new TypeToken<List<InoutDatum>>() {
        }.getType();

        return new Gson().fromJson(lisData, type);
    }

    public TblDailyInOut getDailyTimeSheetTbl(String date) {
        return SQLite.select().from(TblDailyInOut.class)
                .where(TblDailyInOut_Table.date.eq(date))
                .querySingle();
    }

    public List<Activities> getStringtoActivitiesList(String data) {
        Type type = new TypeToken<List<Activities>>() {
        }.getType();

        return new Gson().fromJson(data, type);
    }

    public List<InoutDatum> getStringtoProgressList(String data) {
        Type type = new TypeToken<List<InoutDatum>>() {
        }.getType();

        return new Gson().fromJson(data, type);
    }

    public void clearAll() {
        // Delete a whole table
        Delete.table(TblDailyInOut.class);
    }
}
