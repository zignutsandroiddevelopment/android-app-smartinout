package com.smartinout.dbflowdatabase;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = SmartInOutDb.NAME, version = SmartInOutDb.VERSION)
public class SmartInOutDb {

    public static final String NAME = "SmartInOutDB";
    public static final int VERSION = 1;
}
