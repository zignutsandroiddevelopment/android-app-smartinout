package com.smartinout;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.smartinout.adapters.IntroductionAdapter;
import com.smartinout.base.BaseActivity;
import com.smartinout.databinding.ActivityIntroductionBinding;
import com.smartinout.databinding.ItemIntroSliderBinding;
import com.smartinout.retrofit.ApiUtils;
import com.smartinout.utilities.ActivityUtils;
import com.smartinout.utilities.Prefs;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

public class IntroductionActivity extends BaseActivity implements View.OnClickListener {

    ActivityIntroductionBinding binding;
    private IntroductionAdapter adapter;
    private List<IntroSliderModel> list = new ArrayList<>();
    private List<View> viewList;
    private Prefs pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(IntroductionActivity.this, R.layout.activity_introduction);
        pref = Prefs.getInstance();
        initiateUI();
    }

    @Override
    protected void initiateUI() {

        binding.linearWidgetButton.setOnClickListener(this);

        initAdapter();
    }

    private void initAdapter() {
        fillArrayList();
        adapter = new IntroductionAdapter(IntroductionActivity.this);
        binding.viewPager.setAdapter(adapter);
        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == viewList.size() - 1) {
                    binding.linearWidgetButton.setVisibility(View.VISIBLE);
                    binding.dotPageIndi.setVisibility(View.INVISIBLE);
                } else {
                    binding.linearWidgetButton.setVisibility(View.INVISIBLE);
                    binding.dotPageIndi.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewList = createPageList();
        adapter.setData(viewList);
        binding.dotPageIndi.setupWithViewPager(binding.viewPager, true);
    }

    @NonNull
    private List<View> createPageList() {
        List<View> pageList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            pageList.add(createPageView(i));
        }
        return pageList;
    }

    @NonNull
    private View createPageView(int pos) {
        ItemIntroSliderBinding
                sliderBinding = DataBindingUtil
                .inflate(LayoutInflater.from(this), R.layout.item_intro_slider, null, false);
        sliderBinding.tvMsg1.setText(list.get(pos).getMsg1());
        sliderBinding.tvMsg2.setText(list.get(pos).getMsg2());
        sliderBinding.ivIntro1.setImageResource(list.get(pos).getResource());
        return sliderBinding.getRoot();
    }

    /**
     * @desc fill introduction array list
     */
    private void fillArrayList() {

        IntroSliderModel slider1 = new IntroSliderModel();
        slider1.setMsg1(getResources().getString(R.string.intro_1));
        slider1.setMsg2(getResources().getString(R.string.subintro_1));
        slider1.setResource(R.drawable.ic_intro_slide1);
        list.add(slider1);

        IntroSliderModel slider2 = new IntroSliderModel();
        slider2.setMsg1(getResources().getString(R.string.intro_1));
        slider2.setMsg2(getResources().getString(R.string.subintro_1));
        slider2.setResource(R.drawable.ic_intro_slide1);
        list.add(slider2);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.linearWidgetButton:
                pref.save(ApiUtils.ISFIRSTLAUNCH, true);
                ActivityUtils.launchActivity(IntroductionActivity.this, LoginActivity.class, true, null);
                break;
        }
    }

    public class IntroSliderModel {

        private int resource;
        private String msg1;
        private String msg2;

        public int getResource() {
            return resource;
        }

        public void setResource(int resource) {
            this.resource = resource;
        }

        public String getMsg1() {
            return msg1;
        }

        public void setMsg1(String msg1) {
            this.msg1 = msg1;
        }

        public String getMsg2() {
            return msg2;
        }

        public void setMsg2(String msg2) {
            this.msg2 = msg2;
        }
    }
}
