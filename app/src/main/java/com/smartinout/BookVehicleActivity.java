package com.smartinout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.smartinout.base.BaseActivity;
import com.smartinout.databinding.ActivityBookvehicleBinding;
import com.smartinout.model.vehiclebooking.CreateBookingData;
import com.smartinout.model.vehicleinout.OfficeModel;
import com.smartinout.model.vehicleinout.VehicleInfoData;
import com.smartinout.model.vehicleinout.VehicleInfoModel;
import com.smartinout.model.vehicleinout.starttrip.StartTripData;
import com.smartinout.model.vehicleinout.starttrip.StartTripVehicalData;
import com.smartinout.model.vehicleinout.VehicleInOutData;
import com.smartinout.model.vehicleinout.VehicleInOutModel;
import com.smartinout.utilities.AppUtilities;
import com.smartinout.utilities.Constants;
import com.smartinout.utilities.Prefs;

import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

public class BookVehicleActivity extends BaseActivity implements View.OnClickListener {

    ActivityBookvehicleBinding binding;
    private NavController navController;
    private Bundle bundle;
    private Prefs prefs;
    private VehicleInOutModel bookedData;
    private OfficeModel officeModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(BookVehicleActivity.this, R.layout.activity_bookvehicle);
        initiateUI();
        prefs = Prefs.getInstance();

        Intent intent = getIntent();
        if (intent != null) {
            String idFrom = intent.getExtras().getString(Constants.FROM);
            navController = Navigation.findNavController(getActivity(), R.id.navhost_frag_bookvehicle);
            bundle = new Bundle();
            try {

                if (idFrom != null) {
                    if (idFrom.equalsIgnoreCase(Constants.STATUS_ONGOING_TRIP)) {
                        CreateBookingData bookedData = (CreateBookingData) intent.getExtras().get(Constants.DATA);

                        if (bookedData != null) {

                            bundle.putString(Constants.FROM, Constants.STATUS_ONGOING_TRIP);
                            bundle.putSerializable(Constants.DATA, bookedData);

                            navController.navigate(R.id.fragment_starttrip, bundle);

                            StartTripVehicalData info = bookedData.getVehicalData();

                            String regoDate = AppUtilities.getTimeStampFromDateTime(Constants.YYYY_MM_DD_T_HH_MM_SS_Z, Constants.ddMMyyyy, info.getRegoNextDate());

                            binding.tvVehicleNum.setText(this.getString(R.string.vehiclenumber) + " " + info.getNumber());
                            binding.tvDateTime.setText(AppUtilities.getCurrentDateEEEEddMMMMHHMM());
                            binding.tvBrand.setText(this.getString(R.string.branch) + " " + info.getOfficeData().getOfficeName());
                            binding.tvModel.setText(this.getString(R.string.model) + " " + info.getName() + " - " +info.getBrand());
                            binding.tvRegoDate.setText(this.getString(R.string.rego) + " " + regoDate);
                            binding.tvRegoDate2.setText(this.getString(R.string.idlable) + " "+info.getNumber());

                        }
                    } else if (idFrom.equalsIgnoreCase(Constants.STATUS_START_TRIP)) {
                        bookedData = (VehicleInOutModel) intent.getExtras().get(Constants.DATA);

                        if (bookedData != null) {

                            bundle.putString(Constants.FROM, Constants.STATUS_START_TRIP);
                            bundle.putSerializable(Constants.DATA, bookedData);
                            navController.navigate(R.id.fragment_starttrip, bundle);

                            VehicleInOutData info = bookedData.getVehicalData();

                            prefs.save(Prefs.VEHICLEID, info.getId());


                            String regoDate = AppUtilities.getTimeStampFromDateTime(Constants.YYYY_MM_DD_T_HH_MM_SS_Z, Constants.ddMMyyyy, info.getRegoNextDate());

                            binding.tvVehicleNum.setText(this.getString(R.string.vehiclenumber) + " " + info.getNumber());
                            binding.tvDateTime.setText(AppUtilities.getCurrentDateEEEEddMMMMHHMM());
                            binding.tvBrand.setText(this.getString(R.string.branch) + " " + bookedData.getVehicalData().getOfficeData().getOfficeName());
                            binding.tvModel.setText(this.getString(R.string.model) + " " + info.getName()+ " - " +info.getBrand());
                            binding.tvRegoDate.setText(this.getString(R.string.rego) + " " + regoDate);
                            binding.tvRegoDate2.setText(this.getString(R.string.idlable) + " " + info.getNumber());
                        }
                    } else {

                        StartTripData bookedData = (StartTripData) intent.getExtras().get(Constants.DATA);

                        if (bookedData != null) {

                            bundle.putString(Constants.FROM, Constants.STATUS_END_TRIP);
                            bundle.putSerializable(Constants.DATA, bookedData);
                            navController.navigate(R.id.fragment_stoptrip, bundle);

                            StartTripVehicalData info = bookedData.getVehicalData();

                            String regoDate = AppUtilities.getTimeStampFromDateTime(Constants.YYYY_MM_DD_T_HH_MM_SS_Z, Constants.ddMMyyyy, info.getRegoNextDate());

                            binding.tvVehicleNum.setText(this.getString(R.string.vehiclenumber) + " " + info.getNumber());
                            binding.tvDateTime.setText(AppUtilities.getCurrentDateEEEEddMMMMHHMM());
                            binding.tvBrand.setText(this.getString(R.string.branch)+ " " + info.getOfficeData().getOfficeName());
                            binding.tvModel.setText(this.getString(R.string.model) + " " + info.getName()+ " - " +info.getBrand());
                            binding.tvRegoDate.setText(this.getString(R.string.rego) + " " + regoDate);
                            binding.tvRegoDate2.setText(this.getString(R.string.idlable) + " " + info.getNumber());
                        }

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    protected void initiateUI() {
        binding.layoutToolBarInclude.tvTitle.setText(getResources().getString(R.string.bookvehicle));
        binding.layoutToolBarInclude.ivBack.setVisibility(View.VISIBLE);
        binding.layoutToolBarInclude.ivBack.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:
                BookVehicleActivity.this.finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        int currentId = navController.getCurrentDestination().getId();
        if(currentId == R.id.fragment_fuelreceipt || currentId == R.id.fragment_reportfault){
            navController.navigateUp();
        }else {
            BookVehicleActivity.this.finish();
        }
        //super.onBackPressed();

    }
}
