package com.smartinout.imagepicker;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.view.WindowManager;
import android.widget.Toast;

import com.smartinout.R;

import java.io.File;
import java.io.IOException;

import androidx.core.content.FileProvider;

public class ImagePickerUtils {

    static File getWorkingDirectory(Activity activity) {
        File directory =
                new File(Environment.getExternalStorageDirectory(), activity.getApplicationContext().getPackageName());
        if (!directory.exists()) {
            directory.mkdir();
        }
        return directory;
    }

    static FileUri createImageFile(Activity activity, String prefix) {
        FileUri fileUri = new FileUri();

        File image = null;
        try {
            image = File.createTempFile(prefix + String.valueOf(System.currentTimeMillis()), ".jpg",
                    getWorkingDirectory(activity));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (image != null) {
            fileUri.setFile(image);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                fileUri.setImageUrl(FileProvider.getUriForFile(activity,
                        activity.getApplicationContext().getPackageName(), image));
            } else {
                fileUri.setImageUrl(Uri.parse("file:" + image.getAbsolutePath()));
            }
        }
        return fileUri;
    }

    public static void hideNavigationBar(Activity activity) {
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}