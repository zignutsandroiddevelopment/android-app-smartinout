package com.smartinout.imagepicker;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.smartinout.R;


public class ImagePickerDialog {

    public interface FilePickerInteface {
        void onChoseImage(String filePath);
    }

    Activity activity;
    FilePickerInteface filePickerInteface;

    public ImagePickerDialog(Activity activity
            , FilePickerInteface filePickerInteface) {
        this.activity = activity;
        this.filePickerInteface = filePickerInteface;
    }

    //Initialization of image Picker
    private FilePickUtils filePickUtils;
    private BottomDialog bottomDialog;
    private LifeCycleCallBackManager lifeCycleCallBackManager;

    public void initBottomDialog() {
        filePickUtils = new FilePickUtils(activity, onFileChoose);
        lifeCycleCallBackManager = filePickUtils.getCallBackManager();
        View bottomSheetView = activity.getLayoutInflater()
                .inflate(R.layout.dialog_photo_selector, null);
        bottomDialog = new BottomDialog(activity);
        bottomDialog.setContentView(bottomSheetView);
        final TextView tvCamera = bottomSheetView.findViewById(R.id.tvCamera);
        final TextView tvGallery = bottomSheetView.findViewById(R.id.tvGallery);
        tvCamera.setOnClickListener(onCameraListener);
        tvGallery.setOnClickListener(onGalleryListener);

    }

    private FilePickUtils.OnFileChoose onFileChoose = new FilePickUtils.OnFileChoose() {
        @Override
        public void onFileChoose(String fileUri, int requestCode) {
            bottomDialog.dismiss();
            filePickerInteface.onChoseImage(fileUri);
        }
    };

    private View.OnClickListener onCameraListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            bottomDialog.dismiss();
            filePickUtils.requestImageCamera(FilePickUtils.STORAGE_PERMISSION_CAMERA, true, true);
        }
    };

    private View.OnClickListener onGalleryListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            bottomDialog.dismiss();
            filePickUtils.requestImageGallery(FilePickUtils.STORAGE_PERMISSION_IMAGE, true, true);
        }
    };

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (lifeCycleCallBackManager != null) {
            lifeCycleCallBackManager.onActivityResult(requestCode, resultCode, data);
        }
    }
}
